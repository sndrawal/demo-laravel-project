<?php

namespace App\Modules\StockOut\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\RawMaterial\Entities\RawMaterial;

class StockOutDetail extends Model
{

    protected $fillable = [

    	'stockout_id',
    	'material_id',
    	'qty',
    	'remark'

    ];

    public function materialInfo(){
        return $this->belongsTo(RawMaterial::class,'material_id','id');
    }
    
}
