<?php

namespace App\Modules\StockOut\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Warehouse\Entities\Warehouse;
use App\Modules\StockOut\Entities\StockOutDetail;

class StockOut extends Model
{
    protected $fillable = [

    	'warehouse_id',
    	'shipping_address',
    	'delivery_date',
    	'description'

    ];


    public function stockoutDetail()
    {
        return $this->hasMany(StockOutDetail::class,'stockout_id','id');
    }

    public function warehouseInfo(){
        return $this->belongsTo(Warehouse::class,'warehouse_id','id');
    }
    
}
