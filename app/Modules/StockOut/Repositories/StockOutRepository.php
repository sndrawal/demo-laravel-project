<?php 
namespace App\Modules\StockOut\Repositories;

use App\Modules\StockOut\Entities\StockOut;
use App\Modules\StockOut\Entities\StockOutDetail;

class StockOutRepository implements StockOutInterface
{

    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result =StockOut::when(array_keys($filter, true), function ($query) use ($filter) {
           
                
        })->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 

    }

    public function find($id)
    {
        return StockOut::find($id);
    }


    public function getList()
    {  
        $result = StockOut::pluck('vendor_name', 'id');

        return $result;
    }

    public function save($data)
    {
        return StockOut::create($data);
    }
    
    public function saveStockoutDetail($data)
    {
        return StockOutDetail::create($data);
    }

    public function update($id,$data)
    {
        $result = StockOut::find($id);
        return $result->update($data);
    }

    public function delete($id)
    {
        return StockOut::destroy($id);
    }   

    public function deleteDetail($id){
      return StockOutDetail::where('stockout_id','=',$id)->delete();
    }

    public function findStockoutDetail($pid){
      return StockOutDetail::where('stockout_id','=',$pid)->get();
    }


}