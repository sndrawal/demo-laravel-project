<?php

namespace App\Modules\StockOut\Repositories;

interface StockOutInterface
{
    public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function getList();
    
    public function save($data);
    public function saveStockoutDetail($data);

    public function update($id,$data);

    public function delete($id);
    
    public function deleteDetail($id);

    public function findStockoutDetail($pid);

}