<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {

        /*
        |--------------------------------------------------------------------------
        | Stock Out  CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('stockout', ['as' => 'stockout.index', 'uses' => 'StockOutController@index']);
        //Stock Out Create
        Route::get('stockout/create', ['as' => 'stockout.create', 'uses' => 'StockOutController@create']);
        Route::post('stockout/store', ['as' => 'stockout.store', 'uses' => 'StockOutController@store']);
        //Stock Out Edit
        Route::get('stockout/edit/{id}', ['as' => 'stockout.edit', 'uses' => 'StockOutController@edit'])->where('id','[0-9]+');
        Route::put('stockout/update/{id}', ['as' => 'stockout.update', 'uses' => 'StockOutController@update'])->where('id','[0-9]+');
        //Stock Out Delete
        Route::get('stockout/delete/{id}', ['as' => 'stockout.delete', 'uses' => 'StockOutController@destroy'])->where('id','[0-9]+');
        Route::get('stockout/appendMaterial', ['as' => 'stockout.appendMaterial', 'uses' => 'StockOutController@appendMaterial']);

        Route::post('stockout/get-stockout-detail-ajax', ['as'=>'stockout.get-stockout-detail-ajax','uses'=>'StockOutController@getStockoutDetailAjax']);

});
