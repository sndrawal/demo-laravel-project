@extends('admin::layout')
@section('title')Stock Out @stop
@section('breadcrum')Stock Out @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
@stop

@section('content') 

<div class="card card-body">
    <div class="d-flex justify-content-between">
        <h4>List Of Stock Out</h4>
        <a href="{{ route('stockout.create') }}" class="btn bg-blue mb-2">
            <i class="icon-plus2"></i> Add Stock Out
        </a>
    </div>

    <div class="table-responsive">
        <table class="table text-nowrap table-striped">
            <thead>
                <tr class="bg-slate-600">
                    <th>#</th>
                    <th>Warehouse</th>
                    <th>Shipping Address</th>
                    <th>Delivery Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @if($stockout->total() != 0)
                @foreach($stockout as $key => $value)
                <tr>
                    <td>{{$stockout->firstItem() +$key}}</td>
                    <td>{{ optional($value->warehouseInfo)->warehouse_name }}</td>
                    <td>{{ $value->shipping_address }}</td>
                    <td>{{ $value->delivery_date }}</td>
                    <td class="text-right">
                        <a href="#" class="list-icons-item" data-toggle="dropdown">
                            <i class="icon-more2"></i>
                        </a>

                       

                        <div class="dropdown-menu bd-card dropdown-menu-right">
                            <a class="dropdown-item" href="{{route('stockout.edit',$value->id)}}">
                                <i class="icon-pencil6"></i> Edit
                            </a>

                             <a data-toggle="modal" data-target="#modal_view_stockout" class="dropdown-item view_stockout" stockout_id = "{{$value->id}}"><i class="icon-eye"></i>View Detail</a>

                            <a data-toggle="modal" data-target="#delete-modal-stockout" class="delete-modal dropdown-item delete_stockout" link="{{route('stockout.delete',$value->id)}}">
                                <i class="icon-bin"></i> Delete
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">No Stock Out Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>
    </div>
    <div class="col-12">
        <span class="float-right pagination align-self-end mt-3">
            {{ $stockout->appends(request()->all())->links() }}
        </span>
    </div>
</div>


 <!-- view modal -->
    <div id="modal_view_stockout" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-pink">
                    <h6 class="modal-title">View Stock Out Detail</h6>
                </div>

                <div class="modal-body">
                    <div class="table-responsive result_view_detail">
                         
                    </div><!-- table-responsive -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn bg-teal-400" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- view modal -->

<!-- Warning modal -->
    <div id="delete-modal-stockout" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                 <div class="modal-body">
                    <center>
                        <i class="icon-alert text-danger icon-3x"></i>
                    </center>
                    <br>
                    <center>
                        <h2>Are You Sure Want To Delete ?</h2>
                        <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!-- /warning modal -->


<script type="text/javascript">
    $(document).ready(function(){

        $('.view_stockout').on('click',function(){

         var stockout_id = $(this).attr('stockout_id'); 
        
          $.ajax({
              url: "<?php echo route('stockout.get-stockout-detail-ajax') ?>",
              method: 'POST',
              data: {stockout_id:stockout_id, _token:"{{ csrf_token() }}"},
              success: function(data) {
                $(".result_view_detail").html('');
                $(".result_view_detail").html(data.options);
              }
          });

    });

          $('.delete_stockout').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });

    });
</script>


@endsection