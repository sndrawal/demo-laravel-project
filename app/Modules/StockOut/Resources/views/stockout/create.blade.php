@extends('admin::layout')
@section('title')Stock Out @stop 
@section('breadcrum')Create Stock Out @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Create Stock Out</h5>
        <div class="header-elements">

        </div>
    </div>
    

    <div class="card-body">

        {!! Form::open(['route'=>'stockout.store','method'=>'POST','class'=>'form-horizontal','id'=>'stockout_submit','role'=>'form','files' => true]) !!}
        
            @include('stockout::stockout.partial.action',['btnType'=>'Save']) 
        
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@stop