<div class="appendMaterial">
        <div class="form-group row">
            <div class="col-lg-3">
                <div class="row">
                     <label class="col-form-label col-lg-3">Material:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            {!! Form::select('material_id[]', $material, null, ['id'=>'material_id','placeholder'=>'--Select Material--','class'=>'select-search form-control','required']) !!}
                        </div>
                    </div>
                </div>
            </div>
            
             <div class="col-lg-3">
                <div class="row">
                   <label class="col-form-label col-lg-3">Qty:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-balance"></i></span>
                            </span>
                            {!! Form::text('qty[]', null, ['id'=>'qty','placeholder'=>'Enter Qty','class'=>'qty form-control numeric','required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="row">
                    <label class="col-form-label col-lg-3">Remark:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i></span>
                            </span>
                            {!! Form::text('remark[]', null, ['id'=>'remark','placeholder'=>'Enter Remark','class'=>'remark form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
               <div class="row">
                    <button type="button" class="ml-2 remove_material btn bg-danger-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Remove</button>
                </div>
            </div>

    </div>
</div>

 

<script type="text/javascript">
    $(document).ready(function(){ 
       
        $('.remove_material').on('click',function(){ 
            $(this).parent().parent().parent().remove();
        });
        
    });
</script>