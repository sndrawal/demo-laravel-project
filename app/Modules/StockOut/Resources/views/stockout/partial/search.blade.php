<div class="card card-body filter-option {{ request('search') && request('search') == 'advance' ? '' : 'd-none' }}">
    {!! Form::open(['route' => 'vendor.index', 'method' => 'get']) !!}
    <div class="row">
        <div class="col-md-4">
            <label class="d-block font-weight-semibold">Category:</label>
            <div class="input-group">
                {!! Form::select('vendor_category_id[]', $categories, request('vendor_category_id') ?? null, ['class'=>'form-control multiselect-filtering', 'multiple']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <label class="d-block font-weight-semibold">Vendor Name.:</label>
            <div class="input-group">
                {!! Form::text('vendor_name', request('vendor_name') ?? null, ['class'=>'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-end mt-2">
        <button class="btn bg-primary" type="submit">
            Search Now
        </button>
        <a href="{{ route('vendor.index') }}" data-popup="tooltip" data-placement="top" data-original-title="Refresh Search" class="btn bg-danger ml-2">
            <i class="icon-spinner9"></i>
        </a>
    </div>
    {!! Form::close() !!}
</div>