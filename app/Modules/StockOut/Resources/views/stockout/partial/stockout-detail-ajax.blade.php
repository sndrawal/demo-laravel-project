<table class="table table-striped">
    <thead>
        <tr class="bg-slate-400">
            <th>S.N.</th>
            <th>Material</th>
            <th>Qty</th>
            <th>Remark</th>
        </tr>
    </thead>
    <tbody>

       @foreach($stockoutDetail->stockoutDetail as $key => $value)

       <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ optional($value->materialInfo)->name }}</td>
            <td>{{ $value->qty }}</td>
            <td>{{ $value->remark }}</td>
        </tr>

    @endforeach

</tbody>

</table>
