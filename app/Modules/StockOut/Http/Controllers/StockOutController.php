<?php

namespace App\Modules\StockOut\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Modules\StockOut\Repositories\StockOutInterface;
use App\Modules\RawMaterial\Repositories\RawMaterialInterface;
use App\Modules\Warehouse\Repositories\WarehouseInterface;

class StockOutController extends Controller
{
    protected $stockout;
    protected $raw_material;
    protected $warehouse;
    
    public function __construct(StockOutInterface $stockout,RawMaterialInterface $raw_material, WarehouseInterface $warehouse)
    {
        $this->stockout = $stockout;
        $this->raw_material = $raw_material;
        $this->warehouse = $warehouse;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $search = $request->all();
        $data['stockout'] = $this->stockout->findAll($limit= 50, $search);
        return view('stockout::stockout.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data['is_edit'] = false;
        $data['warehouse'] = $this->warehouse->getList();
        $data['material'] = $this->raw_material->getList();

        return view('stockout::stockout.create', $data);
    }

    public function appendMaterial(Request $request){

         if($request->ajax()){
            $material = $this->raw_material->getList();
            $data = view('stockout::stockout.partial.add-more-material',compact('material'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $data = $request->all();  

        try{ 
            $stockout_data = array(
                    'warehouse_id' =>$data['warehouse_id'],
                    'shipping_address' =>$data['shipping_address'],
                    'delivery_date' =>$data['delivery_date'],
                    'description' =>$data['description']
            );

            $stockoutInfo = $this->stockout->save($stockout_data);
            $stockout_id = $stockoutInfo->id;

            $materials = $data['material_id'];
            $countmaterials = sizeof($materials);

                for($i = 0; $i < $countmaterials; $i++){
           
                     if($data['material_id'][$i]){
                         $stockoutDetaildata['stockout_id'] = $stockout_id;
                         $stockoutDetaildata['material_id'] = $data['material_id'][$i];
                         $stockoutDetaildata['qty'] = $data['qty'][$i];
                         $stockoutDetaildata['remark'] = $data['remark'][$i];

                         $this->stockout->saveStockoutDetail($stockoutDetaildata);
                     }
                         
                 }

            toastr()->success('Stock Out Created Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('stockout.index'));
    }



    public function getStockoutDetailAjax(Request $request){
         if($request->ajax()){

            $stockout_id = $request->stockout_id;
            $stockoutDetail = $this->stockout->find($stockout_id); 
            $data = view('stockout::stockout.partial.stockout-detail-ajax',compact('stockoutDetail'))->render();
            return response()->json(['options'=>$data]);
        }
    }
    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('stockout::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {

        $data['is_edit'] = true;
        $data['warehouse'] = $this->warehouse->getList();
        $data['material'] = $this->raw_material->getList();
        $data['stockout'] = $this->stockout->find($id);

        return view('stockout::stockout.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
         $data = $request->all();  


        try{ 
             $stockout_data = array(
                    'warehouse_id' =>$data['warehouse_id'],
                    'shipping_address' =>$data['shipping_address'],
                    'delivery_date' =>$data['delivery_date'],
                    'description' =>$data['description']
            );

            $stockoutInfo = $this->stockout->update($id,$stockout_data);
            $stockout_id = $id;

            $this->stockout->deleteDetail($id);

            $materials = $data['material_id'];
            $countmaterials = sizeof($materials);

                for($i = 0; $i < $countmaterials; $i++){
           
                     if($data['material_id'][$i]){
                         $stockoutDetaildata['stockout_id'] = $stockout_id;
                         $stockoutDetaildata['material_id'] = $data['material_id'][$i];
                         $stockoutDetaildata['qty'] = $data['qty'][$i];
                         $stockoutDetaildata['remark'] = $data['remark'][$i];

                          $this->stockout->saveStockoutDetail($stockoutDetaildata);
                     }
                         
                 }

            toastr()->success('Stock Out Updated Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('stockout.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
       try{
            $this->stockout->delete($id);
            $this->stockout->deleteDetail($id);
            toastr()->success('Stock Out Deleted Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        return redirect(route('stockout.index')); 
    }
}
