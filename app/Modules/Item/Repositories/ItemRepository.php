<?php 
namespace App\Modules\Item\Repositories;

use App\Modules\Item\Entities\Item;

class ItemRepository implements ItemInterface
{
    
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'ASC'], $status = [0, 1])
    {
        
        $result =Item::when(array_keys($filter, true), function ($query) use ($filter) {

        })
            
            ->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 
    }
    
    public function find($id){
        return Item::find($id);
    }
    
    public function getList(){  
        $result = Item::pluck('name', 'id');
        return $result;
    }

    public function save($data){
        return Item::create($data);
    }
    
    public function update($id,$data){
        $result = Item::find($id);
        return $result->update($data);
    }
    
    public function delete($id){
        return Item::destroy($id);
    }

    public function countTotal()
    {
        return Item::count();
    }


}