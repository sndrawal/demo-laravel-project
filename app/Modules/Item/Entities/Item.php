<?php

namespace App\Modules\Item\Entities;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    protected $fillable = [

    	'name',
    	'normal_price',
    	'promotion_price'

    ];

}
