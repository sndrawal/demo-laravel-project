<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {


        Route::get('item', ['as' => 'item.index', 'uses' => 'ItemController@index']);

        Route::get('item/create', ['as' => 'item.create', 'uses' => 'ItemController@create']);
        Route::post('item/store', ['as' => 'item.store', 'uses' => 'ItemController@store']);

        Route::get('item/edit/{id}', ['as' => 'item.edit', 'uses' => 'ItemController@edit'])->where('id','[0-9]+');
        Route::put('item/update/{id}', ['as' => 'item.update', 'uses' => 'ItemController@update'])->where('id','[0-9]+');

        Route::get('item/delete/{id}', ['as' => 'item.delete', 'uses' => 'ItemController@destroy'])->where('id','[0-9]+');

});
