@extends('admin::layout')
@section('title')Petty @stop 
@section('breadcrum')View Petty @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->
@stop 
@section('content')
<div class="card">
    <div class="card-header bg-teal-400 text-white header-elements-inline">
        <h5 class="card-title">
            Petty: {{ $petty->name }}
        </h5>
    </div>
    <div class="card-body">
        <div class="row">
            @if ($petty->photo)
            <div class="col-md-6">
                <img src="{{ url('/uploads/petty/'.$petty->photo) }}" width="100%" title="Petty Photo">
            </div>
            @endif
            
            @if ($petty->agreement_picture)
            <div class="col-md-6">
                <img src="{{ url('/uploads/petty/'.$petty->agreement_picture) }}" width="100%" title="Petty Agreement Picture">
            </div>
            @endif
        </div>
    </div>
</div>
@stop