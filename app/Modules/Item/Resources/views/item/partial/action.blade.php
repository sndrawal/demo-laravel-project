<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/validation/item.js') }}"></script>
<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

    <div class="row">

        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-form-label col-lg-4">Item:</label>
                    <div class="col-lg-8">
                       <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-box"></i></span>
                        </span>
                        {!! Form::text('name', $value = null, ['placeholder'=>'Enter Item','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-form-label col-lg-4">Normal Price:</label>
                    <div class="col-lg-8">
                       <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text">USD</span>
                        </span>
                        {!! Form::text('normal_price', $value = null, ['placeholder'=>'Enter Normal Price','class'=>'normal_price form-control numeric']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-6">

            <div class="form-group row">
                <label class="col-form-label col-lg-4">Promotion Price:</label>
                    <div class="col-lg-8">
                       <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text">USD</span>
                        </span>
                        {!! Form::text('promotion_price', $value = null, ['placeholder'=>'Enter Promotion Price','class'=>'promotion_price form-control numeric']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

</fieldset>


<div class="text-right"> 
    <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b> {{ $btnType }}</button>
</div>
