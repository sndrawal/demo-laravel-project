<?php

namespace App\Modules\Quotation\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

use App\Modules\Quotation\Repositories\QuotationInterface;
use App\Modules\Quotation\Repositories\QuotationRepository;
use App\Modules\Quotation\Repositories\QuotationItemInterface;
use App\Modules\Quotation\Repositories\QuotationItemRepository;

class QuotationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->quotationRegister();
        $this->quotationItemRegister();
    }

    public function quotationRegister(){
        return $this->app->bind(
             QuotationInterface::class,
             QuotationRepository::class
        );
    }

    public function quotationItemRegister(){
        return $this->app->bind(
             QuotationItemInterface::class,
             QuotationItemRepository::class
        );
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('quotation.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'quotation'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/quotation');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/quotation';
        }, \Config::get('view.paths')), [$sourcePath]), 'quotation');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/quotation');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'quotation');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'quotation');
        }
    }

    /**
     * Register an additional directory of factories.
     * 
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
