<?php 
namespace App\Modules\Quotation\Repositories;


use App\Modules\Quotation\Entities\QuotationField;

class QuotationItemRepository implements QuotationItemInterface
{
    
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result = QuotationField::orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;
    }
    
    public function find($id){
        return QuotationField::find($id);
    }
    
    public function findById($id){
        return QuotationField::where('quotation_id','=',$id)->get();
    }
    
    public function getList(){
       $result = QuotationField::pluck('designation_name', 'id');
       return $result;
   }
    
    public function save($data){
        return QuotationField::create($data);
    }
    
    public function update($id,$data){
        $result = QuotationField::find($id);
        return $result->update($data);
    }
    
    public function delete($id){
        return QuotationField::where('quotation_id','=',$id)->delete($id);
    }
}