<?php

namespace App\Modules\Quotation\Repositories;


interface QuotationItemInterface
{
    public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function findById($invoiceId);
    
    public function getList();

    public function save($data);

    public function update($id,$data);

    public function delete($id);

}