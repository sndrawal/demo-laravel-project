<?php

namespace App\Modules\Quotation\Entities;

use Illuminate\Database\Eloquent\Model;

class QuotationField extends Model
{
    protected $fillable = [

    	'quotation_id',
    	'activity',
    	'description',
    	'qty',
    	'price',
    	'amount'

    ];
}
