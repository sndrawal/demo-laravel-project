<?php

namespace App\Modules\Quotation\Entities;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = [

    	'ship_to',
    	'address',
        'type',
    	'quotation_no',
    	'expiration_date',
    	'ship_date',
        'ship_via',
        'sales_rep',
        'payment_terms',
        'internal_memo',
        'status',
        'accepted_by',
        'accepted_date', 
        'sub_total',
        'discount_percent',
        'discount_amount',
        'tax_amount',
        'grand_total',
        'grand_total_in_words',
        'date'
    ];

    public function getQuotationField()
    {
        return $this->hasMany(QuotationField::class,'quotation_id','id');
    }

}
