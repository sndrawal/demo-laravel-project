<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Quotation Generate</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/global/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

</head>

<body>

    <div class="page-content">
        <div class="content">

            <div class="card">
                <div class="card-body">
                    <div class="row font-size-lg">
                       <div class="col-sm-1">
                       </div>
                       <div class="col-sm-4">
                        <div class="mb-4">
                            <img src="{{asset('uploads/quotation_logo.png')}}" class="mb-3 mt-2" alt="" style="width: 120px;">
                            <ul class="list list-unstyled mb-0">
                                <li><h4>Date: 18-09-2019</h4></li>
                                <br>
                                <li><h4>To,</h4></li>
                                <li><h4>M/s Industries Pvt Ltd.</h4></li>
                                <li><h4>Kathmandu, Nepal</h4></li>
                                <br>
                                <li><h4>Kind Attn: XY Shrestha</h4></li>
                                <li><h4>Email-ID: ab@xyz.com</h4></li>
                                <br>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row font-size-lg text-justify">
                 <div class="col-sm-1">
                 </div>

                 <div class="col-sm-10">
                    <div class="mb-10">
                        <ul class="list list-unstyled mb-0">
                            <li><h4>Dear Sir/Madam,</h4></li>
                            <br>
                            <li><h4>With reference to the inquiry, we are pleased to quote our competitive rates as below, In reference to above referred subject matter and your email enquiry we are pleased to provide you our most competitive quotation.</h4></li>
                            <br>
                            <li><h4>We take this opportunity to introduce to you the capabilities of our organization TATER GROUP. Tater Group is a conglomerate whose reach encompasses manufacturing and trading across the industrial, hydro power, agricultural and banking sectors. We are Nepal’s most trusted supplier of power products and construction materials. Our major service is to provide solutions that enable industries to flourish.</h4></li>
                            <br>
                            <li><h4>Our global network comprises of trusted manufacturers and consumers, earned through 30+ years of quality service and industrial growth. We strive to benefit and inspire vicinity we work in while dominating the market realm by delivering quality products and services. Our legacy of unshakable discretion has enabled us to establish the bonds that keep us at the forefront of commerce.</h4></li>
                            <br>
                            <li><h4>Our Business Sectors:</h4></li>
                            <br>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-1">
                </div>

            </div>

            <div class="row container-fluid ml-2">
                <div class="col-sm-1">
                </div>
                <div class="col-md-10">
                    <div class="mb-10">
                        <dl>
                            <dt class="text-uppercase"><i class="icon-arrow-right15 mr-2"></i>TRADING</dt>
                            <dd>Our major service is trading of construction materials and power equipment within and across the borders.</dd>

                            <dt class="text-uppercase"><i class="icon-arrow-right15 mr-2"></i>MANUFACTURING</dt>
                            <dd>We manufacture construction materials corresponding to the customer’s needs and specifications.</dd>

                            <dt class="text-uppercase"><i class="icon-arrow-right15 mr-2"></i>INVESTMENTS</dt>
                            <dd>We have invested in various small as well as big business projects with an aim to invigorate new ideas.</dd>
                            <br>
                        </dl>
                    </div>
                </div>
                <div class="col-sm-1">
                </div>

            </div>


            <div class="row font-size-lg text-justify">
                 <div class="col-sm-1">
                 </div>

                 <div class="col-sm-10">
                    <div class="mb-10">
                        <ul class="list list-unstyled mb-0">
                            <li><h4>AIMIL is from Trading Sector;</h4></li>
                
                            <li><h4>AIMIL deals in Civil Engineering, Environmental, and Geological & Mining Consultancies with Research Services of international standards to help organizations find an optimal and practical solution to their problems.</h4></li>
                            <br>
                            <li><h4>The broad categorization of our services in the sectors we serve as of now; mentioned hereunder:</h4></li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-1">
                </div>

            </div>


            <div class="row font-size-lg text-justify">
                 <div class="col-sm-1">
                 </div>
                 <div class="col-sm-10">
                    <div class="mb-10">
                        <ul class="list mb-0">
                            <li><h4>Field Geotechnical Investigations</h4></li>
                            <li><h4>Engineering Geology & Geo-hazards</h4></li>
                            <li><h4>Health Monitoring of Civil Structures</h4></li>
                            <li><h4>Numerical Modeling</h4></li>
                            <li><h4>Material Testing Laboratory [soil, rock, cement, aggregate]:</h4></li>
                            <li><h4>Concrete Technology</h4></li>
                            <br>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-1">
                </div>
            </div>

           <div class="row font-size-lg text-justify">
                 <div class="col-sm-1">
                 </div>
                 <div class="col-sm-10">
                    <h4>We can further be reached at our website www.tater-group.com for more details.</h4>
                <div class="table-responsive">
                <table class="table table-lg table-bordered">
                    <thead>
                        <tr class="font-weight-bold">
                            <th><h4>S.No.</h4></th>
                            <th><h4>Cat No.</h4></th>
                            <th><h4>Product Description</h4></th>
                            <th><h4>Qty.</h4></th>
                            <th><h4>Unit Price(NRs.)</h4></th>
                            <th><h4>Delivery</h4></th>
                        </tr>
                    </thead>
                    <tbody>
                       
                        <tr>
                            <td>1</td>
                            <td>AIM-123</td>
                            <td>Abc</td>
                            <td>01</td>
                            <td>1,234,567.00</td>
                            <td>5 Weeks</td>
                        </tr>

                        <tr>
                            <td>2</td>
                            <td>AIM-456</td>
                            <td>Abc</td>
                            <td>01</td>
                            <td>1,234,444.00</td>
                            <td>Ready</td>
                        </tr>

                    </tbody>
                </table>
            </div>
                
            </div>
            <div class="col-sm-1">
                </div>
        </div>


        <div class="row font-size-lg text-justify">
                 <div class="col-sm-1">
                 </div>

                 <div class="col-sm-10">
                    <div class="mb-10">
                        <ul class="list list-unstyled mb-0">
                            <li><h4 class="font-weight-black"><u>TERMS AND CONDITIONS:</u></h4></li>
                
                            <li><h4>Price-Based On Ex-warehouse KTM basis</h4></li>
                            <li><h4>VAT: 13% Extra on above rates</h4></li>
                            <li><h4>Payment: 100% full payment before delivery</h4></li>
                            <li><h4>Warranty: 12 months</h4></li>
                            <br>
                            <li><h4>The opportunity to submit the proposal is very much appreciated and we look forward to hearing from you in the near future. For any further assistance, please feel free to contact undersigned.</h4></li>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <li><h4>Thanking You,</h4></li>
                            <li><h4>With Regards,</h4></li>
                            <br>
                            <br>
                            <li><h4>For, H.G. Ent. Pvt. Ltd.</h4></li>
                            
                        </ul>
                    </div>
                </div>

                <div class="col-sm-1">
                </div>
            </div>


            <div class="row font-size-lg text-justify">
                 <div class="col-sm-5">
                 </div>
                 <div class="col-sm-4">
                    <div class="mb-4">
                        <img src="{{asset('uploads/quotation_main_logo.png')}}" class="mb-3 mt-2" alt="" style="width: 120px;">
                    </div>
                </div>
                <div class="col-sm-3">
                </div>
            </div>


             <div class="row font-size-lg text-justify">
                 <div class="col-sm-1">
                 </div>
                 <div class="col-sm-10">
                    <div class="mb-10">
                        <ul class="list list-unstyled mb-0">
                            <li><h4><span class="font-weight-black">H.G. ENT. PVT. LTD. </span>Kathmandu, Nepal | <span class="font-weight-black">Tel:</span> +977-1-5100413 / 5100374 | <span class="font-weight-black">Fax:</span> +977-1-5100412 | <span class="font-weight-black">Email:</span> info@tater-group.com <span class="font-weight-black">Web:</span> www.tater-group.com.</h4></li>
                            <br>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-1">
                </div>
            </div>



        </div>


        <div class="card-footer text-center">
            <span class="text-muted">Thank you for your business.</span>
        </div>
    </div>
</div>
</div>


</body>

</html>


