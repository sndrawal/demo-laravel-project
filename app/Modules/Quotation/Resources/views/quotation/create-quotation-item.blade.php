@extends('admin::layout')
@section('title')Quotation Items @stop
@section('breadcrum')Create Quotation Items @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800  header-elements-inline">
        <h5 class="card-title">Create Quotation Items</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">

        {!! Form::open(['route'=>'quotation.itemStore','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}
        <input type="hidden" name="quotation_id"  value="{{$quotation_id}}">
        <fieldset class="mb-3">
            <legend class="text-uppercase font-size-sm font-weight-bold"># Quotation Items</legend>

            <div class="text-left mb-1" style="margin-left: 20px;">
                <a href="javascript:void(0)" id="add_items" class="btn bg-success btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b>Add Quotation Item</a>
            </div>


            <div class="quotation-item addQuotationItem">
                <table class="table table-responsive ">
                    <tr>
                        <td>
                            <textarea name="product_description[]" rows="6" cols="50" class="form-control bg-slate-300 text-white border-transparent" placeholder="Enter Product Description"></textarea>
                        </td>
                        <td>
                            <input type="text" name="cat_no[]" class="cat_no form-control bg-slate-300 text-white border-transparent" placeholder="Enter Cat No" />
                        </td>
                        
                        <td>
                            <input type="text" name="qty[]" class="qty form-control bg-slate-300 text-white border-transparent numeric" placeholder="Enter Qty" />
                        </td>

                        <td>
                            <input type="text" name="unit_price[]" class="unit_price form-control bg-slate-300 text-white border-transparent numeric" placeholder="Enter Unit Price (NRs.)" />
                        </td>

                        <td>
                            <input type="text" name="delivery[]" class="delivery form-control bg-slate-300 text-white border-transparent" placeholder="Delivery" />
                        </td>
                    </tr>

                </table>
                <hr>
            </div>
            
            <div class="mb-3 mr-3 text-right">
                <button type="submit" class="btn bg-teal-400">Save Quotation<i class="icon-database-insert"></i></button>
            </div>

           
        </fieldset>
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

<script type="text/javascript">
    $(document).ready(function() {

        $('#add_items').on('click', function() {
            var url="{{route('quotation.add.item')}}";
            $.ajax({
                type: 'GET',
                url: url,
                success: function(data) {
                    $('.addQuotationItem').last().append(data);
                }
            });
        });

        $(document).on('click', '.remove_items', function() {
            $(this).parent().parent().remove();
        });
    });

</script>


@stop
