@extends('admin::layout')
@section('title')Quotation/BOQ Generate @stop
@section('breadcrum')Quotation/BOQ Generate @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<link src="{{asset('admin/invoice.css')}}" />
@stop

@section('content') 

<div class="card">
        <div class="card-header bg-purple-400 header-elements-inline">
            <a href="{{ route('quotation.create') }}" class="btn bg-success-600 btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-pen-plus"></i></b> Add Quotation/BOQ
               </a>
            
        </div>
    </div>




<div class="card">
    <div class="card-header bg-slate-600 header-elements-inline">
        <h5 class="card-title">List of Quotation/BOQ</h5>

    </div>
 
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="bg-slate">
                    <th>#</th>
                    <th>Quotation No</th>
                    <th>Ship To</th>
                    <th>Address</th>
                    <th>Expiration Date</th>
                    <th>Ship Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($quotation->total() != 0)
                @foreach($quotation as $key => $value)

                <tr>
                    <td>{{$quotation->firstItem() +$key}}</td>
                    <td>{{ $value->quotation_no }}</td>
                    <td>{{ $value->ship_to }}</td>
                    <td>{{ $value->address }}</td>
                    <td>{{ $value->expiration_date }}</td>
                    <td>{{ $value->ship_date }}</td>
                    
                    <td>
                        
                        <a class="btn bg-success-400 btn-icon rounded-round" href="{{route('quotation.edit',$value->id)}}" data-popup="tooltip" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil6"></i></a>

                        <a class="btn bg-warning-400 btn-icon rounded-round" href="{{route('quotation.print',$value->id)}}" target="_blank" data-popup="tooltip" data-placement="bottom" data-original-title="Generate Quotation"><i class="icon-file-eye"></i></a>

                        <a data-toggle="modal" data-target="#modal_theme_warning" class="btn bg-danger-400 btn-icon rounded-round delete_invoice" link="{{route('quotation.delete',$value->id)}}" data-popup="tooltip" data-placement="bottom" data-original-title="Delete"><i class="icon-bin"></i></a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">No Quotation/BOQ Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>
        <span style="margin: 5px;float: right;">
            @if($quotation->total() != 0)
                {{ $quotation->links() }}
            @endif
            </span>
    </div>
</div>


<!-- Delete modal -->
<div id="modal_theme_warning" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
         <div class="modal-body">
            <center>
                <i class="icon-alert text-danger icon-3x"></i>
            </center>
            <br>
            <center>
                <h2>Are You Sure Want To Delete ?</h2>
                <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </center>
        </div>
    </div>
</div>
</div>

<!-- /Delete modal -->

<script type="text/javascript">
    $('document').ready(function() {
        $('.delete_invoice').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });
        
         $('.view_quotation').on('click',function(){
            var quotation_id = $(this).attr('quotation_id');
            $.ajax({
             type: 'GET',
             url: 'quotation/show/'+quotation_id,
             success: function (data) {
              $('.result_view_detail').html(data);
            }
          }); 
      });
        
    });
</script>

@endsection