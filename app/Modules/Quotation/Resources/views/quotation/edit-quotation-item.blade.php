@extends('admin::layout')
@section('title')Quotation Item @stop
@section('breadcrum')Updatr Quotation Item @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800  header-elements-inline">
        <h5 class="card-title">Update Quotation Item</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">

        {!! Form::model($quotation,['method'=>'PUT','route'=>['quotation.itemUpdate',$quotation->id],'class'=>'form-horizontal','role'=>'form','files'=>true]) !!}

        <fieldset class="mb-3">
            <legend class="text-uppercase font-size-sm font-weight-bold">#Invoice Items</legend>

            <div class="text-left mb-1" style="margin-left: 20px;">
                <a href="javascript:void(0)" id="add_items" class="btn bg-success btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b>Add Item</a>
            </div>


            <div class="quotation-item addQuotationItem">
                
                @if(count($quotationItem) > 0)
                   @foreach($quotationItem as $key => $value) 
                <table class="table table-responsive ">
                    <tr>
                        <td>
                            <textarea name="product_description[]"  rows="6" cols="50" class="form-control bg-slate-300 text-white border-transparent" placeholder="Enter Product Description">{{$value->product_description}}</textarea>
                        </td>
                        <td>
                            <input type="text" name="cat_no[]" class="cat_no form-control bg-slate-300 text-white border-transparent" placeholder="Enter Cat No" value="{{$value->cat_no}}"/>
                        </td>

                        <td>
                            <input type="text" name="qty[]" class="qty form-control bg-slate-300 text-white border-transparent numeric" placeholder="Enter Qty" value="{{$value->qty}}" />
                        </td>

                        <td>
                            <input type="text" name="unit_price[]" class="unit_price form-control bg-slate-300 text-white border-transparent numeric" placeholder="Enter Unit Price (NRs.)"  value="{{$value->unit_price}}"/>
                        </td>

                        <td>
                            <input type="text" name="delivery[]" class="delivery form-control bg-slate-300 text-white border-transparent" placeholder="Delivery" value="{{$value->product_description}}"/>
                        </td>
                    </tr>
                </table>
                <hr>
                @endforeach
                
                @else
                <table class="table table-responsive ">
                    <tr>
                        <td>
                            <textarea name="product_description[]"  rows="6" cols="50" class="form-control bg-slate-300 text-white border-transparent" placeholder="Enter Product Description"></textarea>
                        </td>
                        <td>
                            <input type="text" name="cat_no[]" class="cat_no form-control bg-slate-300 text-white border-transparent" placeholder="Enter Cat No" value=""/>
                        </td>

                        <td>
                            <input type="text" name="qty[]" class="qty form-control bg-slate-300 text-white border-transparent numeric" placeholder="Enter Qty" value="" />
                        </td>

                        <td>
                            <input type="text" name="unit_price[]" class="unit_price form-control bg-slate-300 text-white border-transparent numeric" placeholder="Enter Unit Price (NRs.)"  value=""/>
                        </td>

                        <td>
                            <input type="text" name="delivery[]" class="delivery form-control bg-slate-300 text-white border-transparent" placeholder="Delivery" value=""/>
                        </td>
                    </tr>

                </table>
                <hr>
                @endif
                
            </div>
            {!! Form::hidden('quotation_id',$quotation_id) !!}

            <div class="text-right">
                <button type="submit" class="btn bg-teal-400">Update <i class="icon-database-insert"></i></button>
            </div>

        </fieldset>

        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->
<script type="text/javascript">
    $(document).ready(function() {

        $('#add_items').on('click', function() {
            var url="{{route('quotation.add.item')}}";
            $.ajax({
                type: 'GET',
                url: url,
                success: function(data) {
                    $('.addQuotationItem').last().append(data);
                }
            });
        });

        $(document).on('click', '.remove_items', function() {
            $(this).parent().parent().remove();
        });
    });

</script>


@stop
