    <script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
    <script src="{{ asset('admin/validation/quotation.js') }}"></script>

    <fieldset class="mb-3">
        <legend class="text-uppercase font-size-sm font-weight-bold"></legend>


        <div class="form-group row">
          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Type :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-lan2"></i></span>
                        </span>
                        {!! Form::select('type',[ 'quotation'=>'Quotation','boq'=>'BOQ'], $value = null, ['id'=>'status','placeholder'=>'Select Type','class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
          </div>

          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Created Date :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-calendar"></i></span>
                        </span>
                        {!! Form::text('date', $value = null, ['id'=>'client_name','placeholder'=>'Enter Created Date','class'=>'form-control daterange-single']) !!}
                    </div>
                </div>
            </div>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Ship To :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-office"></i></span>
                        </span>
                        {!! Form::text('ship_to', $value = null, ['id'=>'ship_to','placeholder'=>'Enter Ship To','class'=>'form-control','required']) !!}
                    </div>
                </div>
            </div>
          </div>

          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Address :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-location4"></i></span>
                        </span>
                        {!! Form::text('address', $value = null, ['id'=>'address','placeholder'=>'Enter Address','class'=>'form-control','required']) !!}
                    </div>
                </div>
            </div>
          </div>
        </div>

         <div class="form-group row">
          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Quotation No. :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-pencil7"></i></span>
                        </span>
                        {!! Form::text('quotation_no', $value = null, ['id'=>'quotation_no','placeholder'=>'Enter Quotation No.','class'=>'form-control','required']) !!}
                    </div>
                </div>
            </div>
          </div>

          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Expiration Date :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-calendar"></i></span>
                        </span>
                        {!! Form::text('expiration_date', $value = null, ['id'=>'expiration_date','placeholder'=>'Enter Expiration Date','class'=>'form-control daterange-single']) !!}
                    </div>
                </div>
            </div>
          </div>
        </div>

         <div class="form-group row">
          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Ship Date :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-calendar"></i></span>
                        </span>
                        {!! Form::text('ship_date', $value = null, ['id'=>'ship_date','placeholder'=>'Enter Ship Date','class'=>'form-control daterange-single']) !!}
                    </div>
                </div>
            </div>
          </div>

          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Ship Via :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-truck"></i></span>
                        </span>
                        {!! Form::text('ship_via', $value = null, ['id'=>'ship_via','placeholder'=>'Enter Ship Via','class'=>'form-control','required']) !!}
                    </div>
                </div>
            </div>
          </div>
        </div>


         <div class="form-group row">
          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Sales Rep. :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-users"></i></span>
                        </span>
                        {!! Form::text('sales_rep', $value = null, ['id'=>'sales_rep','placeholder'=>'Enter Sales Rep.','class'=>'form-control','required']) !!}
                    </div>
                </div>
            </div>
          </div>

          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Payment Terms :<span class="text-danger">*</span></label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-credit-card"></i></span>
                        </span>
                        {!! Form::text('payment_terms', $value = null, ['id'=>'payment_terms','placeholder'=>'Enter Payment Terms','class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
          </div>
        </div>

         <div class="form-group row">
          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Internal Memo :</label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-pencil7"></i></span>
                        </span>
                        {!! Form::text('internal_memo', $value = null, ['id'=>'internal_memo','placeholder'=>'Enter Internal Memo','class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
          </div>

          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Status :</label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-meter2"></i></span>
                        </span>
                        {!! Form::select('status',[ 'accepted'=>'Accepted'], $value = null, ['id'=>'status','class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
          </div>
        </div>

</fieldset>

  <fieldset>
      <legend class="font-weight-bold text-purple">Quotation/Boq Lists</legend>
             @include('quotation::quotation.partial.quotation-item') 
  </fieldset>

  <fieldset>
      <legend class="font-weight-bold text-purple" style="margin-bottom: 7px;">
        <span style="float: right;margin-right: 297px;font-size: 16px;">Grand Total</span>
      </legend>
             @include('quotation::quotation.partial.quotation-balance') 
  </fieldset>

  <div class="form-group row mt-4">
          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Accepted By :</label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-users"></i></span>
                        </span>
                        {!! Form::text('accepted_by', $value = null, ['id'=>'accepted_by','placeholder'=>'Accepted By','class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
          </div>

          <div class="col-lg-6">
                <div class="row">
                  <label class="col-form-label col-lg-3">Accepted Date :</label>
                      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-calendar"></i></span>
                        </span>
                        {!! Form::text('accepted_date', $value = null, ['id'=>'accepted_date','placeholder'=>'Accepted Date','class'=>'form-control daterange-single']) !!}
                    </div>
                </div>
            </div>
          </div>
        </div>


<div class="text-right mt-4">
   <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b> Save</button>
</div>


<script type="text/javascript">
    
    $(document).ready(function(){

       $('.add_quotation').on('click',function(){

            $.ajax({
                    type: 'GET',
                    url: '/admin/quotation/appendquotationItem',
                    success: function (data) {
                        $('.appendQuotationItems').last().after(data.options); 
                    }
                });

         }); 


         $('.price').on('keyup',function(){ 

                var price = $(this).val();
                var qty = $(this).parent().parent().parent().parent().prev().find('.qty').val();
   
                var amount = qty * price;

                $(this).parent().parent().parent().parent().next().find('.amount').val(amount);

                 var arr = document.getElementsByClassName('amount');  
                 var tot=0;
                    for(var i=0;i<arr.length;i++){
                        if(parseInt(arr[i].value))
                            tot += parseInt(arr[i].value);
                    }
              
                var total_amount = tot;
                 $('.sub_total').val(total_amount.toFixed(2));

                 $('.discount_percent').val(0);
                 $('.discount_amount').val(0);

                 var discount_amount = 0;

                 var discounted_amount = total_amount - discount_amount;

                 var tax_value = (13/100) * discounted_amount;
                 $('.tax_amount').val(tax_value.toFixed(2));


                 var grand_total = total_amount + tax_value
                 $('.grand_total').val(grand_total.toFixed(2));
            
            });


         

        $('.remove_quotation').on('click',function(){ 
            $(this).parent().parent().parent().parent().remove();
        });
    
    });

</script>