<div class="mb-1 row">
        <div class="col-lg-8"></div>
          <div class="col-lg-4">
                <div class="row">
                  <label class="col-form-label col-lg-4">Sub Total :</label>
                      <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text">Rs.</span>
                        </span>
                        {!! Form::text('sub_total', $value = null, ['id'=>'sub_total','placeholder'=>'Sub Total','class'=>'sub_total form-control numeric','readonly'=>'readonly']) !!}
                    </div>
                </div>
            </div>
          </div>
    </div>

    <div class="mb-1 row">
        <div class="col-lg-8"></div>
          <div class="col-lg-4">
                <div class="row">
                  <label class="col-form-label col-lg-4">Discount (%) :</label>
                      <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          
                        {!! Form::text('discount_percent', $value = null, ['id'=>'discount_percent','placeholder'=>'Discount Percent','class'=>'discount_percent form-control numeric']) !!}
                    
                        <span class="input-group-prepend">
                            <span class="input-group-text">%</span>
                        </span>

                    </div>
                </div>
            </div>
          </div>
    </div>

    <div class="mb-1 row">
        <div class="col-lg-8"></div>
          <div class="col-lg-4">
                <div class="row">
                  <label class="col-form-label col-lg-4">Discount Amount :</label>
                      <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text">Rs.</span>
                        </span>
                        {!! Form::text('discount_amount', $value = null, ['id'=>'discount_amount','placeholder'=>'Discount Amount','class'=>'discount_amount form-control numeric']) !!}
                    </div>
                </div>
            </div>
          </div>
    </div>

    <div class="mb-1 row">
        <div class="col-lg-8"></div>
          <div class="col-lg-4">
                <div class="row">
                  <label class="col-form-label col-lg-4">Tax(13%) :</label>
                      <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text">Rs.</span>
                        </span>
                        {!! Form::text('tax_amount', $value = null, ['id'=>'tax_amount','placeholder'=>'Tax Amount','class'=>'tax_amount form-control numeric','readonly'=>'readonly']) !!}
                    </div>
                </div>
            </div>
          </div>
    </div>

    <div class="mb-1 row">
        <div class="col-lg-8"></div>
          <div class="col-lg-4">
                <div class="row">
                  <label class="col-form-label col-lg-4">Grand Total :</label>
                      <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text">Rs.</span>
                        </span>
                        {!! Form::text('grand_total', $value = null, ['id'=>'grand_total','placeholder'=>'Grand Total','class'=>'grand_total form-control numeric','readonly'=>'readonly']) !!}
                    </div>
                </div>
            </div>
          </div>
    </div>


<script type="text/javascript">
    
    $(document).ready(function(){

    	$('.discount_percent').on('keyup',function(){

    		var percent = $(this).val();
    		var sub_total = $('.sub_total').val();

    		var calculate = (percent/100) * sub_total;

    		$('.discount_amount').val(calculate.toFixed(2));

    		 var discounted_amount = sub_total - calculate;

                 var tax_value = (13/100) * discounted_amount;
                 $('.tax_amount').val(tax_value.toFixed(2));


                 var grand_total = parseFloat(sub_total) + parseFloat(tax_value);
                 $('.grand_total').val(grand_total.toFixed(2));


    	});

    	$('.discount_amount').on('keyup',function(){

    		var discount_amout = $(this).val();
    		var sub_total = $('.sub_total').val();

    		var calculate = (discount_amout/sub_total) * 100;

    		$('.discount_percent').val(calculate.toFixed(2));


    		 var discounted_amount = sub_total - discount_amout;

                 var tax_value = (13/100) * discounted_amount;
                 $('.tax_amount').val(tax_value.toFixed(2));


                 var grand_total = parseFloat(sub_total) + parseFloat(tax_value);
                 $('.grand_total').val(grand_total.toFixed(2));

    	});
    });

</script>
