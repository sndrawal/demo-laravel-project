@if($edit)

@foreach($quotation->getQuotationField as $key => $items)


<div class="appendQuotationItems">
    <div class="mb-1 row">
        <div class="col-lg-3">
            <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                                {!! Form::text('activity[]', $value = $items->activity, ['id'=>'activity','class'=>'form-control','required','placeholder'=>'Activity']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-3">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i>
                                </span>
                            </span>
                            {!! Form::text('description[]', $value = $items->description, ['id'=>'description','class'=>'form-control','placeholder'=>'Description']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                    {!! Form::text('qty[]', $value = $items->qty, ['id'=>'qty','class'=>'qty form-control numeric','required','placeholder'=>'Qty']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                           
                    {!! Form::text('price[]', $value = $items->price, ['id'=>'price','class'=>'price form-control numeric','required','placeholder'=>'Price']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">Rs.
                                </span>
                            </span>
                            {!! Form::text('amount[]', $value = $items->amount, ['id'=>'amount','class'=>'amount form-control','readonly'=>'readonly','placeholder'=>'Amount']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

    {{ Form::hidden('quotation_item_id[]', $items->id, array('class' => 'quotation_item_id')) }}


@endforeach


@else

<div class="appendQuotationItems">
    <div class="mb-1 row">
        <div class="col-lg-3">
            <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                                {!! Form::text('activity[]', $value = null, ['id'=>'activity','class'=>'form-control','required','placeholder'=>'Activity']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-3">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i>
                                </span>
                            </span>
                            {!! Form::text('description[]', $value = null, ['id'=>'description','class'=>'form-control','placeholder'=>'Description']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                    {!! Form::text('qty[]', $value = null, ['id'=>'qty','class'=>'qty form-control numeric','placeholder'=>'Qty']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                           
                    {!! Form::text('price[]', $value = null, ['id'=>'price','class'=>'price form-control numeric','placeholder'=>'Price']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">Rs.
                                </span>
                            </span>
                            {!! Form::text('amount[]', $value = null, ['id'=>'amount','class'=>'amount form-control','readonly'=>'readonly','placeholder'=>'Amount']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>


@endif

<div class="mb-1 row">
     <div class="col-lg-2">
       <div class="row">
                <button type="button" class="ml-2 add_quotation btn bg-success-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Add More</button>
        </div>
    </div>
</div>


 



