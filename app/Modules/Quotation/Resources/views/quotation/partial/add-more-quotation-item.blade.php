<div class="appendQuotationItems">
    <div class="row mb-1">
        <div class="col-lg-3">
            <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                                {!! Form::text('activity[]', $value = null, ['id'=>'activity','class'=>'form-control','required','placeholder'=>'Activity']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-3">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i>
                                </span>
                            </span>
                            {!! Form::text('description[]', $value = null, ['id'=>'description','class'=>'form-control','placeholder'=>'Description']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                    {!! Form::text('qty[]', $value = null, ['id'=>'qty','class'=>'qty form-control numeric','placeholder'=>'Qty']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                           
                    {!! Form::text('price[]', $value = null, ['id'=>'price','class'=>'price form-control numeric','placeholder'=>'Price']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">Rs.
                                </span>
                            </span>
                            {!! Form::text('amount[]', $value = null, ['id'=>'amount','class'=>'amount form-control','readonly'=>'readonly','placeholder'=>'Amount']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

<div class="mb-1 row">
        <div class="col-lg-10"></div>
         <div class="col-lg-2">
           <div class="row">
            <button type="button" class="remove_quotation btn bg-danger border-danger text-danger-800 btn-icon ml-2" style="margin-left: 151px !important;"><i class="icon-trash"></i></button>

            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    
    $(document).ready(function(){

         $('.price').on('keyup',function(){ 

                var price = $(this).val();
                var qty = $(this).parent().parent().parent().parent().prev().find('.qty').val();
   
                var amount = qty * price;

                $(this).parent().parent().parent().parent().next().find('.amount').val(amount);

                  var arr = document.getElementsByClassName('amount');  
                 var tot=0;
                    for(var i=0;i<arr.length;i++){
                        if(parseInt(arr[i].value))
                            tot += parseInt(arr[i].value);
                    }
               
                var total_amount = tot;
                 $('.sub_total').val(total_amount.toFixed(2));

                 $('.discount_percent').val(0);
                 $('.discount_amount').val(0);

                 var discount_amount = 0;

                 var discounted_amount = total_amount - discount_amount

                 var tax_value = (13/100) * discounted_amount;
                 $('.tax_amount').val(tax_value.toFixed(2));


                 var grand_total = total_amount + tax_value
                 $('.grand_total').val(grand_total.toFixed(2));

            });


        $('.remove_quotation').on('click',function(){ 
            $(this).parent().parent().parent().parent().remove();
        });
    
    });

</script>
 



