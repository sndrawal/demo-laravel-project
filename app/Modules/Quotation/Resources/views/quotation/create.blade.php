@extends('admin::layout')
@section('title')Quotation/BOQ @stop 
@section('breadcrum')Create Quotation/BOQ @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800  header-elements-inline">
        <h5 class="card-title">Create Quotation/BOQ</h5>
        <div class="header-elements">

        </div>
    </div>
    
    <div class="card-body">

        {!! Form::open(['route'=>'quotation.store','method'=>'POST','class'=>'form-horizontal','id' => 'quotation_submit','role'=>'form','files' => true]) !!}
        
            @include('quotation::quotation.partial.action',['btnType'=>'Save']) 
        
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@stop