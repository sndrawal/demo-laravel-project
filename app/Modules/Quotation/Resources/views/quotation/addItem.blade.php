<div class="appendQuotationItems">
    <div class="form-group row">
        <div class="col-lg-4">
            <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                                {!! Form::text('activity[]', $value = null, ['id'=>'activity','class'=>'form-control','required','placeholder'=>'Activity']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i>
                                </span>
                            </span>
                    {!! Form::text('description[]', $value = null, ['id'=>'description','class'=>'form-control numeric','placeholder'=>'Description']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">Rs.
                                </span>
                            </span>
                    {!! Form::text('qty[]', $value = null, ['id'=>'qty','class'=>'qty form-control','placeholder'=>'Rate']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">Rs.
                                </span>
                            </span>
                    {!! Form::text('price[]', $value = null, ['id'=>'price','class'=>'price form-control','placeholder'=>'Price']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">Rs.
                                </span>
                            </span>
                            {!! Form::text('amount[]', $value = null, ['id'=>'amount','class'=>'amount form-control','readonly'=>'readonly','placeholder'=>'Amount']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <button type="button" class="ml-2 add_product btn bg-success-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Add</button>
            </div>
        </div>

    </div>

</div>