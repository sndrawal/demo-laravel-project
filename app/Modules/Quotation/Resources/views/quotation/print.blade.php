<!DOCTYPE html>
<html lang="en">

 @php
    $type = ($quotation->type == 'quotation') ? 'Quotation' : 'BOQ' ;
@endphp
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ $type }} Generate</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/global/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

</head>

<body onload="window.print();">

<div class="page-content">
    <div class="content">

        <div class="card">
             <div class="card-body">
                <div class="row font-size-lg">
                    <div class="col-sm-1">
                    </div>
                    <div class="col-5 mt-5">
                        <h4><b>{{ $org_data->company_name }}</b></h4>
                         <ul class="list list-unstyled mb-0">
                            <li class="mt-0">+977 {{ $org_data->phone_1 }}</li>
                            <li class="mt-0">{{ $org_data->email_1 }}</li>
                            <li class="mt-0">{{ $org_data->website }}</li>
                         </ul> 
                    </div>
                    <div class="col-2 mt-2">
                       
                        <h2 class="text-center"><b>{{ $type }}</b></h2>
                    </div>
                    <div class="col-3 mt-5">
                             @if($org_status == 0)
                                    <img src="{{asset('uploads/setting/'.$org_data->company_logo)}}" class="mb-3 mt-2" alt="" style="width: 150px; float: right;">
                                @elseif($org_status == 1)
                                    <img src="{{asset('uploads/organization_logo/'.$org_data->org_logo)}}" class="mb-3 mt-2" alt="" style="width: 150px; float: right;">
                                @endif
                    </div>
                    <div class="col-sm-1">
                    </div>
                </div>

                 <div class="row font-size-lg">
                    <div class="col-sm-1">
                    </div>

                    <div class="col-5 mt-2">
                        <div class="card">
                            <div class="card-header bg-grey-400 header-elements-inline">
                                <h6 class="card-title font-weight-bold">Address</h6>
                            </div>


                            <div class="card-body pb-0" style="">
                                <div class="row">
                                    <div class="media flex-column flex-sm-row mt-0 mb-3">
                                        <div class="media-body">
                                          <ul class="list list-unstyled mb-0 font-weight-semibold">
                                                <li class="mt-0">{{ $quotation->ship_to }}</li>
                                                <li class="mt-0">{{ $quotation->address }}</li>
                                             </ul> 
                                        </div>
                                   </div>

                               </div>
                           </div>
                        </div>
                    </div>

                     <div class="col-5 mt-2">
                        <div class="card">
                            <div class="card-header bg-grey-400 header-elements-inline">
                                <h6 class="card-title font-weight-bold">Ship To</h6>
                            </div>


                            <div class="card-body pb-0" style="">
                                <div class="row">
                                    <div class="media flex-column flex-sm-row mt-0 mb-3">
                                        <div class="media-body">
                                          <ul class="list list-unstyled mb-0 font-weight-semibold">
                                                <li class="mt-0">{{ $quotation->ship_to }}</li>
                                                <li class="mt-0">{{ $quotation->address }}</li>
                                             </ul> 
                                        </div>
                                   </div>

                               </div>
                           </div>
                        </div>
                    </div>

                    <div class="col-sm-1">
                    </div>
                 </div>


                  <div class="row font-size-lg">
                    <div class="col-sm-3">
                    </div>

                    <div class="col-6 mt-2">

                        <div class="card">

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="bg-grey-400">
                                            <th>QUOTATION NO.</th>
                                            <th>DATE</th>
                                            <th>EXPIRATION DATE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="font-weight-semibold">
                                            <td>{{ $quotation->quotation_no }}</td>
                                            <td>{{ date('d/m/Y',strtotime($quotation->date)) }}</td>
                                            <td>{{ date('d/m/Y',strtotime($quotation->expiration_date)) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>    

                    <div class="col-sm-3">
                    </div>
                 </div>


                 <div class="row font-size-lg">
                    <div class="col-sm-1">
                    </div>

                    <div class="col-10 mt-2">

                        <div class="card">

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="bg-grey-400">
                                            <th>SHIP DATE</th>
                                            <th>SHIP VIA</th>
                                            <th>SALES REP</th>
                                            <th>PAYMENT TERMS</th>
                                            <th>INTERNAL MEMO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="font-weight-semibold">
                                            <td>{{ date('d/m/Y',strtotime($quotation->ship_date)) }}</td>
                                            <td>{{ $quotation->ship_via }}</td>
                                            <td>{{ $quotation->sales_rep }}</td>
                                            <td>{{ $quotation->payment_terms }}</td>
                                            <td>{{ $quotation->internal_memo }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>    

                    <div class="col-sm-1">
                    </div>
                 </div>


                 <div class="row font-size-lg">
                    <div class="col-sm-1">
                    </div>

                    <div class="col-10 mt-4">

                        <div class="card">

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="bg-grey-400">
                                            <th>ACTIVIY</th>
                                            <th>DESCRIPTION</th>
                                            <th>QTY</th>
                                            <th>PRICE</th>
                                            <th>AMOUNT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quotation->getQuotationField as $key => $value)
                                            <tr class="font-weight-semibold">
                                                <td>{{ $value->activity }}</td>
                                                <td>{{ $value->description }}</td>
                                                <td>{{ $value->qty }}</td>
                                                <td>Rs.{{ number_format($value->price,2) }}</td>
                                                <td>Rs.{{ number_format($value->amount,2) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>    

                    <div class="col-sm-1">
                    </div>
                 </div>

                 <div class="row font-size-lg">
                    <div class="col-sm-7">
                    </div>

                    <div class="col-4">

                        <div class="card">

                            <div class="table-responsive">
                                <table class="table">
                                   
                                    <tbody>
                                            <tr class="font-weight-semibold">
                                                <td>SUBTOTAL</td>
                                                <td>{{ number_format($quotation->sub_total,2) }}</td>
                                            </tr>
                                            <tr class="font-weight-semibold">
                                                <td>DISCOUNT ({{ number_format($quotation->discount_percent,2) }}%)</td>
                                                <td>- {{ number_format($quotation->discount_amount,2) }}</td>
                                            </tr>
                                            <tr class="font-weight-semibold">
                                                <td>TAX (13%)</td>
                                                <td>{{ number_format($quotation->tax_amount,2) }}</td>
                                            </tr>
                                            <tr class="font-weight-semibold">
                                                <td>TOTAL</td>
                                                <td class="font-weight-bold">NRs {{ number_format($quotation->grand_total,2) }}</td>
                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>    

                    <div class="col-sm-1">
                    </div>
                 </div>


                 <div class="row font-size-lg">
                    <div class="col-sm-1">
                    </div>

                   <div class="col-5 mt-4">
                         <ul class="list list-unstyled mb-0">
                            <li class="mt-0"><span class="mr-4">Accepted By. : </span>{{ $quotation->accepted_by }} </li>
                         </ul> 
                    </div>
                    <div class="col-5 mt-4">
                         <ul class="list list-unstyled mb-0">
                            <li class="mt-0"><span class="mr-4">Accepted Date. : </span>{{ date('d/m/Y',strtotime($quotation->accepted_date)) }}</li>
                         </ul> 
                    </div>  

                    <div class="col-sm-1">
                    </div>
                 </div>


                 <div class="row mt-5">
                    <div class="col-sm-1">
                    </div>
                    
                    <div class="col-11 mt-4">
                        <span>For Standard terms & condition, Please visit www.sastranetwork.com</span>
                    </div>
                 </div>

            </div>
        </div>

    </div>
</div>


</body>

</html>


