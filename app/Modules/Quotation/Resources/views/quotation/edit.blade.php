@extends('admin::layout')
@section('title')Quotation @stop 
@section('breadcrum')Edit Quotation @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800 header-elements-inline">
        <h5 class="card-title">Edit Quotation</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">

        {!! Form::model($quotation,['method'=>'PUT','route'=>['quotation.update',$quotation->id],'id' => 'quotation_submit','class'=>'form-horizontal','role'=>'form','files'=>true]) !!} @include('quotation::quotation.partial.action',['btnType'=>'Update & Next']) {!! Form::close() !!}
        
    </div>
</div>
<!-- /form inputs -->

@stop