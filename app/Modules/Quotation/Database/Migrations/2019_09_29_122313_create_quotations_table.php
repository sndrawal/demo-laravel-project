<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->increments('id');

            $table->text('ship_to')->nullable();
            $table->text('address')->nullable();
            $table->string('type')->nullable();
            $table->string('quotation_no')->nullable();
            $table->date('expiration_date')->nullable();
            $table->date('ship_date')->nullable();
            $table->string('ship_via')->nullable();
            $table->text('sales_rep')->nullable();
            $table->text('payment_terms')->nullable();
            $table->text('internal_memo')->nullable();
            $table->string('status')->nullable();
            $table->text('accepted_by')->nullable();
            $table->date('accepted_date')->nullable();
            $table->double('sub_total',14,2)->nullable();
            $table->double('discount_percent',14,2)->nullable();
            $table->double('discount_amount',14,2)->nullable();
            $table->double('tax_amount',14,2)->nullable();
            $table->double('grand_total',14,2)->nullable();
            $table->text('grand_total_in_words')->nullable();
            $table->date('date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
