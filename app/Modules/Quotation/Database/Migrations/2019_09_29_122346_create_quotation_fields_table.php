<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_fields', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('quotation_id');
            $table->text('activity')->nullable();
            $table->text('description')->nullable();
            $table->double('qty',14,2)->nullable();
            $table->double('price',14,2)->nullable();
            $table->double('amount',14,2)->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_fields');
    }
}
