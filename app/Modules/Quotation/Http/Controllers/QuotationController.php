<?php

namespace App\Modules\Quotation\Http\Controllers;

use App\Modules\Setting\Repositories\SettingInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use PDF;

use App\Modules\Quotation\Repositories\QuotationInterface;
use App\Modules\Quotation\Repositories\QuotationItemInterface;


class QuotationController extends Controller
{

    protected $quotation;
    protected $quotationItem;
    /**
     * @var SettingInterface
     */
    private $setting;

    /**
     * QuotationController constructor.
     * @param QuotationInterface $quotation
     * @param OrganizationInterface $organization
     * @param QuotationItemInterface $quotationItem
     */
    public function __construct(QuotationInterface $quotation, QuotationItemInterface $quotationItem,SettingInterface $setting)
    {
        $this->quotation = $quotation;
        $this->quotationItem = $quotationItem;
        $this->setting = $setting;
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

//        return view('quotation::quotation.print');

         $data['quotation'] = $this->quotation->findAll($limit= 50);
         return view('quotation::quotation.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['edit'] = false;
        return view('quotation::quotation.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

         try{ 
            
            $quotationData = array(

                'ship_to' => $data['ship_to'],
                'address' => $data['address'],
                'type' => $data['type'],
                'quotation_no' => $data['quotation_no'],
                'expiration_date' => $data['expiration_date'],
                'ship_date' => $data['ship_date'],
                'ship_via' => $data['ship_via'],
                'sales_rep' => $data['sales_rep'],
                'payment_terms' => $data['payment_terms'],
                'internal_memo' => $data['internal_memo'],
                'accepted_by' => $data['accepted_by'],
                'accepted_date' => $data['accepted_date'],
                'status' => 'pending',
                'sub_total' => $data['sub_total'],
                'discount_percent' => $data['discount_percent'],
                'discount_amount' => $data['discount_amount'],
                'tax_amount' => $data['tax_amount'],
                'grand_total' => $grand_total = $data['grand_total'],
                'grand_total_in_words' => $this->quotation->numberTowords($grand_total),
                'date' => $data['date']

            ); 
            
            $quotationInfo = $this->quotation->save($quotationData);
            $quotation_id = $quotationInfo->id;
            $activity = $data['activity'];

            foreach($activity as $key => $value){

                $quotationItemData = array(
                    'quotation_id' => $quotation_id,
                    'activity' => $data['activity'][$key],
                    'description' => $data['description'][$key],
                    'qty' => $data['qty'][$key],
                    'price' => $data['price'][$key],
                    'amount' => $data['amount'][$key]
                );

                $this->quotationItem->save($quotationItemData);

            }

            alertify()->success('Quotation Created Successfully');
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }

        return redirect(route('quotation.index'));
     
    }

    public function appendQuotationItem(Request $request){
        if($request->ajax()){ 
                $data = view('quotation::quotation.partial.add-more-quotation-item')->render();
                return response()->json(['options'=>$data]);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $data['quotation']=$quotation=$this->quotation->find($id);
        $data['date']=date('d-n-Y',strtotime($quotation->created_at));
        $data['quotationField']=$quotation->getQuotationField;
        return view('quotation::quotation.view',$data)->render();
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data['edit'] = true;
        $data['quotation'] = $this->quotation->find($id);
        return view('quotation::quotation.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)   
    {
        $data = $request->all();
       
        try{

             $quotationData = array(

                'ship_to' => $data['ship_to'],
                'address' => $data['address'],
                'type' => $data['type'],
                'quotation_no' => $data['quotation_no'],
                'expiration_date' => $data['expiration_date'],
                'ship_date' => $data['ship_date'],
                'ship_via' => $data['ship_via'],
                'sales_rep' => $data['sales_rep'],
                'payment_terms' => $data['payment_terms'],
                'internal_memo' => $data['internal_memo'],
                'accepted_by' => $data['accepted_by'],
                'accepted_date' => $data['accepted_date'],
                'status' => 'pending',
                'sub_total' => $data['sub_total'],
                'discount_percent' => $data['discount_percent'],
                'discount_amount' => $data['discount_amount'],
                'tax_amount' => $data['tax_amount'],
                'grand_total' => $grand_total = $data['grand_total'],
                'grand_total_in_words' => $this->quotation->numberTowords($grand_total),
                'date' => $data['date']

            ); 

            $this->quotation->update($id,$quotationData);

             $activity = $data['activity'];

            foreach($activity as $key => $value){

                $quotation_item_id = $data['quotation_item_id'][$key];
               
                $quotationItemData = array(
                    'quotation_id' => $id,
                    'activity' => $data['activity'][$key],
                    'description' => $data['description'][$key],
                    'qty' => $data['qty'][$key],
                    'price' => $data['price'][$key],
                    'amount' => $data['amount'][$key]
                );

                if($quotation_item_id){
                    $this->quotationItem->update($quotation_item_id,$quotationItemData);
                }else{
                     $this->quotationItem->save($quotationItemData);
                }
            }


             alertify()->success('Quotation Updated Successfully');
        }catch(\Throwable $e){
           alertify($e->getMessage())->error();
        }
        
        return redirect(route('quotation.index'));
  
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $this->quotationItem->delete($id);
            $this->quotation->delete($id);
            
             alertify()->success('Quotation Deleted Successfully');
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }
      return redirect(route('quotation.index'));  
    }
  


    public function printQuotation($id){
        $data['quotation']=$quotation=$this->quotation->find($id);
        $data['date']=date('d-n-Y',strtotime($quotation->created_at));
        $data['quotationField']=$quotation->getQuotationField;
        $data['org_data']=$this->setting->getSetting();
        $data['org_status']=0;
    
        return view('quotation::quotation.print',$data);
    }

}
