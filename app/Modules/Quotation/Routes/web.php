<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {
    
     
       
        Route::get('quotation', ['as' => 'quotation.index', 'uses' => 'QuotationController@index']);
    
        Route::get('quotation/create', ['as' => 'quotation.create', 'uses' => 'QuotationController@create']);
        Route::post('quotation/store', ['as' => 'quotation.store', 'uses' => 'QuotationController@store']);
    
        Route::get('quotation/edit/{id}', ['as' => 'quotation.edit', 'uses' => 'QuotationController@edit'])->where('id','[0-9]+');
        Route::put('quotation/update/{id}', ['as' => 'quotation.update', 'uses' => 'QuotationController@update'])->where('id','[0-9]+');
    
        Route::get('quotation/delete/{id}', ['as' => 'quotation.delete', 'uses' => 'QuotationController@destroy'])->where('id','[0-9]+');
    
        Route::get('quotation/appendquotationItem', ['as' => 'quotation.appendquotationItem', 'uses' => 'QuotationController@appendQuotationItem']);





        Route::get('quotation/item', ['as' => 'quotation.item', 'uses' => 'QuotationController@item']);
        Route::post('quotation/itemStore', ['as' => 'quotation.itemStore', 'uses' => 'QuotationController@itemStore']);
       
        Route::get('quotation/editItem/{id}', ['as' => 'quotation.editItem', 'uses' => 'QuotationController@editItem'])->where('id','[0-9]+');
        Route::put('quotation/itemUpdate/{id}', ['as' => 'quotation.itemUpdate', 'uses' => 'QuotationController@itemUpdate'])->where('id','[0-9]+');

        Route::get('quotation/get-item', ['as' => 'quotation.add.item', 'uses' => 'QuotationController@addItem']);

        Route::get('quotation/show/{id}', ['as' => 'quotation.view', 'uses' => 'QuotationController@show'])->where('id','[0-9]+');

        Route::get('quotation/print/{id}', ['as' => 'quotation.print', 'uses' => 'QuotationController@printQuotation'])->where('id','[0-9]+');

});
