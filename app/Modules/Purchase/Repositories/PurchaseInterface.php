<?php

namespace App\Modules\Purchase\Repositories;

interface PurchaseInterface
{
    public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function getList();
    
    public function save($data);
    public function savePurchaseDetail($data);

    public function update($id,$data);

    public function delete($id);
    
    public function deleteDetail($id);
    
    public function NumberIntoWords($number);

    public function findPurchaseDetail($pid);

}