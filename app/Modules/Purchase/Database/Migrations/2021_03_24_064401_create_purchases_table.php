<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('vendor_id')->nullable();
            $table->integer('warehouse_id')->nullable();
            $table->double('sub_total',14,2)->nullable();
            $table->double('discount_percent',14,2)->nullable();
            $table->double('discount_amount',14,2)->nullable();
            $table->double('total_after_discount',14,2)->nullable();
            $table->double('vat_amount',14,2)->nullable();
            $table->double('grand_total',14,2)->nullable();
            $table->text('number_in_words')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
