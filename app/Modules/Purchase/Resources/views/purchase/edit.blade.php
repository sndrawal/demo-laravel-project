@extends('admin::layout')
@section('title')Purchase @stop 
@section('breadcrum')Edit Purchase @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Edit Purchase</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">

        {!! Form::model($purchase,['method'=>'PUT','route'=>['purchase.update',$purchase->id],'class'=>'form-horizontal','id'=>'purchase_submit','role'=>'form','files'=>true]) !!}
        	 @include('purchase::purchase.partial.action',['btnType'=>'Update']) 
        {!! Form::close() !!}
        
    </div>
</div>
<!-- /form inputs -->

@stop