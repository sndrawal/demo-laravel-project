<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_select2.js')}}"></script>
<script src="{{ asset('admin/validation/purchase.js')}}"></script>

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

    <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Vendor Name:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::select('vendor_id', $vendor, null, ['id'=>'vendor_id','placeholder'=>'--Select Vendor--','class'=>'form-control select-search']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
               <label class="col-form-label col-lg-3">Warehouse Name:</label>
               <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                {!! Form::select('warehouse_id', $warehouse, null, ['id'=>'warehouse_id','placeholder'=>'--Select Warehouse--','class'=>'form-control select-search']) !!}
                </div>
               </div>
            </div>
        </div>

    </div>


</fieldset>


<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold">Purchase Information</legend>


    @if($is_edit)
        @foreach($purchase->purchaseDetail as $key => $valcontent)

            <div class="appendMaterial">
                <div class="form-group row">
                    <div class="col-lg-2">
                        <div class="row">
                             <label class="col-form-label col-lg-3">Material:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    {!! Form::select('material_id[]', $material, $valcontent->material_id, ['id'=>'material_id','placeholder'=>'--Select Material--','class'=>'select-search form-control','required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="row">
                           <label class="col-form-label col-lg-3">Lots:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-cube4"></i></span>
                                    </span>
                                    {!! Form::text('lots[]', $valcontent->lots, ['id'=>'lots','placeholder'=>'Enter Lots','class'=>'lots form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="row">
                           <label class="col-form-label col-lg-3">Qty:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-balance"></i></span>
                                    </span>
                                    {!! Form::text('qty[]', $valcontent->qty, ['id'=>'qty','placeholder'=>'Enter Qty','class'=>'qty form-control numeric','required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="row">
                            <label class="col-form-label col-lg-3">Rate:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-coins"></i></span>
                                    </span>
                                    {!! Form::text('rate[]', $valcontent->rate, ['id'=>'rate','placeholder'=>'Enter Rate','class'=>'rate form-control numeric','required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="row">
                            <label class="col-form-label col-lg-3">Amount:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-coins"></i>
                                        </span>
                                    </span>
                                    {!! Form::text('amount[]', $valcontent->amount, ['id'=>'amount','placeholder'=>'Enter Amount','class'=>'amount form-control','readonly']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="col-lg-2">
                   <div class="row">
                           <button type="button" class="ml-2 remove_material btn bg-danger-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Remove</button>
                    </div>
                </div>

            </div>
        </div>


        @endforeach
    @endif

    <div class="appendMaterial">
        <div class="form-group row">
            <div class="col-lg-2">
                <div class="row">
                     <label class="col-form-label col-lg-3">Material:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            {!! Form::select('material_id[]', $material, null, ['id'=>'material_id','placeholder'=>'--Select Material--','class'=>'select-search form-control','required']) !!}
                        </div>
                    </div>
                </div>
            </div>

             <div class="col-lg-2">
                <div class="row">
                   <label class="col-form-label col-lg-3">Lot:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cube4"></i></span>
                            </span>
                            {!! Form::text('lots[]', null, ['id'=>'lots','placeholder'=>'Enter Lots','class'=>'lots form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>

             <div class="col-lg-2">
                <div class="row">
                   <label class="col-form-label col-lg-3">Qty:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-balance"></i></span>
                            </span>
                            {!! Form::text('qty[]', null, ['id'=>'qty','placeholder'=>'Enter Qty','class'=>'qty form-control numeric','required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="row">
                    <label class="col-form-label col-lg-3">Rate:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-coins"></i></span>
                            </span>
                            {!! Form::text('rate[]', null, ['id'=>'rate','placeholder'=>'Enter Rate','class'=>'rate form-control numeric','required']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="row">
                    <label class="col-form-label col-lg-3">Amount:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-coins"></i>
                                </span>
                            </span>
                            {!! Form::text('amount[]', null, ['id'=>'amount','placeholder'=>'Enter Amount','class'=>'amount form-control','readonly']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 float-right">
                 <div class="row">
                    <button type="button" class="add_material btn bg-success-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Add More Material</button>
                </div>
            </div>

    </div>
</div>

</fieldset>


 <fieldset class="mb-3 quotatoin_balance">
            <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
             @include('purchase::purchase.partial.purchase-balance') 
    </fieldset>


<div class="text-right">
     <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>{{ $btnType }}</button>
</div>



 <script type="text/javascript">
    $(document).ready(function(){ 
        $('.add_material').on('click',function(){
            $.ajax({
                    type: 'GET',
                    url: '/admin/purchase/appendMaterial',
                    success: function (data) {
                        $('.appendMaterial').last().append(data.options); 
                         $('.select-search').select2();
                         $('.numeric').keyup(function() {
                            if (this.value.match(/[^0-9.]/g)) {
                                this.value = this.value.replace(/[^0-9.]/g, '');
                            }
                        });
                    }
                });
        });

        $('.remove_material').on('click',function(){ 
            $(this).parent().parent().parent().remove();
        });


        $(document).on('keyup','.rate',function(){

            var qty = $(this).parent().parent().parent().parent().parent().find('.qty').val(); 
            var rate = $(this).val();

            var amount = qty * rate;
            $(this).parent().parent().parent().parent().parent().find('.amount').val(amount); 

            var arr = document.getElementsByClassName('amount');  
                 var tot=0;
                    for(var i=0;i<arr.length;i++){
                        if(parseInt(arr[i].value))
                            tot += parseInt(arr[i].value);
                    }
              
                var total_amount = tot;
                 $('.sub_total').val(total_amount.toFixed(2));

                 $('.discount_percent').val(0);
                 $('.discount_amount').val(0);

                 var discount_amount = 0;

                 var discounted_amount = total_amount - discount_amount;

                $('.total_after_discount').val(discounted_amount);

                 var vat_value = (13/100) * discounted_amount;
                 $('.vat_amount').val(vat_value.toFixed(2));


                 var grand_total = total_amount + vat_value;
                 $('.grand_total').val(grand_total.toFixed(2));


        });

    }); 
</script>