<table class="table table-striped">
    <thead>
        <tr class="bg-slate-400">
            <th>S.N.</th>
            <th>Material</th>
            <th>Lots</th>
            <th>Qty</th>
            <th>Rate</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>

       @foreach($purchaseDetail->purchaseDetail as $key => $value)

       <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ optional($value->materialInfo)->name }}</td>
            <td>{{ $value->lots }}</td>
            <td>{{ $value->qty }}</td>
            <td>Rs. {{ $value->rate}}</td>
            <td>Rs. {{ number_format($value->amount,2)}}</td>
        </tr>

    @endforeach
    <tr>
        <td colspan="4"></td>
        <td style="border-top: 1px solid red;"><b>Sub Total</b></td>
        <td style="border-top: 1px solid red;"><b>Rs. {{ number_format($purchaseDetail->sub_total,2)}}</b></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td><b>Discount(%)</b></td>
        <td><b>Rs. {{ number_format($purchaseDetail->discount_percent,2)}}</b></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td><b>Discount Amt(-)</b></td>
        <td><b>Rs. {{ number_format($purchaseDetail->discount_amount,2)}}</b></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td><b>Total After Discount</b></td>
        <td><b>Rs. {{ number_format($purchaseDetail->total_after_discount,2)}}</b></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td><b>VAT(+)</b></td>
        <td><b>Rs. {{ number_format($purchaseDetail->vat_amount,2)}}</b></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td><b>Grand Total</b></td>
        <td><b>Rs. {{ number_format($purchaseDetail->grand_total,2)}}</b></td>
    </tr>
    <tr>
        <td colspan="6"><b>Note : {{$purchaseDetail->number_in_words}}</b></td>
    </tr>

</tbody>

</table>
