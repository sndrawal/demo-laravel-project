@extends('admin::layout')
@section('title')Purchase @stop
@section('breadcrum')Purchase @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
@stop

@section('content') 

<div class="card card-body">
    <div class="d-flex justify-content-between">
        <h4>List Of Purchase</h4>
        <a href="{{ route('purchase.create') }}" class="btn bg-blue mb-2">
            <i class="icon-plus2"></i> Add Purchase
        </a>
    </div>


    <div class="table-responsive">
        <table class="table text-nowrap table-striped">
            <thead>
                <tr class="bg-slate-600">
                    <th>#</th>
                    <th>Vendor Name</th>
                    <th>Warehouse</th>
                    <th>Total Amount</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @if($purchase->total() != 0)
                @foreach($purchase as $key => $value)
                <tr>
                    <td>{{$purchase->firstItem() +$key}}</td>
                    <td>{{ optional($value->vendorInfo)->vendor_name }}</td>
                    <td>{{ optional($value->warehouseInfo)->warehouse_name }}</td>
                    <td>Rs.{{ number_format($value->grand_total) }}</td>
                    <td class="text-right">
                        <a href="#" class="list-icons-item" data-toggle="dropdown">
                            <i class="icon-more2"></i>
                        </a>

                       

                        <div class="dropdown-menu bd-card dropdown-menu-right">
                            <a class="dropdown-item" href="{{route('purchase.edit',$value->id)}}">
                                <i class="icon-pencil6"></i> Edit
                            </a>

                             <a data-toggle="modal" data-target="#modal_view_purchase" class="dropdown-item view_purchase" purchase_id = "{{$value->id}}"><i class="icon-eye"></i>View Detail</a>

                            <a data-toggle="modal" data-target="#delete-modal-purchase" class="delete-modal dropdown-item delete_purchase" link="{{route('purchase.delete',$value->id)}}">
                                <i class="icon-bin"></i> Delete
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">No Purchase Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>
    </div>
    <div class="col-12">
        <span class="float-right pagination align-self-end mt-3">
            {{ $purchase->appends(request()->all())->links() }}
        </span>
    </div>
</div>


 <!-- view modal -->
    <div id="modal_view_purchase" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h6 class="modal-title">View Purchase Detail</h6>
                </div>

                <div class="modal-body">
                    <div class="table-responsive result_view_detail">
                         
                    </div><!-- table-responsive -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn bg-teal-400" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- view modal -->

<!-- Warning modal -->
    <div id="delete-modal-purchase" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                 <div class="modal-body">
                    <center>
                        <i class="icon-alert text-danger icon-3x"></i>
                    </center>
                    <br>
                    <center>
                        <h2>Are You Sure Want To Delete ?</h2>
                        <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!-- /warning modal -->


<script type="text/javascript">
    $(document).ready(function(){

        $('.view_purchase').on('click',function(){

         var purchase_id = $(this).attr('purchase_id'); 
        
          $.ajax({
              url: "<?php echo route('purchase.get-purchase-detail-ajax') ?>",
              method: 'POST',
              data: {purchase_id:purchase_id, _token:"{{ csrf_token() }}"},
              success: function(data) {
                $(".result_view_detail").html('');
                $(".result_view_detail").html(data.options);
              }
          });

    });

          $('.delete_purchase').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });

    });
</script>


@endsection