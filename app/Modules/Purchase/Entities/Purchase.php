<?php

namespace App\Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Vendor\Entities\Vendor;
use App\Modules\Warehouse\Entities\Warehouse;
use App\Modules\Purchase\Entities\PurchaseDetail;


class Purchase extends Model
{

    protected $fillable = [

    	'vendor_id',
    	'warehouse_id',
    	'sub_total',
        'discount_percent',
        'discount_amount',
        'total_after_discount',
        'vat_amount', 
        'grand_total',
        'number_in_words',

    ];
    

    public function purchaseDetail()
    {
        return $this->hasMany(PurchaseDetail::class,'purchase_id','id');
    }

    public function vendorInfo(){
        return $this->belongsTo(vendor::class,'vendor_id','id');
    }

    public function warehouseInfo(){
        return $this->belongsTo(Warehouse::class,'warehouse_id','id');
    }
}
