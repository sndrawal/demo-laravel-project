<?php

namespace App\Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\RawMaterial\Entities\RawMaterial;

class PurchaseDetail extends Model
{

    protected $fillable = [

    	'purchase_id',
    	'material_id',
    	'qty',
    	'rate',
    	'amount',
    	'lots'

    ];
    
   	public function materialInfo(){
        return $this->belongsTo(RawMaterial::class,'material_id','id');
    }

}
