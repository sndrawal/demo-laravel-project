<?php

namespace App\Modules\Purchase\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Modules\Purchase\Repositories\PurchaseInterface;
use App\Modules\Vendor\Repositories\VendorInterface;
use App\Modules\RawMaterial\Repositories\RawMaterialInterface;
use App\Modules\Warehouse\Repositories\WarehouseInterface;
 
class PurchaseController extends Controller
{
    protected $purchase;
    protected $vendor;
    protected $raw_material;
    protected $warehouse;
    
    public function __construct(PurchaseInterface $purchase, VendorInterface $vendor, RawMaterialInterface $raw_material, WarehouseInterface $warehouse)
    {
        $this->purchase = $purchase;
        $this->vendor = $vendor;
        $this->raw_material = $raw_material;
        $this->warehouse = $warehouse;
    }
    

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $search = $request->all();
        $data['purchase'] = $this->purchase->findAll($limit= 50, $search);
        return view('purchase::purchase.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data['is_edit'] = false;
        $data['vendor'] = $this->vendor->getList();
        $data['warehouse'] = $this->warehouse->getList();
        $data['material'] = $this->raw_material->getList();

        return view('purchase::purchase.create', $data);
    }

    
    public function appendMaterial(Request $request){

         if($request->ajax()){
            $material = $this->raw_material->getList();
            $data = view('purchase::purchase.partial.add-more-material',compact('material'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $data = $request->all();  

        $amountInWords = $this->purchase->NumberIntoWords($data['grand_total']);

        try{ 
            $purchase_data = array(
                    'vendor_id' =>$data['vendor_id'],
                    'warehouse_id' =>$data['warehouse_id'],
                    'sub_total' =>$data['sub_total'],
                    'discount_percent' =>$data['discount_percent'],
                    'discount_amount' =>$data['discount_amount'],
                    'total_after_discount' =>$data['total_after_discount'],
                    'vat_amount' =>$data['vat_amount'],
                    'grand_total' =>$data['grand_total'],
                    'number_in_words' =>$amountInWords
            );

            $purchaseInfo = $this->purchase->save($purchase_data);
            $purchase_id = $purchaseInfo->id;

            $materials = $data['material_id'];
            $countmaterials = sizeof($materials);

                for($i = 0; $i < $countmaterials; $i++){
           
                     if($data['material_id'][$i]){
                         $purchaseDetaildata['purchase_id'] = $purchase_id;
                         $purchaseDetaildata['material_id'] = $data['material_id'][$i];
                         $purchaseDetaildata['qty'] = $data['qty'][$i];
                         $purchaseDetaildata['rate'] = $data['rate'][$i];
                         $purchaseDetaildata['amount'] = $data['amount'][$i];
                         $purchaseDetaildata['lots'] = $data['lots'][$i];

                         $this->purchase->savePurchaseDetail($purchaseDetaildata);
                     }
                         
                 }

            toastr()->success('Purchase Created Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('purchase.index'));

    }

    public function getPurchaseDetailAjax(Request $request){
         if($request->ajax()){

            $purchase_id = $request->purchase_id;
            $purchaseDetail = $this->purchase->find($purchase_id); 
            $data = view('purchase::purchase.partial.purchase-detail-ajax',compact('purchaseDetail'))->render();
            return response()->json(['options'=>$data]);
        }
    }
      

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('purchase::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $data['is_edit'] = true;
        $data['vendor'] = $this->vendor->getList();
        $data['warehouse'] = $this->warehouse->getList();
        $data['material'] = $this->raw_material->getList();
        $data['purchase'] = $this->purchase->find($id);

        return view('purchase::purchase.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();  

        $amountInWords = $this->purchase->NumberIntoWords($data['grand_total']);

        try{ 
            $purchase_data = array(
                    'vendor_id' =>$data['vendor_id'],
                    'warehouse_id' =>$data['warehouse_id'],
                    'sub_total' =>$data['sub_total'],
                    'discount_percent' =>$data['discount_percent'],
                    'discount_amount' =>$data['discount_amount'],
                    'total_after_discount' =>$data['total_after_discount'],
                    'vat_amount' =>$data['vat_amount'],
                    'grand_total' =>$data['grand_total'],
                    'number_in_words' =>$amountInWords
            );

            $purchaseInfo = $this->purchase->update($id,$purchase_data);
            $purchase_id = $id;

            $this->purchase->deleteDetail($id);

            $materials = $data['material_id'];
            $countmaterials = sizeof($materials);

                for($i = 0; $i < $countmaterials; $i++){
           
                     if($data['material_id'][$i]){
                         $purchaseDetaildata['purchase_id'] = $purchase_id;
                         $purchaseDetaildata['material_id'] = $data['material_id'][$i];
                         $purchaseDetaildata['qty'] = $data['qty'][$i];
                         $purchaseDetaildata['rate'] = $data['rate'][$i];
                         $purchaseDetaildata['amount'] = $data['amount'][$i];
                         $purchaseDetaildata['lots'] = $data['lots'][$i];

                         $this->purchase->savePurchaseDetail($purchaseDetaildata);
                     }
                         
                 }

            toastr()->success('Purchase Updated Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('purchase.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try{
            $this->purchase->delete($id);
            $this->purchase->deleteDetail($id);
            toastr()->success('Purchase Deleted Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        return redirect(route('purchase.index'));  
    }
}
