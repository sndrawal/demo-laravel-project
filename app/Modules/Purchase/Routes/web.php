<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {

        /*
        |--------------------------------------------------------------------------
        | Purchase  CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('purchase', ['as' => 'purchase.index', 'uses' => 'PurchaseController@index']);
        //Purchase Create
        Route::get('purchase/create', ['as' => 'purchase.create', 'uses' => 'PurchaseController@create']);
        Route::post('purchase/store', ['as' => 'purchase.store', 'uses' => 'PurchaseController@store']);
        //Purchase Edit
        Route::get('purchase/edit/{id}', ['as' => 'purchase.edit', 'uses' => 'PurchaseController@edit'])->where('id','[0-9]+');
        Route::put('purchase/update/{id}', ['as' => 'purchase.update', 'uses' => 'PurchaseController@update'])->where('id','[0-9]+');
        //Purchase Delete
        Route::get('purchase/delete/{id}', ['as' => 'purchase.delete', 'uses' => 'PurchaseController@destroy'])->where('id','[0-9]+');
        Route::get('purchase/appendMaterial', ['as' => 'purchase.appendMaterial', 'uses' => 'PurchaseController@appendMaterial']);

        Route::post('purchase/get-purchase-detail-ajax', ['as'=>'purchase.get-purchase-detail-ajax','uses'=>'PurchaseController@getPurchaseDetailAjax']);


});
