@extends('admin::layout')
@section('title')Warehouse @stop 
@section('breadcrum')Edit Warehouse @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Edit Warehouse</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">

        {!! Form::model($warehouse,['method'=>'PUT','route'=>['warehouse.update',$warehouse->id],'class'=>'form-horizontal','id'=>'warehouse_submit','role'=>'form','files'=>true]) !!} @include('warehouse::warehouse.partial.action',['btnType'=>'Update']) {!! Form::close() !!}
        
    </div>
</div>
<!-- /form inputs -->

@stop