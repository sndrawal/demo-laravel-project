@extends('admin::layout')
@section('title')Warehouse @stop
@section('breadcrum')Warehouse @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
@stop

@section('content') 

@include('warehouse::warehouse.partial.search')
 
 <div class="card card-body">
     <div class="d-flex justify-content-between mb-2">
        <h4>List Of Warehouse</h4>
         <div class="button-group"> 
             <a href="{{ route('warehouse.create') }}" class="btn bg-success-600 btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-pen-plus"></i></b> Add Warehouse</a>
        </div>
    </div> 


    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="bg-slate">
                    <th>#</th>
                    <th>Warehouse Name</th>
                    <th>Location</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($warehouse->total() != 0)
                @foreach($warehouse as $key => $value)

                <tr>
                    <td>{{ ++$key}}</td>
                    <td>{{ $value->warehouse_name }}</td>
                    <td>{{ $value->location }}</td>
                    <td>{{ optional($value->warehouseType)->dropvalue }}</td>

                    <td>
                        <a class="btn bg-info btn-icon rounded-round" href="{{route('warehouse.edit',$value->id)}}" data-popup="tooltip" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil6"></i></a>

                        <a data-toggle="modal" data-target="#modal_theme_warning" class="btn bg-danger btn-icon rounded-round delete_warehouse" link="{{route('warehouse.delete',$value->id)}}" data-popup="tooltip" data-placement="bottom" data-original-title="Delete"><i class="icon-bin"></i></a>

                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">No Warehouse Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>

        <span style="margin: 5px;float: right;">
            @if($warehouse->total() != 0)
                {{ $warehouse->links() }}
            @endif
            </span>
    </div>
</div>

 <!-- Warning modal -->
    <div id="modal_theme_warning" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                 <div class="modal-body">
                    <center>
                        <i class="icon-alert text-danger icon-3x"></i>
                    </center>
                    <br>
                    <center>
                        <h2>Are You Sure Want To Delete ?</h2>
                        <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!-- /warning modal -->

<script type="text/javascript">
    $('document').ready(function() {
        $('.delete_warehouse').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });
    });
</script>

@endsection