<div class="card card-body">
    {!! Form::open(['route' => 'warehouse.index', 'method' => 'get']) !!}
    <div class="row">
 
         <div class="col-md-3">
            <label class="d-block font-weight-semibold">Warehouse Name:</label>
            <div class="input-group">
                {!! Form::select('warehouse_name[]', $warehouse_name, request('site_assigned') ?? null, ['class'=>'form-control multiselect-filtering', 'multiple']) !!}
            </div>
        </div>

         <div class="col-md-3">
            <label class="d-block font-weight-semibold">Location:</label>
            <div class="input-group">
                {!! Form::select('location_name[]', $location_name, request('site_assigned') ?? null, ['class'=>'form-control multiselect-filtering', 'multiple']) !!}
            </div>
        </div>

         <div class="col-md-3">
            <label class="d-block font-weight-semibold">Type:</label>
            <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-dots"></i></span>
                </span>
                {!! Form::select('type',$warehouse_type, $value = null, ['id'=>'type','placeholder'=>'Select Type','class'=>'form-control select-search']) !!}
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-end mt-2">
        <button class="btn bg-primary" type="submit">
            Search Now
        </button>
        <a href="{{ route('warehouse.index') }}" data-popup="tooltip" data-placement="top" data-original-title="Refresh Search" class="btn bg-danger ml-2">
            <i class="icon-spinner9"></i>
        </a>
    </div>
    {!! Form::close() !!}
</div>
