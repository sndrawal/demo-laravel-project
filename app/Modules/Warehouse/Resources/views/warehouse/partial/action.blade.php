<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/validation/warehouse.js') }}"></script>

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>


    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
               <label class="col-form-label col-lg-4">Warehouse Name:<span class="text-danger">*</span></label>
                    <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-home7"></i>
                                </span>
                            </span>
                            {!! Form::text('warehouse_name', $value = null, ['id'=>'warehouse_name','placeholder'=>'Enter Warehoue Name','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-form-label col-lg-4">Location:<span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                       <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-location3"></i></span>
                        </span>
                            {!! Form::text('location', $value = null, ['id'=>'location','placeholder'=>'Enter Location','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
               <label class="col-form-label col-lg-4">Type:</label>
                    <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-dots"></i>
                                </span>
                            </span>
                        {!! Form::select('type',$warehouse_type, $value = null, ['id'=>'type','placeholder'=>'Select Type','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>


</fieldset>


<div class="text-right">
    <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b> {{ $btnType }}</button>
</div>
