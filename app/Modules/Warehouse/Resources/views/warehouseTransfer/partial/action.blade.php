<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<style>
    .select2-container {
        width: 80% !important
    }
</style> 
<fieldset class="mb-3">
            <legend class="text-uppercase font-size-sm font-weight-bold"></legend>


            <div class="row">

            <div class="col-md-4"> 
                <div class="form-group">
                    <label>Category Name:<span class="text-danger">*</span></label>
                    <div class="input-group">
                             <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-tree7"></i></span>
                        </span>
                     {!! Form::select('category_id',$category, $value = null, ['id'=>'category_id','placeholder'=>'Select Category','class'=>'form-control select-search']) !!}

                    </div>
                </div>
            </div>

            <div class="col-md-4"> 
                <div class="form-group">
                    <label>Sub Category Name:</label>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-tree7"></i></span>
                        </span>
                    @if($is_edit)
                        {!! Form::select('sub_id',$subcat, $value = null, ['id'=>'sub_id','placeholder'=>'Select Category','class'=>'form-control select-search']) !!}
                    @else
                        {!! Form::select('sub_id', [''=>'Select Subcategory'],null,['id'=>'sub_id','class'=>'form-control select-search']) !!}
                    @endif

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label>Product Name:<span class="text-danger">*</span></label>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-tree7"></i></span>
                        </span>
                    @if($is_edit)
                        {!! Form::select('product_id',$product, $value = null, ['id'=>'product_id','placeholder'=>'Select Product','class'=>'form-control select-search']) !!}
                    @else
                        {!! Form::select('product_id', [''=>'Select Product'],null,['id'=>'product_id','class'=>'form-control select-search']) !!}
                    @endif

                    </div>
                </div>
            </div>

        <div class="col-md-12 mb-4 product_stock">
                
        </div>
    </div>

     <div class="row product_transfer_section">
            
        </div>

    </fieldset>


    <div class="text-right">
        
        <button type="submit" class="btn btn-warning btn-labeled btn-labeled-left mr-2"><b><i class="icon-database-insert"></i></b> Transfer</button>

    </div>



    <script type="text/javascript">
    $(document).ready(function(){

    $("select[name='category_id']").change(function(){
      var category_id = $(this).val();
      var token = $("input[name='_token']").val();
      $.ajax({
          url: "<?php echo route('select-ajax') ?>",
          method: 'POST',
          data: {category_id:category_id, _token:token},
          success: function(data) {
            $("select[name='sub_id'").html('');
            $("select[name='sub_id'").html(data.options);
          }
      });
    });


    $('#category_id, #sub_id').on('change',function(){
        var category_id = $('#category_id').val();
        var sub_cat_id = $('#sub_id').val();

        if(sub_cat_id == ''){
            var cat_id = category_id;
        }else{
            var cat_id = sub_cat_id;   
        }

        var token = $("input[name='_token']").val();
          $.ajax({
              url: "<?php echo route('select-product-ajax') ?>",
              method: 'POST',
              data: {cat_id:cat_id, _token:token},
              success: function(data) {
                $("select[name='product_id'").html('');
                $("select[name='product_id'").html(data.options);
              }
          });


    });

    $('#product_id').on('change',function(){

         var product_id = $('#product_id').val();
        
         var token = $("input[name='_token']").val();
          $.ajax({
              url: "<?php echo route('product-detail-ajax') ?>",
              method: 'POST',
              data: {product_id:product_id, _token:token},
              success: function(data) {
                $(".product_stock").html('');
                $(".product_stock").html(data.options);
              }
          });

    });


});
</script>

 <script type="text/javascript">
    
    $('.select-search').select2();
</script>