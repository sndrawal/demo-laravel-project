<div class="col-md-3">
                <div class="form-group">
                    <label>From Warehouse:</label>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-balance"></i></span>
                        </span>

                        {!! Form::text('warehouse_from_name', $value = $transfer_product_info->warehouseName->warehouse_name, ['id'=>'warehouse_from','class'=>'form-control','readonly'=>'readonly']) !!}

                         {{ Form::hidden('purchased', $transfer_product_info->id, array('class' => 'purchaseId')) }}
                         
                         {{ Form::hidden('warehouse_from', $transfer_product_info->warehouse_id, array('class' => 'warehouse_from')) }}

                         {{ Form::hidden('original_cost', $transfer_product_info->selling_cost, array('class' => 'original_cost')) }}

                         {{ Form::hidden('transfer_lot', $transfer_product_info->lots, array('class' => 'transfer_lot')) }}

                         {{ Form::hidden('expiry_date', $transfer_product_info->expiry_date, array('class' => 'expiry_date')) }}

                         {{ Form::hidden('stock_in_hand', $transfer_product_info->stock_qty, array('class' => 'stock_in_hand')) }}

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>To Warehouse:<span class="text-danger">*</span></label>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-pencil-ruler"></i></span>
                        </span>

                       {!! Form::select('warehouse_to',$warehouse, $value = null, ['id'=>'warehouse_to','placeholder'=>'Select Warehouse','class'=>'form-control']) !!}


                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Qty:<span class="text-danger">*</span></label>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-pencil-ruler"></i></span>
                        </span>

                        {!! Form::text('transfer_qty', $value = Null, ['id'=>'transfer_qty','placeholder'=>'Enter Transfer Qty','class'=>'form-control numeric','required']) !!}

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Selling Price:<span class="text-danger">*</span></label>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-coins"></i></span>
                        </span>

                        {!! Form::text('latest_cost', $value = $transfer_product_info->selling_cost, ['id'=>'latest_cost','placeholde'=>'Enter Selling Cost','class'=>'form-control numeric','required']) !!}

                    </div>
                </div>
            </div>



<!-- Warning modal -->
<div id="modal_folder_add" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card-body text-center">
                    <i class="icon-alert icon-2x text-danger-400 border-danger-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
                    <h5 class="card-title">Alert !!!</h5>
                    <p class="mb-3">You have excess Qty more than Available Stock. Please Recheck and Enter Qty. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /warning modal -->



<script type="text/javascript">

    $(document).ready(function(){
      $('#transfer_qty').keyup(function(){
        
        var purchase_id = $('.purchaseId').val();
        var transfer_qty = $('#transfer_qty').val();

        var token = $("input[name='_token']").val();
          $.ajax({
              url: "<?php echo route('check-stock-qty-ajax') ?>",
              method: 'POST',
              data: {purchase_id:purchase_id, qty:transfer_qty, _token:token},
              success: function(data) {
                if(data == 1){
                    $('#modal_folder_add').modal('show');
                    $('#transfer_qty').val('');
                 }
              }
          });

    });
});
</script>
