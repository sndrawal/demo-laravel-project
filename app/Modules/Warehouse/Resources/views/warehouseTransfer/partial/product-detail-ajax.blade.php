<fieldset>
    <legend class="border-bottom-slate font-weight-black text-danger">Available Stock To Transfer [ Choose Any One ]</legend>

    <table class='table table-striped mb30' id='table1' cellspacing='0' width='100%'>
     <tbody>
        <tr>
            <th>#</th>
            <th>Product Name</th>
            <th>Warehouse Name</th>
            <th>Selling Cost</th>
            <th>Lots</th>
            <th>Qty</th>
            <th>Expiry Date</th>
        </tr>
        
        @if(sizeof($purchase_product_info) > 0)
        @foreach($purchase_product_info as $key => $value) 
        <tr>
            <td><input type="radio" class="purchase_id" name="purchase_id" value="{{$value->id}}"></td>
            <td>{{ $value->productName->product_name }}</td>
            <td>{{ $value->warehouseName->warehouse_name }}
            <input type="hidden" class="warehouse_id" name="warehouse_id" value="{{$value->warehouse_id}}">
          </td>
            <td>Rs.{{ number_format($value->selling_cost,2) }} /-</td>
            <td>{{$value->lots}}</td>
            <td>{{$value->stock_qty.' '.$value->categoryName->category_unit}}</td>
            <td>{{$value-> expiry_date }}</td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="5">No Purchase Stock Available !!!</td>
        </tr>
        @endif
    </tbody>
</table>

</fieldset>



<script type="text/javascript">

    $(document).ready(function(){
     $('.purchase_id').on('click',function(){

        var purchase_id = $(this).val();

        var token = $("input[name='_token']").val();
          $.ajax({
              url: "<?php echo route('category-transfer-ajax') ?>",
              method: 'POST',
              data: {purchase_id:purchase_id, _token:token},
              success: function(data) {
                $(".product_transfer_section").html('');
                $(".product_transfer_section").html(data.options);
              }
          });

    });
});
</script>