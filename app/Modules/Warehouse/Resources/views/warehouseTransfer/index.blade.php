@extends('admin::layout')
@section('title')Warehouse Category Transfer @stop
@section('breadcrum')Warehouse Category Transfer @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
@stop

@section('content') 

<div class="card">
    <div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">
        <a href="{{ route('warehouseTransfer.create') }}" class="btn bg-teal-400"><i class="icon-plus-circle2"></i> Transfer Warehouse Category </a>
    </div>
</div>


<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">List of Warehouse Category Transfer</h5>

    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="bg-slate">
                    <th>#</th>
                    <th>Product Name</th>
                    <th>From Warehouse</th>
                    <th>To Warehouse</th>
                    <th>Transfer Qty</th>
                </tr>
            </thead>
            <tbody>
                @if($warehouseTransfer->total() != 0)
                @foreach($warehouseTransfer as $key => $value)

                <tr>
                   <td>{{$warehouseTransfer->firstItem() +$key}}</td>
                    <td>{{ $value->purchaseInfo->productName->product_name }}</td>
                    <td>{{ $value->warehouseFrom->warehouse_name }}</td>
                    <td>{{ $value->warehouseTo->warehouse_name }}</td>
                    <td>{{ $value->transfer_qty.' '.$value->purchaseInfo->categoryName->category_unit  }}</td>

                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">No Warehouse Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>
        <span style="margin: 5px;float: right;">
            @if($warehouseTransfer->total() != 0)
                {{ $warehouseTransfer->links() }}
            @endif
            </span>
    </div>
</div>


<!-- Warning modal -->
    <div id="modal_product_sale_info" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h6 class="modal-title">View Transfer Product Items</h6>
                </div>

                <div class="modal-body">
                    <div class="table-responsive result_view_detail">
                         
                    </div><!-- table-responsive -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn bg-teal-400" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- warning modal -->

@endsection