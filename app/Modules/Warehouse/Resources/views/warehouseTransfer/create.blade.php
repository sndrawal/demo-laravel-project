@extends('admin::layout')
@section('title')Transfer Warehouse Category @stop 
@section('breadcrum')Transfer Warehouse Category @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<script src="{{ asset('admin/validation/warehouse_category.js') }}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>

<!-- /theme JS files -->

@stop @section('content')
<style>
    .select2-container {
        width: 70% !important
    }
</style>  
<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Transfer Warehouse Category</h5>
        <div class="header-elements">

        </div>
    </div>
    

    <div class="card-body">

        {!! Form::open(['route'=>'warehouseTransfer.store','method'=>'POST','class'=>'form-horizontal','id'=>'warehouse_transfer_submit','role'=>'form','files' => true]) !!}
        
            @include('warehouse::warehouseTransfer.partial.action',['btnType'=>'Save']) 
        
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@stop

 <script type="text/javascript">
    
    $('.select-search').select2();
</script>