<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseCategoryTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_category_transfers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('purchase_id')->nullable();
            $table->integer('warehouse_from')->nullable();
            $table->integer('warehouse_to')->nullable();
            $table->double('original_cost',14,2)->nullable();
            $table->double('latest_cost',14,2)->nullable();
            $table->integer('transfer_qty')->nullable();
            $table->string('transfer_lot')->nullable();
            $table->date('expiry_date')->nullable();
            $table->integer('transfer_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_category_transfers');
    }
}
