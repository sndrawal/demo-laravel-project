<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {

        /*
        |--------------------------------------------------------------------------
        | warehouse  CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('warehouse', ['as' => 'warehouse.index', 'uses' => 'WarehouseController@index']);
        //warehouse Create
        Route::get('warehouse/create', ['as' => 'warehouse.create', 'uses' => 'WarehouseController@create']);
        Route::post('warehouse/store', ['as' => 'warehouse.store', 'uses' => 'WarehouseController@store']);
        //warehouse Edit
        Route::get('warehouse/edit/{id}', ['as' => 'warehouse.edit', 'uses' => 'WarehouseController@edit'])->where('id','[0-9]+');
        Route::put('warehouse/update/{id}', ['as' => 'warehouse.update', 'uses' => 'WarehouseController@update'])->where('id','[0-9]+');
        //warehouse Delete
        Route::get('warehouse/delete/{id}', ['as' => 'warehouse.delete', 'uses' => 'WarehouseController@destroy'])->where('id','[0-9]+');


        /*
        |--------------------------------------------------------------------------
        | warehouse Transfer CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('warehouseTransfer', ['as' => 'warehouseTransfer.index', 'uses' => 'WarehouseCategoryTransferController@index']);
        //warehouse Create
        Route::get('warehouseTransfer/create', ['as' => 'warehouseTransfer.create', 'uses' => 'WarehouseCategoryTransferController@create']);
        Route::post('warehouseTransfer/store', ['as' => 'warehouseTransfer.store', 'uses' => 'WarehouseCategoryTransferController@store']);
        //warehouse Edit
        Route::get('warehouseTransfer/edit/{id}', ['as' => 'warehouseTransfer.edit', 'uses' => 'WarehouseCategoryTransferController@edit'])->where('id','[0-9]+');
        Route::put('warehouseTransfer/update/{id}', ['as' => 'warehouseTransfer.update', 'uses' => 'WarehouseCategoryTransferController@update'])->where('id','[0-9]+');
        //warehouse Delete
        Route::get('warehouseTransfer/delete/{id}', ['as' => 'warehouseTransfer.delete', 'uses' => 'WarehouseCategoryTransferController@destroy'])->where('id','[0-9]+');


        Route::post('product-detail-ajax', ['as'=>'product-detail-ajax','uses'=>'WarehouseCategoryTransferController@productDetailAjax']);
        Route::post('category-transfer-ajax', ['as'=>'category-transfer-ajax','uses'=>'WarehouseCategoryTransferController@categoryTransferAjax']);




});
