<?php

namespace App\Modules\Warehouse\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Modules\Warehouse\Repositories\WarehouseInterface;
use App\Modules\Dropdown\Repositories\DropdownInterface;


class WarehouseController extends Controller
{
    protected $warehouse;
    
    public function __construct(WarehouseInterface $warehouse, DropdownInterface $dropdown)
    {
        $this->warehouse = $warehouse;
        $this->dropdown = $dropdown;
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $search = $request->all();
        $data['warehouse'] = $this->warehouse->findAll($limit= 50,$search);
        $data['warehouse_name'] = $this->warehouse->getUniqueWarehouse();
        $data['location_name'] = $this->warehouse->getUniqueLocation();
        $data['warehouse_type'] = $this->dropdown->getFieldBySlug('warehouse_type');

        return view('warehouse::warehouse.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['warehouse_type'] = $this->dropdown->getFieldBySlug('warehouse_type');
        return view('warehouse::warehouse.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
         try{
            $this->warehouse->save($data);
            toastr()->success('Warehouse Created Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('warehouse.index'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data['warehouse_type'] = $this->dropdown->getFieldBySlug('warehouse_type');
        $data['warehouse'] = $this->warehouse->find($id);
        return view('warehouse::warehouse.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        try{
            $this->warehouse->update($id,$data);
             toastr()->success('Warehouse Updated Successfully');
        }catch(\Throwable $e){
           toastr()->error($e->getMessage());
        }
        
        return redirect(route('warehouse.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $this->warehouse->delete($id);
             toastr()->success('Warehouse Deleted Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
      return redirect(route('warehouse.index'));  
    }
}
