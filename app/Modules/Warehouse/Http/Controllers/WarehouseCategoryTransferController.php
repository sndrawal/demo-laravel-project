<?php

namespace App\Modules\Warehouse\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Modules\Warehouse\Repositories\WarehouseInterface;
use App\Modules\Warehouse\Repositories\WarehouseCategoryTransferInterface;
use App\Modules\Category\Repositories\CategoryInterface;
use App\Modules\Purchase\Repositories\PurchaseInterface;

use Illuminate\Support\Facades\Auth;

class WarehouseCategoryTransferController extends Controller
{

    protected $warehouse;
    protected $warehouseTransfer;
    protected $category;
    protected $purchase;
    
    public function __construct(WarehouseInterface $warehouse, WarehouseCategoryTransferInterface $warehouseTransfer,CategoryInterface $category,PurchaseInterface $purchase)
    {
        $this->warehouse = $warehouse;
        $this->warehouseTransfer = $warehouseTransfer;
        $this->category = $category;
        $this->purchase = $purchase;
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $search = $request->all();
        $data['warehouseTransfer'] = $this->warehouseTransfer->findAll($limit= 50,$search);
        return view('warehouse::warehouseTransfer.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['is_edit'] = false;
        $data['category'] = $this->category->getMainCat('category_name', 'id');
        $data['warehouse'] = $this->warehouse->getList('warehouse_name', 'id');
        return view('warehouse::warehouseTransfer.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all(); 

        $userInfo = Auth::user();
        $transfer_by = $userInfo->id;

        $purchase_id = $data['purchase_id'];
        $warehouse_from = $data['warehouse_from'];
        $warehouse_to = $data['warehouse_to'];
        $original_cost = $data['original_cost'];
        $latest_cost = $data['latest_cost'];
        $transfer_qty = $data['transfer_qty'];
        $transfer_lot = $data['transfer_lot'];
        $expiry_date = $data['expiry_date'];
        $transfer_qty = $data['transfer_qty'];
        $stock_in_hand = $data['stock_in_hand'];
        
         try{

            /* --------------------------------------------------
            Storing Stock Transfer from One Warehouse to Another
            -----------------------------------------------------*/
            $transfer_data =array(

                'purchase_id' =>$purchase_id,
                'warehouse_from' =>$warehouse_from,
                'warehouse_to' =>$warehouse_to,
                'original_cost' =>$original_cost,
                'latest_cost' =>$latest_cost,
                'transfer_qty' =>$transfer_qty,
                'transfer_lot' =>$transfer_lot,
                'expiry_date' =>$expiry_date,
                'transfer_by' => $transfer_by,
            );

            $this->warehouseTransfer->save($transfer_data);

            $remaining_qty = $stock_in_hand - $transfer_qty;

            /* --------------------------------------------------
            Once the Stock is Transfer, We need to deduce from Purchase 
            -----------------------------------------------------*/

            $purchase_update = array(
                'stock_qty' => $remaining_qty
            );
            $this->purchase->update($purchase_id,$purchase_update);

            /* --------------------------------------------------
            Again, Once the Stock is Transfer, We need to add new Purchase for New product and Cost
            -----------------------------------------------------*/
            $purchaseInfo = $this->purchase->find($purchase_id);

            $supplier_id = $purchaseInfo->supplier_id;
            $category_id = $purchaseInfo->category_id;
            $product_id  = $purchaseInfo->product_id ;
            $warehouse_id = $warehouse_to;
            $selling_cost  = $latest_cost;
            $total_qty = $transfer_qty;
            $stock_qty = $transfer_qty;
            $lots = $transfer_lot;
            $expiry_date = $expiry_date;
            $created_by = $transfer_by;

        
            $purchase_add = array(

                'supplier_id' => $supplier_id,
                'category_id' => $category_id,
                'product_id' => $product_id,
                'warehouse_id' => $warehouse_id,
                'selling_cost' => $selling_cost,
                'total_qty' => $total_qty,
                'stock_qty' => $stock_qty,
                'lots' => $lots,
                'expiry_date' => $expiry_date,
                'created_by' => $created_by,

            );
            $this->purchase->save($purchase_add);

            alertify()->success('Successfully Warehouse Product item Transfer');
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }
        
        return redirect(route('warehouseTransfer.index'));
    }

   
   public function productDetailAjax(Request $request){ 
        if($request->ajax()){
            $purchase_product_info =$this->purchase->getByproductId($request->product_id);  
            $data = view('warehouse::warehouseTransfer.partial.product-detail-ajax',compact('purchase_product_info'))->render();
            return response()->json(['options'=>$data]);
        }
   }

   public function categoryTransferAjax(Request $request){
        if($request->ajax()){
            $transfer_product_info =$this->purchase->find($request->purchase_id);
            $warehouse = $this->warehouse->getList('warehouse_name', 'id');
            $data = view('warehouse::warehouseTransfer.partial.stock-transfer-section-ajax',compact('transfer_product_info','warehouse'))->render();
            return response()->json(['options'=>$data]);
        }
   }

}
