<?php 
namespace App\Modules\Warehouse\Repositories;

use App\Modules\Warehouse\Entities\Warehouse;

class WarehouseRepository implements WarehouseInterface
{
    
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result =Warehouse::when(array_keys($filter, true), function ($query) use ($filter) {
            if (isset($filter['warehouse_name']) && !is_null($filter['warehouse_name'])) {
                $query->whereIn('warehouse_name', $filter['warehouse_name']);
            }
            if (isset($filter['location_name']) && !is_null($filter['location_name'])) {
                $query->whereIn('location', $filter['location_name']);
            }
             if (isset($filter['type'])) {
                $query->where('type', '=', $filter['type']);
            }
        })
            
            ->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 
        
    }
    
    public function find($id){
        return Warehouse::find($id);
    }
    
   public function getList(){  
       $vendorType = Warehouse::pluck('warehouse_name', 'id');
      
       return $vendorType;
   }
    
    public function save($data){
        return Warehouse::create($data);
    }
    
    public function update($id,$data){
        $vendor = Warehouse::find($id);
        return $vendor->update($data);
    }
    
    public function delete($id){
        $bound = Warehouse::find($id);
        return $bound->delete();
    }
    
    public function countTotal(){
        return Warehouse::count();
    }

    public function getUniqueWarehouse(){
        return Warehouse::select('warehouse_name')
        ->groupBy('warehouse_name')
        ->pluck('warehouse_name', 'warehouse_name')
        ->filter(function($value, $key) {
            return  $value != null;
        });
    }

    public function getUniqueLocation(){
        return Warehouse::select('location')
        ->groupBy('location')
        ->pluck('location', 'location')
        ->filter(function($value, $key) {
            return  $value != null;
        });    
    }

}