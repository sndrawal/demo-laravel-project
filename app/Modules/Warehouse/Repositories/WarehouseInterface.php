<?php

namespace App\Modules\Warehouse\Repositories;

interface WarehouseInterface
{
    public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function getList();
    
    public function save($data);

    public function update($id,$data);

    public function delete($id);
    
    public function countTotal();

    public function getUniqueWarehouse();

    public function getUniqueLocation();
    

}