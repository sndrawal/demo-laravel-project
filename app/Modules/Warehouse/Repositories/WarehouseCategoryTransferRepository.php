<?php 
namespace App\Modules\Warehouse\Repositories;

use App\Modules\Warehouse\Entities\WarehouseCategoryTransfer;

class WarehouseCategoryTransferRepository implements WarehouseCategoryTransferInterface
{
    
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result =WarehouseCategoryTransfer::when(array_keys($filter, true), function ($query) use ($filter) {
             if (isset($filter['vendor_type_id'])) {
                $query->where('vendor_type_id', '=', $filter['vendor_type_id']);
            }
            if (isset($filter['search_value'])) {
                $query->where('vendor_name', 'like', '%' . $filter['search_value'] . '%');
            }
        })
            
            ->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 
        
    }
    
    public function find($id){
        return WarehouseCategoryTransfer::find($id);
    }
    
   public function getList(){  
       $vendorType = WarehouseCategoryTransfer::pluck('warehouse_name', 'id');
      
       return $vendorType;
   }
    
    public function save($data){
        return WarehouseCategoryTransfer::create($data);
    }
    
    public function update($id,$data){
        $warehouse = WarehouseCategoryTransfer::find($id);
        return $warehouse->update($data);
    }
    
    public function delete($id){
        $bound = WarehouseCategoryTransfer::find($id);
        return $bound->delete();
    }
    
    public function countTotal(){
        return WarehouseCategoryTransfer::count();
    }

}