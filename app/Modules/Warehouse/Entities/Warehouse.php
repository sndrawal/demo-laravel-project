<?php

namespace App\Modules\Warehouse\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Dropdown\Entities\Dropdown;

class Warehouse extends Model
{
    protected $fillable = [

    	'warehouse_name',
    	'location',
    	'type'

    ];

  public function warehouseType(){
        return $this->belongsTo(Dropdown::class,'type');
    }
}

