<?php

namespace App\Modules\Warehouse\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Purchase\Entities\Purchase;
use App\Modules\Warehouse\Entities\Warehouse;

class WarehouseCategoryTransfer extends Model
{
    protected $fillable = [

    	'purchase_id',
    	'warehouse_from',
    	'warehouse_to',
    	'original_cost',
    	'latest_cost',
    	'transfer_qty',
    	'transfer_lot',
    	'expiry_date',
        'transfer_by'
    ];

    public function purchaseInfo(){ 
        return $this->belongsTo(Purchase::class,'purchase_id','id');
    }

    public function warehouseFrom(){
        return $this->belongsTo(Warehouse::class,'warehouse_from','id');
    }
    public function warehouseTo(){
        return $this->belongsTo(Warehouse::class,'warehouse_to','id');
    }


}