<!-- Secondary navbar -->
@inject('menuRoles', '\App\Modules\User\Services\CheckUserRoles')
@php
$currentRoute = Request::route()->getName();
$Route = explode('.',$currentRoute);
@endphp
<div class="navbar navbar-expand-md navbar-dark border-top-0"
@if($setting != null) style="background-color: {{$setting->secondary_navbar_color}};" @endif>
<div class="d-md-none w-100">
    <button type="button" class="navbar-toggler d-flex align-items-center w-100" data-toggle="collapse"
    data-target="#navbar-navigation">
    <i class="icon-menu-open mr-2"></i>
    Main navigation
</button>
</div>

    <div class="navbar-collapse collapse" id="navbar-navigation">
        <ul class="navbar-nav navbar-nav-highlight">
            @if($menuRoles->assignedRoles('dashboard'))
                <li class="nav-item">
                    <a href="{{route('dashboard')}}" class="navbar-nav-link @if($Route[0]=='dashboard') active @endif"> Dashboard</a>
                </li>
            @endif

            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    General
                </a>

                <div class="dropdown-menu">
                    <div class="dropdown-header">Employee</div>
                    @if($menuRoles->assignedRoles('employment.index'))
                    <a href="{{route('employment.index')}}" class="dropdown-item @if($Route[0]=='employment') active @endif"><i class="icon-users"></i> Employee Management</a>
                    @endif
                    @if($menuRoles->assignedRoles('employee.list'))
                    <a href="{{route('employee.list')}}" class="dropdown-item @if($Route[0]=='employee') active @endif"><i class="icon-address-book2"></i> Employee Directory</a>
                    @endif
                </div>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Configuration
                </a>

                <div class="dropdown-menu">
                    <div class="dropdown-header">Setting</div>
                    @if($menuRoles->assignedRoles('role.index'))
                    <a href="{{route('role.index')}}" class="dropdown-item @if($Route[0]=='role') active @endif"><i class="icon-user-lock"></i> Role Management</a>
                    @endif
                    @if($menuRoles->assignedRoles('dropdown.index'))
                    <a href="{{route('dropdown.index')}}" class="dropdown-item @if($Route[0]=='dropdown') active @endif"><i class="icon-move-vertical"></i> Dropdown Management</a>
                    @endif
                    @if($menuRoles->assignedRoles('setting.index'))
                    <a href="{{route('setting.index')}}" class="dropdown-item @if($Route[0]=='setting') active @endif"><i class="icon-cogs"></i> Setting</a>
                    @endif
                </div>
            </li>

            @if($menuRoles->assignedRoles('dailyClient.index') OR $menuRoles->assignedRoles('hotLead.index') OR $menuRoles->assignedRoles('coldLead.index') OR $menuRoles->assignedRoles('salesPipeline.index') OR $menuRoles->assignedRoles('convertedLead.index') OR $menuRoles->assignedRoles('rejectedLead.index'))
                <li class="nav-item">
                    <a href="{{route('dailyClient.index')}}" class="navbar-nav-link @if($Route[0]=='dailyClient') active @endif">Lead</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('contact'))
                <li class="nav-item">
                    <a href="{{route('contact.index')}}" class="navbar-nav-link @if($Route[0]=='contact') active @endif">Contact</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('agreement'))
                <li class="nav-item">
                    <a href="{{route('agreement.index')}}" class="navbar-nav-link @if($Route[0]=='agreement') active @endif">Agreeement</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('vendor'))
                <li class="nav-item">
                    <a href="{{route('vendor.index')}}" class="navbar-nav-link @if($Route[0]=='vendor') active @endif">Vendor</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('warehouse'))
                <li class="nav-item">
                    <a href="{{route('warehouse.index')}}" class="navbar-nav-link @if($Route[0]=='warehouse') active @endif">Warehouse</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('purchase'))
                <li class="nav-item">
                    <a href="{{route('purchase.index')}}" class="navbar-nav-link @if($Route[0]=='purchase') active @endif">Stock- In</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('stockout'))
                <li class="nav-item">
                    <a href="{{route('stockout.index')}}" class="navbar-nav-link @if($Route[0]=='stockout') active @endif">Stock-Out</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('rawmaterial'))
                <li class="nav-item">
                    <a href="{{route('rawmaterial.index')}}" class="navbar-nav-link @if($Route[0]=='rawmaterial') active @endif">Raw Material</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('quotation'))
                <li class="nav-item">
                    <a href="{{route('quotation.index')}}" class="navbar-nav-link @if($Route[0]=='quotation') active @endif"></i> Quotation</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('procurement'))
                <li class="nav-item">
                    <a href="{{route('procurement.index')}}" class="navbar-nav-link @if($Route[0]=='procurement') active @endif"> Requisition Raise</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('requisition'))
                <li class="nav-item">
                    <a href="{{route('requisition.index')}}" class="navbar-nav-link @if($Route[0]=='requisition') active @endif">Requisition</a>
                </li>
            @endif
            @if($menuRoles->assignedRoles('purchaseOrder'))
                <li class="nav-item">
                    <a href="{{route('purchaseOrder.index')}}" class="navbar-nav-link @if($Route[0]=='purchaseOrder') active @endif">Purchase Order</a>
                </li>
            @endif

        </ul>
    </div>
</div>
<!-- /secondary navbar -->