@inject('menuRoles', '\App\Modules\User\Services\CheckUserRoles')
@php
    $currentRoute = Request::route()->getName();
    $Route = explode('.',$currentRoute);
@endphp

<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md"  @if($setting != null)) style="background-color: {{$setting->secondary_navbar_color}};" @endif>

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->
    <!-- Sidebar content -->
    <div class="sidebar-content">
        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">General</div>
                    <i class="icon-menu" title="General"></i></li>

                <li class="nav-item">
                    <a href="{{url('admin/dashboard')}}" class="nav-link @if($Route[0]=='dashboard') active @endif" data-popup="tooltip" data-original-title="Dashboard" data-placement="right"><i class="icon-home4"></i><span>Dashboard</span>
                    </a>
                </li>
                @if($menuRoles->assignedRoles('employment.index'))
                    <li class="nav-item">
                        <a href="{{route('employment.index')}}" class="nav-link @if($Route[0]=='employment') active @endif" data-popup="tooltip" data-original-title="Employee Management" data-placement="right"><i class="icon-users4"></i><span>Employee Management</span>
                        </a>
                    </li>
                @endif
                @if($menuRoles->assignedRoles('employee.list'))
                    <li class="nav-item">
                        <a href="{{route('employee.list')}}" class="nav-link @if($Route[0]=='employee') active @endif" data-popup="tooltip" data-original-title="Employee Directory" data-placement="right"><i class="icon-address-book2"></i><span>Employee Directory</span>
                        </a>
                    </li>
                @endif

                @if($menuRoles->assignedRoles('dropdown.index'))
                    <li class="nav-item">
                        <a href="{{route('dropdown.index')}}" class="nav-link @if($Route[0]=='dropdown') active @endif" data-popup="tooltip" data-original-title="Dropdown Management" data-placement="right"><i class="icon-circle-down2"></i><span>Dropdown Management</span>
                        </a>
                    </li>
                @endif
                @if($menuRoles->assignedRoles('setting.create'))
                    <li class="nav-item">
                        <a href="{{route('setting.create')}}" class="nav-link @if($Route[0]=='setting') active @endif" data-popup="tooltip" data-original-title="Setting Management" data-placement="right"><i class="icon-question7"></i><span>Setting Management</span>
                        </a>
                    </li>
                @endif

                @if($menuRoles->assignedRoles('role.index') || Auth::user()->user_type == 'super_admin')
                    <li class="nav-item">
                        <a href="{{route('role.index')}}" class="nav-link @if($Route[0]=='role') active @endif" data-popup="tooltip" data-original-title="Role Management"  data-placement="right"><i class="icon-pencil5"></i><span>Role Management</span>
                        </a>
                    </li>
                @endif


                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Automobile Features
                    </div>
                    <i class="icon-menu" title="Advance Construction Features"></i>
                </li>

                @if($menuRoles->assignedRoles('tender.index'))
                    <li class="nav-item">
                        <a href="{{route('tender.index')}}" class="nav-link @if($Route[0]=='tender') active @endif" data-popup="tooltip" data-original-title="Tender Management" data-placement="right"><i class="icon-design"></i><span>Tender Management</span>
                        </a>
                    </li>
                @endif

                @if($menuRoles->assignedRoles('vehicle.index'))
                    <li class="nav-item">
                        <a href="{{route('vehicle.index')}}" class="nav-link @if($Route[0]=='vehicle') active @endif" data-popup="tooltip" data-original-title="Vehicle Management" data-placement="right"><i class="icon-bus"></i><span>Vehicle Management</span>
                        </a>
                    </li>
                @endif

                @if($menuRoles->assignedRoles('document.index'))
                    <li class="nav-item">
                        <a href="{{route('document.index')}}" class="nav-link @if($Route[0]=='document') active @endif" data-popup="tooltip" data-original-title="Document Management" data-placement="right"><i class="icon-stack-text"></i><span>Document Management</span>
                        </a>
                    </li>
                @endif

                @if($menuRoles->assignedRoles('warehouse.index'))
                    <li class="nav-item">
                        <a href="{{route('warehouse.index')}}" class="nav-link @if($Route[0]=='warehouse') active @endif" data-popup="tooltip" data-original-title="Warehouse Management" data-placement="right"><i class="icon-home7"></i><span>Warehouse Management</span>
                        </a>
                    </li>
                @endif

                @if($menuRoles->assignedRoles('rawmaterial.index') || $menuRoles->assignedRoles('vendor.index') || $menuRoles->assignedRoles('purchase.index') || $menuRoles->assignedRoles('stockout.index'))
                    <li class="nav-item nav-item-submenu">
                      <a href="#" class="nav-link"><i class="icon-cart-add"></i> <span>Stock Management</span></a>
                      <ul class="nav nav-group-sub" data-submenu-title="Icons">

                         @if ($menuRoles->assignedRoles('rawmaterial.index'))
                          <li class="nav-item">
                            <a href="{{ route('rawmaterial.index') }}" class="nav-link dropdown-item rounded {{ $Route[0] == 'rawmaterial' ? 'active' : '' }}">Raw Material</a>
                          </li>
                         @endif

                         @if ($menuRoles->assignedRoles('vendor.index'))
                          <li class="nav-item">
                            <a href="{{ route('vendor.index') }}" class="nav-link dropdown-item rounded {{ $Route[0] == 'vendor' ? 'active' : '' }}">Vendor</a>
                          </li>
                         @endif

                         @if($menuRoles->assignedRoles('purchase.index'))
                          <li class="nav-item">
                            <a href="{{route('purchase.index')}}" class="nav-link dropdown-item rounded @if($Route[0]=='purchase') active @endif">Stock-In / Purchase</a>
                          </li>
                         @endif

                        @if($menuRoles->assignedRoles('stockout.index'))
                          <li class="nav-item">
                            <a href="{{route('stockout.index')}}" class="nav-link dropdown-item rounded @if($Route[0]=='stockout') active @endif">Stock-Out</a>
                          </li>
                         @endif
                      </ul>
                    </li>
                 @endif  

                @if($menuRoles->assignedRoles('insurance.index') || $menuRoles->assignedRoles('insurancetype.index') || $menuRoles->assignedRoles('insuranceCompany.index'))
                    <li class="nav-item nav-item-submenu">
                      <a href="#" class="nav-link"><i class="icon-shield2"></i> <span>Insurance Management</span></a>
                      <ul class="nav nav-group-sub" data-submenu-title="Icons">

                         @if ($menuRoles->assignedRoles('insuranceCompany.index'))
                          <li class="nav-item">
                            <a href="{{ route('insuranceCompany.index') }}" class="nav-link dropdown-item rounded {{ $Route[0] == 'insuranceCompany' ? 'active' : '' }}">Insurance Company</a>
                          </li>
                         @endif

                         @if ($menuRoles->assignedRoles('insurancetype.index'))
                          <li class="nav-item">
                            <a href="{{ route('insurancetype.index') }}" class="nav-link dropdown-item rounded {{ $Route[0] == 'insurancetype' ? 'active' : '' }}">Insurance Type</a>
                          </li>
                         @endif

                         @if($menuRoles->assignedRoles('insurance.index'))
                          <li class="nav-item">
                            <a href="{{route('insurance.index')}}" class="nav-link dropdown-item rounded @if($Route[0]=='insurance') active @endif">Insurance</a>
                          </li>
                         @endif

                      </ul>
                    </li>
                 @endif    

                @if($menuRoles->assignedRoles('procurement.index') || $menuRoles->assignedRoles('insurancetype.index') || $menuRoles->assignedRoles('insuranceCompany.index'))
                    <li class="nav-item nav-item-submenu">
                      <a href="#" class="nav-link"><i class="icon-cart5"></i> <span>Procurement Management</span></a>
                      <ul class="nav nav-group-sub" data-submenu-title="Icons">

                         @if ($menuRoles->assignedRoles('procurement.index'))
                          <li class="nav-item">
                            <a href="{{ route('procurement.index') }}" class="nav-link dropdown-item rounded {{ $Route[0] == 'procurement' ? 'active' : '' }}">Requirement Raise</a>
                          </li>
                         @endif

                         @if ($menuRoles->assignedRoles('requisition.index'))
                          <li class="nav-item">
                            <a href="{{ route('requisition.index') }}" class="nav-link dropdown-item rounded {{ $Route[0] == 'requisition' ? 'active' : '' }}">Material Requisition</a>
                          </li>
                         @endif

                         @if($menuRoles->assignedRoles('purchaseOrder.index'))
                          <li class="nav-item">
                            <a href="{{route('purchaseOrder.index')}}" class="nav-link dropdown-item rounded @if($Route[0]=='purchaseOrder') active @endif">Purchase Order</a>
                          </li>
                         @endif

                      </ul>
                    </li>
                 @endif    

                @if($menuRoles->assignedRoles('agreement.index'))
                    <li class="nav-item">
                        <a href="{{route('agreement.index')}}" class="nav-link @if($Route[0]=='agreement') active @endif" data-popup="tooltip" data-original-title="Agreement Management" data-placement="right"><i class="icon-stack-text"></i><span>Agreement Management</span>
                        </a>
                    </li>
                @endif

                @if($menuRoles->assignedRoles('project.index'))
                    <li class="nav-item">
                        <a href="{{route('project.index')}}" class="nav-link @if($Route[0]=='project') active @endif" data-popup="tooltip" data-original-title="Project Management" data-placement="right"><i class="icon-briefcase"></i><span>Project Management</span>
                        </a>
                    </li>
                @endif

                @if($menuRoles->assignedRoles('pettycontractor.index'))
                    <li class="nav-item">
                        <a href="{{route('pettycontractor.index')}}" class="nav-link @if($Route[0]=='pettycontractor') active @endif" data-popup="tooltip" data-original-title="Petty Contractor" data-placement="right"><i class="icon-user-tie"></i><span>Petty Contractor</span>
                        </a>
                    </li>
                @endif

           </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>