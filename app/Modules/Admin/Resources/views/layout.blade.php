<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>

    <!-- Global stylesheets -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{asset('admin/global/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets_1/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets_1/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets_1/css/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets_1/css/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets_1/css/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <link href="{{asset('admin/assets/css/extra/newsticker.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/css/extra/extra.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/custom.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/css/extra/extra.css')}}" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="{{asset('admin/style.css')}}" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->

    <!-- Core JS files -->
    <script src="{{asset('admin/global/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('admin/global/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/loaders/blockui.min.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/ui/slinky.min.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/ui/fab.min.js')}}"></script>
    <!-- /core JS files -->
   @toastr_css
    <!-- Theme JS files -->
    <script src="{{asset('admin/global/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>

    <script src="{{asset('admin/assets_1/js/app.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.rawgit.com/alertifyjs/alertify.js/v1.0.10/dist/js/alertify.js"></script>
    <script src="{{asset('admin/global/js/demo_pages/datatables_basic.js')}}"></script>
    <script src="{{asset('admin/master.js')}}"></script>

    <script src="{{asset('admin/assets/js/extra/newsticker.js')}}"></script>
    <script src="{{asset('admin/assets/js/extra/custom.js')}}"></script>

    @toastr_js

<!-- /theme JS files -->
    @yield('script')

    <style>
        .select2-container {
        width: 89.5% !important
        }
    </style>

    
</head>

<body>


<!-- Page header -->
<div class="page-header" >

    <!-- Main navbar -->
@include('admin::includes.header')
<!-- /main navbar -->

<!-- Floating menu -->
@include('admin::includes.menu')
<!-- /floating menu -->

</div>
<!-- /page header -->


<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">
            @include('admin::includes.page_header')
            @toastr_render
            @yield('content')

        </div>
        <!-- /content area -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->


<!-- Footer -->
@include('admin::includes.footer')
<!-- /footer -->

</body>
</html>
