@extends('admin::layout')
@section('title')Dashboard @stop
@section('breadcrum')Dashboard @stop
@section('content')

            
<script src="https://bidhee-sales.bidheegroup.com/admin/assets/js/extra/custom-dashboard.js"></script>

<script src="https://bidhee-sales.bidheegroup.com/admin/global/js/plugins/visualization/echarts/echarts.min.js"></script>
<script src="https://bidhee-sales.bidheegroup.com/admin/daily_work.js"></script>
<script src="https://bidhee-sales.bidheegroup.com/admin/sales_state.js"></script>

<section class="ccrm-newsticker">
    <div class="row">
        <div class="col-12">
            <div class="best-ticker bn-effect-scroll bn-direction-ltr" id="newsticker" style="height: 40px; line-height: 38px; border-radius: 2px; border-width: 1px;">
                <div class="bn-news">
                    <ul style="width: 1553.88px;">
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-teal">
                                        <i class="icon-hammer rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Employee - <strong>15</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-slate-700">
                                        <i class="icon-tree7 rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Daily Leads - <strong>172</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-danger">
                                        <i class="icon-user-check rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Hot Lead - <strong>23</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-indigo-700">
                                        <i class="icon-brain rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Cold Lead - <strong>156</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-warning-800">
                                        <i class="icon-point-up rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Sales Pipeline - <strong>12</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-success-700">
                                        <i class="icon-thumbs-up3 rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Converted Lead - <strong>16</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-danger-800">
                                        <i class="icon-thumbs-down3 rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Rejected Lead - <strong>25</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-blue-800">
                                        <i class="icon-reading rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Project Lead - <strong>0</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-brown-800">
                                        <i class="icon-hammer2 rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Project Bid - <strong>0</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-primary-600">
                                        <i class="icon-bookmark rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Agreement - <strong>6</strong></h6>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ccrm-main-header">
    <div class="row">
        <div class="col-12">
            <h5 class="m-0">Dashboard - <strong>Overview</strong></h5>
        </div>
    </div>
</section>

<div class="row mt-2">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body text-center">
                <i class="icon-medal-star icon-2x text-pink-400 border-pink-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
                <h5 class="card-title">Reward CRM</h5>
                <p class="mb-3">Related To Order and Sales. Also, Point Award to Customer after Sales Completion.</p>
                <a href="{{route('reward.index')}}" class="btn bg-pink-400">Reward Dashbord</a>
            </div>
        </div>
    </div>

</div>



<section class="ccrm-links">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{route('agreement.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-blue pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-users4 icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Agreement</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{route('dailyClient.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-green pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-hammer-wrench icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Leads</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{route('contact.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-green-2 pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-color-sampler icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Contacts</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{route('quotation.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-purple pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-stats-growth icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Quotation</h6>
                    </div>
                </div>
            </a>
        </div>
       

        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{route('vendor.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-brown pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-list-ordered icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Vendor</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{route('warehouse.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-red pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-hammer icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Warehouse</h6>
                    </div>
                </div>
            </a>
        </div>

            
        

    


    </div>
</section>

<section class="ccrm-no-card"> 
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-background alpha-primary border-0 card-body flex-row align-items-center">
                        <div class="media d-flex align-items-center justify-content-between w-100">
                            <i class="icon-bookmark text-primary-600 icon-4x rounded-round pb-1"></i>
                            <div class="media-body text-right">
                                <h4 class="mb-0">Total Proposal / Worth</h4>
                                <h2 class="mb-0 font-weight-semibold">6 / Rs.230,000.00/-</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-background alpha-orange border-0 card-body flex-row align-items-center">
                        <div class="media d-flex align-items-center justify-content-between w-100">
                            <i class="icon-certificate text-orange-600 icon-4x rounded-round pb-1"></i>
                            <div class="media-body text-right">
                                <h4 class="mb-0">Total Contract / Worth</h4>
                                <h2 class="mb-0 font-weight-semibold">0 / Rs.220,000.00/-</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-background alpha-green border-0 card-body flex-row align-items-center">
                        <div class="media d-flex align-items-center justify-content-between w-100">
                            <i class="icon-reload-alt text-green-600 icon-4x rounded-round pb-1"></i>
                            <div class="media-body text-right">
                                <h4 class="mb-0">Total Conversion / Worth</h4>
                                <h2 class="mb-0 font-weight-semibold">2 / Rs.1,000.00/-</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-background alpha-danger border-0 card-body flex-row align-items-center">
                        <div class="media d-flex align-items-center justify-content-between w-100">
                            <i class="icon-close2 text-danger-600 icon-4x rounded-round pb-1"></i>
                            <div class="media-body text-right">
                                <h4 class="mb-0">Total Rejection / Worth</h4>
                                <h2 class="mb-0 font-weight-semibold">7 / Rs.365,000.00/-</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card bd-card">
                <div class="bg-white card-header header-elements-inline">
                    <h5 class="card-title text-uppercase font-weight-semibold">Sales Statistics</h5>
                   
                    <div class="d-flex w-25">
                        <span class="text-teal"><i class="icon-history mr-2"></i>Last 7 Days</span>
                    </div>
                </div>
                <div class="casrd-body">
                    <div class="row">
                        <div class="col-md-5"> 
                            <div class="d-flex justify-content-center align-items-center flex-column border-right-1 border-grey-light h-100">

                               <input type="hidden" value="500000" id="billed_amount">
                               <input type="hidden" value="1500000" id="contracted_amount">
                               <input type="hidden" value="7822500" id="sales_pipeline_amount">

                               <div class="svg-center mt-3" id="pie_basic"><svg width="186" height="186"><g transform="translate(93,93)"><g class="d3-arc" style="stroke: rgb(255, 255, 255); stroke-width: 2px; cursor: pointer;"><path style="fill: rgb(74, 25, 174);" d="M5.572142936120457e-15,91A91,91 0 0,1 -74.52080258168286,52.226908606405665L0,0Z"></path></g><g class="d3-arc" style="stroke: rgb(255, 255, 255); stroke-width: 2px; cursor: pointer;"><path style="fill: rgb(39, 184, 73);" d="M-74.52080258168286,52.226908606405665A91,91 0 0,1 -87.16234651133617,26.14814239363406L0,0Z"></path></g><g class="d3-arc" style="stroke: rgb(255, 255, 255); stroke-width: 2px; cursor: pointer;"><path style="fill: rgb(114, 102, 53);" d="M-87.16234651133617,26.14814239363406A91,91 0 1,1 2.7860714680602287e-14,91L0,0Z"></path></g></g></svg></div>
                               <ul class="text-left d-flex align-items-center justify-content-center list-unstyled flex-wrap m-0 pt-4">
                                <li class="pr-2">
                                    <div class="ccrm-badge">
                                        <span class="ccrm-badge--purple"></span>
                                        <div class="ccrm-badge--title">Contract Signed - <strong class="order_class">Rs.1,500,000.00</strong></div>
                                    </div>
                                </li>
                                <li class="pr-2">
                                    <div class="ccrm-badge">
                                        <span class="ccrm-badge--green-2"></span>
                                        <div class="ccrm-badge--title">Invoiced Billing - <strong class="purchase_class">Rs.500,000.00</strong></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="ccrm-badge">
                                        <span class="ccrm-badge--brown"></span>
                                        <div class="ccrm-badge--title">Sales Pipeline - <strong class="purchase_class">Rs.7,822,500.00</strong></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="chart-container pt-2 pb-3 pl-2 pr-2">
                            <input type="hidden" value="Thursday Friday Saturday Sunday Monday Tuesday Wednesday" id="lead_day">
                            <input type="hidden" value="0 0 0 0 0 0 0" id="final_daily_lead">
                            <input type="hidden" value="0 0 0 0 0 0 0" id="final_conveted_lead">
                            <input type="hidden" value="0 0 0 0 0 0 0" id="final_rejected_lead">
                            <div class="chart has-fixed-height" id="area_basic" style="user-select: none; position: relative;" _echarts_instance_="ec_1617873143960"><div style="position: relative; overflow: hidden; width: 691px; height: 400px; padding: 0px; margin: 0px; border-width: 0px;"><canvas style="position: absolute; left: 0px; top: 0px; width: 691px; height: 400px; user-select: none; padding: 0px; margin: 0px; border-width: 0px;" data-zr-dom-id="zr_0" width="691" height="400"></canvas></div><div></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>




<section class="project-bid pb-leads">
    <div class="row">
        <div class="col-12">
            <div class="card bd-card">
                <div class="bg-danger-400 card-header header-elements-inline border-bottom-0">
                    <h5 class="card-title text-uppercase font-weight-semibold">Lead Management</h5>
                </div>
                <div class="card-body">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a href="#basic-pill1" class="nav-link active" data-toggle="tab">
                                <div class="nav-item_title">
                                    <i class="icon-menu7 d-block mb-1 mt-1"></i>
                                    <span>Daily Leads</span>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#basic-pill2" class="nav-link" data-toggle="tab">
                                <div class="nav-item_title">
                                    <i class="icon-sync d-block mb-1 mt-1"></i>
                                    <span>Hot Lead</span>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#basic-pill3" class="nav-link" data-toggle="tab">
                                <div class="nav-item_title">
                                    <i class="icon-list3 d-block mb-1 mt-1"></i>
                                    <span>Cold Lead</span>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#basic-pill4" class="nav-link" data-toggle="tab">
                                <div class="nav-item_title">
                                    <i class="icon-package d-block mb-1 mt-1"></i>
                                    <span>Sales Pipeline</span>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#basic-pill5" class="nav-link" data-toggle="tab">
                                <div class="nav-item_title">
                                    <i class="icon-check d-block mb-1 mt-1"></i>
                                    <span>Converted Leads</span>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#basic-pill6" class="nav-link" data-toggle="tab">
                                <div class="nav-item_title">
                                    <i class="icon-x d-block mb-1 mt-1"></i>
                                    <span>Rejected Leads</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="basic-pill1">
                            <div class="pb-block">
                                <div class="row">

                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=4">Nector SPA</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">7th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Aakriti Acharya</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=5">Raj Trading</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">7th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Aakriti Acharya</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=6">Chakreshwori Nirman Sewa</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">15th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Manik Maharjan</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=10">Nector SPA</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">8th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Aakriti Acharya</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                                                                                                    
                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                            
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-end">
                                             <a href="https://bidhee-sales.bidheegroup.com/admin/dailyClient" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-history" data-popup="tooltip" data-placement="bottom" data-original-title="Go To Lead"></i></b>View All</a>
                                        </div>
                                    </div>
                                                                                                            
                                </div>
                            </div>

                            <div class="tab-pane fade" id="basic-pill2">
                               <div class="pb-block">
                                <div class="row">

                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=36">JB Builders Pvt.Ltd</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">12th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Manik Maharjan</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=52">Model Construction Pvt.Ltd</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">12th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Manik Maharjan</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.350,000/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=68">Pacific Builders Pvt.Ltd.</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">28th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Manik Maharjan</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=71">APM Construction &amp; Builders Pvt.Ltd</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">28th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Manik Maharjan</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                                                                                                    
                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                            
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-end">
                                             <a href="https://bidhee-sales.bidheegroup.com/admin/dailyClient" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-history" data-popup="tooltip" data-placement="bottom" data-original-title="Go To Lead"></i></b>View All</a>
                                        </div>
                                    </div>
                                                                                                            
                                </div>
                            </div>
                            <div class="tab-pane fade" id="basic-pill3">
                               <div class="pb-block">
                                <div class="row">

                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=1">Heifer International Nepal</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">7th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Bimal Devkota</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=7">Sahara Microfinance</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">12th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Rajan Shrestha</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=9">Shreenagar Agro ind. pvt. ltd.</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">12th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Rajan Shrestha</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=12">APM Construction</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">8th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Aakriti Acharya</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                                                                                                    
                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                            
                                    <div class="row"> 
                                        <div class="col-12 d-flex justify-content-end">
                                         
                                            <a href="https://bidhee-sales.bidheegroup.com/admin/dailyClient" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-history" data-popup="tooltip" data-placement="bottom" data-original-title="Go To Lead"></i></b>View All</a>

                                        </div>
                                    </div>
                                                                                                            
                                </div>
                            </div>
                            <div class="tab-pane fade" id="basic-pill4">
                               <div class="pb-block">
                                <div class="row">

                                                                                                           
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=2">LP Gas Association</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">15th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Aakriti Acharya</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                           
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=3">The Realtors</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">7th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Aakriti Acharya</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                           
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=8">CAB Construction</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">15th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Manik Maharjan</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.847,500/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                           
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=35">Khalti</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">12th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Rajan Shrestha</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.150,000/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                                                                                                   
                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                            
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-end">
                                           <a href="https://bidhee-sales.bidheegroup.com/admin/dailyClient" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-history" data-popup="tooltip" data-placement="bottom" data-original-title="Go To Lead"></i></b>View All</a>
                                        </div>
                                    </div>
                                                                                                            
                                </div>
                            </div>
                            <div class="tab-pane fade" id="basic-pill5">
                               <div class="pb-block">
                                <div class="row">

                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=58">Religare Construction Pvt.Ltd</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">30th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Manik Maharjan</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.350,000/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=106">bidhee</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">17th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Administration </strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.10,000/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=145">Varosa Service</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">1st Jan, 70</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Suman Shrestha</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=146">Hotel Le Himalaya</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">1st Jan, 70</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Suman Shrestha</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                                                                                                    
                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                       
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-end">
                                             <a href="https://bidhee-sales.bidheegroup.com/admin/dailyClient" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-history" data-popup="tooltip" data-placement="bottom" data-original-title="Go To Lead"></i></b>View All</a>
                                        </div>
                                    </div>
                                                                                                            
                                </div>
                            </div>
                            <div class="tab-pane fade" id="basic-pill6">
                               <div class="pb-block">
                                <div class="row">

                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=42">Room to Read</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">23rd Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Raj Sukla</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=56">Kathmandu College of Management</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">12th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Suman Lama</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=72">Bota Momo</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">14th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Suman Lama</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                            
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <div class="card rounded border-top-3 border-top-purple-400 border-bottom-3 border-bottom-purple-400">
                                            <div class="card-header d-flex align-items-center justify-content-between">
                                                <h4 class="card-title"><span class="font-weight-semibold"><a class="text-teal" href="https://bidhee-sales.bidheegroup.com/admin/dailyClient/profile?id=93">Vatsal Impex Pvt. Ltd.</a></span></h4>
                                            </div>
                                            <div class="card-body pb-card-info">
                                                <span class="d-flex align-items-center pb-2">Next Meeting Date: <strong class="pl-1">16th Jan, 20</strong></span>
                                                <span class="d-flex align-items-center">Created By: <strong class="pl-1 text-success">Suman Lama</strong></span>

                                                <div class="mt-2 border-0 d-flex align-items-center justify-content-between">
                                                    <h6 class="m-0 text-muted">Approach Cost:</h6>
                                                    <h3 class="m-0 font-weight-semibold">Rs.0/-</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                                                                                                                                    
                                    </div>

                                                                                                                                                                                                                                                                                                                                                                                                            
                                    <div class="row">
                                        <div class="col-12 d-flex justify-content-end">
                                             <a href="https://bidhee-sales.bidheegroup.com/admin/dailyClient" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-history" data-popup="tooltip" data-placement="bottom" data-original-title="Go To Lead"></i></b>View All</a>
                                        </div>
                                    </div>
                                                                                                            
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Zoom option -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Daily Work Report</h5>
                    <div class="header-elements">
                        <input type="hidden" value="" id="sale_date">
                        <input type="hidden" value="" id="no_of_approach">
                        <input type="hidden" value="" id="no_of_meeting">
                        <input type="hidden" value="" id="no_of_proposal_sent">
                        <input type="hidden" value="" id="no_of_contract_sent">
                        <input type="hidden" value="" id="no_of_conversion">
                        <input type="hidden" value="" id="no_of_rejected_client">
                    </div>
                </div>

                <div class="card-body">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="line_zoom" style="user-select: none; position: relative;" _echarts_instance_="ec_1617873143959"><div style="position: relative; overflow: hidden; width: 2443px; height: 400px; padding: 0px; margin: 0px; border-width: 0px;"><canvas style="position: absolute; left: 0px; top: 0px; width: 2443px; height: 400px; user-select: none; padding: 0px; margin: 0px; border-width: 0px;" data-zr-dom-id="zr_0" width="2443" height="400"></canvas></div><div></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /zoom option -->



@stop
