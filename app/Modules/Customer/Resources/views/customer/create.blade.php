@extends('admin::layout')
@section('title')Retailer @stop
@section('breadcrum')Add Retailer @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>


<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-teal-700 header-elements-inline">
        <h5 class="card-title">Add Retailer</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">

        {!! Form::open(['route'=>'retailer.store','method'=>'POST','class'=>'form-horizontal','id'=>'retailer_submit','role'=>'form','files' => true]) !!}
            @include('retailer::retailer.partial.action',['btnType'=>'Save'])
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@stop
