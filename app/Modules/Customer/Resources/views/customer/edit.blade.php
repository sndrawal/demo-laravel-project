@extends('admin::layout')
@section('title')Customer @stop
@section('breadcrum')Edit Customer @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>

<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-teal-700 header-elements-inline">
        <h5 class="card-title">Edit Customer</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">

        {!! Form::model($customer,['method'=>'PUT','route'=>['customer.update',$customer->id],'class'=>'form-horizontal','id'=>'customer_submit','role'=>'form','files'=>true]) !!}
        @include('customer::customer.partial.action',['btnType'=>'Update'])
        {!! Form::close() !!}

    </div>
</div>
<!-- /form inputs -->

@stop
