<?php

namespace App\Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [

    	'customer_name',
    	'mobile_no',
    	'address',
    	'email',
    	'point',
    	'point_amount'

    ];
}
