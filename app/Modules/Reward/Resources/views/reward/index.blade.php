@extends('admin::layout')
@section('title')Reward Dashboard @stop
@section('breadcrum')Reward Dashboard @stop
@section('content')

<section class="ccrm-links">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <a href="{{route('item.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-blue pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-cube4 icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Items</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <a href="{{route('customer.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-green pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-users2 icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Customer</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <a href="{{route('order.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-green-2 pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-store2 icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Order</h6>
                    </div>
                </div>
            </a>
        </div>

    </div>
</section>


<section class="ccrm-no-card"> 
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-background alpha-primary border-0 card-body flex-row align-items-center">
                        <div class="media d-flex align-items-center justify-content-between w-100">
                            <i class="icon-cart5 text-primary-600 icon-4x rounded-round pb-1"></i>
                            <div class="media-body text-right">
                                <h4 class="mb-0">Total Orders</h4>
                                <h2 class="mb-0 font-weight-semibold">{{$no_of_order}} </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-background alpha-orange border-0 card-body flex-row align-items-center">
                        <div class="media d-flex align-items-center justify-content-between w-100">
                            <i class="icon-coin-dollar  text-orange-600 icon-4x rounded-round pb-1"></i>
                            <div class="media-body text-right">
                                <h4 class="mb-0">Total Sales Amount (Excluded GST)</h4>
                                <h2 class="mb-0 font-weight-semibold">${{number_format($total_sales_amount,2)}}/-</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-background alpha-primary border-0 card-body flex-row align-items-center">
                        <div class="media d-flex align-items-center justify-content-between w-100">
                            <i class="icon-cart5 text-primary-600 icon-4x rounded-round pb-1"></i>
                            <div class="media-body text-right">
                                <h4 class="mb-0">Total Items</h4>
                                <h2 class="mb-0 font-weight-semibold">{{$total_item}} </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-background alpha-orange border-0 card-body flex-row align-items-center">
                        <div class="media d-flex align-items-center justify-content-between w-100">
                            <i class="icon-users2 text-orange-600 icon-4x rounded-round pb-1"></i>
                            <div class="media-body text-right">
                                <h4 class="mb-0">Total Customer</h4>
                                <h2 class="mb-0 font-weight-semibold">{{$total_customer}} </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

@stop
