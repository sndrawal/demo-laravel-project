<?php

namespace App\Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Item\Entities\Item;

class OrderDetail extends Model
{
    protected $fillable = [

    	'order_id',
    	'item_id',
    	'qty',
    	'rate',
    	'amount'

    ];

    public function ItemInfo(){
        return $this->belongsTo(Item::class, 'item_id');
    }
}
