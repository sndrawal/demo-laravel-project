<?php

namespace App\Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Customer\Entities\Customer;

class Order extends Model
{
    protected $fillable = [

    	'order_code',
    	'customer_type',	
    	'customer_id',
        'sales_type',
        'sub_total',
        'gst_amount',
        'grand_total',
        'number_in_words',
        'order_date',
        'status'

    ];

    public function getOrderDetailField()
    {
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }

    public function Customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
