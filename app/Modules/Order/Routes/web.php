<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'permission']], function () {

        /*
        |--------------------------------------------------------------------------
        |order  CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('order', ['as' => 'order.index', 'uses' => 'OrderController@index']);

        // order Create
        Route::get('order/create', ['as' => 'order.create', 'uses' => 'OrderController@create']);
        Route::post('order/store', ['as' => 'order.store', 'uses' => 'OrderController@store']);
        //order Edit
        Route::get('order/edit/{id}', ['as' => 'order.edit', 'uses' => 'OrderController@edit'])->where('id','[0-9]+');
        Route::put('order/update/{id}', ['as' => 'order.update', 'uses' => 'OrderController@update'])->where('id','[0-9]+');
        // order Delete
        Route::get('order/delete/{id}', ['as' => 'order.delete', 'uses' => 'OrderController@destroy'])->where('id','[0-9]+');

        Route::post('order/get-item-info-ajax', ['as'=>'order.get-item-info-ajax','uses'=>'OrderController@getItemInfoAjax']);

        Route::get('order/appendItem', ['as'=>'order.appendItem','uses'=>'OrderController@appendItem']);

        Route::post('order/updateStatus', ['as' => 'order.updateStatus', 'uses' => 'OrderController@updateStatus']);

});
