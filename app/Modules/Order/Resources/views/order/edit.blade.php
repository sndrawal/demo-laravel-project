@extends('admin::layout')
@section('title')Order @stop 
@section('breadcrum')Edit Order @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Edit Order</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">

        {!! Form::model($order,['method'=>'PUT','route'=>['order.update',$order->id],'class'=>'form-horizontal','id'=>'order_submit','role'=>'form','files'=>true]) !!} 									
        	@include('order::order.partial.action',['btnType'=>'Update'])
        {!! Form::close() !!}
        
    </div>
</div>
<!-- /form inputs -->

@stop