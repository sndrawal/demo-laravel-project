<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>


<div class="appendQuotationItem">
<div class="form-group row">
        <div class="col-lg-4 ajax_product_brand">
            <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                                {!! Form::select('item_id[]',$items, $value = null, ['id'=>'item_id','placeholder'=>'Select Item','class'=>'item_id form-control select-search']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i>
                                </span>
                            </span>
                            {!! Form::text('qty[]', $value = null, ['id'=>'qty','class'=>'qty form-control numeric','placeholder'=>'Qty']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">USD
                                </span>
                            </span>
                            {!! Form::text('rate[]', $value = null, ['id'=>'rate','class'=>'rate form-control numeric','placeholder'=>'Rate','readonly']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">USD
                                </span>
                            </span>
                            {!! Form::text('amount[]', $value = null, ['id'=>'amount','class'=>'amount form-control numeric','readonly'=>'readonly','placeholder'=>'Amount']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <button type="button" class="ml-2 add_item btn bg-success-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b>Add</button>
            </div>
        </div>

    </div>
</div>



<script type="text/javascript">
    $('.select-search').select2();
</script>


<script type="text/javascript">
    $(document).ready(function(){

         $('.item_id').on('change',function(){
                var item_id = $(this).val();
                var sales_type = $('#sales_type').val();

                 var token = $("input[name='_token']").val();
                      $.ajax({
                          url: "<?php echo route('order.get-item-info-ajax') ?>",
                          method: 'POST',
                          context: this,
                          data: {item_id:item_id,sales_type:sales_type, _token:token},
                          success: function(data) {
                            $(this).parent().parent().parent().parent().next().next().find('.rate').val(data);
                          }
                      });
            });

            $('.qty').on('keyup',function(){

                var qty = $(this).val();
                var rate = $(this).parent().parent().parent().parent().next().find('.rate').val();  
                
                var final_cost = qty * rate; 

                if(isNaN(final_cost)){
                  $(this).parent().parent().parent().parent().next().next().find('.amount').val(0);
                }else{
                  $(this).parent().parent().parent().parent().next().next().find('.amount').val(final_cost);
                }

                var arr = document.getElementsByClassName('amount');  
                 var tot=0;
                    for(var i=0;i<arr.length;i++){
                        if(parseFloat(arr[i].value))
                            tot += parseFloat(arr[i].value);
                    }
              
                var total_amount = tot; 
                $('.sub_total').val(total_amount.toFixed(2));

                 var gst_rate = 6;
                 var gst_value = (gst_rate/100) * total_amount;
                 $('.gst_amount').val(gst_value.toFixed(2));


                 var grand_total = total_amount + gst_value;
                 $('.grand_total').val(grand_total.toFixed(2));

               
            });

            

         $('.add_item').on('click',function(){
                
                 $.ajax({
                    type: 'GET',
                    url: '/admin/order/appendItem',
                    success: function (data) { 
                        $('.appendQuotationItem').last().append(data.options); 
                    }
                });


            });

    });
</script>