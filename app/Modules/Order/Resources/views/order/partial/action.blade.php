<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('admin/validation/order.js')}}"></script>

<style>
    .select2-container {
        width: 93.5% !important
    }
</style>
 
<fieldset class="mb-3">
        <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
        @php
          $readonly = ($is_edit AND $order) ? 'readonly' : "";
          $disabled = ($is_edit AND $order) ? 'disabled' : "";
        @endphp
        <div class="form-group row">

            <div class="col-lg-6 customer_section">
                <div class="row">
                <label class="col-form-label col-lg-2">Customer Type:</label>
                 <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                       <div class="input-group">
                         {!! Form::select('customer_type',[ 'new'=>'New','old'=>'Old'], $value = null, ['id'=>'customer_type','class'=>'form-control','placeholder'=>'Select Customer Type',$disabled]) !!}
                    </div>
                 </div>
                </div>
            </div>

        </div>

        @php
            $display_customer_new_section = ($is_edit AND $order->customer_type == 'new') ? "display:block" : "display:none";
            $display_customer_old_section = ($is_edit AND $order->customer_type == 'old') ? "display:block" : "display:none";
        @endphp

        <div class="form-group row">
            <div class="col-lg-6 customer_old_section" style="{{ $display_customer_old_section }}">
                <div class="row">
                <label class="col-form-label col-lg-2">Select Customer:</label>
                 <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                       <div class="input-group">
                          {!! Form::select('customer_id',$customer, $value = null, ['id'=>'customer_id', 'class'=>'form-control select-search','placeholder'=>'Select Customer',$disabled]) !!}
                    </div>
                 </div>
                </div>
            </div>

        </div>

        @php
            $customer_name = ($is_edit AND $order) ? optional($order->Customer)->customer_name : "";
            $mobile_no = ($is_edit AND $order) ? optional($order->Customer)->mobile_no : "";
            $address = ($is_edit AND $order) ? optional($order->Customer)->address : "";
            $email = ($is_edit AND $order) ? optional($order->Customer)->email : "";

        @endphp

        <div class="form-group row">
            <div class="col-lg-6 customer_new_section" style="{{ $display_customer_new_section }}">
                <div class="row">
                <label class="col-form-label col-lg-2">Customer Name:<span class="text-danger">*</span></label>
                 <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                       <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-users2"></i></span>
                        </span>
                          {!! Form::text('customer_name', $value = $customer_name, ['id'=>'customer_name','placeholder'=>'Enter Customer Name','class'=>'form-control','required',$readonly]) !!}
                    </div>
                 </div>
                </div>
            </div>

            <div class="col-lg-6 customer_new_section" style="{{ $display_customer_new_section }}">
                <div class="row">
                <label class="col-form-label col-lg-2">Mobile No.:<span class="text-danger">*</span></label>
                 <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                       <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-mobile2"></i></span>
                        </span>
                         {!! Form::text('mobile_no', $value = $mobile_no, ['id'=>'mobile_no','placeholder'=>'Enter Mobile No.','class'=>'form-control',$readonly]) !!}
                    </div>
                 </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6 customer_new_section" style="{{ $display_customer_new_section }}">
                <div class="row">
                <label class="col-form-label col-lg-2">Customer Address:<span class="text-danger">*</span></label>
                 <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                       <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-location4"></i></span>
                        </span>
                          {!! Form::text('address', $value = $address, ['id'=>'address','placeholder'=>'Enter Customer Address','class'=>'form-control',$readonly]) !!}
                    </div>
                 </div>
                </div>
            </div>

            <div class="col-lg-6 customer_new_section" style="{{ $display_customer_new_section }}">
                <div class="row">
                <label class="col-form-label col-lg-2">Customer Email:<span class="text-danger">*</span></label>
                 <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                       <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-envelop3"></i></span>
                        </span>
                         {!! Form::email('email', $value = $email, ['id'=>'email','placeholder'=>'Enter Customer Email','class'=>'form-control',$readonly]) !!}
                    </div>
                 </div>
                </div>
            </div>
        </div>

   <div class="row">

        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Sales Type:</label>
                    <div class="col-lg-9">
                       <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-dots"></i></span>
                        </span>
                         {!! Form::select('sales_type',[ 'Normal'=>'Normal','Promotion'=>'Promotion'], $value = null, ['id'=>'sales_type','class'=>'form-control','placeholder'=>'Select Sales Type',$disabled]) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-6">

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Order Date:</label>
                    <div class="col-lg-9">
                       <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-calendar"></i></span>
                        </span>
                        {!! Form::date('order_date', $value = null, ['placeholder'=>'Enter Order Date','class'=>'order_date form-control',$readonly]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </fieldset>

        <fieldset class="mb-3">
        <legend class="text-uppercase font-size-sm font-weight-bold">Item Order</legend>

        @if($is_edit)
            @include('order::order.partial.order-item-edit') 
        @endif

         @include('order::order.partial.order-item-more') 
    </fieldset> 


     <fieldset class="mb-3 quotatoin_balance">
            <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
             @include('order::order.partial.order-balance') 
    </fieldset>


    <div class="text-right">
          <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>{{ $btnType }}</button>
    </div>
</form>

<script type="text/javascript">
    
    $('.select-search').select2();

    $(document).ready(function(){

        $(document).on('change','#customer_type',function(){
            var ctype = $(this).val();
            if(ctype == 'new'){
                $('.customer_new_section').show();
                $('.customer_old_section').hide();
                $('#customer_id').attr('required',false);      
                $('#customer_name').attr('required',true);
                $('#mobile_no').attr('required',true);
                $('#address').attr('required',true);
                $('#email').attr('required',true);
            }else{
                $('.customer_old_section').show();
                $('#customer_id').attr('required',true);
                $('.customer_new_section').hide();
                $('#customer_name').attr('required',false);
                $('#mobile_no').attr('required',false);
                $('#address').attr('required',false);
                $('#email').attr('required',false);
            }
        });

    });
</script>