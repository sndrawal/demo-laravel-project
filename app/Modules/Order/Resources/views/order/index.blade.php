@extends('admin::layout')
@section('title')Order @stop
@section('breadcrum')Order @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>

@stop
@section('content')

<div class="card card-body">
    <div class="d-flex justify-content-between mb-4">
        <h4>List Of Orders</h4>
        <div class="button-group"> 

            <a href="{{route('order.create')}}" class="ml-2 btn bg-success-600 btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-plus2"></i></b> Create Order </a>

            <a href="{{route('reward.index')}}" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-flip-horizontal2"></i></b> Back To Reward </a>
        </div>

    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr class="bg-slate">
                    <th>S.N.</th>
                    <th>Order Code</th>
                    <th>Sales Type</th>
                    <th>Customer Name</th>
                    <th>Grand Total</th>
                    <th>Order Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                
                @if($order->total() != 0)
                @foreach($order as $key => $value)
                
                @php
                    if($value->status == 'Open'){
                        $prio_color = 'bg-warning';
                        $icon = 'icon-history';
                        $modal ='modal';
                    }
                    if($value->status == 'Completed'){
                        $prio_color = 'bg-success';
                        $icon = 'icon-thumbs-up3';
                        $modal ='';
                    }
                    if($value->status == 'Out Of Stock'){
                        $prio_color = 'bg-danger';
                        $icon = 'icon-thumbs-down3 ';
                        $modal ='';
                    }

                    $full_name = optional($value->Customer)->customer_name; 
                @endphp
                <tr>
                    <td>{{$order->firstItem() +$key}}</td>
                    <td>{{ $value->order_code }}</td>
                    <td>{{ $value->sales_type }}</td>
                    <td>{{ $full_name }}</td>
                    <td>${{ number_format($value->grand_total,2) }}</td>
                    <td>{{ $value->order_date }}</td>
                     <td><a data-toggle="{{$modal}}" data-target="#modal_theme_status" class="btn {{$prio_color}} text-default btn-icon btn-sm rounded-round ml-2 update_status" order_id ="{{$value->id}}" data-popup="tooltip"data-placement="bottom" data-original-title="{{ $value->status }}"><i class="{{ $icon}}"></i></a></td>

                    <td>
                        <a class="btn bg-info btn-icon rounded-round" href="{{route('order.edit',$value->id)}}" data-popup="tooltip" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil6"></i></a>
                        
                        <a data-toggle="modal" data-target="#modal_theme_warning" class="btn bg-danger btn-icon rounded-round delete_order" link="{{route('order.delete',$value->id)}}" data-popup="tooltip" data-placement="bottom" data-original-title="Delete"><i class="icon-bin"></i></a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">No Order Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>
        <span style="margin: 5px;float: right;">
            @if($order->total() != 0)
                {{ $order->links() }}
            @endif
            </span>
    </div>
</div>



<!-- Warning modal -->
        <div id="modal_theme_status" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-warning-800">
                        <h6 class="modal-title"><i class="icon-user-check mr-1"></i> Order Status</h6>
                    </div>


                    <div class="modal-body">
                        {!! Form::open(['route'=>'order.updateStatus','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}

                        {{ Form::hidden('order_id', '', array('class' => 'order_id')) }}

                        <div class="form-group row">
                            <label class="col-form-label col-lg-4">Status:</label>
                            <div class="col-lg-8">
                                {!! Form::select('status',['Completed'=>'Completed','Out Of Stock'=>'Out Of Stock'], $value = null, ['id'=>'status','class'=>'form-control','placeholder'=>'Select Status']) !!}
                            </div>
                        </div>


                        <div class="text-center">
                                <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>Update Status</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
        <!-- /warning modal -->


 <!-- Warning modal -->
    <div id="modal_theme_warning" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                 <div class="modal-body">
                    <center>
                        <i class="icon-alert text-danger icon-3x"></i>
                    </center>
                    <br>
                    <center>
                        <h2>Are You Sure Want To Delete ?</h2>
                        <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!-- /warning modal -->


<script type="text/javascript">
    $('document').ready(function() {

        $('.update_status').on('click',function(){

            var order_id = $(this).attr('order_id');
            $('.order_id').val(order_id);
        });


        $('.delete_order').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });

    });
 </script>

@endsection
