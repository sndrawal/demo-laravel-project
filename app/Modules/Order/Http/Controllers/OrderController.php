<?php

namespace App\Modules\Order\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Modules\Order\Repositories\OrderInterface;
use App\Modules\Order\Repositories\ItemOrderInterface;
use App\Modules\Item\Repositories\ItemInterface;
use App\Modules\Customer\Repositories\CustomerInterface;

class OrderController extends Controller
{

    protected $order;
    protected $item_order;
    protected $items;
    protected $customer;

    public function __construct(OrderInterface $order, ItemOrderInterface $item_order, ItemInterface $items, CustomerInterface $customer)
    {
        $this->order = $order;
        $this->item_order = $item_order;
        $this->items = $items;
        $this->customer = $customer;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $search =$request->all();

         $data['order'] = $this->order->findAll($limit= 50,$search);
         $data['search_value'] = $search;
         return view('order::order.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['is_edit'] = false;
        $data['items']=$this->items->getList();
        $data['customer']=$this->customer->getList();
        return view('order::order.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all(); 
        
        $itemList = $data['item_id'];
        $count = sizeof($itemList);

        $amountInWords = $this->order->NumberIntoWords($data['grand_total']);

        try{ 

                $customer_type = $data['customer_type'];
                if($customer_type == 'new'){
                        
                    $customerData = array(
                        'customer_name' => $data['customer_name'],
                        'mobile_no' => $data['mobile_no'],
                        'address' => $data['address'],
                        'email' => $data['email']
                    );

                    $customerInfo = $this->customer->save($customerData);
                    $customer_id = $customerInfo->id;
                }else{
                    $customer_id = $data['customer_id'];
                }

            $orderData = array(

                    'customer_type' =>$data['customer_type'],
                    'customer_id' =>$customer_id,
                    'sales_type' =>$data['sales_type'],
                    'grand_total' =>$data['grand_total'],
                    'number_in_words' =>$amountInWords,
                    'order_date' => date('Y-m-d'), 
                    'status' => 'Open'

            );

            $orderInfo = $this->order->save($orderData);
            $order_id = $orderInfo->id;

            $order_update =array(
                'order_code' => 'OD - '.date('Y').' - '.str_pad($order_id, 4, '0', STR_PAD_LEFT) 
            );
            $this->order->update($order_id,$order_update);

                for($i = 0; $i < $count; $i++){
            
                    if($data['item_id'][$i]){    

                         $productField['order_id'] = $order_id;
                         $productField['item_id']  =  $data['item_id'][$i];
                         $productField['qty'] = $data['qty'][$i];
                         $productField['rate'] = $data['rate'][$i];
                         $productField['amount'] = $data['amount'][$i];

                     $this->item_order->save($productField);
                    }
                }

            toastr()->success('Order and Details Created Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
       
        return redirect(route('order.index'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('order::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['is_edit'] = true;
        $data['order'] = $this->order->find($id);
        $data['orderItems'] = $this->item_order->find($id);
        $data['items']=$this->items->getList();
        $data['customer']=$this->customer->getList();
        return view('order::order.edit',$data);

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all(); 
        
        $itemList = $data['item_id'];
        $count = sizeof($itemList);

        $amountInWords = $this->order->NumberIntoWords($data['grand_total']);
        
        try{ 

            $orderData = array(
                    'grand_total' =>$data['grand_total'],
                    'number_in_words' =>$amountInWords,
            );

            $orderInfo = $this->order->update($id,$orderData);
            $order_id = $id;
                
                $this->item_order->delete($id);

                for($i = 0; $i < $count; $i++){
            
                    if($data['item_id'][$i]){    

                         $productField['order_id'] = $order_id;
                         $productField['item_id']  =  $data['item_id'][$i];
                         $productField['qty'] = $data['qty'][$i];
                         $productField['rate'] = $data['rate'][$i];
                         $productField['amount'] = $data['amount'][$i];

                        $this->item_order->save($productField);
                    }
                }

            toastr()->success('Order and Details Updated Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
       
        return redirect(route('order.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $this->item_order->delete($id);
            $this->order->delete($id);
            
             toastr()->success('Order and Details Deleted Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
      return redirect(route('order.index'));  
    }

    public function getItemInfoAjax(Request $request){
         if($request->ajax()){

            $item_id = $request->item_id;
            $sales_type = $request->sales_type;
            $itemDetail = $this->items->find($item_id); 

            if($sales_type == 'Normal'){
                 $rate = $itemDetail->normal_price; 
            }else{
                 $rate = $itemDetail->promotion_price; 
            }
           
            return $rate; 
        }
    }

    public function appendItem(Request $request){

         if($request->ajax()){
            $items=$this->items->getList();
            $data = view('order::order.partial.add-more-item',compact('items'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    public function updateStatus(Request $request){
        $input = $request->all();
        $id = $input['order_id'];
        $status = $input['status'];

        $updateData = array(
            'status' =>$input['status']
        );

        $this->order->update($id, $updateData);

        if($status == 'Completed'){

            $orderInfo = $this->order->find($id);  
            $customer_id = $orderInfo->customer_id;

            $customerInfo = $this->customer->find($customer_id); 
            $point = $customerInfo->point;
            $point_amount = $customerInfo->point_amount;

            $new_point = $point + 1;
            $point_amount = $point_amount + 0.01;

            $customerData = array(
                'point' =>$new_point,
                'point_amount' =>$point_amount

            );

            $this->customer->update($customer_id, $customerData);

        }

        toastr()->success('Order Status Updated Successfully');

        return redirect(route('order.index'));
    }

}
