<?php

namespace App\Modules\Order\Repositories;

interface ItemOrderInterface
{
    public function findAll($sale_id,$limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function save($data);

    public function update($id,$data);

    public function delete($id);
    
    public function countTotal();

    public function findAllData($id);
    
    public function getSalesAmount();
}