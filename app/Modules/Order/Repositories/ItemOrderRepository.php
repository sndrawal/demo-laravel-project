<?php 
namespace App\Modules\Order\Repositories;

use App\Modules\Order\Entities\OrderDetail;
use App\Modules\Order\Entities\Order;
use Illuminate\Support\Facades\DB;
class ItemOrderRepository implements ItemOrderInterface
{
    
    public function findAll($order_id, $limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result = OrderDetail::when(array_keys($filter, true), function ($query) use ($filter) {
        })->where('order_id', '=', $order_id)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        
        return $result;    
        
    }
    
    public function find($id){
        return OrderDetail::where('order_id','=',$id)->get();
    }
    
    public function save($data){
        return OrderDetail::create($data);
    }
    
    public function update($id,$data){
        $result = OrderDetail::find($id);
        return $result->update($data);
    }
    
    public function delete($id){
       return OrderDetail::where('order_id','=',$id)->delete($id);
    }
    
    public function countTotal(){
        return OrderDetail::count();
    }

    public function findAllData($id){
        return OrderDetail::where('order_id','=',$id)->get();
    }

    public function getSalesAmount(){

        $result = DB::table('orders')
            ->join('order_details', 'orders.id', '=', 'order_details.order_id')
            ->where('orders.status','=','Completed')
            ->sum('order_details.amount');

        return $result;
    }

}



