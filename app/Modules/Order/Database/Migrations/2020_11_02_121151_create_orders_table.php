<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('order_code')->nullable();
            $table->string('customer_type')->nullable();
            $table->integer('customer_id')->nullable();
            $table->string('sales_type')->nullable();
            $table->double('sub_total',14,2)->nullable();
            $table->double('gst_amount',14,2)->nullable();
            $table->double('grand_total',14,2)->nullable();
            $table->text('number_in_words')->nullable();
            $table->date('order_date')->nullable();
            $table->string('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
