<?php

namespace App\Modules\Vendor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VendorRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor_name' => 'required',
            'product_name' => 'required',
            'amount' => 'required|numeric',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function messages()
    {
        return [
            'vendor_name.required' => 'Vendor Name is required',
            'product_name.required' => 'Product Name is required',
            'amount.required' => 'Amount is required',
        ];
    }
}
