<?php

namespace App\Modules\Vendor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Modules\Vendor\Repositories\VendorInterface;
use App\Modules\Vendor\Http\Requests\VendorRequest;
use App\Modules\Dropdown\Repositories\DropdownInterface;

// For excel
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class VendorController extends Controller
{ 
    protected $vendor, $dropdown;
    
    public function __construct(VendorInterface $vendor, DropdownInterface $dropdown)
    {
        $this->dropdown = $dropdown;
        $this->vendor = $vendor;
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $search = $request->all();
        $data['categories'] = $this->dropdown->getFieldBySlug('vendor_category');
        $data['vendor'] = $this->vendor->findAll($limit= 50, $search);
        return view('vendor::vendor.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['is_edit'] = false;
        $data['categories'] = $this->dropdown->getFieldBySlug('vendor_category');
        return view('vendor::vendor.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        try{ 
            $vendor_data = array(

                    'vendor_name' =>$data['vendor_name'],
                    'address' =>$data['address'],
                    'contact_number' =>$data['contact_number'],
                    'email' =>$data['email'],
                    'pan_vat_no' =>$data['pan_vat_no'],
                    'vendor_category_id' =>$data['vendor_category_id'],
                    'factory_location' =>$data['factory_location'],
                    'office_location' =>$data['office_location']

            );

            $vendorInfo = $this->vendor->save($vendor_data);
            $vendor_id = $vendorInfo->id;

            $content = $data['contact_person'];
            $countcontent = sizeof($content);

                for($i = 0; $i < $countcontent; $i++){
           
                     if($data['contact_person'][$i]){
                         $strengthdata['vendor_id'] = $vendor_id;
                         $strengthdata['contact_person'] = $data['contact_person'][$i];
                         $strengthdata['mobile1'] = $data['mobile1'][$i];
                         $strengthdata['mobile2'] = $data['mobile2'][$i];
                         $strengthdata['personal_email'] = $data['personal_email'][$i];

                         $this->vendor->saveContent($strengthdata);
                     }

                         
                 }

            toastr()->success('Vendor Created Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('vendor.index'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('vendor::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data['is_edit'] = true;
        $data['vendor'] = $this->vendor->find($id);
        $data['categories'] = $this->dropdown->getFieldBySlug('vendor_category');
        
        return view('vendor::vendor.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        try{

             $vendor_data = array(

                    'vendor_name' =>$data['vendor_name'],
                    'address' =>$data['address'],
                    'contact_number' =>$data['contact_number'],
                    'email' =>$data['email'],
                    'pan_vat_no' =>$data['pan_vat_no'],
                    'vendor_category_id' =>$data['vendor_category_id'],
                    'factory_location' =>$data['factory_location'],
                    'office_location' =>$data['office_location']

            );

            $this->vendor->update($id,$vendor_data);

            $vendor_id = $id;
            $this->vendor->deleteContent($id);

            
            $content = $data['contact_person'];
            $countcontent = sizeof($content);

                for($i = 0; $i < $countcontent; $i++){
           
                     if($data['contact_person'][$i]){
                         $strengthdata['vendor_id'] = $vendor_id;
                         $strengthdata['contact_person'] = $data['contact_person'][$i];
                         $strengthdata['mobile1'] = $data['mobile1'][$i];
                         $strengthdata['mobile2'] = $data['mobile2'][$i];
                         $strengthdata['personal_email'] = $data['personal_email'][$i];

                          $this->vendor->saveContent($strengthdata);
                     }                        
                 }

            toastr()->success('Vendor Updated Successfully');
        }catch(\Throwable $e){
           toastr()->error($e->getMessage());
       }

       return redirect(route('vendor.index'));
   }

   public function getVendorDetailAjax(Request $request){
         if($request->ajax()){

            $vendor_id = $request->vendor_id;
            $vendorDetail = $this->vendor->findVendorContact($vendor_id); 
            $data = view('vendor::vendor.partial.vendor-detail-ajax',compact('vendorDetail'))->render();
            return response()->json(['options'=>$data]);
         }
   }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $this->vendor->delete($id);
            toastr()->success('Vendor Deleted Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        return redirect(route('vendor.index'));  
    }

    public function appendContact(){
         $data = view('vendor::vendor.partial.add-contact-ajax')->render();
        return response()->json(['options'=>$data]);
    }

    public function upload(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('upload_vendor')) {
            $file_info = $request->upload_vendor->getClientOriginalName();
            $extension = \File::extension($file_info);
            if ($extension == 'xlsx' || $extension == 'xlx') {
                $file_directory = public_path().'/uploads/uploads_xls/';
                $new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $extension;
                if(move_uploaded_file($_FILES['upload_vendor']['tmp_name'], $file_directory . $new_file_name))
                {    
                    $inputFileName = $file_directory . $new_file_name;
                    $file_type = IOFactory::identify($inputFileName);
                    $objReader = IOFactory::createReader($file_type);

                    $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                    $vendor_data  = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

                    array_shift($vendor_data);
                    foreach ($vendor_data as $key => $vendor) {
                        $findvendor = $this->vendor->findUniqueVendor($vendor['A'], $vendor['C']);

                        if (!$findvendor) {

                              $vendor_category = $this->dropdown->getFieldBySlug('vendor_category');
                                $fid = $this->dropdown->getFieldIdFromSlug('vendor_category')->first()->fid;

                                $find = $this->dropdown->findByTitle($fid, $vendor['F']);
                                if ($find->count() < 1 && $vendor['F']) {
                                    $newDropdown = $this->dropdown->save([
                                        'fid' => $fid,
                                        'dropvalue' => $vendor['F']
                                    ]);
                                    $vendor['F'] = $newDropdown->id;
                                } else {
                                    $vendor['F'] = $find->first()->id;
                                }

                            $data = [
                                'vendor_name' => $vendor['A'],
                                'address' => $vendor['B'],
                                'contact_number' => $vendor['C'],
                                'email' => $vendor['D'],
                                'pan_vat_no' => $vendor['E'],
                                'vendor_category_id' => $vendor['F'],
                                'factory_location' => $vendor['G'],
                                'office_location' => $vendor['H'],

                            ];
                            try {
                                $this->vendor->save($data);
                            } catch (\Throwable $e) {
                                toastr($e->getMessage())->error();
                            }
                        }
                    }
                }
                toastr()->success('Vendor Details Added Successfully');
                return redirect()->back();
            }
        }
        toastr()->error('Please Choose a Correct file');
        return redirect()->back();
    }

    public function downloadExcel(){

        $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
    
            ];
        
        $year = date('Y');
        
        $objPHPExcel = new Spreadsheet();        
        $worksheet = $objPHPExcel->getActiveSheet();

        $objPHPExcel->setActiveSheetIndex(0);
        // set Header

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Vendor Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Address');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Contact Number');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Pan/Vat Number');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Vendor Category');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Factory Location');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Office Location');
         
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);
          
        
        
        $vendor_info = $this->vendor->findAll();
        $num = 2;
        
        foreach($vendor_info as $key => $value){
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$num, $value->vendor_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$num, $value->address);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$num, $value->contact_number);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$num, $value->email);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$num, $value->pan_vat_no);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$num, $value->vendorCategory['dropvalue']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$num, $value->factory_location);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$num, $value->office_location);
           
            $num++;
        }
         
        $writer = new Xlsx($objPHPExcel);
        $file = 'vendor_'.$year;
        $filename = $file.'.xlsx';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache 
        
        $writer->save('php://output');

    }



}
