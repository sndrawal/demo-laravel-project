@extends('admin::layout')
@section('title')Vendor @stop 
@section('breadcrum')Create Vendor @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->
<script src="{{ asset('admin/validation/vendor.js')}}"></script>

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Create Vendor</h5>
        <div class="header-elements">

        </div>
    </div>
    

    <div class="card-body">

        {!! Form::open(['route'=>'vendor.store','method'=>'POST','class'=>'form-horizontal','id'=>'vendor_submit','role'=>'form','files' => true]) !!}
        
            @include('vendor::vendor.partial.action',['btnType'=>'Save']) 
        
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@stop