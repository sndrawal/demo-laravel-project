<!DOCTYPE html>
<html>
<head>
    <title>Vendor Purchase Report</title>
    <link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/global/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">

    <!-- Core JS files -->
    <script src="{{asset('admin/global/js/main/jquery.min.js')}}"></script>

    <style type="text/css" media="print">
        @page {
            size: auto;   /* auto is the initial value */
            margin: 0;  /* this affects the margin in the printer settings */
        }
    </style>
</head>
<body class="card">
    <center>
        <h3>
            <b>{{ projectSetting() ? projectSetting()->company_name : "Demo Construction" }}</b><br>
            Vendor Purchase Report<br>
        </h3>
    </center>
    <div class="col-md-8 offset-md-2">
        <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="3" class="text-center"><b>Site Wise Purchases</b></th>
            </tr>
            <tr>
                <th>S.no.</th>
                <th>Site</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @php
                $total = 0;
            @endphp
            @foreach ($data as $value)
                <tr>
                    <td>{{ ++$loop->index }}</td>
                    <td>{{ $value['project_bid'] }}</td>
                    <td>{{ $value['vendor_purchases'] }}</td>
                </tr>
                @php
                    $total += $value['vendor_purchases'];
                @endphp
            @endforeach
            <tr>
                <td></td>
                <td><b>Total Amount</b></td>
                <td><b>Rs. {{ $total }}</b></td>
            </tr>
        </tbody>
    </table>
    </div>
    <div class="card-body">
        <button class="btn btn-primary float-right" type="button" onclick="print_procurement_daily_report();">
            <i class="icon-printer"></i> Print
        </button>
    </div>

    <script>
        function print_procurement_daily_report()
        {
            $('.btn').hide();
            window.print();
            $('.btn').show();
        }
    </script>
</body>
</html>