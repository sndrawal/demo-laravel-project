<div class="mt-3 mb-3">
    <div class="row">
        <div class="col-lg-12">
            <div class="w-100 d-flex justify-content-end">
                <div class="btn-group pl-2" data-popup="tooltip" data-placement="top" data-original-title="Import">
                    <a href="#" class="btn bg-grey" data-toggle="modal" data-target="#modal_default"><i class="icon-import"></i></a>
                </div>
                <div id="modal_default" class="modal fade" tabindex="-1">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Import</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="bd-import">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4>Upload Vendor Data Sheet</h4>
                                            <form method="POST" action="{{ route('vendor.upload') }}" accept-charset="UTF-8" class="form-horizontal" role="form" enctype="multipart/form-data" id="equipment-upload-form">
                                                @csrf
                                                <div class="position-relative">
                                                    <input type="file" class="form-control h-auto" name="upload_vendor">
                                                </div>
                                                <span class="form-text text-muted">Accepted formats: xls, xlsx</span>
                                                <button type="submit" class="btn bg-primary mt-3">Upload</button>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card mb-0 bg-grey">
                                                <div class="card-body text-center">
                                                    <i class="icon-file-spreadsheet icon-2x border-3 rounded-round p-3 mb-3 mt-1"></i>
                                                    <h6>Get Vendor Data Sample Sheet</h6>
                                                    <p>
                                                        Before Uploading Vendor Data - Please make sure, You have correct Vendor Data Format as similar to Sample Vendor Data.
                                                    </p>
                                                    <a href="{{ url('samples/vendor_upload_sample.xlsx') }}" target="_blank" class="btn bg-white">
                                                        <i class="icon-download4 mr-2"></i> Download
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn bg-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            <!--     <div class="pl-2 justify-content-center">
                    <a href="{{ route('equipment.downloadExcel', request()->all()) }}" class="btn bg-indigo-400" target="_blank">
                        <i class="icon-file-excel"></i>
                    </a>
                </div> -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
 $(document).ready(function () {
    $('#equipment-upload-form').validate({
        rules: {
            upload_vendor: 'required'
        },
        messages: {
            upload_vendor: "Please Select A File."
        },
        errorElement: "em",
        errorPlacement: function (error, element) {  console.log(element)
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents(".col-lg-9").addClass("form-group-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element.parent());
            }

            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!element.parent().parent().next("div")[0]) {
                $("<div class='form-control-feedback'><i class='icon-cross2 text-danger'></i></div>").insertAfter(element);
            }
        },
        success: function (label, element) { console.log(element);
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!$(element).next("div")[0]) { 
                $("<div class='form-control-feedback'><i class='icon-checkmark4 text-success'></i></div>").insertAfter($(element));
            }
        },
        highlight: function (element, errorClass, validClass) {  
            $(element).parent().find('span .input-group-text').addClass("alpha-danger text-danger border-danger ").removeClass("alpha-success text-success border-success");
            $(element).addClass("border-danger").removeClass("border-success");
            $(element).parent().parent().addClass("text-danger").removeClass("text-success");
            $(element).next('div .form-control-feedback').find('i').addClass("icon-cross2 text-danger").removeClass("icon-checkmark4 text-success");
        },
        unhighlight: function (element, errorClass, validClass) { 
            $(element).parent().find('span .input-group-text').addClass("alpha-success text-success border-success").removeClass("alpha-danger text-danger border-danger ");
            $(element).addClass("border-success").removeClass("border-danger");
            $(element).parent().parent().addClass("text-success").removeClass("border-danger");
            $(element).next('div .form-control-feedback').find('i').addClass("icon-checkmark4 text-success").removeClass("icon-cross2 text-danger");
        }
    });
});
</script>