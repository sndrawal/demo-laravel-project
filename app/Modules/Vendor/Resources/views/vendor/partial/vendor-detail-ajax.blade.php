<table class="table table-striped">
    <thead>
        <tr class="bg-slate-400">
            <th>S.N.</th>
            <th>Contact Person</th>
            <th>Mobile 1</th>
            <th>Mobile 2</th>
            <th>Personal Email</th>
        </tr>
    </thead>
    <tbody>
        @php $total = 0; @endphp
       @if($vendorDetail->total() != 0)

       @foreach($vendorDetail as $key => $value)

       <tr>
            <td>{{ $vendorDetail->firstItem() +$key }}</td>
            <td>{{ $value->contact_person }}</td>
            <td>{{ $value->mobile1 }}</td>
            <td>{{ $value->mobile2}}</td>
            <td>{{ $value->personal_email}}</td>
        </tr>

    @endforeach
     
    @else
    <tr>
        <td colspan="6">No Vendor Contact Added !!!</td>
    </tr>
    @endif
</tbody>

</table>