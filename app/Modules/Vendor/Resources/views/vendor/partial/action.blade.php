<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

    <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Vendor Name:<span class="text-danger">*</span></label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-truck"></i></span>
                    </span>
                    {!! Form::text('vendor_name', null, ['id'=>'vendor_name','placeholder'=>'Enter Vendor Name','class'=>'form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
               <label class="col-form-label col-lg-3">Vendor Category:<span class="text-danger">*</span></label>
               <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                   <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-truck"></i></span>
                    </span>
                {!! Form::select('vendor_category_id', $categories, null, ['placeholder'=>'--Select Vendor Category--','class'=>'form-control']) !!}
                </div>
               </div>
            </div>
        </div>

    </div>


     <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
              <label class="col-form-label col-lg-3">Address:<span class="text-danger">*</span></label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                     <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-location3"></i></span>
                     </span>
                    {!! Form::text('address', null, ['id'=>'address','placeholder'=>'Enter Address','class'=>'form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Contact Number:<span class="text-danger">*</span></label>
               <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-address-book3"></i></span>
                      </span>
                    {!! Form::text('contact_number', null, ['id'=>'contact_number','placeholder'=>'Enter Contact Number','class'=>'form-control numeric']) !!}
                </div>
               </div>
            </div>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
              <label class="col-form-label col-lg-3">Pan/Vat Number:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-address-book3"></i></span>
                        </span>
                        {!! Form::text('pan_vat_no', null, ['placeholder'=>'Enter Pan/Vat Number','class'=>'form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                 <label class="col-form-label col-lg-3">Email:<span class="text-danger">*</span></label>
               <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-envelop"></i></span>
                    </span>
                    {!! Form::text('email', null, ['id'=>'email','placeholder'=>'Enter Email','class'=>'form-control']) !!}
                </div>
               </div>
            </div>
        </div>

    </div> 

      <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
              <label class="col-form-label col-lg-3">Factory Location:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-location4"></i></span>
                        </span>
                        {!! Form::text('factory_location', null, ['id'=>'factory_location','placeholder'=>'Enter Factory Location','class'=>'form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                 <label class="col-form-label col-lg-3">Office Location</label>
               <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-location4"></i></span>
                    </span>
                    {!! Form::text('office_location', null, ['id'=>'office_location','placeholder'=>'Enter Office Location','class'=>'form-control']) !!}
                </div>
               </div>
            </div>
        </div>

    </div>

</fieldset>


<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold">Contact Information</legend>


    @if($is_edit)
        @foreach($vendor->VendorContact as $key => $valcontent)

            <div class="appendcontact">
                <div class="form-group row">
                    <div class="col-lg-3">
                        <div class="row">
                             <label class="col-form-label col-lg-3">Contact Person:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                      <span class="input-group-text"><i class="icon-address-book"></i></span>
                                    </span>
                                    {!! Form::text('contact_person[]', $valcontent->contact_person, ['id'=>'contact_person','placeholder'=>'Enter Contact Person','class'=>'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                           <label class="col-form-label col-lg-3">Mobile 1:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-address-book3"></i></span>
                                    </span>
                                    {!! Form::text('mobile1[]', $valcontent->mobile1, ['id'=>'mobile1','placeholder'=>'Enter Mobile Number 1','class'=>'form-control numeric']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label class="col-form-label col-lg-3">Mobile 2:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-address-book3"></i></span>
                                    </span>
                                    {!! Form::text('mobile2[]', $valcontent->mobile2, ['id'=>'mobile2','placeholder'=>'Enter Mobile Number 2','class'=>'form-control numeric']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label class="col-form-label col-lg-3">Email:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-hat"></i>
                                        </span>
                                    </span>
                                    {!! Form::email('personal_email[]', $valcontent->personal_email, ['id'=>'personal_email','placeholder'=>'Enter Personal Email','class'=>'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
</div>


        @endforeach
    @endif

    <div class="appendcontact">
        <div class="form-group row">
            <div class="col-lg-3">
                <div class="row">
                     <label class="col-form-label col-lg-3">Contact Person:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                              <span class="input-group-text"><i class="icon-address-book"></i></span>
                            </span>
                            {!! Form::text('contact_person[]', null, ['id'=>'contact_person','placeholder'=>'Enter Contact Person','class'=>'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="row">
                   <label class="col-form-label col-lg-3">Mobile 1:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-address-book3"></i></span>
                            </span>
                            {!! Form::text('mobile1[]', null, ['id'=>'mobile1','placeholder'=>'Enter Mobile Number 1','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="row">
                    <label class="col-form-label col-lg-3">Mobile 2:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-address-book3"></i></span>
                            </span>
                            {!! Form::text('mobile2[]', null, ['id'=>'mobile2','placeholder'=>'Enter Mobile Number 2','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="row">
                    <label class="col-form-label col-lg-3">Email:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-hat"></i>
                                </span>
                            </span>
                            {!! Form::email('personal_email[]', $value = null, ['id'=>'personal_email','placeholder'=>'Enter Personal Email','class'=>'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<div class="col-lg-2 float-right">
             <div class="row">
                <button type="button" class="add_contact btn bg-success-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Add More Information</button>
            </div>
        </div>

</fieldset>




<div class="text-right">
     <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>{{ $btnType }}</button>
</div>



 <script type="text/javascript">
    $(document).ready(function(){ 
        $('.add_contact').on('click',function(){
            $.ajax({
                    type: 'GET',
                    url: '/admin/vendor/appendContact',
                    success: function (data) {
                        $('.appendcontact').last().append(data.options); 
                    }
                });
        });
    }); 
        </script>