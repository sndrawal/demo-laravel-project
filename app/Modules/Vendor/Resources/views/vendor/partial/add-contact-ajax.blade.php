 <div class="appendcontact">
            <div class="form-group row">
                <div class="col-lg-3">
                <div class="row">
                     <label class="col-form-label col-lg-3">Contact Person:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                              <span class="input-group-text"><i class="icon-address-book"></i></span>
                            </span>
                            {!! Form::text('contact_person[]', null, ['id'=>'contact_person','placeholder'=>'Enter Contact Person','class'=>'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="row">
                   <label class="col-form-label col-lg-3">Mobile 1:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                             <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-address-book3"></i></span>
                            </span>
                            {!! Form::text('mobile1[]', null, ['id'=>'mobile1','placeholder'=>'Enter Mobile Number 1','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="row">
                    <label class="col-form-label col-lg-3">Mobile 2:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-address-book3"></i></span>
                            </span>
                            {!! Form::text('mobile2[]', null, ['id'=>'mobile2','placeholder'=>'Enter Mobile Number 2','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="row">
                    <label class="col-form-label col-lg-3">Email:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-hat"></i>
                                </span>
                            </span>
                            {!! Form::email('personal_email[]', $value = null, ['id'=>'personal_email','placeholder'=>'Enter Personal Email','class'=>'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>

                <div class="col-lg-2">
                   <div class="row">
                           <button type="button" class="ml-2 remove_contact btn bg-danger-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Remove</button>
                    </div>
                </div>
            </div>
        </div>




<script type="text/javascript">
    $(document).ready(function(){ 
       
        $('.remove_contact').on('click',function(){ 
            $(this).parent().parent().parent().remove();
        });
        
    });
</script>