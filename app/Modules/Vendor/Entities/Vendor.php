<?php

namespace App\Modules\Vendor\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Dropdown\Entities\Dropdown;

class vendor extends Model
{
    protected $fillable = [
        
        'vendor_name',
        'address',
        'contact_number',
        'email',
        'pan_vat_no',
        'vendor_category_id',
        'factory_location',
        'office_location'
        
    ];

    public function VendorContact()
    {
        return $this->hasMany(VendorContact::class, 'vendor_id');
    }


    public function vendorCategory(){
        return $this->belongsTo(Dropdown::class,'vendor_category_id','id');
    }
    
}
