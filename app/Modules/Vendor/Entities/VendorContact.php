<?php

namespace App\Modules\Vendor\Entities;

use Illuminate\Database\Eloquent\Model;

class VendorContact extends Model
{
    protected $fillable = [

    	'vendor_id',
    	'contact_person',
    	'mobile1',
        'mobile2',
        'personal_email'
    ];
}
