<?php

namespace App\Modules\Vendor\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

use App\Modules\Vendor\Repositories\VendorTypeInterface;
use App\Modules\Vendor\Repositories\VendorTypeRepository;
use App\Modules\Vendor\Repositories\VendorInterface;
use App\Modules\Vendor\Repositories\VendorRepository;

class VendorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->vendorTypeRegister();
        $this->vendorRegister();
    }
    
    public function vendorTypeRegister(){
        $this->app->bind(
            VendorTypeInterface::class,
            VendorTypeRepository::class
        );
    }
    
    public function vendorRegister(){
        $this->app->bind(
            VendorInterface::class,
            VendorRepository::class
        );
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('vendor.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'vendor'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/vendor');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/vendor';
        }, \Config::get('view.paths')), [$sourcePath]), 'vendor');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/vendor');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'vendor');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'vendor');
        }
    }

    /**
     * Register an additional directory of factories.
     * 
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
