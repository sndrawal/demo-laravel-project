<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {

        /*
        |--------------------------------------------------------------------------
        | Vendor  CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('vendor', ['as' => 'vendor.index', 'uses' => 'VendorController@index']);
        Route::post('vendor/upload', ['as' => 'vendor.upload', 'uses' => 'VendorController@upload']);
        //Vendor Create
        Route::get('vendor/create', ['as' => 'vendor.create', 'uses' => 'VendorController@create']);
        Route::post('vendor/store', ['as' => 'vendor.store', 'uses' => 'VendorController@store']);
        //Vendor Edit
        Route::get('vendor/edit/{id}', ['as' => 'vendor.edit', 'uses' => 'VendorController@edit'])->where('id','[0-9]+');
        Route::put('vendor/update/{id}', ['as' => 'vendor.update', 'uses' => 'VendorController@update'])->where('id','[0-9]+');
        //Vendor Delete
        Route::get('vendor/delete/{id}', ['as' => 'vendor.delete', 'uses' => 'VendorController@destroy'])->where('id','[0-9]+');
        Route::get('vendor/report/{id}', 'VendorController@report')->name('vendor.report');

        Route::get('vendor/appendContact', ['as' => 'vendor.appendContact', 'uses' => 'VendorController@appendContact']);

        Route::post('vendor/vendor.get-vendor-detail-ajax', ['as'=>'vendor.get-vendor-detail-ajax','uses'=>'VendorController@getVendorDetailAjax']);

        Route::get('vendor/downloadExcel', ['as' => 'vendor.downloadExcel', 'uses' => 'VendorController@downloadExcel']);

});
