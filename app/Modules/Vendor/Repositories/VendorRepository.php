<?php 
namespace App\Modules\Vendor\Repositories;

use App\Modules\Vendor\Entities\Vendor;
use App\Modules\Vendor\Entities\VendorContact;
use App\Modules\ProjectRequisition\Entities\MaterialPurchaseOrder;

class VendorRepository implements VendorInterface
{

    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result =Vendor::when(array_keys($filter, true), function ($query) use ($filter) {
           
                if (isset($filter['vendor_category_id']) && !is_null($filter['vendor_category_id'])) {
                    $query->whereIn('vendor_category_id', $filter['vendor_category_id']);
                }
                if (isset($filter['vendor_name']) && !is_null($filter['vendor_name'])) {
                    $query->where('vendor_name', 'like', '%'.$filter['vendor_name'].'%');
                }
                
        })->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 

    }

    public function find($id)
    {
        return Vendor::find($id);
    }

    public function findVendorContact($id, $limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]){

        $result =VendorContact::when(array_keys($filter, true), function ($query) use ($filter) {
           
        })->where('vendor_id','=',$id)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 

    }


    public function getList()
    {  
        $vendorType = Vendor::pluck('vendor_name', 'id');

        return $vendorType;
    }

    public function save($data)
    {
        return Vendor::create($data);
    }

    public function saveContent($data)
    {
        return VendorContact::create($data);
    }

    public function update($id,$data)
    {
        $vendor = Vendor::find($id);
        return $vendor->update($data);
    }

    public function delete($id)
    {
        return Vendor::destroy($id);
    }   

    public function deleteContent($id)
    {
        return VendorContact::where('vendor_id','=',$id)->delete($id);
    }

    public function countTotal()
    {
        return Vendor::count();
    }


    public function totalPurchase($pid, $vendor_id)
    {
        $purchaseOrders = MaterialPurchaseOrder::whereProjectId($pid)
        ->whereVendorId($vendor_id)
        ->get();
        $total = 0;
        foreach ($purchaseOrders as $key => $order) {
            foreach ($order->details as $key => $value) {
                $total += $value->amount;
            }
        }
        return $total;
    }

    public function findUniqueVendor($name, $contact_number)
    {
        return Vendor::where('vendor_name', $name)->where('contact_number', $contact_number)->first();
    }
}