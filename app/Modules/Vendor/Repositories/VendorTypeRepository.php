<?php 
namespace App\Modules\Vendor\Repositories;


use App\Modules\Vendor\Entities\VendorType;

class VendorTypeRepository implements VendorTypeInterface
{
    
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result = VendorType::orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;
    }
    
    public function find($id){
        return VendorType::find($id);
    }
    
   public function getList(){  
       $vendorType = VendorType::pluck('name', 'id');
      
       return $vendorType;
   }
    
    public function save($data){
        return VendorType::create($data);
    }
    
    public function update($id,$data){
        $vendorType = VendorType::find($id);
        return $vendorType->update($data);
    }
    
    public function delete($id){
        return VendorType::destroy($id);
    }
}