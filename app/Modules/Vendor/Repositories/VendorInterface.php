<?php

namespace App\Modules\Vendor\Repositories;

interface VendorInterface
{
    public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function findVendorContact($id);
    
    public function getList();
    
    public function save($data);

    public function saveContent($data);

    public function update($id,$data);

    public function delete($id);

    public function deleteContent($id);
    
    public function countTotal();

    public function totalPurchase($pid, $vendor_id);

    public function findUniqueVendor($name, $contact_number);
}