<?php

namespace App\Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;

class RequisitionDetail extends Model
{

    protected $fillable = [

    	'requisition_id',
    	'material_name',
    	'qty',
    	'rate',
    	'amount',
    	'remark',
    	'status'

    ];
    
}
