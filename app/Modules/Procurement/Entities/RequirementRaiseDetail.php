<?php

namespace App\Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;

class RequirementRaiseDetail extends Model
{

    protected $fillable = [

    	'requirement_raise_id',
    	'material_name',
    	'qty',
    	'purpose',
    	'remark'

    ];
    
}
