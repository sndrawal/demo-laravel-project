<?php

namespace App\Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\User\Entities\User;
use App\Modules\Dropdown\Entities\Dropdown;
use App\Modules\Procurement\Entities\RequirementRaiseDetail;

class RequirementRaise extends Model
{

    protected $fillable = [

    	'raise_by',
    	'department_id',
    	'status',
        'date',
    	'created_by'

    ];

    public function requirementDetail(){
        return $this->hasMany(RequirementRaiseDetail::class,'requirement_raise_id','id');
    }
    
    public function userInfo(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function department(){
        return $this->belongsTo(Dropdown::class,'department_id');
    }
}
