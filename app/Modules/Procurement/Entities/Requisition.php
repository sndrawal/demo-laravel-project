<?php

namespace App\Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\User\Entities\User;
use App\Modules\Dropdown\Entities\Dropdown;
use App\Modules\Procurement\Entities\RequirementRaise;
use App\Modules\Procurement\Entities\RequisitionDetail;

class Requisition extends Model
{

    protected $fillable = [

    	'requirement_raise_id',
    	'status',
    	'approved_by'

    ];

    public function requirementInfo(){
        return $this->belongsTo(RequirementRaise::class,'requirement_raise_id');
    }
    
   public function requisitionDetail(){
        return $this->hasMany(RequisitionDetail::class,'requisition_id','id');
    }

    public function approveInfo(){
        return $this->belongsTo(User::class,'approved_by');
    }
}
