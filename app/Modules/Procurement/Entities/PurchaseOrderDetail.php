<?php

namespace App\Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Procurement\Entities\RequisitionDetail;

class PurchaseOrderDetail extends Model
{
    protected $fillable = [

    	'purchase_order_id',
    	'requisition_detail_id',
    	'material_name',
    	'qty',
    	'rate',
    	'amount'

    ];

    public function requisitionDetail(){
        return $this->belongsTo(RequisitionDetail::class,'requisition_detail_id');
    }
    
}
