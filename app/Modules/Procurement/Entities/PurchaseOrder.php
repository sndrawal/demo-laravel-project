<?php

namespace App\Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Vendor\Entities\Vendor;
use App\Modules\Warehouse\Entities\Warehouse;
use App\Modules\Procurement\Entities\PurchaseOrderDetail;

class PurchaseOrder extends Model
{

    protected $fillable = [


    	'voucher_no',
    	'mode_of_payment',
    	'bank_name',
    	'cheque_no',
    	'platform',
    	'no_of_day',
    	'supplier_ref_no',
    	'vendor_id',
    	'dispatch_through', 
    	'destination',
    	'terms_of_delivery',
    	'site',
    	'warehouse_id',
    	'distribution_channel',
    	'issued_by',
    	'sub_total',
        'discount_percent',
        'discount_amount',
        'total_after_discount',
        'vat_amount', 
        'grand_total',
        'number_in_words',

    ];

    public function purchaseOrderDetail(){
        return $this->hasMany(PurchaseOrderDetail::class,'purchase_order_id','id');
    }

    public function issueInfo(){
        return $this->belongsTo(User::class,'issued_by');
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class,'warehouse_id');
    }

    public function vendor(){
        return $this->belongsTo(Vendor::class,'vendor_id');
    }

    
}
