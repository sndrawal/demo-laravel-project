<?php 
namespace App\Modules\Procurement\Repositories;

use App\Modules\Procurement\Entities\RequirementRaise;
use App\Modules\Procurement\Entities\RequirementRaiseDetail;

class RequirementRaiseRepository implements RequirementRaiseInterface
{

    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result =RequirementRaise::when(array_keys($filter, true), function ($query) use ($filter) {

            
            if (isset($filter['search_from_to'])) { 

                $dateRange = explode('---', $filter['search_from_to']);
                
                $query->where('date', '>=', $dateRange[0]);
                $query->where('date', '<=', $dateRange[1]);
            }

            if (isset($filter['department_id']) && !is_null($filter['department_id'])) {
                $query->whereIn('department_id', $filter['department_id']);
            }
            if (isset($filter['raise_by']) && !is_null($filter['raise_by'])) {
                $query->whereIn('raise_by', $filter['raise_by']);
            }
                
        })->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 

    }

    public function find($id)
    {
        return RequirementRaise::find($id);
    }


    public function getList()
    {  
        $result = RequirementRaise::pluck('vendor_name', 'id');

        return $result;
    }

    public function save($data)
    {
        return RequirementRaise::create($data);
    }
    
    public function saveDetail($data)
    {
        return RequirementRaiseDetail::create($data);
    }

    public function update($id,$data)
    {
        $result = RequirementRaise::find($id);
        return $result->update($data);
    }

    public function delete($id)
    {
        return RequirementRaise::destroy($id);
    }   

    public function deleteDetail($id){
      return RequirementRaiseDetail::where('requirement_raise_id','=',$id)->delete();
    }

    public function findDetail($pid){
      return RequirementRaiseDetail::where('requirement_raise_id','=',$pid)->get();
    }

    public function getUniqueRaiseBy(){
        return RequirementRaise::select('raise_by')
        ->groupBy('raise_by')
        ->pluck('raise_by', 'raise_by')
        ->filter(function($value, $key) {
            return  $value != null;
        });
    }

}