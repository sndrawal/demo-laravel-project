<?php 
namespace App\Modules\Procurement\Repositories;

use App\Modules\Procurement\Entities\PurchaseOrder;
use App\Modules\Procurement\Entities\PurchaseOrderDetail;

class PurchaseOrderRepository implements PurchaseOrderInterface
{

    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result =PurchaseOrder::when(array_keys($filter, true), function ($query) use ($filter) {
           
            if (isset($filter['search_from_to'])) { 

                $dateRange = explode('---', $filter['search_from_to']);
                
                $query->where('created_at', '>=', $dateRange[0]);
                $query->where('created_at', '<=', $dateRange[1]);
            }

            if (isset($filter['voucher_no']) && !is_null($filter['voucher_no'])) {
                $query->whereIn('voucher_no', $filter['voucher_no']);
            }
            if (isset($filter['vendor_id']) && !is_null($filter['vendor_id'])) {
                $query->whereIn('vendor_id', $filter['vendor_id']);
            }
            if (isset($filter['warehouse_id']) && !is_null($filter['warehouse_id'])) {
                $query->whereIn('warehouse_id', $filter['warehouse_id']);
            }
            if (isset($filter['supplier_ref_no']) && !is_null($filter['supplier_ref_no'])) {
                $query->whereIn('supplier_ref_no', $filter['supplier_ref_no']);
            }
                
        })->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 

    }

    public function find($id)
    {
        return PurchaseOrder::find($id);
    }

    public function save($data)
    {
        return PurchaseOrder::create($data);
    }
    
    public function saveDetail($data)
    {
        return PurchaseOrderDetail::create($data);
    }

    public function getUniqueVoucher(){
        return PurchaseOrder::select('voucher_no')
        ->groupBy('voucher_no')
        ->pluck('voucher_no', 'voucher_no')
        ->filter(function($value, $key) {
            return  $value != null;
        });
    }
    
    public function getUniqueSupplierref(){
        return PurchaseOrder::select('supplier_ref_no')
        ->groupBy('supplier_ref_no')
        ->pluck('supplier_ref_no', 'supplier_ref_no')
        ->filter(function($value, $key) {
            return  $value != null;
        });
    }

}