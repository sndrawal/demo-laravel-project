<?php 
namespace App\Modules\Procurement\Repositories;

use App\Modules\Procurement\Entities\Requisition;
use App\Modules\Procurement\Entities\RequisitionDetail;

class RequisitionRepository implements RequisitionInterface
{

    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result =Requisition::when(array_keys($filter, true), function ($query) use ($filter) {
           
                
        })->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 

    }

    public function findByReq($req_id){
        return Requisition::where('requirement_raise_id','=',$req_id)->get();
    }


    public function find($id)
    {
        return Requisition::find($id);
    }


    public function getList()
    {  
        $result = Requisition::pluck('vendor_name', 'id');

        return $result;
    }

    public function save($data)
    {
        return Requisition::create($data);
    }
    
    public function saveDetail($data)
    {
        return RequisitionDetail::create($data);
    }

    public function update($id,$data)
    {
        $result = Requisition::find($id);
        return $result->update($data);
    }
    
    public function updateDetail($id,$data)
    {
        $result = RequisitionDetail::find($id);
        return $result->update($data);
    }

    public function delete($id)
    {
        return Requisition::destroy($id);
    }   

    public function deleteDetail($id){
      return RequisitionDetail::where('requisition_id','=',$id)->delete();
    }


    public function NumberIntoWords($number){

       $no = round($number);  
       $point = round($number - $no, 2) * 100; 

       $decimal = explode('.', number_format($number, 2))[1];
       $firstdecimal = intval($decimal/10)*10;
       $lastdecimal = $decimal % 10;

       
       $hundred = null;
       $digits_1 = strlen($no);
       $i = 0;
       $str = array();
       $words = array('0' => '', '1' => 'One', '2' => 'Two',
        '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
        '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
        '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
        '13' => 'Thirteen', '14' => 'Fourteen',
        '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
        '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
        '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
        '60' => 'Sixty', '70' => 'Seventy',
        '80' => 'Eighty', '90' => 'Ninety');
       $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
       while ($i < $digits_1) {
         $divider = ($i == 2) ? 10 : 100;
         $number = floor($no % $divider);
         $no = floor($no / $divider);
         $i += ($divider == 10) ? 1 : 2;
         if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                " " . $digits[$counter] . $plural . " " . $hundred
                :
                $words[floor($number / 10) * 10]
                . " " . $words[$number % 10] . " "
                . $digits[$counter] . $plural . " " . $hundred;
         } else $str[] = null;
      }
      $str = array_reverse($str);
      $result = implode('', $str);

      $points = ($point) ?  $words[$firstdecimal] . " " . $words[$lastdecimal] : 'zero';

      return $result . "Rupees  " . $points . " Paise Only";

    }

    public function findDetail($pid){
      return RequisitionDetail::where('requisition_id','=',$pid)->get();
    }   

    public function checkApprovedDetail($pid){
      return RequisitionDetail::where('status','!=','Pending')->where('requisition_id','=',$pid)->count();
    }

    public function getApprovedRequisition(){
      return RequisitionDetail::where('status','=','Approved')->get();
    }



}