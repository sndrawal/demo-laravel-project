<?php

namespace App\Modules\Procurement\Repositories;

interface RequirementRaiseInterface
{
    public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function getList();
    
    public function save($data);
    public function saveDetail($data);

    public function update($id,$data);

    public function delete($id);
    
    public function deleteDetail($id);
    
    public function findDetail($pid);
    
    public function getUniqueRaiseBy();

}