<?php

namespace App\Modules\Procurement\Repositories;

interface PurchaseOrderInterface
{
    public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
        
    public function save($data);
    public function saveDetail($data);
    
    public function getUniqueVoucher();
    
    public function getUniqueSupplierref();
 }