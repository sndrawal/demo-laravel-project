<?php

namespace App\Modules\Procurement\Repositories;

interface RequisitionInterface
{
    public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function findByReq($req_id);

    public function find($id);
    
    public function getList();
    
    public function save($data);
    public function saveDetail($data);

    public function update($id,$data);
    public function updateDetail($id,$data);

    public function delete($id);
    
    public function deleteDetail($id);
    
    public function NumberIntoWords($number);

    public function findDetail($pid);

    public function checkApprovedDetail($pid);
    
    public function getApprovedRequisition();

}