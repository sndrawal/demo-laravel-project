<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

    <table class="table table-striped">
        <thead>
            <tr class="bg-purple-400">
                <th>Raise By</th>
                <th>Department</th>
                <th>Date</th>
                <th>Created By</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ optional($requisition->requirementInfo)->raise_by }}</td>
                <td>{{ optional($requisition->requirementInfo->department)->dropvalue }}</td>
                <td>{{ optional($requisition->requirementInfo)->date }}</td>
                <td>{{ optional($requisition->requirementInfo->userInfo)->first_name .' '. optional($requisition->requirementInfo->userInfo)->last_name }}</td>
            </tr>

        </tbody>
    </table>

</fieldset>

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold">Material Requisition List</legend>

    <table class="table">
        <thead>
            <tr class="bg-pink-400">
                <th>Material Name</th>
                <th>Qty</th>
                <th>Rate</th>
                <th>Amount</th>
                <th>Remark</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($requisition->requisitionDetail as $key => $valcontent)
                @php
                $readonly = ($valcontent->status == 'Approved' || $valcontent->status == 'PO Issued' ) ? 'readonly="readonly"' : '';
                $disabled = ($valcontent->status == 'Approved' || $valcontent->status == 'PO Issued') ? 'disabled="disabled"' : '';
                $color = ($valcontent->status == 'PO Issued') ? 'class=bg-grey-300' : '';
                @endphp
                <tr {{$color}}>
                    <td>{!! Form::text('material_name[]', $valcontent->material_name, ['id'=>'material_name','placeholder'=>'Enter Material','class'=>'material_name form-control',$readonly]) !!}</td>
                    <td>{!! Form::text('qty[]', $valcontent->qty, ['id'=>'qty','placeholder'=>'Enter Qty','class'=>'qty form-control numeric',$readonly]) !!}</td>
                    <td>{!! Form::text('rate[]', $valcontent->rate, ['id'=>'rate','placeholder'=>'Enter Rate','class'=>'rate form-control numeric',$readonly]) !!}</td>
                    <td>{!! Form::text('amount[]', $valcontent->amount, ['id'=>'amount','placeholder'=>'Enter Amount','class'=>'amount form-control numeric',$readonly]) !!}</td>
                    <td>{!! Form::text('remark[]', $valcontent->remark, ['id'=>'remark','placeholder'=>'Enter Remark','class'=>'remark form-control',$readonly]) !!}</td>
                    <td>{!! Form::select('status[]',[ 'Pending'=>'Pending','Approved'=>'Approved','PO Issued'=>'PO Issued'], $value = $valcontent->status, ['id'=>'status','placeholder'=>'Select Status','class'=>'form-control',$disabled]) !!}</td>
                       
                    @if($valcontent->status == 'Approved' || $valcontent->status == 'PO Issued')
                        {{ Form::hidden('status[]', $valcontent->status) }}
                    @endif
                </tr>
                @endforeach

                {{ Form::hidden('requisition_id', $requisition->id, array('class' => 'requisition_id')) }}

        </tbody>
    </table>

</fieldset>

<div class="text-right">
     <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>Update Requisition</button>
</div>

<script type="text/javascript">
    $(document).ready(function(){ 

        $(document).on('keyup','.rate',function(){

            var qty = $(this).parent().parent().find('.qty').val();  
            var rate = $(this).val();

            var amount = qty * rate;
            $(this).parent().parent().find('.amount').val(amount); 

          });

    }); 
</script>