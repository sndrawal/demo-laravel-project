@extends('admin::layout')
@section('title')Material Requisition @stop 
@section('breadcrum')Edit Material Requisition @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Edit Material Requisition</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">

        {!! Form::model($requisition,['method'=>'PUT','route'=>['requisition.update',$requisition->id],'class'=>'form-horizontal','id'=>'requisition_submit','role'=>'form','files'=>true]) !!}
        	 @include('procurement::requisition.partial.action',['btnType'=>'Update']) 
        {!! Form::close() !!}
        
    </div>
</div>
<!-- /form inputs -->

@stop