@extends('admin::layout')
@section('title')Material Requisition @stop
@section('breadcrum')Material Requisition  @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
@stop

@section('content') 

<div class="card card-body">
    <div class="d-flex justify-content-between mb-2">
        <h4>List Of Material Requisition </h4>
    </div>


    <div class="table-responsive">
        <table class="table text-nowrap table-striped">
            <thead>
                <tr class="bg-slate-600">
                    <th>#</th>
                    <th>Raisa By</th>
                    <th>Department</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($requisition->total() != 0)
                @foreach($requisition as $key => $value)

                <tr>
                    <td>{{$requisition->firstItem() +$key}}</td>
                    <td>{{ optional($value->requirementInfo)->raise_by }}</td>
                    <td>{{ optional($value->requirementInfo->department)->dropvalue }}</td>
                    <td>{{ optional($value->requirementInfo)->date }}</td>
                    <td>
                        <a class="btn bg-pink btn-icon rounded-round" href="{{ route('requisition.edit',$value->id) }}" data-popup="tooltip" data-placement="bottom" data-original-title="Review Requisition"><i class="icon-hour-glass2 spinner"></i></a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">No Material Requisition Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>
    </div>
    <div class="col-12">
        <span class="float-right pagination align-self-end mt-3">
            {{ $requisition->appends(request()->all())->links() }}
        </span>
    </div>
</div>



@endsection