<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_select2.js')}}"></script>
<script src="{{ asset('admin/validation/po.js')}}"></script>

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>


    <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Voucher No.:<span class="text-danger">*</span></label></label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::text('voucher_no', null, ['id'=>'voucher_no','placeholder'=>'Enter Voucher No.','class'=>'voucher_no form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
               <label class="col-form-label col-lg-3">Mode of Payment:<span class="text-danger">*</span></label></label>
               <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::select('mode_of_payment',[ 'Cash'=>'Cash','Cheque'=>'Cheque','Credit'=>'Credit','Wire-Transfer'=>'Wire-Transfer','QR Payment'=>'QR Payment'], $value = null, ['id'=>'mode_of_payment','placeholder'=>'Select Mode of Payment','class'=>'form-control']) !!}
                </div>
               </div>
            </div>
        </div>

    </div>


        <div class="form-group row mp_cheque" style="display: none;">

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Bank Name:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::text('bank_name', null, ['id'=>'bank_name','placeholder'=>'Enter Bank Name','class'=>'bank_name form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Cheque No.:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::text('cheque_no', null, ['id'=>'cheque_no','placeholder'=>'Enter Cheque No.','class'=>'cheque_no form-control']) !!}
                </div>
             </div>
            </div>
        </div>

    </div>

    <div class="form-group row wire_credit" style="display: none;">

        <div class="col-lg-6 mp_wire" style="display: none;">
            <div class="row">
             <label class="col-form-label col-lg-3">Platform:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::select('platform',[ 'IPS'=>'IPS','FonPay'=>'FonPay','Other'=>'Other'], $value = null, ['id'=>'platform','placeholder'=>'Select Platform','class'=>'form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6 mp_credit" style="display: none;">
            <div class="row">
             <label class="col-form-label col-lg-3">No.of Day:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::text('no_of_day', null, ['id'=>'no_of_day','placeholder'=>'Enter No.of Days','class'=>'no_of_day form-control']) !!}
                </div>
             </div>
            </div>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Supplier Ref No.:<span class="text-danger">*</span></label></label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::text('supplier_ref_no', null, ['id'=>'supplier_ref_no','placeholder'=>'Enter Supplier Ref No.','class'=>'supplier_ref_no form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Vendor Name:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::select('vendor_id', $vendor, null, ['id'=>'vendor_id','placeholder'=>'--Select Vendor--','class'=>'form-control select-search']) !!}
                </div>
             </div>
            </div>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Dispatch Through :</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::select('dispatch_through',[ 'Self'=>'Self','Company'=>'Company'], $value = null, ['id'=>'dispatch_through','class'=>'form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Destination:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::text('destination', null, ['id'=>'destination','placeholder'=>'Enter Destination','class'=>'destination form-control']) !!}
                </div>
             </div>
            </div>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Terms of Delivery :</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::text('terms_of_delivery', null, ['id'=>'terms_of_delivery','placeholder'=>'Enter Terms of Delivery','class'=>'terms_of_delivery form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Site:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::select('site',[ 'Site'=>'Site','Store'=>'Store'], $value = null, ['id'=>'site','class'=>'form-control']) !!}
                </div>
             </div>
            </div>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-lg-6 site_warehouse" style="display: none;">
            <div class="row">
               <label class="col-form-label col-lg-3">Warehouse Name:</label>
               <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                {!! Form::select('warehouse_id', $warehouse, null, ['id'=>'warehouse_id','placeholder'=>'--Select Warehouse--','class'=>'form-control select-search']) !!}
                </div>
               </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Distribution Channel:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::text('distribution_channel', null, ['id'=>'distribution_channel','placeholder'=>'Enter Distribution Channel','class'=>'distribution_channel form-control']) !!}
                </div>
             </div>
            </div>
        </div>

    </div>

</fieldset>


@inject('material_requisition_list', '\App\Modules\Procurement\Repositories\RequisitionRepository')

@php
$requirementlist = $material_requisition_list->getApprovedRequisition();
@endphp

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold">Requirement List</legend>

     @foreach($requirementlist as $key => $valcontent)
     
       <div class="appendMaterial">
                <div class="form-group row">
                    <div class="col-lg-2">
                        <div class="row">
                             <label class="col-form-label col-lg-3">Material:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    {!! Form::text('material_name[]', $valcontent->material_name, ['id'=>'material_name','placeholder'=>'Enter Material Name','class'=>'material_name form-control','required']) !!}
                                    {{ Form::hidden('requisition_detail_id[]', $valcontent->id, array('class' => 'requisition_detail_id')) }}

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="row">
                           <label class="col-form-label col-lg-3">Qty:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-balance"></i></span>
                                    </span>
                                    {!! Form::text('qty[]', $valcontent->qty, ['id'=>'qty','placeholder'=>'Enter Qty','class'=>'qty form-control numeric','required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="row">
                           <label class="col-form-label col-lg-3">Rate:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-balance"></i></span>
                                    </span>
                                   {!! Form::text('rate[]', $valcontent->rate, ['id'=>'rate','placeholder'=>'Enter Rate','class'=>'rate form-control numeric']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="row">
                           <label class="col-form-label col-lg-3">Amount:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-balance"></i></span>
                                    </span>
                                   {!! Form::text('amount[]', $valcontent->amount, ['id'=>'amount','placeholder'=>'Enter Amount','class'=>'amount form-control numeric']) !!}
                                </div>
                            </div>
                        </div>
                    </div>


                <div class="col-lg-2">
                   <div class="row">
                           <button type="button" class="ml-2 remove_material btn bg-danger-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Remove</button>
                    </div>
                </div>

            </div>
        </div>


     @endforeach


</fieldset>
    <div class="text-center">
       <button type="button" class="ml-2 btn bg-success-800 btn-labeled btn-labeled-left calculate"><b><i class="icon-pen-plus"></i></b> Calculate</button>
    </div>

 <fieldset class="mb-3 po_balance" style="display: none;">
            <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
             @include('purchase::purchase.partial.purchase-balance') 
    </fieldset>


<div class="text-right store_po" style="display: none;">
     <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>{{ $btnType }}</button>
</div>


<script type="text/javascript">
    $(document).ready(function(){ 

        $('#mode_of_payment').on('change',function(){

            var mode_val =$(this).val();

           if(mode_val == 'Cheque'){
                $('.mp_cheque').show();
                $('.mp_wire').hide();
                $('.mp_credit').hide();
                $('.wire_credit').hide();
            }else if(mode_val == 'Credit'){
                $('.mp_credit').show();
                $('.mp_cheque').hide();
                $('.mp_wire').hide();
                $('.wire_credit').show();
            }else if(mode_val == 'Wire-Transfer'){
                $('.mp_wire').show();       
                $('.mp_cheque').hide();       
                $('.mp_credit').hide();     
                $('.wire_credit').show();  
            }else{
                $('.mp_wire').hide();
                $('.mp_cheque').hide();
                $('.mp_credit').hide();
                $('.wire_credit').hide();
            }       
        });
        
        $('#site').on('change',function(){
             var site_val =$(this).val();

             if(site_val == 'Store'){
                $('.site_warehouse').show();
             }else{
                $('.site_warehouse').hide();
             }
        });


        $('.remove_material').on('click',function(){ 
            $(this).parent().parent().parent().remove();
        });


        $(document).on('click','.calculate',function(){


            var arr = document.getElementsByClassName('amount');  
                 var tot=0;
                    for(var i=0;i<arr.length;i++){
                        if(parseInt(arr[i].value))
                            tot += parseInt(arr[i].value);
                    }
              
                var total_amount = tot;
                 $('.sub_total').val(total_amount.toFixed(2));

                 $('.discount_percent').val(0);
                 $('.discount_amount').val(0);

                 var discount_amount = 0;

                 var discounted_amount = total_amount - discount_amount;

                $('.total_after_discount').val(discounted_amount);

                 var vat_value = (13/100) * discounted_amount;
                 $('.vat_amount').val(vat_value.toFixed(2));


                 var grand_total = total_amount + vat_value;
                 $('.grand_total').val(grand_total.toFixed(2));

                 $('.po_balance').show();
                 $('.store_po').show();
        });

    }); 
</script>