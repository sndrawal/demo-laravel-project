    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Purchase Order :: Invoice</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{asset('admin/global/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('admin/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('admin/css/layout.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('admin/css/components.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('admin/css/colors.min.css')}}" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

    </head>

    <body>

        <div class="page-content">
            <div class="content">
                <div class="card">
                    <div class="card-body">
                        
                        <div class="row font-size-lg text-justify">
                               <div class="col-sm-1">
                               </div>
                                   
                                <div class="col-sm-10">

                                    <ul class="list list-unstyled mb-0">
                                        <li class="text-center text-uppercase"><h4><u><b>Purchase Order</b></u></h4></li>
                                        <br>
                                    </ul>
                                   

                                </div>

                                <div class="col-sm-1">
                                </div>
                            </div>

                                <div class="card font-size-sm">
                                    <div class="table-responsive">
                                        <h5>
                                        <table class="table text-nowrap">
                                            <tbody>
                                                <tr>
                                                    <td rowspan="3">
                                                        <span>Invoice To</span>
                                                        <p><b>Bidhee Pvt.Ltd</b></p>
                                                        <p>Basuki Marg, Kathmandu 44600</p>
                                                        <p>E-mail : info@bidhee.com
                                                    </td>
                                                    <td>
                                                        <span>Voucher No.</span>
                                                        <p><b>{{$poDetail->voucher_no}}</b></p>
                                                    </td>
                                                    <td>
                                                        <span>Dated</span>
                                                        <p><b>{{ date('Y-m-d',strtotime($poDetail->created_at)) }}</b></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <span>Mode of Payment</span>
                                                        <p><b>{{$poDetail->mode_of_payment}}</b></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Suplier Ref. No</span>
                                                        <p><b>{{$poDetail->supplier_ref_no}}</b></p>
                                                    </td>
                                                    <td>
                                                        <span>Vendor</span>
                                                        <p><b>{{optional($poDetail->vendor)->vendor_name}}</b></p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <span>Despatch Through</span>
                                                        <p><b>{{$poDetail->dispatch_through}}</b></p>
                                                    </td>
                                                     <td>
                                                        <span>Destination</span>
                                                        <p><b>{{$poDetail->destination}}</b></p>
                                                    </td>
                                                     <td>
                                                        <span>Terms of Delivery</span>
                                                        <p><b>{{$poDetail->terms_of_delivery}}</b></p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <span>Site</span>
                                                        <p><b>{{$poDetail->site}}</b></p>
                                                    </td>
                                                     <td>
                                                        @if($poDetail->site =='Store')
                                                            <span>Warehouse</span>
                                                            <p><b>{{optional($poDetail->warehouse)->warehouse_name}}</b></p>
                                                        @endif
                                                    </td>
                                                     <td>
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>
                                        </h5>
                                    </div>
                                </div>


                                <fieldset class="mb-3">
                                    <legend class="text-uppercase font-size-sm font-weight-bold">Material Detail</legend>

                                     <div class="table-responsive">
                                            <table class="table text-nowrap table-striped">
                                                <thead>
                                                    <tr class="bg-slate-600">
                                                        <th>#</th>
                                                        <th>Material Name</th>
                                                        <th>Qty</th>
                                                        <th>Rate</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($poDetail->purchaseOrderDetail as $key => $povalue)

                                                    <tr>
                                                        <td>{{$key+1}}</td>
                                                        <td>{{ $povalue->material_name }}</td>
                                                        <td>{{ $povalue->qty }}</td>
                                                        <td>{{ $povalue->rate }}</td>
                                                        <td>Rs. {{ number_format($povalue->amount,2) }}</td>
                                                    </tr>

                                                    @endforeach
                                                   
                                                </tbody>

                                            </table>
                                        </div>


                                </fieldset>

                                <fieldset class="mb-3">
                                    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
                                        <div class="mb-1 row">
                                                <div class="col-lg-8"></div>
                                                  <div class="col-lg-4">
                                                        <div class="row">
                                                          <label class="col-form-label col-lg-4">Sub Total :</label>
                                                              <div class="col-lg-8 form-group-feedback form-group-feedback-right" style="margin-top: 7px;">
                                                                <div class="input-group">
                                                                <span><b>Rs. {{ number_format($poDetail->sub_total,2)}}</b></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                                            </div>

                                            <div class="mb-1 row">
                                                <div class="col-lg-8"></div>
                                                  <div class="col-lg-4">
                                                        <div class="row">
                                                          <label class="col-form-label col-lg-4">Discount (%) :</label>
                                                              <div class="col-lg-8 form-group-feedback form-group-feedback-right" style="margin-top: 7px;">
                                                                <div class="input-group">
                                                                  <span><b>{{ number_format($poDetail->discount_percent,2)}} %</b></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                                            </div>

                                            <div class="mb-1 row">
                                                <div class="col-lg-8"></div>
                                                  <div class="col-lg-4">
                                                        <div class="row">
                                                          <label class="col-form-label col-lg-4">Discount Amount (-):</label>
                                                              <div class="col-lg-8 form-group-feedback form-group-feedback-right" style="margin-top: 7px;">
                                                                <div class="input-group">
                                                                   <span><b>Rs. {{ number_format($poDetail->discount_amount,2)}}</b></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                                            </div>

                                              <div class="mb-1 row">
                                                <div class="col-lg-8"></div>
                                                  <div class="col-lg-4">
                                                        <div class="row">
                                                          <label class="col-form-label col-lg-4">Total After Discount :</label>
                                                              <div class="col-lg-8 form-group-feedback form-group-feedback-right" style="margin-top: 7px;">
                                                                <div class="input-group">
                                                                   <span><b>Rs. {{ number_format($poDetail->total_after_discount,2)}}</b></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                                            </div>

                                            <div class="mb-1 row">
                                                <div class="col-lg-8"></div>
                                                  <div class="col-lg-4">
                                                        <div class="row">
                                                          <label class="col-form-label col-lg-4">VAT (+):</label>
                                                              <div class="col-lg-8 form-group-feedback form-group-feedback-right" style="margin-top: 7px;">
                                                                <div class="input-group">
                                                                   <span><b>Rs. {{ number_format($poDetail->vat_amount,2)}}</b></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                                            </div>

                                            <div class="mb-1 row">
                                                <div class="col-lg-8"></div>
                                                  <div class="col-lg-4">
                                                        <div class="row">
                                                          <label class="col-form-label col-lg-4">Grand Total :</label>
                                                              <div class="col-lg-8 form-group-feedback form-group-feedback-right" style="margin-top: 7px;">
                                                                <div class="input-group">
                                                                   <span><b>Rs. {{ number_format($poDetail->grand_total,2)}}</b></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                                            </div>

                                </fieldset>


                    </div>
                </div>
            </div>
        </div>


    </body>
    </html>


