<div class="card card-body filter-option">
    {!! Form::open(['route' => 'purchaseOrder.index', 'method' => 'get']) !!}
    <div class="row">
        <div class="col-md-4">
            <label class="d-block font-weight-semibold">Issue Date:</label>
            <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="text-blue icon-calendar2"></i>
                    </span>
                </span>
                @php
                    $search_from_to = (array_key_exists('search_from_to',$search_value)) ? $search_value['search_from_to'] : '';
                @endphp
                {!! Form::text('search_from_to', $value = $search_from_to  , ['id'=>'search_from','placeholder'=>'Search From','class'=>'form-control form-control-lg  daterange-buttons','readonly']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <label class="d-block font-weight-semibold">Voucher No.:</label>
            <div class="input-group">
                @php
                    $search_voucher_no = (array_key_exists('voucher_no',$search_value)) ? $search_value['voucher_no'] : '';
                @endphp
                {!! Form::select('voucher_no[]', $voucher, $search_voucher_no, ['class'=>'form-control multiselect-filtering', 'multiple']) !!}

            </div>
        </div>
        <div class="col-md-4">
            <label class="d-block font-weight-semibold">Vendor:</label>
            <div class="input-group">
                @php
                    $search_vendor = (array_key_exists('vendor_id',$search_value)) ? $search_value['vendor_id'] : '';
                @endphp
                {!! Form::select('vendor_id[]', $vendor, $search_vendor, ['class'=>'form-control multiselect-filtering', 'multiple']) !!}
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <label class="d-block font-weight-semibold">Warehouse:</label>
            <div class="input-group">
                @php
                    $search_warehouse = (array_key_exists('warehouse_id',$search_value)) ? $search_value['warehouse_id'] : '';
                @endphp
                {!! Form::select('warehouse_id[]', $warehouse, $search_warehouse, ['class'=>'form-control multiselect-filtering', 'multiple']) !!}
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <label class="d-block font-weight-semibold">Supplier Ref No.:</label>
            <div class="input-group">
                @php
                    $search_supplier_ref_no = (array_key_exists('supplier_ref_no',$search_value)) ? $search_value['supplier_ref_no'] : '';
                @endphp
                {!! Form::select('supplier_ref_no[]', $supplierref, $search_supplier_ref_no, ['class'=>'form-control multiselect-filtering', 'multiple']) !!}
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-end mt-2">
        <button class="btn bg-primary" type="submit">
            Search Now
        </button>
        <a href="{{ route('purchaseOrder.index') }}" data-popup="tooltip" data-placement="top" data-original-title="Refresh Search" class="btn bg-danger ml-2">
            <i class="icon-spinner9"></i>
        </a>
    </div>
    {!! Form::close() !!}
</div>