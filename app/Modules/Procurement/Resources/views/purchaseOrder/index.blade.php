@extends('admin::layout')
@section('title')Purchase Order @stop
@section('breadcrum')Purchase Order @stop

@section('script')

<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>

@stop

@section('content') 

@include('procurement::purchaseOrder.partial.search')

<div class="card card-body">
    <div class="d-flex justify-content-between mb-2">
        <h4>List Of Purchase Order </h4>
        <a href="{{ route('purchaseOrder.create') }}" class="btn bg-blue mb-2">
            <i class="icon-plus2"></i> Create Purchase Order
        </a>
    </div>


    <div class="table-responsive">
        <table class="table text-nowrap table-striped">
            <thead>
                <tr class="bg-slate-600">
                    <th>#</th>
                    <th>Voucher No.</th>
                    <th>Mode of Payment</th>
                    <th>Supplier Ref No.</th>
                    <th>Vendor Name</th>
                    <th>Grand Total</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($purchase_order->total() != 0)
                @foreach($purchase_order as $key => $value)

                <tr>
                    <td>{{$purchase_order->firstItem() +$key}}</td>
                    <td>{{ $value->voucher_no }}</td>
                    <td>{{ $value->mode_of_payment }}</td>
                    <td>{{ $value->supplier_ref_no }}</td>
                    <td>{{ optional($value->vendor)->vendor_name }}</td>
                    <td>Rs. {{ number_format($value->grand_total,2) }}</td>
                    <td>
                        <a data-toggle="modal" data-target="#modal_view_invoice"  class="btn bg-purple btn-icon rounded-round view_invoice" po_id="{{$value->id}}" data-popup="tooltip" data-placement="bottom" data-original-title="View Invoice"><i class="icon-files-empty"></i></a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">No Purchase Order Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>
    </div>
    <div class="col-12">
        <span class="float-right pagination align-self-end mt-3">
            {{ $purchase_order->appends(request()->all())->links() }}
        </span>
    </div>
</div>



 <!-- view modal -->
    <div id="modal_view_invoice" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header bg-purple">
                    <h6 class="modal-title">View Invoice</h6>
                </div>

                <div class="modal-body">
                    <div class="table-responsive result_view_detail">
                         
                    </div><!-- table-responsive -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn bg-teal-400" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- view modal -->

    <script type="text/javascript">
        $(document).ready(function(){
              $('.view_invoice').on('click',function(){

                 var po_id = $(this).attr('po_id'); 
                
                  $.ajax({
                      url: "<?php echo route('purchaseOrder.view-invoice-ajax') ?>",
                      method: 'POST',
                      data: {po_id:po_id, _token:"{{ csrf_token() }}"},
                      success: function(data) {
                        $(".result_view_detail").html('');
                        $(".result_view_detail").html(data.options);
                      }
                  });

            });
        });
    </script>


@endsection