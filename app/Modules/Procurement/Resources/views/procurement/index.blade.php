@extends('admin::layout')
@section('title')Requirement Raise @stop
@section('breadcrum')Requirement Raise @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
@stop

@section('content') 

@include('procurement::procurement.partial.search')

<div class="card card-body">
    <div class="d-flex justify-content-between">
        <h4>List Of Requirement Raise</h4>
        <a href="{{ route('procurement.create') }}" class="btn bg-blue mb-2">
            <i class="icon-plus2"></i> Add Requirement Raise
        </a>
    </div>


    <div class="table-responsive">
        <table class="table text-nowrap table-striped">
            <thead>
                <tr class="bg-slate-600">
                    <th>#</th>
                    <th>Raise By</th>
                    <th>Department</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @if($procurement->total() != 0)
                @foreach($procurement as $key => $value)
                @php

                    if($value->status == 'Pending'){
                        $prio_color = 'bg-warning';
                        $icon = 'icon-history';
                        $modal ='modal';
                    }
                    if($value->status == 'Approved'){
                        $prio_color = 'bg-success';
                        $icon = 'icon-thumbs-up3';
                        $modal ='';
                    }
                    if($value->status == 'Reject'){
                        $prio_color = 'bg-danger';
                        $icon = 'icon-thumbs-down3 ';
                        $modal ='';
                    }

                @endphp
                <tr>
                    <td>{{$procurement->firstItem() +$key}}</td>
                    <td>{{ $value->raise_by }}</td>
                    <td>{{ optional($value->department)->dropvalue }}</td>
                    <td>{{ $value->date }}</td>
                    <td><a data-toggle="{{$modal}}" data-target="#modal_theme_status" class="btn {{$prio_color}} text-default btn-icon btn-sm rounded-round ml-2 update_status" requirement_id ="{{$value->id}}" data-popup="tooltip"data-placement="bottom" data-original-title="{{ $value->status }}"><i class="{{ $icon}}"></i></a></td>
                    <td class="text-right">
                        <a href="#" class="list-icons-item" data-toggle="dropdown">
                            <i class="icon-more2"></i>
                        </a>

                        <div class="dropdown-menu bd-card dropdown-menu-right">
                            <a class="dropdown-item" href="{{route('procurement.edit',$value->id)}}">
                                <i class="icon-pencil6"></i> Edit
                            </a>
                            @if($value->status == 'Approved')
                                <a class="dropdown-item" href="{{route('requisition.index',['req_id'=>$value->id])}}">
                                    <i class="icon-bag"></i> Move to Material Requisition
                                </a>
                            @endif

                             <a data-toggle="modal" data-target="#modal_view_requirement" class="dropdown-item view_requirement" requirement_id = "{{$value->id}}"><i class="icon-eye"></i>View Detail</a>

                            <a data-toggle="modal" data-target="#delete-modal-procurement" class="delete-modal dropdown-item delete_procurement" link="{{route('procurement.delete',$value->id)}}">
                                <i class="icon-bin"></i> Delete
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">No Requirement Raise Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>
    </div>
    <div class="col-12">
        <span class="float-right pagination align-self-end mt-3">
            {{ $procurement->appends(request()->all())->links() }}
        </span>
    </div>
</div>


<!-- Warning modal -->
        <div id="modal_theme_status" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-warning-800">
                        <h6 class="modal-title"><i class="icon-user-check mr-1"></i> Requirement Status</h6>
                    </div>


                    <div class="modal-body">
                        {!! Form::open(['route'=>'procurement.updateStatus','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}

                        {{ Form::hidden('requirement_id', '', array('class' => 'requirement_id')) }}

                        <div class="form-group row">
                            <label class="col-form-label col-lg-4">Status:</label>
                            <div class="col-lg-8">
                                {!! Form::select('status',['Pending'=>'Pending','Approved'=>'Approved','Reject'=>'Reject'], $value = null, ['id'=>'status','class'=>'form-control','placeholder'=>'Select Status']) !!}
                            </div>
                        </div>


                        <div class="text-center">
                                <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>Update Status</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
        <!-- /warning modal -->

 <!-- view modal -->
    <div id="modal_view_requirement" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-purple">
                    <h6 class="modal-title">View Requirement Raise Detail</h6>
                </div>

                <div class="modal-body">
                    <div class="table-responsive result_view_detail">
                         
                    </div><!-- table-responsive -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn bg-teal-400" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- view modal -->

<!-- Warning modal -->
    <div id="delete-modal-procurement" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                 <div class="modal-body">
                    <center>
                        <i class="icon-alert text-danger icon-3x"></i>
                    </center>
                    <br>
                    <center>
                        <h2>Are You Sure Want To Delete ?</h2>
                        <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!-- /warning modal -->


<script type="text/javascript">
    $(document).ready(function(){

        $('.update_status').on('click',function(){

            var requirement_id = $(this).attr('requirement_id');

            $('.requirement_id').val(requirement_id);
        });

        $('.view_requirement').on('click',function(){

         var requirement_id = $(this).attr('requirement_id'); 
        
          $.ajax({
              url: "<?php echo route('procurement.get-requirement-detail-ajax') ?>",
              method: 'POST',
              data: {requirement_id:requirement_id, _token:"{{ csrf_token() }}"},
              success: function(data) {
                $(".result_view_detail").html('');
                $(".result_view_detail").html(data.options);
              }
          });

    });

          $('.delete_procurement').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });

    });
</script>


@endsection