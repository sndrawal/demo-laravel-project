@extends('admin::layout')
@section('title')Requirement Raise @stop 
@section('breadcrum')Create Requirement Raise @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Create Requirement Raise</h5>
        <div class="header-elements">

        </div>
    </div>
    

    <div class="card-body">

        {!! Form::open(['route'=>'procurement.store','method'=>'POST','class'=>'form-horizontal','id'=>'procurement_submit','role'=>'form','files' => true]) !!}
        
            @include('procurement::procurement.partial.action',['btnType'=>'Save']) 
        
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@stop