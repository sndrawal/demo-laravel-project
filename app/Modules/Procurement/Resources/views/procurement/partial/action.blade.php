<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_select2.js')}}"></script>
<script src="{{ asset('admin/validation/purchase.js')}}"></script>

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

    <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
             <label class="col-form-label col-lg-3">Raise By:</label>
             <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::text('raise_by', null, ['id'=>'raise_by','placeholder'=>'Enter Raise By','class'=>'raise_by form-control']) !!}
                </div>
             </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
               <label class="col-form-label col-lg-3">Department:</label>
               <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                <div class="input-group">
                    {!! Form::select('department_id',$department, $value = null, ['id'=>'department_id','placeholder'=>'Select Department','class'=>'select-search form-control']) !!}
                </div>
               </div>
            </div>
        </div>

    </div>


</fieldset>


<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold">Requirement Information</legend>

 
    @if($is_edit)
        @foreach($procurement->requirementDetail as $key => $valcontent)

            <div class="appendMaterial">
                <div class="form-group row">
                    <div class="col-lg-2">
                        <div class="row">
                             <label class="col-form-label col-lg-3">Material:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    {!! Form::text('material_name[]', $valcontent->material_name, ['id'=>'material_name','placeholder'=>'Enter Material Name','class'=>'material_name form-control','required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="row">
                           <label class="col-form-label col-lg-3">Qty:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-balance"></i></span>
                                    </span>
                                    {!! Form::text('qty[]', $valcontent->qty, ['id'=>'qty','placeholder'=>'Enter Qty','class'=>'qty form-control numeric','required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label class="col-form-label col-lg-3">Purpose:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-coins"></i></span>
                                    </span>
                                    {!! Form::text('purpose[]', $valcontent->purpose, ['id'=>'purpose','placeholder'=>'Enter Purpose','class'=>'purpose form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label class="col-form-label col-lg-3">Remark:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-coins"></i>
                                        </span>
                                    </span>
                                    {!! Form::text('remark[]', $valcontent->remark, ['id'=>'remark','placeholder'=>'Enter Remark','class'=>'remark form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="col-lg-2">
                   <div class="row">
                           <button type="button" class="ml-2 remove_material btn bg-danger-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Remove</button>
                    </div>
                </div>

            </div>
        </div>


        @endforeach
    @endif

    <div class="appendMaterial">
        <div class="form-group row">
                <div class="col-lg-2">
                        <div class="row">
                             <label class="col-form-label col-lg-3">Material:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    {!! Form::text('material_name[]', null, ['id'=>'material_name','placeholder'=>'Enter Material Name','class'=>'material_name form-control','required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="row">
                           <label class="col-form-label col-lg-3">Qty:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-balance"></i></span>
                                    </span>
                                    {!! Form::text('qty[]', null, ['id'=>'qty','placeholder'=>'Enter Qty','class'=>'qty form-control numeric','required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label class="col-form-label col-lg-3">Purpose:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-coins"></i></span>
                                    </span>
                                    {!! Form::text('purpose[]', null, ['id'=>'purpose','placeholder'=>'Enter Purpose','class'=>'purpose form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label class="col-form-label col-lg-3">Remark:</label>
                            <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-coins"></i>
                                        </span>
                                    </span>
                                    {!! Form::text('remark[]', null, ['id'=>'remark','placeholder'=>'Enter Remark','class'=>'remark form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

            <div class="col-lg-2 float-right">
                 <div class="row">
                    <button type="button" class="add_material btn bg-success-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Add More Material</button>
                </div>
            </div>

    </div>
</div>

</fieldset>


<div class="text-right">
     <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>{{ $btnType }}</button>
</div>



 <script type="text/javascript">
    $(document).ready(function(){ 
        $('.add_material').on('click',function(){
            $.ajax({
                    type: 'GET',
                    url: '/admin/procurement/appendMaterial',
                    success: function (data) {
                        $('.appendMaterial').last().append(data.options); 
                         $('.select-search').select2();
                         $('.numeric').keyup(function() {
                            if (this.value.match(/[^0-9.]/g)) {
                                this.value = this.value.replace(/[^0-9.]/g, '');
                            }
                        });
                    }
                });
        });

        $('.remove_material').on('click',function(){ 
            $(this).parent().parent().parent().remove();
        });


    }); 
</script>