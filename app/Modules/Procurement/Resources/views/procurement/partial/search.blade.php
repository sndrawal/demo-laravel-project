<div class="card card-body filter-option">
    {!! Form::open(['route' => 'procurement.index', 'method' => 'get']) !!}
    <div class="row">
        <div class="col-md-4">
            <label class="d-block font-weight-semibold">Raise By:</label>
            <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="text-blue icon-calendar2"></i>
                    </span>
                </span>
                @php
                    $search_from_to = (array_key_exists('search_from_to',$search_value)) ? $search_value['search_from_to'] : '';
                @endphp
                {!! Form::text('search_from_to', $value = $search_from_to  , ['id'=>'search_from','placeholder'=>'Search From','class'=>'form-control form-control-lg  daterange-buttons','readonly']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <label class="d-block font-weight-semibold">Raise By:</label>
            <div class="input-group">
                @php
                    $search_raise_by = (array_key_exists('raise_by',$search_value)) ? $search_value['raise_by'] : '';
                @endphp
                {!! Form::select('raise_by[]', $raiseBy, $search_raise_by, ['class'=>'form-control multiselect-filtering', 'multiple']) !!}

            </div>
        </div>
        <div class="col-md-4">
            <label class="d-block font-weight-semibold">Department:</label>
            <div class="input-group">
                @php
                    $search_department = (array_key_exists('department_id',$search_value)) ? $search_value['department_id'] : '';
                @endphp
                {!! Form::select('department_id[]', $department, $search_department, ['class'=>'form-control multiselect-filtering', 'multiple']) !!}
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-end mt-2">
        <button class="btn bg-primary" type="submit">
            Search Now
        </button>
        <a href="{{ route('procurement.index') }}" data-popup="tooltip" data-placement="top" data-original-title="Refresh Search" class="btn bg-danger ml-2">
            <i class="icon-spinner9"></i>
        </a>
    </div>
    {!! Form::close() !!}
</div>