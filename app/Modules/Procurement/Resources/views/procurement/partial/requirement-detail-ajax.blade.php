<table class="table table-striped">
    <thead>
        <tr class="bg-pink-400">
            <th>Raise By</th>
            <th>Department</th>
            <th>Status</th>
            <th>Date</th>
            <th>Created By</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $requirementDetail->raise_by }}</td>
            <td>{{ optional($requirementDetail->department)->dropvalue }}</td>
            <td>{{ $requirementDetail->status }}</td>
            <td>{{ $requirementDetail->date}}</td>
            <td>{{ optional($requirementDetail->userInfo)->first_name .' '. optional($requirementDetail->userInfo)->last_name }}</td>
        </tr>

    </tbody>
</table>
<br>
<table class="table table-striped">
    <thead>
        <tr class="bg-slate-400">
            <th>S.N.</th>
            <th>Material</th>
            <th>Qty</th>
            <th>Purpose</th>
            <th>Remark</th>
        </tr>
    </thead>
    <tbody>

       @foreach($requirementDetail->requirementDetail as $key => $value)

       <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $value->material_name }}</td>
            <td>{{ $value->qty }}</td>
            <td>{{ $value->purpose }}</td>
            <td>{{ $value->remark }}</td>
        </tr>

    @endforeach

</tbody>

</table>
