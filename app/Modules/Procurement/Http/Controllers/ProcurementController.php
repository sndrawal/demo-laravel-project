<?php

namespace App\Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Procurement\Repositories\RequirementRaiseInterface;
use App\Modules\Dropdown\Repositories\DropdownInterface;

class ProcurementController extends Controller
{
    protected $requirement;
    protected $dropdown;

    public function __construct(RequirementRaiseInterface $requirement, DropdownInterface $dropdown)
    {
        $this->requirement = $requirement;
        $this->dropdown = $dropdown;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $search = $request->all();
        $data['department'] = $this->dropdown->getFieldBySlug('department');
        $data['raiseBy'] = $this->requirement->getUniqueRaiseBy();
        $data['procurement'] = $this->requirement->findAll($limit= 50, $search);
        $data['search_value'] = $search;

        return view('procurement::procurement.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data['is_edit'] = false;
        $data['department'] = $this->dropdown->getFieldBySlug('department');

        return view('procurement::procurement.create', $data);
    }

    public function appendMaterial(Request $request){

         if($request->ajax()){
            $data = view('procurement::procurement.partial.add-more-material')->render();
            return response()->json(['options'=>$data]);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $data = $request->all();  

        $user = Auth::user();

        try{ 
            $requirement_data = array(
                    'raise_by' =>$data['raise_by'],
                    'department_id' =>$data['department_id'],
                    'status' =>'Pending',
                    'date' =>date('Y-m-d'),
                    'created_by' =>$user->id
            );

            $requirementInfo = $this->requirement->save($requirement_data);
            $requirement_id = $requirementInfo->id;

            $materials = $data['material_name'];
            $countmaterials = sizeof($materials);

                for($i = 0; $i < $countmaterials; $i++){
           
                     if($data['material_name'][$i]){
                         $RequirementDetaildata['requirement_raise_id'] = $requirement_id;
                         $RequirementDetaildata['material_name'] = $data['material_name'][$i];
                         $RequirementDetaildata['qty'] = $data['qty'][$i];
                         $RequirementDetaildata['purpose'] = $data['purpose'][$i];
                         $RequirementDetaildata['remark'] = $data['remark'][$i];

                         $this->requirement->saveDetail($RequirementDetaildata);
                     }
                         
                 }

            toastr()->success('Requirement Raise Created Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('procurement.index'));
    }

    public function updateStatus(Request $request){
        $input = $request->all();
        $id = $input['requirement_id'];

        $updateData = array(
            'status' =>$input['status']
        );

        $this->requirement->update($id, $updateData);

        toastr()->success('Requirement Raise Status Updated Successfully');

        return redirect(route('procurement.index'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $data['is_edit'] = true;
        $data['department'] = $this->dropdown->getFieldBySlug('department');
        $data['procurement'] = $this->requirement->find($id);

        return view('procurement::procurement.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();  

        $user = Auth::user();

        try{ 
            $requirement_data = array(
                    'raise_by' =>$data['raise_by'],
                    'department_id' =>$data['department_id']
            );

            $requirementInfo = $this->requirement->update($id,$requirement_data);
            $requirement_id = $id;
            
            $this->requirement->deleteDetail($id);

            $materials = $data['material_name'];
            $countmaterials = sizeof($materials);

                for($i = 0; $i < $countmaterials; $i++){
           
                     if($data['material_name'][$i]){
                         $RequirementDetaildata['requirement_raise_id'] = $requirement_id;
                         $RequirementDetaildata['material_name'] = $data['material_name'][$i];
                         $RequirementDetaildata['qty'] = $data['qty'][$i];
                         $RequirementDetaildata['purpose'] = $data['purpose'][$i];
                         $RequirementDetaildata['remark'] = $data['remark'][$i];

                         $this->requirement->saveDetail($RequirementDetaildata);
                     }
                         
                 }

            toastr()->success('Requirement Raise Updated Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('procurement.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try{
            $this->requirement->delete($id);
            $this->requirement->deleteDetail($id);
            toastr()->success('Requirement Raise Deleted Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        return redirect(route('procurement.index'));  
    }

    public function getRequirementDetailAjax(Request $request)
    {
        if($request->ajax()){

            $requirement_id = $request->requirement_id;
            $requirementDetail = $this->requirement->find($requirement_id); 
            $data = view('procurement::procurement.partial.requirement-detail-ajax',compact('requirementDetail'))->render();
            return response()->json(['options'=>$data]);
        }    
    }

}
