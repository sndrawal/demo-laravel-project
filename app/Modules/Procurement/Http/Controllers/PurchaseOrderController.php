<?php

namespace App\Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Procurement\Repositories\RequisitionInterface;
use App\Modules\Procurement\Repositories\RequirementRaiseInterface;
use App\Modules\Procurement\Repositories\PurchaseOrderInterface;
use App\Modules\Vendor\Repositories\VendorInterface;
use App\Modules\Warehouse\Repositories\WarehouseInterface;

class PurchaseOrderController extends Controller
{
    protected $requirement;
    protected $requisition;
    protected $purchase_order;
    protected $vendor;
    protected $warehouse;

    public function __construct(RequirementRaiseInterface $requirement, RequisitionInterface $requisition, PurchaseOrderInterface $purchase_order, VendorInterface $vendor, WarehouseInterface $warehouse)
    {
        $this->requirement = $requirement;
        $this->requisition = $requisition;
        $this->purchase_order = $purchase_order;
        $this->vendor = $vendor;
        $this->warehouse = $warehouse;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $input = $request->all();  

        $data['vendor'] = $this->vendor->getList();
        $data['warehouse'] = $this->warehouse->getList();
        $data['voucher'] = $this->purchase_order->getUniqueVoucher();
        $data['supplierref'] = $this->purchase_order->getUniqueSupplierref();
        $data['purchase_order'] = $this->purchase_order->findAll($limit= 50, $input);
        $data['search_value'] = $input;

        return view('procurement::purchaseOrder.index',$data);
    } 

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {


        $data['requirementlist'] = $materialCheck = $this->requisition->getApprovedRequisition();

        if(sizeof($materialCheck) > 0){

        $data['is_edit'] = false;
        $data['vendor'] = $this->vendor->getList();
        $data['warehouse'] = $this->warehouse->getList();

        }else{
            toastr()->error('No Material Requisition To Create PO.');
            return redirect(route('purchaseOrder.index')); 

        }

        
        return view('procurement::purchaseOrder.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $data = $request->all(); 

        $amountInWords = $this->requisition->NumberIntoWords($data['grand_total']);

        $user = Auth::user();
        
        try{ 
            $po_data = array(
                    'voucher_no' => $data['voucher_no'],
                    'mode_of_payment' => $data['mode_of_payment'],
                    'bank_name' => $data['bank_name'],
                    'cheque_no' => $data['cheque_no'],
                    'platform' => $data['platform'],
                    'no_of_day' => $data['no_of_day'],
                    'supplier_ref_no' => $data['supplier_ref_no'],
                    'vendor_id' => $data['vendor_id'],
                    'dispatch_through' => $data['dispatch_through'],
                    'destination' => $data['destination'],
                    'terms_of_delivery' => $data['terms_of_delivery'],
                    'site' => $data['site'],
                    'warehouse_id' => $data['warehouse_id'],
                    'distribution_channel' => $data['distribution_channel'],
                    'issued_by' => $user->id,
                    'sub_total' => $data['sub_total'],
                    'discount_percent' => $data['discount_percent'],
                    'discount_amount' => $data['discount_amount'],
                    'total_after_discount' => $data['total_after_discount'],
                    'vat_amount' => $data['vat_amount'],
                    'grand_total' => $data['grand_total'],
                    'number_in_words' =>$amountInWords
            );

            $purchaseOrderInfo = $this->purchase_order->save($po_data);
            $purchase_order_id = $purchaseOrderInfo->id;

            $materials = $data['material_name'];
            $countmaterials = sizeof($materials);

                for($i = 0; $i < $countmaterials; $i++){
           
                    if($data['material_name'][$i]){

                        $rd_id = $data['requisition_detail_id'][$i];

                         $purchaseDetaildata['purchase_order_id'] = $purchase_order_id;
                         $purchaseDetaildata['material_name'] = $data['material_name'][$i];
                         $purchaseDetaildata['requisition_detail_id'] = $data['requisition_detail_id'][$i];
                         $purchaseDetaildata['qty'] = $data['qty'][$i];
                         $purchaseDetaildata['rate'] = $data['rate'][$i];
                         $purchaseDetaildata['amount'] = $data['amount'][$i];

                        $this->purchase_order->saveDetail($purchaseDetaildata);

                         //Update Requistion Detail Status with PO Issued From Approved
                        
                        $requisition_detail_data = array(
                            'status' => 'PO Issued'
                        );

                       $this->requisition->updateDetail($rd_id,$requisition_detail_data);

                    }
                         
                 }

            toastr()->success('Purchase Order Created Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('purchaseOrder.index'));
    }

    public function viewInvoiceAjax(Request $request)
    {
        if($request->ajax()){

            $po_id = $request->po_id;
            $poDetail = $this->purchase_order->find($po_id); 
            $data = view('procurement::purchaseOrder.partial.view-invoice-ajax',compact('poDetail'))->render();
            return response()->json(['options'=>$data]);
        }    
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('procurement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
