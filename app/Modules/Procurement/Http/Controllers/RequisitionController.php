<?php

namespace App\Modules\Procurement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Procurement\Repositories\RequisitionInterface;
use App\Modules\Procurement\Repositories\RequirementRaiseInterface;

class RequisitionController extends Controller
{
    protected $requirement;
    protected $requisition;

    public function __construct(RequirementRaiseInterface $requirement, RequisitionInterface $requisition)
    {
        $this->requirement = $requirement;
        $this->requisition = $requisition;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $input = $request->all();

        if(array_key_exists('req_id', $input)){

            $requirement_id = $input['req_id'];
            $requirementDetail = $this->requirement->find($requirement_id);

            $checkData = $this->requisition->findByReq($requirement_id);   
            if(sizeof($checkData) <= 0){
             
                 $requisitiondate = array(
                        'requirement_raise_id' => $requirement_id,
                        'status' => 'Pending'
                 );
                
                $requisitionInfo = $this->requisition->save($requisitiondate);
                $requisition_id = $requisitionInfo->id;

                foreach ($requirementDetail->requirementDetail as $key => $value) {
                   
                    $requisitionDetail = array(
                            
                        'requisition_id' => $requisition_id,
                        'material_name' => $value->material_name,
                        'qty' => $value->qty,
                        'status' => 'Awaiting'

                    );
                    $this->requisition->saveDetail($requisitionDetail);
                }
            }

        }

        $data['requisition'] = $this->requisition->findAll($limit= 50, $input);
        return view('procurement::requisition.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('procurement::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $data['requisition'] = $this->requisition->find($id);

        return view('procurement::requisition.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $data = $request->all(); 

        $requisition_id = $data['requisition_id'];

        $user = Auth::user();

        try{ 
             
            $this->requisition->deleteDetail($requisition_id);

            $materials = $data['material_name'];
            $countmaterials = sizeof($materials); 

                for($i = 0; $i < $countmaterials; $i++){
           
                     if($data['material_name'][$i]){
                         $RequisitionDetaildata['requisition_id'] = $requisition_id;
                         $RequisitionDetaildata['material_name'] = $data['material_name'][$i];
                         $RequisitionDetaildata['qty'] = $data['qty'][$i];
                         $RequisitionDetaildata['rate'] = $data['rate'][$i];
                         $RequisitionDetaildata['amount'] = $data['amount'][$i];
                         $RequisitionDetaildata['remark'] = $data['remark'][$i];
                         $RequisitionDetaildata['status'] = $data['status'][$i];

                         $this->requisition->saveDetail($RequisitionDetaildata);
                     }
                         
                 }

            $appovedCount = $this->requisition->checkApprovedDetail($requisition_id);
            
            if($appovedCount > 0){
                
                $requisition_data = array(
                    'status' => 'Approved',
                    'approved_by' =>$user->id
                );

                $this->requisition->update($id,$requisition_data);
            
            }     

            toastr()->success('Requisition Detail Updated Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('requisition.index'));

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
