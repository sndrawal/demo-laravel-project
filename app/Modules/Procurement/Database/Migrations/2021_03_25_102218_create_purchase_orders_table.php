<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('voucher_no')->nullable();
            $table->string('mode_of_payment')->nullable();
            $table->text('bank_name')->nullable();
            $table->text('cheque_no')->nullable();
            $table->string('platform')->nullable();
            $table->integer('no_of_day')->nullable();
            $table->string('supplier_ref_no')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->string('dispatch_through')->nullable();
            $table->string('destination')->nullable();
            $table->string('terms_of_delivery')->nullable();
            $table->string('site')->nullable();
            $table->integer('warehouse_id')->nullable();
            $table->string('distribution_channel')->nullable();
            $table->integer('issued_by')->nullable();
            $table->double('sub_total',14,2)->nullable();
            $table->double('discount_percent',14,2)->nullable();
            $table->double('discount_amount',14,2)->nullable();
            $table->double('total_after_discount',14,2)->nullable();
            $table->double('vat_amount',14,2)->nullable();
            $table->double('grand_total',14,2)->nullable();
            $table->text('number_in_words')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}