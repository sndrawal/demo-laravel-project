<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequirementRaiseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requirement_raise_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('requirement_raise_id')->nullable();
            $table->text('material_name')->nullable();
            $table->double('qty',14,2)->nullable();
            $table->text('purpose')->nullable();
            $table->text('remark')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requirement_raise_details');
    }
}

