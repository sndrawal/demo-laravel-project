<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {

        /*
        |--------------------------------------------------------------------------
        | Procurement  CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('procurement', ['as' => 'procurement.index', 'uses' => 'ProcurementController@index']);
        //Procurement Create
        Route::get('procurement/create', ['as' => 'procurement.create', 'uses' => 'ProcurementController@create']);
        Route::post('procurement/store', ['as' => 'procurement.store', 'uses' => 'ProcurementController@store']);

        Route::post('procurement/updateStatus', ['as' => 'procurement.updateStatus', 'uses' => 'ProcurementController@updateStatus']);

        //Procurement Edit
        Route::get('procurement/edit/{id}', ['as' => 'procurement.edit', 'uses' => 'ProcurementController@edit'])->where('id','[0-9]+');
        Route::put('procurement/update/{id}', ['as' => 'procurement.update', 'uses' => 'ProcurementController@update'])->where('id','[0-9]+');
        //Procurement Delete
        Route::get('procurement/delete/{id}', ['as' => 'procurement.delete', 'uses' => 'ProcurementController@destroy'])->where('id','[0-9]+');
        Route::get('procurement/appendMaterial', ['as' => 'procurement.appendMaterial', 'uses' => 'ProcurementController@appendMaterial']);

        Route::post('procurement/get-requirement-detail-ajax', ['as'=>'procurement.get-requirement-detail-ajax','uses'=>'ProcurementController@getRequirementDetailAjax']);


        /*
        |--------------------------------------------------------------------------
        | Material Requisition  CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('requisition', ['as' => 'requisition.index', 'uses' => 'RequisitionController@index']);
        //Material Requisition Create
        Route::get('requisition/create', ['as' => 'requisition.create', 'uses' => 'RequisitionController@create']);
        Route::post('requisition/store', ['as' => 'requisition.store', 'uses' => 'RequisitionController@store']);

        Route::post('requisition/updateStatus', ['as' => 'requisition.updateStatus', 'uses' => 'RequisitionController@updateStatus']);

        //Material Requisition Edit
        Route::get('requisition/edit/{id}', ['as' => 'requisition.edit', 'uses' => 'RequisitionController@edit'])->where('id','[0-9]+');
        Route::put('requisition/update/{id}', ['as' => 'requisition.update', 'uses' => 'RequisitionController@update'])->where('id','[0-9]+');
        //Material Requisition Delete
        Route::get('requisition/delete/{id}', ['as' => 'requisition.delete', 'uses' => 'RequisitionController@destroy'])->where('id','[0-9]+');
        Route::get('requisition/appendMaterial', ['as' => 'requisition.appendMaterial', 'uses' => 'RequisitionController@appendMaterial']);

        Route::post('requisition/get-requisition-detail-ajax', ['as'=>'requisition.get-requisition-detail-ajax','uses'=>'RequisitionController@getRequisitionDetailAjax']);


        /*
        |--------------------------------------------------------------------------
        | Purchase Order  CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('purchaseOrder', ['as' => 'purchaseOrder.index', 'uses' => 'PurchaseOrderController@index']);
        //Purchase Order Create
        Route::get('purchaseOrder/create', ['as' => 'purchaseOrder.create', 'uses' => 'PurchaseOrderController@create']);
        Route::post('purchaseOrder/store', ['as' => 'purchaseOrder.store', 'uses' => 'PurchaseOrderController@store']);

        Route::post('purchaseOrder/view-invoice-ajax', ['as'=>'purchaseOrder.view-invoice-ajax','uses'=>'PurchaseOrderController@viewInvoiceAjax']);



});
