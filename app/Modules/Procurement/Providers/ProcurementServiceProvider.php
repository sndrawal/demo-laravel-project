<?php

namespace App\Modules\Procurement\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

use App\Modules\Procurement\Repositories\RequirementRaiseInterface;
use App\Modules\Procurement\Repositories\RequirementRaiseRepository;

use App\Modules\Procurement\Repositories\RequisitionInterface;
use App\Modules\Procurement\Repositories\RequisitionRepository;

use App\Modules\Procurement\Repositories\PurchaseOrderInterface;
use App\Modules\Procurement\Repositories\PurchaseOrderRepository;

class ProcurementServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Procurement';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'procurement';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->requirementRegister();
        $this->requisitionRegister();
        $this->purchaseorderRegister();
    }
    
    public function requirementRegister(){
        $this->app->bind(
            RequirementRaiseInterface::class,
            RequirementRaiseRepository::class
        );
    }

    public function requisitionRegister(){
        $this->app->bind(
            RequisitionInterface::class,
            RequisitionRepository::class
        );
    }
    
    public function purchaseorderRegister(){
        $this->app->bind(
            PurchaseOrderInterface::class,
            PurchaseOrderRepository::class
        );
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
