<?php

namespace App\Modules\Lead\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Lead\Entities\Lead;

class leadActivities extends Model
{
    protected $fillable = [

    	'lead_id',
    	'lead_executive',
    	'lead_status',
    	'next_follow_up_date',
    	'additional_message_box',
    	'created_by'

    ];

    public function getLead(){
        return $this->belongsTo(Lead::class,'lead_id','id');
    }
}
