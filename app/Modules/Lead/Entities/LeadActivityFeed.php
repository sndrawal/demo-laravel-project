<?php

namespace App\Modules\Lead\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Lead\Entities\Note;
use App\Modules\Lead\Entities\Meeting;
use App\Modules\Lead\Entities\Call;
use App\Modules\SupportTicket\Entities\Ticket;

class LeadActivityFeed extends Model
{
    protected $fillable = [

    	'lead_id',
    	'contact_id',
    	'method',
    	'method_id'
    ];

    
    public static function getNoteByLead($note_id){
        return Note::find($note_id);
    }

    public static function getTaskByLead($ticket_id){
        return Ticket::find($ticket_id);
    }

    public static function getMeetingByLead($meeting_id){
        return Meeting::find($meeting_id);
    }

    public static function getCallByLead($call_id){
        return Call::find($call_id);
    }
}
