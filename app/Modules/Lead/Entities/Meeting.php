<?php

namespace App\Modules\Lead\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Lead\Entities\Lead;
use App\Modules\Contact\Entities\Contact;
use App\Modules\User\Entities\User;

class Meeting extends Model
{
    protected $fillable = [

    	'lead_id',
    	'contact_id',
    	'meeting_about',
    	'attendee',
    	'date',
    	'time',
    	'duration',
    	'description',
    	'created_by',
    	'updated_by'

    ];

	public function lead(){
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function contact(){
        return $this->belongsTo(Contact::class,'contact_id','id');
    }

    public function createUser(){
        return $this->belongsTo(User::class,'created_by','id');
    }
    public function updateUser(){
        return $this->belongsTo(User::class,'updated_by','id');
    }

     public static function getUserByAttendee($user_id){
        return User::where('id','=',$user_id)->get()->first();
    }

}
