<?php

namespace App\Modules\Lead\Entities;

use Illuminate\Database\Eloquent\Model;

class ProjectFolder extends Model
{
    protected $fillable = [

    	'lead_id',
        'folder_name',
        'folder_slug',
        
    ];
}
