<?php

namespace App\Modules\Lead\Entities;

use Illuminate\Database\Eloquent\Model;

class ManageFile extends Model
{
	const FILE_PATH = '/uploads/lead_projects/';
	
    protected $fillable = [

        'project_folder_id',
        'upload_filename',
        'file_type',

    ];
}
