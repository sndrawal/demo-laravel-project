<?php

namespace App\Modules\Lead\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Lead\Entities\Lead;
use App\Modules\Contact\Entities\Contact;
use App\Modules\User\Entities\User;

class leadComment extends Model
{
    protected $fillable = [

    	'lead_id',
    	'contact_id',
    	'note_id',
    	'meeting_id',
    	'call_id',
    	'comment',
    	'comment_by'

    ];

    public function lead(){
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function contact(){
        return $this->belongsTo(Contact::class,'contact_id','id');
    }

    public function CommentBy(){
        return $this->belongsTo(User::class,'comment_by','id');
    }

    public static function getNoteById($note_id){
        return LeadComment::where('note_id','=',$note_id)->get();
    }

    public static function getMeetingCommentById($meeting_id){
        return LeadComment::where('meeting_id','=',$meeting_id)->get();
    }

    public static function getCallCommentById($call_id){
        return LeadComment::where('call_id','=',$call_id)->get();
    }

}
