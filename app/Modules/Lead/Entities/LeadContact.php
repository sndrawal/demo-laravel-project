<?php

namespace App\Modules\Lead\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Lead\Entities\Lead;
use App\Modules\Contact\Entities\Contact;

class LeadContact extends Model
{
    protected $fillable = [

    	'lead_id',
    	'contact_id'

    ];

    public function lead(){
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function contact(){
        return $this->belongsTo(Contact::class,'contact_id','id');
    }

}
