<?php

namespace App\Modules\Lead\Entities;

use App\Modules\Dropdown\Entities\dropdown;
use App\Modules\User\Entities\User;
use App\Modules\Lead\Entities\LeadLog;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    const FILE_PATH = '/uploads/leads/';
    
    protected $fillable = [
        
        'company_logo',
        'company_name',
        'client_address',
        'contact_person',
        'contact_no_1',
        'contact_no_2',
        'designation',
        'email',
        'purpose_of_meeting',
        'means_of_communication',
        'services',
        'source_of_leads',
        'client_approach_cost',
        'client_approach_discount',
        'client_approach_final_cost',
        'next_meeting_date',
        'brief_description',
        
        'service_required',
        'terminate_reason',
        'cost',
        'discount',
        'final_cost',
        'active_lead_services',
        'additional_note',
        'client_expectation_note',
            
        'segment',   
        'status',
        'lead_created_date',
        'next_visit_date',
        'created_by',
        'updated_by'
    ];

    public function getFileFullPathAttribute()
    {
        return self::FILE_PATH . $this->file_name;
    }

    public function getUser(){
        return $this->belongsTo(User::class,'created_by','id');
    }
    public function getUpdatedUser(){
        return $this->belongsTo(User::class,'updated_by','id');
    }

    public function leadStatusList(){
        return [
            ' Daily Client Approved' => 0,
            'Hot Leads' =>1,
            'Cold Leads'=>2,
            'Sales Pipeline'=>3,
            'Converted Leads'=>4,
            'Rejected Leads'=>5

        ];
    }

    public function dropdown(){
        return $this->belongsTo(Dropdown::class,'segment','id');
    }


    public function purposeMeeting(){
        return $this->belongsTo(Dropdown::class,'purpose_of_meeting','id');
    }

    public function leadHistory(){
        return $this->hasMany(LeadLog::class,'lead_id','id')->orderBy('created_at','DESC');
    }

}
