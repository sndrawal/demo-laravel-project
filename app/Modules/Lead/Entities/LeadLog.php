<?php

namespace App\Modules\Lead\Entities;

use Illuminate\Database\Eloquent\Model;

class leadLog extends Model
{
    protected $fillable = [

    	'lead_id',
    	'contact_id',
    	'log_message',

    ];
}
