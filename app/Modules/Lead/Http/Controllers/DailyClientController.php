<?php

namespace App\Modules\Lead\Http\Controllers;

use App\Modules\Employment\Repositories\EmploymentInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Lead\Repositories\DailyClientInterface;
use App\Modules\Dropdown\Repositories\DropdownInterface;

use App\Modules\Lead\Repositories\LeadLogInterface;
use App\Modules\Lead\Repositories\LeadActivityInterface;

use App\Modules\Lead\Repositories\NoteInterface;
use App\Modules\Lead\Repositories\MeetingInterface;
use App\Modules\Lead\Repositories\CallInterface;
use App\Modules\SupportTicket\Repositories\SupportTicketInterface;
use App\Modules\Lead\Repositories\LeadActivityFeedInterface;

use App\Modules\Contact\Repositories\ContactInterface;

use App\Modules\User\Repositories\UserInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;  
class DailyClientController extends Controller
{
      
     protected $dailyclient; 
     protected $dropdown;
     protected $user;
     protected $log;
     protected $activity;

     protected $note;
     protected $meeting;
     protected $call;
     protected $task;
     protected $contact;
     protected $feed;
     protected $master_user_ids;

    /**
     * @var EmploymentInterface
     */
    private $employment;
    

    /**
     * DailyClientController constructor.
     * @param DailyClientInterface $dailyclient
     * @param DropdownInterface $dropdown
     * @param UserInterface $user
     * @param EmploymentInterface $employment
     * @param ContactInterface $contact
     */
    public function __construct(DailyClientInterface $dailyclient, DropdownInterface $dropdown, UserInterface $user , EmploymentInterface $employment,LeadLogInterface $log,LeadActivityInterface $activity,NoteInterface $note,MeetingInterface $meeting,CallInterface $call,SupportTicketInterface $task,ContactInterface $contact,LeadActivityFeedInterface $feed)
    {
        $this->dailyclient = $dailyclient;
        $this->dropdown = $dropdown;
        $this->user = $user;
        $this->employment = $employment;
        $this->log = $log;
        $this->activity = $activity;
        
        $this->note = $note;
        $this->meeting = $meeting;
        $this->call = $call;
        $this->task = $task;
        $this->contact = $contact;
        $this->feed = $feed;
        $this->master_user_ids = array();

    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $search = $request->all();


        $data=$this->indexData($search);

        if($search AND array_key_exists('type', $search)){
           
            if ($request->ajax()) {
                return view('lead::dailyClient.partial.list-grid', $data)->render();
            }
          return view('lead::dailyClient.index',$data);
        }else{
            if ($request->ajax()) {
                return view('lead::dailyClient.partial.list', $data)->render();
            }
           return view('lead::dailyClient.index_list',$data);
        }

    }

    public function dashboard(Request $Request){

        $data['running_lead'] = $this->dailyclient->findConvertedLead($limit = 50);
        return view('lead::dailyClient.dashboard',$data);
    }


    public function profile(Request $request){

        $input = $request->all();

        $employer_user= $this->user->getAllActiveUser();
        
        $employee_data = array();
        foreach ($employer_user as $key => $user) {

            $employee_data += array(
                $user->id => $user->first_name . ' ' . $user->last_name
            );
        }

        $data['assoicateContact'] = $this->dailyclient->findContactById($input['id']);
        $data['count_assoicateContact'] = $this->dailyclient->countContactById($input['id']);

        $data['assigned_user'] = $employee_data;
        $data['ticket_type'] = $this->dropdown->getFieldBySlug('ticket_type');
        $data['lead_profile'] = $this->dailyclient->find($input['id']);


        $data['communication'] =$this->dropdown->getFieldBySlug('means_of_communication'); // 1 is for Mean of Commication in Field Table
        $data['purpose_of_meeting'] =$this->dropdown->getFieldBySlug('purpose_of_meeting'); // 2 is for Purpose of Meeting in Field Table
        $data['services'] =$this->dropdown->getFieldBySlug('services'); // 3 is for Service in Field Table
        $data['active_services'] =$this->dropdown->getFieldBySlug('services'); // 3 is for Service in Field Table
        $data['source_of_lead'] =$this->dropdown->getFieldBySlug('source_of_leads'); // 4 is for Source of Leads in Field Table
        $data['segment'] = $this->dropdown->getFieldBySlug('segment');
        $data['is_edit'] = false;


        /** Lead Tab Detail  **/
         $data['lead_activity_feed'] = $this->dailyclient->getLeadActivityFeed('lead_id',$input['id'],$limit=10);
        $data['note_detail'] = $this->note->findAll('lead_id',$input['id'],$limit=10);
        $data['task_detail'] = $this->task->getLeadTask('lead_id',$input['id'],$limit=10);
        $data['meeting_detail'] = $this->meeting->findAll('lead_id',$input['id'],$limit=10);

        $data['call_detail'] = $this->call->findAll('lead_id',$input['id'],$limit=10);

        $data['note_count'] = $this->note->countNote('lead_id',$input['id']);
        $data['task_count'] = $this->task->countLeadTask('lead_id',$input['id']);
        $data['meeting_count'] = $this->meeting->countMeeting('lead_id',$input['id']);
        $data['call_count'] = $this->call->countCall('lead_id',$input['id']);

        $contactList = $this->contact->findAll();
        $contact_data = array();
        foreach ($contactList as $key => $contact) {

            $contact_data += array(
                $contact->id => $contact->first_name . ' ' . $contact->last_name
            );
        }
        $data['contacts'] = $contact_data;


        return view('lead::dailyClient.profile',$data);
    }

    public function associateContact(Request $request)
    {
         $data = $request->all();

         $lead_id = $data['lead_id'];
         $contact_id = $data['contact_id'];
         $associate_lead = $data['associate_lead'];

         $checkAssociate = $this->dailyclient->checkAssociateLeadContact($lead_id,$contact_id);

         if($checkAssociate >0){
                alertify()->error('Lead Already Associated With Contact');
         }else{
             $this->dailyclient->associateLeadContact($data);
             alertify()->success('Lead Associated With Contact');
         }

         if($associate_lead == 1){
           return redirect(route('dailyClient.profile',['id'=>$lead_id]));
         }else{
            return redirect(route('contact.profile',['id'=>$contact_id]));
         }

    }


    public function findChildId($userId){

        $parent_id = $userId;
        $childUser = $this->user->getChild($parent_id);

        if(!$childUser->isEmpty()){

                foreach ($childUser as $key => $value) {
                     $subchild_user_id = $value->id; 
                     array_push($this->master_user_ids, $subchild_user_id);
                     $this->findChildId($subchild_user_id);
                }
                
        }

        return $this->master_user_ids;

    }

    protected function indexData($search){
        $userInfo = Auth::user();
        $user_type = $userInfo->user_type;
        $id = $userInfo->id;

        $child_client = array();

        $parent_id = $userInfo->parent_id;  
        if($parent_id !== 0){
  

            /* Checking Via Multiple Id*/
            $id_array =[];
            $id = $userInfo->id;
            array_push($this->master_user_ids, $id);
            $multi_ids = $this->findChildId($id);
            $data['dailyClient'] = $this->dailyclient->findAllLead($multi_ids,$limit =50,$search);
            /*End of Multiple Id*/

            $data['employee'] = $this->user->getAllChildUser($multi_ids);

        }else{

            $data['dailyClient'] = $this->dailyclient->findAll($limit =50,$search);
            $data['employee'] = $this->user->getAllMarketing();
        }

        $data['daily_client']  = $this->dailyclient->getAllClientByStatus($id,0);

        $data['hot_client'] = $this->dailyclient->getAllClientByStatus($id,1);
        $data['cold_client'] = $this->dailyclient->getAllClientByStatus($id,2);
        $data['salespipeline_client'] = $this->dailyclient->getAllClientByStatus($id,3);
        $data['converted_client'] = $this->dailyclient->getAllClientByStatus($id,4);
        $data['rejected_client'] = $this->dailyclient->getAllClientByStatus($id,5);



        $data['services'] =$this->dropdown->getFieldBySlug('services'); // 3 is for Service in Field Table
        $data['source_of_lead'] =$this->dropdown->getFieldBySlug('source_of_leads'); // 4 is for Source of Leads in Field Table
        
        $data['search_value'] =  $search;

        return $data;

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
         $data['communication'] =$this->dropdown->getFieldBySlug('means_of_communication'); // 1 is for Mean of Commication in Field Table
         $data['purpose_of_meeting'] =$this->dropdown->getFieldBySlug('purpose_of_meeting'); // 2 is for Purpose of Meeting in Field Table
         $data['services'] =$this->dropdown->getFieldBySlug('services'); // 3 is for Service in Field Table
         $data['source_of_lead'] =$this->dropdown->getFieldBySlug('source_of_leads'); // 4 is for Source of Leads in Field Table
         $data['is_edit'] = false;

        $data['contact_type'] = $this->dropdown->getFieldBySlug('contact_type');
        $data['state'] = $this->dropdown->getFieldBySlug('state');
        $data['district'] = $this->dropdown->getFieldBySlug('district');
        $data['countries'] = $this->employment->getCountries();
        $data['salutation'] = $this->dropdown->getFieldBySlug('salutation');
        $data['segment'] = $this->dropdown->getFieldBySlug('segment');
        return view('lead::dailyClient.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        $means_of_communication = json_encode($data['means_of_communication']);
        $purpose_of_meeting = json_encode($data['purpose_of_meeting']);
        $services = json_encode($data['services']);
        $source_of_leads = json_encode($data['source_of_leads']);
        
        unset($data['means_of_communication']);
        unset($data['purpose_of_meeting']);
        unset($data['services']);
        unset($data['source_of_leads']);
        
        $data['means_of_communication'] = $means_of_communication;
        $data['purpose_of_meeting'] = $purpose_of_meeting;
        $data['services'] = $services;
        $data['source_of_leads'] = $source_of_leads;
        
        $data['lead_created_date'] = date('Y-m-d');
        
        try{
            $userInfo = Auth::user();
            $user_id = $userInfo->id;
            $data['created_by'] =$user_id;
            $data['status'] = '0';
            $lead_info = $this->dailyclient->save($data);

            $lead_history['lead_id']=$lead_info->id;
            $lead_history['log_message']=$userInfo->first_name." ".$userInfo->last_name." Created lead of ".$lead_info->company_name.'.';
            $log_info = $this->log->save($lead_history);

            /** Activity Feed Set **/

            $feed['lead_id'] = $lead_info->id;
            $feed['method'] = 'Lead_Status';
            $feed['method_id'] = $log_info->id;

            $this->feed->save($feed);

            /** End of Activity Feed Set **/

            alertify()->success('Daily Client Created Successfully');
            
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }
        
        return redirect(route('dailyClient.index'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('lead::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['is_edit'] = true;
        $data['client'] = $clientInfo =  $this->dailyclient->find($id);

        $employer_user = $this->user->getAllMarketing();
        $employee_data = array();
        foreach ($employer_user as $key => $user) {

            $employee_data += array(
                $user->id => $user->first_name . ' ' . $user->last_name
            );
        }

        $data['users'] = $employee_data;
        $data['communication'] =$this->dropdown->getFieldBySlug('means_of_communication'); // 1 is for Mean of Commication in Field Table
        $data['purpose_of_meeting'] =$this->dropdown->getFieldBySlug('purpose_of_meeting'); // 2 is for Purpose of Meeting in Field Table
        $data['services'] =$this->dropdown->getFieldBySlug('services'); // 3 is for Service in Field Table
        $data['active_services'] =$this->dropdown->getFieldBySlug('services'); // 3 is for Service in Field Table
        $data['source_of_lead'] =$this->dropdown->getFieldBySlug('source_of_leads'); // 4 is for Source of Leads in Field Table
        $data['segment'] = $this->dropdown->getFieldBySlug('segment');
        return view('lead::dailyClient.edit',$data);
        
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all(); 
        
        $means_of_communication = json_encode($data['means_of_communication']);
        $purpose_of_meeting = json_encode($data['purpose_of_meeting']);
        $services = json_encode($data['services']);
        $source_of_leads = json_encode($data['source_of_leads']);
        
        unset($data['means_of_communication']);
        unset($data['purpose_of_meeting']);
        unset($data['services']);
        unset($data['source_of_leads']);
        
        $data['means_of_communication'] = $means_of_communication;
        $data['purpose_of_meeting'] = $purpose_of_meeting;
        $data['services'] = $services;
        $data['source_of_leads'] = $source_of_leads;
        
       if (array_key_exists("active_lead_services",$data)){
            $active_lead_services = json_encode($data['active_lead_services']);
             unset($data['active_lead_services']);
             $data['active_lead_services'] = $active_lead_services;     
        }

        $is_from_profile = $data['profile_lead'];
            
        try{

            $new_status = $data['status'];
            $clientInfo =  $this->dailyclient->find($id);
            $previous_status = $clientInfo->status;

            $userInfo = Auth::user();
            $user_id = $userInfo->id;

            if($previous_status != $new_status){  

                $Masterstatus = array('1'=>'Hot Leads','2'=>'Cold Leads','3'=>'Sales Pipeline','4'=>'Converted','5'=>'Rejected');

       
               if(array_key_exists($previous_status, $Masterstatus)){
                    $prev_Staus = $Masterstatus[$previous_status];
               }


               if(array_key_exists($new_status, $Masterstatus)){
                    $nStaus = $Masterstatus[$new_status]; 
               }


                
                $data['updated_by'] =$user_id;

                $lead_history['lead_id']=$id;
                $lead_history['log_message'] = $userInfo->first_name." ".$userInfo->last_name." Updated Status From ".$prev_Staus.' To '.$nStaus.'.';

                $log_info = $this->log->save($lead_history);

                   /** Activity Feed Set **/

                    $feed['lead_id'] = $id;
                    $feed['method'] = 'Lead_Status';
                    $feed['method_id'] = $log_info->id;

                    $this->feed->save($feed);

                    /** End of Activity Feed Set **/
            }


            $company_name = $data['company_name'];

            $this->dailyclient->update($id,$data);

            $lead_history['lead_id']=$id;
            $lead_history['log_message']=$userInfo->first_name." ".$userInfo->last_name." Updated lead of ".$company_name.'.';
            $this->log->save($lead_history);


            alertify()->success('Lead Updated Successfully');
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }
        
        if($is_from_profile == '1'){
            return redirect(route('dailyClient.profile',['id'=>$id]));
        }else{
        return redirect(route('dailyClient.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
   public function destroy($id)
    {
        try{

            $clientInfo =  $this->dailyclient->find($id);

            $userInfo = Auth::user();
            $lead_history['lead_id']=$id;
            $lead_history['log_message'] = $userInfo->first_name." ".$userInfo->last_name." Deleted lead of ".$clientInfo->company_name.'.';
            $this->log->save($lead_history);


            $this->dailyclient->deleteContactLead($id);

            $this->dailyclient->delete($id);
            alertify()->success('Lead Deleted Successfully');
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }
      return redirect(route('dailyClient.index'));
    }
    
   public function updateStatus(Request $request){
        
        $data = $request->all();  
        if(array_key_exists('deal_lead', $data)){
            $deal_lead = $data['deal_lead'];
            unset($data['deal_lead']);
        }else{
            $deal_lead = 0;
        }

        $service_required = $data['service_required'];
        if($service_required == 'Hot Leads'){
             $data['status'] = '1';
        }
        elseif($service_required == 'Cold Leads'){
             $data['status'] = '2';
        }
        elseif($service_required == 'Sales Pipeline'){
            $data['status'] = '3';
        }
        elseif($service_required == 'Converted'){
            $data['status'] = '4';
        }
        elseif($service_required == 'Rejected'){
            $data['status'] = '5';
        }
        
        if (array_key_exists("active_lead_services",$data)){
            $active_lead_services = json_encode($data['active_lead_services']);
            unset($data['active_lead_services']);
            $data['active_lead_services'] = $active_lead_services;
        }
        
        $client_id = $data['client_id'];  

         $clientInfo =  $this->dailyclient->find($client_id);
         $previous_status = $clientInfo->status;
         $Masterstatus = array('1'=>'Hot Leads','2'=>'Cold Leads','3'=>'Sales Pipeline','4'=>'Converted','5'=>'Rejected');

               if(array_key_exists($previous_status, $Masterstatus)){
                    $lead_status = $Masterstatus[$previous_status];
               }else{
                $lead_status = 'Daily Client';
               }

        // $lead_status = 'Daily Client';
        $userInfo = Auth::user();
        $lead_history['lead_id']=$client_id;
        $lead_history['log_message'] = $userInfo->first_name." ".$userInfo->last_name." Updated Status From ".$lead_status.' To '.$service_required.'.';
        $log_info = $this->log->save($lead_history);

           /** Activity Feed Set **/

            $feed['lead_id'] = $client_id;
            $feed['method'] = 'Lead_Status';
            $feed['method_id'] = $log_info->id;

            $this->feed->save($feed);

            /** End of Activity Feed Set **/


        $this->dailyclient->update($client_id,$data);
        alertify()->success('Lead Status Updated Successfully');    

        if($deal_lead == '1'){
            return redirect(route('dailyClient.profile',['id'=>$client_id]));
        }else{
            return redirect(route('dailyClient.index'));
        }   
    }
    
     public function updateLeadStatus(Request $request){
        
        $data = $request->all(); 

        $client_id = $data['client_id'];
        $data['status'] = '2';
        
        $this->dailyclient->update($client_id,$data);
        alertify()->success('Lead Status Updated Successfully');         
        return redirect(route('dailyClient.index'));
    }
    
    /**
     * Show the specified resource.
     * @return Response
     */
    public function view(Request $request)
    {
        $data = $request->all();
        
        $id = $data['id'];
        $daily_client = $this->dailyclient->find($id);

        $services = json_decode($daily_client->services); 
        $service_lead = '';
        if(!empty($services)){
            foreach($services as $key => $service){
                $service_lead .= ' - '. $this->dropdown->getDropdownById($service)->dropvalue;
                $service_lead .= '<br>';
            }
        }
        
        $meeting = json_decode($daily_client->purpose_of_meeting); 
        $purpose_of_meeting = '';
        if(!empty($meeting)){
            foreach($meeting as $key => $value){
                $purpose_of_meeting .= ' - '. $this->dropdown->getDropdownById($value)->dropvalue;
                $purpose_of_meeting .= '<br>';
            }
        }
        
        $communications = json_decode($daily_client->means_of_communication); 
        $means_of_communicate = '';
        if(!empty($communications)){
            foreach($communications as $key => $communication){
                $means_of_communicate .= ' - '. $this->dropdown->getDropdownById($communication)->dropvalue;
                $means_of_communicate .= '<br>';
            }
        }
        
        $sourceLead = json_decode($daily_client->source_of_leads); 
        $source_lead = '';
         if(!empty($sourceLead)){
            foreach($sourceLead as $key => $source){
                $source_lead .= ' - '. $this->dropdown->getDropdownById($source)->dropvalue;
                $source_lead .= '<br>';
            }
         }
        
        $activeLeadServices = json_decode($daily_client->active_lead_services); 
        $active_lead_service = '';
        if($activeLeadServices){
            foreach($activeLeadServices as $key => $val){
                $active_lead_service .= ' - '. $this->dropdown->getDropdownById($val)->dropvalue;
                $active_lead_service .= '<br>';
            }
        }
        
        
        $html ='';
            $html .= "<table class='table table-striped mb30' id='table1' cellspacing='0' width='100%' frame='box'>";
            $html .= "<thead>";
            $html .= " <tr>";
            $html .= "<th width='25%'></th>";
            $html .= "<th width='25%'></th>";
            $html .= "<th width='25%'></th>";
            $html .= "<th width='25%'></th>";
            $html .= "</tr>";
            $html .= "</thead>";
            $html .= "<tbody>";
            $html .= "<tr>";
            $html .= "<td colspan='4'><h3>Daily Client Approach</h3></td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td class='text-success'>Company Name : </td>";
            $html .= "<td>".$daily_client->company_name."</td>";
            $html .= "<td class='text-success'>Address</td>";
            $html .= "<td>".$daily_client->client_address ."</td>";
            $html .= "</tr>";

            $html .= "<tr>";
            $html .= "<td class='text-success'>Contact Person: </td>";
            $html .= "<td>".$daily_client->contact_person."</td>";
            $html .= "<td class='text-success'>Contact Number 1 : </td>";
            $html .= "<td>".$daily_client->contact_no_1."</td>";
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Contact Number 2 : </td>";
            $html .= "<td>".$daily_client->contact_no_2."</td>";
            $html .= "<td class='text-success'>Purpose of Meeting: </td>";
            $html .= "<td>".$purpose_of_meeting."</td>";  
            $html .= "</tr>";

            $html .= "<tr>";
            $html .= "<td class='text-success'>Designation: </td>";
            $html .= "<td>".$daily_client->designation."</td>";
            $html .= "<td class='text-success'>Email: </td>";
            $html .= "<td>".$daily_client->email."</td>";
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Mean of Communication: </td>";
            $html .= "<td>".$means_of_communicate."</td>";
            $html .= "<td class='text-success'>Source of Lead</td>";
            $html .= "<td>".$source_lead."</td>";
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Services</td>";
            $html .= "<td>".$service_lead."</td>";
            $html .= "<td class='text-success'>Client Approach Cost : </td>";
            $html .= "<td>Rs.".$daily_client->client_approach_cost."/-</td>";  
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Approach Discount : </td>";
            $html .= "<td>".$daily_client->client_approach_discount."%</td>";
            $html .= "<td class='text-success'>Final Approach Cost : </td>";
            $html .= "<td>Rs.".$daily_client->client_approach_final_cost."/-</td>";
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Brief Description Note : </td>";
            $html .= "<td colspan='3'>".$daily_client->brief_description."</td>";
            $html .= "</tr>";

            if($daily_client->status == 1 || $daily_client->status == 2 || $daily_client->status == 3 || $daily_client->status == 4){
                $lead_name  = $daily_client->status == 1 ? 'Hot Lead' : ($daily_client->status == 2 ? 'Cold Lead' : ($daily_client->status == 3 ? 'Sales Pipeline' : 'Converted Lead'));
                $html .= "<tr>";
                $html .= "<td colspan='4'><h3>".$lead_name."</h3></td>";
                $html .= "</tr>";
        
                $html .= "<tr>";
                $html .= "<td class='text-success'>Active Service : </td>";
                $html .= "<td>".$active_lead_service."</td>"; 
                $html .= "<td class='text-success'>Cost : </td>";
                $html .= "<td>Rs.".$daily_client->cost."/-</td>";
                $html .= "</tr>";
        
                $html .= "<tr>";
                $html .= "<td class='text-success'>Discount : </td>";
                $html .= "<td>".$daily_client->discount."%</td>";
                $html .= "<td class='text-success'>Final Cost : </td>";
                $html .= "<td>Rs.".$daily_client->final_cost."/-</td>";
                $html .= "</tr>";
        
                $html .= "<tr>";
                $html .= "<td class='text-success'>Additional Note : </td>";
                $html .= "<td>".$daily_client->additional_note."</td>";
                $html .= "<td class='text-success'>Client Expectation Note : </td>";
                $html .= "<td>".$daily_client->client_expectation_note."</td>";
                $html .= "</tr>";
            }
            elseif($daily_client->status == 5){
                $html .= "<tr>";
                $html .= "<td colspan='4'><h3>Rejected Lead</h3></td>";
                $html .= "</tr>";
        
                $html .= "<tr>";
                $html .= "<td class='text-success'>Termination Reason : </td>";
                $html .= "<td>".$daily_client->terminate_reason."</td>";
                $html .= "<td></td>";
                $html .= "<td></td>";
                $html .= "</tr>";
            }

           
        
            $html .= "</tbody>";
            $html .= "</table>";

        return  $html;
    }

    public function summary_view($id)
    {
        $daily_client = $this->dailyclient->find($id);

        $services = json_decode($daily_client->services); 
        $service_lead = '';
        if(!empty($services)){
            foreach($services as $key => $service){
                $service_lead .= ' - '. $this->dropdown->getDropdownById($service)->dropvalue;
                $service_lead .= '<br>';
            }
        }
        
        $meeting = json_decode($daily_client->purpose_of_meeting); 
        $purpose_of_meeting = '';
        if(!empty($meeting)){
            foreach($meeting as $key => $value){
                $purpose_of_meeting .= ' - '. $this->dropdown->getDropdownById($value)->dropvalue;
                $purpose_of_meeting .= '<br>';
            }
        }
        
        $communications = json_decode($daily_client->means_of_communication); 
        $means_of_communicate = '';
        if(!empty($communications)){
            foreach($communications as $key => $communication){
                $means_of_communicate .= ' - '. $this->dropdown->getDropdownById($communication)->dropvalue;
                $means_of_communicate .= '<br>';
            }
        }
        
        $sourceLead = json_decode($daily_client->source_of_leads); 
        $source_lead = '';
         if(!empty($sourceLead)){
            foreach($sourceLead as $key => $source){
                $source_lead .= ' - '. $this->dropdown->getDropdownById($source)->dropvalue;
                $source_lead .= '<br>';
            }
         }
        
        $activeLeadServices = json_decode($daily_client->active_lead_services); 
        $active_lead_service = '';
        if($activeLeadServices){
            foreach($activeLeadServices as $key => $val){
                $active_lead_service .= ' - '. $this->dropdown->getDropdownById($val)->dropvalue;
                $active_lead_service .= '<br>';
            }
        }
        
        
        $html ='';
            $html .= "<table class='table table-striped mb30' id='table1' cellspacing='0' width='100%' frame='box'>";
           
            $html .= "<tr>";
            $html .= "<td class='text-success'>Company Name : </td>";
            $html .= "<td>".$daily_client->company_name."</td>";
            $html .= "<td class='text-success'>Address</td>";
            $html .= "<td>".$daily_client->client_address ."</td>";
            $html .= "</tr>";

            $html .= "<tr>";
            $html .= "<td class='text-success'>Contact Person: </td>";
            $html .= "<td>".$daily_client->contact_person."</td>";
            $html .= "<td class='text-success'>Contact Number 1 : </td>";
            $html .= "<td>".$daily_client->contact_no_1."</td>";
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Contact Number 2 : </td>";
            $html .= "<td>".$daily_client->contact_no_2."</td>";
            $html .= "<td class='text-success'>Purpose of Meeting: </td>";
            $html .= "<td>".$purpose_of_meeting."</td>";  
            $html .= "</tr>";

            $html .= "<tr>";
            $html .= "<td class='text-success'>Designation: </td>";
            $html .= "<td>".$daily_client->designation."</td>";
            $html .= "<td class='text-success'>Email: </td>";
            $html .= "<td>".$daily_client->email."</td>";
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Mean of Communication: </td>";
            $html .= "<td>".$means_of_communicate."</td>";
            $html .= "<td class='text-success'>Source of Lead</td>";
            $html .= "<td>".$source_lead."</td>";
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Services</td>";
            $html .= "<td>".$service_lead."</td>";
            $html .= "<td class='text-success'>Client Approach Cost : </td>";
            $html .= "<td>Rs.".$daily_client->client_approach_cost."/-</td>";  
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Approach Discount : </td>";
            $html .= "<td>".$daily_client->client_approach_discount."%</td>";
            $html .= "<td class='text-success'>Final Approach Cost : </td>";
            $html .= "<td>Rs.".$daily_client->client_approach_final_cost."/-</td>";
            $html .= "</tr>";
    
            $html .= "<tr>";
            $html .= "<td class='text-success'>Brief Description Note : </td>";
            $html .= "<td colspan='3'>".$daily_client->brief_description."</td>";
            $html .= "</tr>";

            if($daily_client->status == 1 || $daily_client->status == 2 || $daily_client->status == 3 || $daily_client->status == 4){
                $lead_name  = $daily_client->status == 1 ? 'Hot Lead' : ($daily_client->status == 2 ? 'Cold Lead' : ($daily_client->status == 3 ? 'Sales Pipeline' : 'Converted Lead'));
                $html .= "<tr>";
                $html .= "<td colspan='4'><h3>".$lead_name."</h3></td>";
                $html .= "</tr>";
        
                $html .= "<tr>";
                $html .= "<td class='text-success'>Active Service : </td>";
                $html .= "<td>".$active_lead_service."</td>"; 
                $html .= "<td class='text-success'>Cost : </td>";
                $html .= "<td>Rs.".$daily_client->cost."/-</td>";
                $html .= "</tr>";
        
                $html .= "<tr>";
                $html .= "<td class='text-success'>Discount : </td>";
                $html .= "<td>".$daily_client->discount."%</td>";
                $html .= "<td class='text-success'>Final Cost : </td>";
                $html .= "<td>Rs.".$daily_client->final_cost."/-</td>";
                $html .= "</tr>";
        
                $html .= "<tr>";
                $html .= "<td class='text-success'>Additional Note : </td>";
                $html .= "<td>".$daily_client->additional_note."</td>";
                $html .= "<td class='text-success'>Client Expectation Note : </td>";
                $html .= "<td>".$daily_client->client_expectation_note."</td>";
                $html .= "</tr>";
            }
            elseif($daily_client->status == 5){
                $html .= "<tr>";
                $html .= "<td colspan='4'><h3>Rejected Lead</h3></td>";
                $html .= "</tr>";
        
                $html .= "<tr>";
                $html .= "<td class='text-success'>Termination Reason : </td>";
                $html .= "<td>".$daily_client->terminate_reason."</td>";
                $html .= "<td></td>";
                $html .= "<td></td>";
                $html .= "</tr>";
            }

           
        
            $html .= "</tbody>";
            $html .= "</table>";

        return  $html;
    }

     public function downloadSheet(Request $request)
    {
        

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        ];

        $year = date('Y');


        $objPHPExcel = new Spreadsheet();
        $worksheet = $objPHPExcel->getActiveSheet();

        // set Header

        $objPHPExcel->getActiveSheet(0)->SetCellValue('A1', 'Lead ID');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('B1', 'Company');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('C1', 'Client Address');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('D1', 'Contact Person');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('E1', 'Contact No 1');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('F1', 'Contact No 2');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('G1', 'Designation');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('H1', 'Email');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('I1', 'Purpose Of Meeting');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('J1', 'Means of Communication');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('K1', 'Services');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('L1', 'Client Approach Cost');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('M1', 'Client Approach Discount'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('N1', 'Client Approach Final Cost'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('O1', 'source Of Leads'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('P1', 'Next Meeting Date');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('Q1', 'Brief Description');  
        $objPHPExcel->getActiveSheet(0)->SetCellValue('R1', 'Service Required'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('S1', 'Terminate Reason'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('T1', 'Cost'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('U1', 'Discount'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('V1', 'Final Cost'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('W1', 'Active Lead Services'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('X1', 'Additional Note'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('Y1', 'Client Expectation Note'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('Z1', 'Status'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('AA1', 'Created By'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('AB1', 'Lead Created Date'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('AC1', 'Updated By'); 
        $objPHPExcel->getActiveSheet(0)->SetCellValue('AD1', 'Visit Date'); 

        
        
        $objPHPExcel->getActiveSheet()->getStyle('A1:AD1')->applyFromArray($styleArray);


        $lead_data = $this->dailyclient->getAllLeadData();
       
        $num = 2;
        foreach ($lead_data as $key => $value) {
                        $status = ($value['status'] == '0') ? 'Inactive' : 'Active';
                       
            if($value['status'] == 0) {
              $status = 'dailyclient';
            }
            else if($value['status'] == 1) {
              $status = 'hot_lead';
            }
            else if($value['status'] == 2) {
              $status = 'cold_lead';
            }
            else if($value['status'] == 3) {
              $status = 'service_pipeline';
            }
            else if($value['status'] == 4) {
              $status = 'converted';
            }
            else if($value['status'] == 5){
              $status = 'rejected';
            }
            else {
              $status = '';
            }


            $objPHPExcel->getActiveSheet(0)->SetCellValue('A' . $num, $value['id']);
           

           
        


            if (!empty($value['purpose_of_meeting'])) {
                $purpose_meeting = json_decode($value['purpose_of_meeting']);
                $purpose_of_meeting = $this->dropdown->find($purpose_meeting[0]);
                $meeting = $purpose_of_meeting ? $purpose_of_meeting->dropvalue : '';
                
            }
            else{
                 $meeting = '';
            }

            if (!empty($value['means_of_communication'])) {
                $means_of_communications = json_decode($value['means_of_communication']);
                $means_of_communication = $this->dropdown->find($means_of_communications[0]);
                $communication = $means_of_communication ? $means_of_communication->dropvalue : '';
                
            }
            else{
                 $communication = '';
            }

            if (!empty($value['services'])) {
                $servic = json_decode($value['services']);
                $services = $this->dropdown->find($servic[0]);
                $service = $services ? $services->dropvalue : '';
                
            }
            else{
                 $service = '';
            }
            if (!empty($value['active_lead_services'])) {
                $active_service = json_decode($value['active_lead_services']);
                $activeServices = $this->dropdown->find($active_service[0]);
                $activeService = $activeServices ? $activeServices->dropvalue : '';
                
            }
            else{
                 $activeService = '';
            }

            if (!empty($value['source_of_leads'])) {
                $lead_source = json_decode($value['source_of_leads']);
                $source_of_leads = $this->dropdown->find($lead_source[0]);
                $leads_source = $source_of_leads ? $source_of_leads->dropvalue : '';
                
            }
            else{
                 $leads_source = '';
            }


             if (!empty($value['created_by'])) {
                $created = $this->user->find($value['created_by']);
                $created_by = $created['first_name'].' '.$created['middle_name'] .' '.$created['last_name '];
                
            }
            else{
                 $created_by = '';
            }

             if (!empty($value['updated_by'])) {
                $updated = $this->user->find($value['updated_by']);
                $updated_by = $updated['first_name'].' '.$updated['middle_name'] .' '.$updated['last_name '];
                
            }
            else{
                 $updated_by = '';
            }

          
             
            $objPHPExcel->getActiveSheet(0)->SetCellValue('B' . $num, $value['company_name']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('C' . $num, $value['client_address']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('D' . $num, $value['contact_person']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('E' . $num, $value['contact_no_1']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('F' . $num, $value['contact_no_2']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('G' . $num, $value['designation']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('H' . $num, $value['email']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('I' . $num, $meeting);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('J' . $num, $communication);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('K' . $num, $service);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('L' . $num, $value['client_approach_cost']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('M' . $num, $value['client_approach_discount']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('N' . $num, $value['client_approach_final_cost']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('O' . $num, $leads_source);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('P' . $num, $value['next_meeting_date']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('Q' . $num, $value['brief_description']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('R' . $num, $value['service_required']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('S' . $num, $value['terminate_reason']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('T' . $num, $value['cost']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('U' . $num, $value['discount']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('V' . $num, $value['final_cost']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('W' . $num, $activeService);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('X' . $num, $value['additional_note']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('Y' . $num, $value['client_expectation_note']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('Z' . $num, $status);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('AA' . $num, $created_by);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('AB' . $num, $value['lead_created_date']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('AC' . $num, $updated_by);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('AD' . $num, $value['visit_date']);


          

            $num++;

        }


        $writer = new Xlsx($objPHPExcel);
        $file = 'lead_' . $year;
        $filename = $file . '.xlsx';
        header('Content-Type: application/openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache 

        $writer->save('php://output');

        exit;


    }
}
