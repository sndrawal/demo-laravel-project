<?php

namespace App\Modules\Lead\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Modules\User\Repositories\UserInterface;
use App\Modules\Lead\Repositories\MeetingInterface;
use App\Modules\Lead\Repositories\LeadActivityFeedInterface;
use App\Modules\Lead\Repositories\LeadLogInterface;
use Illuminate\Support\Facades\Auth;
use App\Modules\Notification\Repositories\NotificationInterface;

class MeetingController extends Controller
{
     protected $meeting;
     protected $user;
     protected $feed;
     protected $log;
    protected $notification;

     public function __construct(MeetingInterface $meeting, LeadLogInterface $log, LeadActivityFeedInterface $feed, UserInterface $user, NotificationInterface $notification)
    {
        $this->meeting = $meeting;
        $this->user = $user;
        $this->feed = $feed;
        $this->log = $log;
        $this->notification = $notification;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('lead::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('lead::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    { 
        $data = $request->all();

        if(isset($data['attendee'])){
            $data['attendee'] = json_encode($data['attendee']);
        }else{
            $data['attendee'] = '';
        }


        if(isset($data['lead_id'])){
            $lead_id = $data['lead_id'];
            $contact_id = NULL;
        }else{
            $contact_id = $data['contact_id'];
            $lead_id = NULL;
        }

         try{
            $userInfo = Auth::user();
            $user_id = $userInfo->id;
            $data['created_by'] =$user_id;
            $meeting_info = $this->meeting->save($data);

            /** Lead Log Start **/

            $lead_history['lead_id'] = $lead_id;
            $lead_history['contact_id'] = $contact_id;
            $lead_history['log_message']=$userInfo->first_name." ".$userInfo->last_name." Schedule Meeting about ".$data['meeting_about']." on ".$data['date']. " at ".$data['time'] ;
            $this->log->save($lead_history);

            /** Lead Log End **/
            
            /** Activity Feed Set **/

            $feed['lead_id'] = $lead_id;
            $feed['contact_id'] = $contact_id;
            $feed['method'] = 'Meeting';
            $feed['method_id'] = $meeting_info['id'];

            $this->feed->save($feed);

            /** End of Activity Feed Set **/

            alertify()->success('Meeting Schedule Successfully');
            
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }


        if(isset($lead_id)){
            return redirect(route('dailyClient.profile',['id'=>$lead_id]));
        }else{
             return redirect(route('contact.profile',['id'=>$contact_id]));
        }

    }


      public function storeMeetingComment(Request $request){
         if($request->ajax()){
            $commentData['lead_id'] = $request->lead_id;
            $commentData['meeting_id'] = $request->meeting_id;
            $commentData['comment'] = $request->comment;
            $userInfo = Auth::user();
            $user_id = $userInfo->id;
            $commentData['comment_by'] =$user_id;


              /* ---------------------------------------------------
           //             Notification Start
           // ------------------------------------------------------*/
                    $message = auth()->user()->first_name . " " . auth()->user()->last_name. " has left comment on your meeting section. Please check.g ";
                    $link = route('dailyClient.profile',['id'=>$commentData['lead_id']]);

                    $notification_data = array(
                        'creator_user_id' => auth()->user()->id,
                        'notified_user_id' => $commentData['lead_id'],
                        'message' => $message,
                        'link' => $link,
                        'is_read' => '0',
                    );

                    $this->notification->save($notification_data);
           //          /* ---------------------------------------------------
           //                      Notification End
           //          ------------------------------------------------------*/

            $commentInfo = $this->meeting->saveMeetingComment($commentData);

            $commentData['date'] = $commentInfo->created_at->diffForHumans();

            $commentData['comment_by'] = $userInfo->first_name.' '.$userInfo->middle_name.' '. $userInfo->last_name;


            $data = view('lead::dailyClient.partial.comment-section',compact('commentData'))->render();

            return response()->json(['options'=>$commentData]);
         }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('lead::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('lead::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{

            $MeetingInfo =  $this->meeting->find($id); 

            $userInfo = Auth::user();
            $lead_history['lead_id']= $lead_id = $MeetingInfo->lead_id;
            $lead_history['contact_id']= $contact_id = $MeetingInfo->contact_id;
            $lead_history['log_message'] = $userInfo->first_name." ".$userInfo->last_name." Deleted Schedule Meeting of ".$MeetingInfo->meeting_about.'.';
            $this->log->save($lead_history);

            $this->feed->deleteFeed('Meeting',$id);

            $this->meeting->delete($id);
            alertify()->success('Schedule Meeting Deleted Successfully');
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }

       if($lead_id){
            return redirect(route('dailyClient.profile',['id'=>$lead_id]));
        }else{
             return redirect(route('contact.profile',['id'=>$contact_id]));
        }
    }
}
