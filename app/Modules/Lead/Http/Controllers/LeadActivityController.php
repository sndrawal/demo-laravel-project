<?php

namespace App\Modules\Lead\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Lead\Repositories\LeadActivityInterface;

class LeadActivityController extends Controller
{
     protected $activity;

     public function __construct(LeadActivityInterface $activity)
    {
        $this->activity = $activity;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $input = $request->all();

        $lead_id = $input['lead_id'];
        $data['lead_id']=$lead_id;
        $data['activity'] = $this->activity->findAll($lead_id,$limit= 50);
        return view('lead::activity.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        $input = $request->all();

        $lead_id = $input['lead_id'];
        $data['lead_id']=$lead_id;

        return view('lead::activity.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $lead_id = $data['lead_id'];

        $userInfo = Auth::user();
        $user_id = $userInfo->id;

        $data['created_by'] = $user_id;

        try{
            $this->activity->save($data);
            alertify()->success('Lead Activity Stored Successfully');
        } catch(\Throwable $e) {
            alertify($e->getMessage())->error();
        }

        return redirect(route('activity.index',['lead_id'=>$lead_id]));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('lead::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('lead::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
