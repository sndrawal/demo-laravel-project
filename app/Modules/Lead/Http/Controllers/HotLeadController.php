<?php

namespace App\Modules\Lead\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;


use App\Modules\Lead\Repositories\HotLeadInterface;
use App\Modules\Dropdown\Repositories\DropdownInterface;

class HotLeadController extends Controller
{
     protected $hotLead;
     protected $dropdown;
    
    public function __construct(HotLeadInterface $hotLead, DropdownInterface $dropdown)
    {
        $this->hotLead = $hotLead;
        $this->dropdown = $dropdown;
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $userInfo = Auth::user();
        $user_type = $userInfo->user_type;
        $id = (($user_type == 'super_admin') OR ($user_type == 'admin')) ? '' : $userInfo->id;
        

        $data['hotLead'] = $this->hotLead->findAll($id,$limit = 50);  
        return view('lead::hotLead.index',$data);
    }

    
    /**
     * Show the specified resource.
     * @return Response
     */
    public function view(Request $request)
    {
        $data = $request->all();
        
        $id = $data['id'];
        $active_lead = $this->hotLead->find($id);

        $services = json_decode($active_lead->services); 
        $service_lead = '';
        if(!empty($services)){
            foreach($services as $key => $service){
                $service_lead .= ' - '. $this->dropdown->getDropdownById($service)->dropvalue;
                $service_lead .= '<br>';
            }
        }
        
        $meeting = json_decode($active_lead->purpose_of_meeting); 
        $purpose_of_meeting = '';
        if(!empty($meeting)){
            foreach($meeting as $key => $value){
                $purpose_of_meeting .= ' - '. $this->dropdown->getDropdownById($value)->dropvalue;
                $purpose_of_meeting .= '<br>';
            }
        }
        
        $communications = json_decode($active_lead->means_of_communication); 
        $means_of_communicate = '';
        if(!empty($communications)){
            foreach($communications as $key => $communication){
                $means_of_communicate .= ' - '. $this->dropdown->getDropdownById($communication)->dropvalue;
                $means_of_communicate .= '<br>';
            }
        }
        
        $sourceLead = json_decode($active_lead->source_of_leads); 
        $source_lead = '';
        if(!empty($sourceLead)){
            foreach($sourceLead as $key => $source){
                $source_lead .= ' - '. $this->dropdown->getDropdownById($source)->dropvalue;
                $source_lead .= '<br>';
            }
         }
        
        $hotLeadServices = json_decode($active_lead->active_lead_services); 
        $active_lead_service = '';
        if($hotLeadServices){
            foreach($hotLeadServices as $key => $val){
                $active_lead_service .= ' - '. $this->dropdown->getDropdownById($val)->dropvalue;
                $active_lead_service .= '<br>';
            }
        }
        
        $html ='';
              $html .= "<table class='table table-striped mb30' id='table1' cellspacing='0' width='100%' frame='box'>";
              $html .= "<thead>";
              $html .= " <tr>";
              $html .= "<th width='25%'></th>";
              $html .= "<th width='25%'></th>";
              $html .= "<th width='25%'></th>";
              $html .= "<th width='25%'></th>";
              $html .= "</tr>";
              $html .= "</thead>";
              $html .= "<tbody>";
              $html .= "<tr>";
              $html .= "<td colspan='4'><h3>Daily Client Approach</h3></td>";
              $html .= "</tr>";
              $html .= "<tr>";
              $html .= "<td class='text-success'>Company Name : </td>";
              $html .= "<td>".$active_lead->company_name."</td>";
              $html .= "<td class='text-success'>Address</td>";
              $html .= "<td>".$active_lead->client_address ."</td>";
              $html .= "</tr>";

              $html .= "<tr>";
              $html .= "<td class='text-success'>Contact Person: </td>";
              $html .= "<td>".$active_lead->contact_person."</td>";
              $html .= "<td class='text-success'>Contact Number 1 : </td>";
              $html .= "<td>".$active_lead->contact_no_1."</td>";
              $html .= "</tr>";
        
              $html .= "<tr>";
              $html .= "<td class='text-success'>Contact Number 2 : </td>";
              $html .= "<td>".$active_lead->contact_no_2."</td>";
              $html .= "<td class='text-success'>Purpose of Meeting: </td>";
              $html .= "<td>".$purpose_of_meeting."</td>";  
              $html .= "</tr>";

              $html .= "<tr>";
              $html .= "<td class='text-success'>Designation: </td>";
              $html .= "<td>".$active_lead->designation."</td>";
              $html .= "<td class='text-success'>Email: </td>";
              $html .= "<td>".$active_lead->email."</td>";
              $html .= "</tr>";
        
              $html .= "<tr>";
              $html .= "<td class='text-success'>Mean of Communication: </td>";
              $html .= "<td>".$means_of_communicate."</td>";
              $html .= "<td class='text-success'>Source of Lead</td>";
              $html .= "<td>".$source_lead."</td>";
              $html .= "</tr>";
        
              $html .= "<tr>";
              $html .= "<td class='text-success'>Services</td>";
              $html .= "<td>".$service_lead."</td>";
              $html .= "<td class='text-success'>Client Approach Cost : </td>";
              $html .= "<td>Rs.".$active_lead->client_approach_cost."/-</td>";  
              $html .= "</tr>";
        
              $html .= "<tr>";
              $html .= "<td class='text-success'>Approach Discount : </td>";
              $html .= "<td>".$active_lead->client_approach_discount."%</td>";
              $html .= "<td class='text-success'>Final Approach Cost : </td>";
              $html .= "<td>Rs.".$active_lead->client_approach_final_cost."/-</td>";
              $html .= "</tr>";
        
              $html .= "<tr>";
              $html .= "<td class='text-success'>Brief Description Note : </td>";
              $html .= "<td colspan='3'>".$active_lead->brief_description."</td>";
              $html .= "</tr>";
        
              $html .= "<tr>";
              $html .= "<td colspan='4'><h3>Hot Lead</h3></td>";
              $html .= "</tr>";
        
              $html .= "<tr>";
              $html .= "<td class='text-success'>Active Service : </td>";
              $html .= "<td>".$active_lead_service."</td>"; 
              $html .= "<td class='text-success'>Cost : </td>";
              $html .= "<td>Rs.".$active_lead->cost."/-</td>";
              $html .= "</tr>";
        
              $html .= "<tr>";
              $html .= "<td class='text-success'>Discount : </td>";
              $html .= "<td>".$active_lead->discount."%</td>";
              $html .= "<td class='text-success'>Final Cost : </td>";
              $html .= "<td>Rs.".$active_lead->final_cost."/-</td>";
              $html .= "</tr>";
        
              $html .= "<tr>";
              $html .= "<td class='text-success'>Additional Note : </td>";
              $html .= "<td>".$active_lead->additional_note."</td>";
              $html .= "<td class='text-success'>Client Expectation Note : </td>";
              $html .= "<td>".$active_lead->client_expectation_note."</td>";
              $html .= "</tr>";
            
              $html .= "</tbody>";
              $html .= "</table>";

        return  $html;
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('lead::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('lead::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('lead::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
