<?php

namespace App\Modules\Lead\Http\Controllers;

use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Modules\Lead\Repositories\FolderInterface;
use App\Modules\Lead\Repositories\FileInterface;
use App\Modules\Lead\Repositories\ConvertedLeadInterface;

class ManageFolderController extends Controller
{
     protected $folder;
     protected $file;
     protected $lead;
    
    public function __construct(FolderInterface $folder, ConvertedLeadInterface $lead, FileInterface $file)
    {
        $this->folder = $folder;
        $this->file = $file;
        $this->lead = $lead;
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $input= $request->all();   
        $data['lead_id']  = $lead_id = $input['lead_id'];
        $data['folders'] = $this->folder->findAll($lead_id); 
        return view('lead::manageFolder.index',$data);
    }
    
    public function createFolder(Request $request)
    {
        $input= $request->all();  
        if (!$input['folder_name']) {
            alertify('Please Enter Folder Name.')->error();
            return redirect()->back();
        }
        $data['lead_id']  = $lead_id = $input['lead_id'];

          try{
            
            $input['folder_slug'] = $folder_slug = str_slug($input['folder_name'], '_');
            $check_folder =  $this->folder->checkFolder($lead_id,$folder_slug);
            
              if(count($check_folder) >0){
                  alertify()->error('Folder Already Created.'); 
                   $data['folders'] = $this->folder->findAll($lead_id); 
                  return view('lead::manageFolder.index',$data);
              }
              
            $this->folder->save($input);
              
            $lead_info = $this->lead->find($lead_id);
            $path = public_path().'/uploads/lead_projects/' . $lead_info->slug.'/'.$folder_slug;
                File::makeDirectory($path, $mode = 0777, true, true);
            
              alertify()->success('Folder Created Successfully');
            
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }
        
        $datas['folders'] = $this->folder->findAll($lead_id); 
        $datas['lead_id'] =$lead_id;
        
        return view('lead::manageFolder.index',$datas);
        
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('lead::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('lead::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('lead::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $input = $request->all();
        
         $data['bid_id'] = $bid_id = $input['bid_id'];
       
        try{
            //deleteDirectory($directory);
            $folder_info = $this->folder->findById($id);
            $project_bid_info = $this->projectBid->find($bid_id);   
            $path = public_path().'/uploads/projects/' . $project_bid_info->slug.'/'.$folder_info->folder_slug;
                
            File::deleteDirectory($path);
            
            //First Delete Files of Folder
             $this->file->deletebyFolderId($id);
            
            //Then Delete Folder
            $this->folder->delete($id);
            
            alertify()->success('Folder Deleted Successfully'); 
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }
        
        $datas['folders'] = $this->folder->findAll($bid_id); 
        $datas['bid_id'] = $bid_id;

        return view('lead::manageFolder.index',$datas);
    }
}
