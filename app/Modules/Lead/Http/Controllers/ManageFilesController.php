<?php

namespace App\Modules\Lead\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;


use App\Modules\Lead\Repositories\FileInterface;
use App\Modules\Lead\Repositories\FolderInterface;
use App\Modules\Lead\Repositories\ConvertedLeadInterface;

class ManageFilesController extends Controller
{
     protected $file;
     protected $lead;
     protected $folder;
    
    public function __construct(FileInterface $file, ConvertedLeadInterface $lead, FolderInterface $folder)
    {
        $this->file = $file;
        $this->lead = $lead;
        $this->folder = $folder;
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $input= $request->all(); 
        $data['lead_id']  = $lead_id = $input['lead_id'];
        $data['folder_id'] = $folder_id = $input['id'];
        $data['files'] = $this->file->findAll($folder_id);
        $data['folder'] = $this->folder->findById($folder_id);
        //$data['project_name'] =$this->projectBid->find($lead_id)->slug;
        return view('lead::managefile.index',$data);
    }

    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('projectdashboard::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request,$id)
    {
         $images = $request->all();
        
       // dd($images);
        $lead_id  =$images['lead_id'];
        
        $folder_files = $request->file('folder_file');

         try{
             
            if($request->hasfile('folder_file')){  
                  
            $folderFiles = $this->file->uploadFile($folder_files, 'folder_file',$id);

            foreach ($folderFiles as $file) {
                $extension = \File::extension($file);

                $fileData =
                    [
                        'project_folder_id' => $id,
                        'upload_filename' => $file,
                        'file_type' => $extension
                    ];
                 $this->file->save($fileData);
                
            }
             alertify()->success('Files Uploaded Successfully');   
         }
            
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }
        
            return redirect(route('ManageFiles.index',['lead_id'=>$lead_id,'id'=>$id]));
    
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('projectdashboard::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('projectdashboard::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
