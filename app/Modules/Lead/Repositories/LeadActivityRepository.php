<?php 
namespace App\Modules\Lead\Repositories;


use App\Modules\Lead\Entities\LeadActivities;

class LeadActivityRepository implements LeadActivityInterface
{
    
    public function findAll($id,$limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
            $result =LeadActivities::where('lead_id','=',$id)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
           
        return $result;     
       
    }
    
    public function find($id){
        return LeadActivities::find($id);
    }
    
   public function getList(){  
       $result = LeadActivities::pluck('title', 'id');
       return $result;
   }
    
    public function save($data){
        return LeadActivities::create($data);
    }
    
    public function update($id,$data){
        $result = LeadActivities::find($id);
        return $result->update($data);
    }
    
    public function delete($id){
        return LeadActivities::destroy($id);
    }


}