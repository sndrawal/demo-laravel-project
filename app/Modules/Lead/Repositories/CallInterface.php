<?php

namespace App\Modules\Lead\Repositories;


interface CallInterface
{
    public function findAll($field,$value,$limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function getList();

    public function save($data);

    public function update($id,$data);

    public function delete($id);

    public function countCall($field,$value);

    public function saveCallComment($data);
}

