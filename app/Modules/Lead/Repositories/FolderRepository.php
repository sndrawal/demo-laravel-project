<?php 
namespace App\Modules\Lead\Repositories;


use App\Modules\Lead\Entities\ProjectFolder;
use App\Modules\ProjectBid\Entities\ProjectBid;

class FolderRepository implements FolderInterface
{
    
    public function findAll($id)
    {
       $result = ProjectFolder::where('lead_id', '=', $id)->get();
        
         return $result;
    }
    
    public function find($pid){
         $result = ProjectFolder::where('lead_id', '=', $pid)->count();
         return $result;
    }
    
    public function findById($id){
        return ProjectFolder::find($id);
    }
    
    
   public function getList(){
       $folder = ProjectFolder::pluck('file_name', 'id');
       return $folder;
   }
    
    public function save($data){
        return ProjectFolder::create($data);
    }
    
    public function update($id,$data){
        $folder = ProjectFolder::find($id);
        return $folder->update($data);
    }
    
    public function delete($id){
        return ProjectFolder::destroy($id);
    }
    
    public function uploadFile($files, $key_name,$pid){
        
        $project_name = ProjectBid::find($pid)->slug;
        
        $filePath = [];

        foreach ($files as $file) {

            if (isset($key_name)) {

                $imageName = $file->getClientOriginalName();

                $fileName = date('Y-m-d-h-i-s') . '-' . preg_replace('[ ]', '-', $imageName);
               // $file->move(public_path() . self::$path . '/' . $key_name, $fileName);
                $file->move(public_path() . ManageFile::FILE_PATH.'/'.$project_name.'/BOQ/', $fileName);


                $filePath[] = $fileName;
            }
        }

        return $filePath;
    }
    
    public function checkFolder($lead_id, $slug){
         $result = ProjectFolder::where('lead_id','=',$lead_id)->where('folder_slug', '=', $slug)->get();
        
         return $result;
    }
}