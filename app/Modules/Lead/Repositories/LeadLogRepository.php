<?php
/**
 * Created by PhpStorm.
 * User: bidhee
 * Date: 7/18/19
 * Time: 11:01 AM
 */

namespace App\Modules\Lead\Repositories;


use App\Modules\Lead\Entities\LeadLog;

class LeadLogRepository implements LeadLogInterface
{
    public function save($data)
    {
        return LeadLog::create($data);
    }

}