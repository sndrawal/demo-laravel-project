<?php 
namespace App\Modules\Lead\Repositories;


use App\Modules\Lead\Entities\LeadActivityFeed;

class LeadActivityFeedRepository implements LeadActivityFeedInterface
{
    
    public function findAll($id,$limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
            $result =LeadActivityFeed::where('lead_id','=',$id)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
           
        return $result;     
       
    }
    
    public function save($data){
        return LeadActivityFeed::create($data);
    }
    
     public function deleteFeed($method,$id){ 
        return LeadActivityFeed::where('method','=',$method)->where('method_id','=',$id)->delete($id);
    }

    public function updateTask($method,$id,$feed){

        $result = LeadActivityFeed::where('method','=',$method)->where('method_id','=',$id)->update($feed);
    }

}