<?php

namespace App\Modules\Lead\Repositories;


interface LeadActivityFeedInterface
{
    public function findAll($id,$limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function save($data);

    public function deleteFeed($method,$id);

    public function updateTask($method,$id,$feed);

}