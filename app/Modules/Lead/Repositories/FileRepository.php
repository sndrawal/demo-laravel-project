<?php 
namespace App\Modules\Lead\Repositories;


use App\Modules\Lead\Entities\ManageFile;
use App\Modules\Lead\Entities\ProjectFolder;
use App\Modules\ProjectBid\Entities\ProjectBid;

class FileRepository implements FileInterface
{
    
    public function deletebyFolderId($id){
        return ManageFile::where('project_folder_id', '=', $id)->delete();
    }

    public function findAll($id)
    {
       $result = ManageFile::where('project_folder_id', '=', $id)->get();
        return $result;
    }
    
    public function find($id){
        return ManageFile::find($id);
    }
    
   public function getList(){
       $ManageFile = ManageFile::pluck('file_name', 'id');
       return $ManageFile;
   }
    
    public function save($data){
        return ManageFile::create($data);
    }
    
    public function update($id,$data){
        $ManageFile = ManageFile::find($id);
        return $ManageFile->update($data);
    }
    
    public function delete($id){
        return ManageFile::destroy($id);
    }
    
    public function uploadFile($files, $key_name,$id){
        
        
        $folder_name = ProjectFolder::find($id)->folder_slug;
        
        $filePath = [];

        foreach ($files as $file) {

            if (isset($key_name)) {

                $imageName = $file->getClientOriginalName();

                $fileName = date('Y-m-d-h-i-s') . '-' . preg_replace('[ ]', '-', $imageName);
               // $file->move(public_path() . self::$path . '/' . $key_name, $fileName);
                $file->move(public_path() . ManageFile::FILE_PATH.'/'.$folder_name.'/', $fileName);


                $filePath[] = $fileName;
            }
        }

        return $filePath;
    }
}