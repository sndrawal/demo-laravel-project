<?php 
namespace App\Modules\Lead\Repositories;


use App\Modules\Lead\Entities\Call;
use App\Modules\Lead\Entities\LeadComment;

class CallRepository implements CallInterface
{
    
    public function findAll($field,$value,$limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
       
        $result =Call::where($field,'=',$value)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;     
    }
    
    public function find($id){
        return Call::find($id);
    }
    
   public function getList(){  
       $result = Call::pluck('title', 'id');
       return $result;
   }
    
    public function save($data){
        return Call::create($data);
    }
    
    public function update($id,$data){
        $result = Call::find($id);
        return $result->update($data);
    }
    
    public function delete($id){
        return Call::destroy($id);
    }

    public function countCall($field,$value){
        return Call::where($field,'=',$value)->count();  
     }

      public function saveCallComment($data){
        return LeadComment::create($data);
     }


}