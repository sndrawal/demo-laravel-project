<?php 
namespace App\Modules\Lead\Repositories;


use App\Modules\Lead\Entities\Meeting;
use App\Modules\Lead\Entities\LeadComment;

class MeetingRepository implements MeetingInterface
{
    
    public function findAll($field,$value,$limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
       
        $result =Meeting::where($field,'=',$value)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;     
    }
    
    public function find($id){
        return Meeting::find($id);
    }
    
   public function getList(){  
       $result = Meeting::pluck('title', 'id');
       return $result;
   }
    
    public function save($data){
        return Meeting::create($data);
    }
    
    public function update($id,$data){
        $result = Meeting::find($id);
        return $result->update($data);
    }
    
    public function delete($id){
        return Meeting::destroy($id);
    }

    public function countMeeting($field,$value){
        return Meeting::where($field,'=',$value)->count();  
     }

     public function saveMeetingComment($data){
        return LeadComment::create($data);
     }
     
}