<?php 
namespace App\Modules\Lead\Repositories;


use App\Modules\Lead\Entities\Note;
use App\Modules\Lead\Entities\LeadComment;

class NoteRepository implements NoteInterface
{
    
    public function findAll($field,$value,$limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
       
        $result =Note::where($field,'=',$value)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;     
    }
    
    public function find($id){
        return Note::find($id);
    }
    
   public function getList(){  
       $result = Note::pluck('title', 'id');
       return $result;
   }
    
    public function save($data){
        return Note::create($data);
    }
    
    public function update($id,$data){
        $result = Note::find($id);
        return $result->update($data);
    }
    
    public function delete($id){
        return Note::destroy($id);
    }

     public function countNote($field,$value){
        return Note::where($field,'=',$value)->count();  
     }

     public function saveNoteComment($data){
        return LeadComment::create($data);
     }


}