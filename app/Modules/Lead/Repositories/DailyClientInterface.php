<?php

namespace App\Modules\Lead\Repositories;


interface DailyClientInterface
{
    public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function findAllLead($subchild_user_id,$limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function findConvertedLead($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function getList();

    public function save($data);

    public function update($id,$data);

    public function delete($id);
     
    public function upload($file);
    
    public function getAllClientByStatus($id,$status_id,$limit=null);

    public function countAllClientByStatus($id,$status_id);
    
    public function getLatestMeeting();

    public function getAllLeadData($filter = []);

    public function getLeadActivityFeed($field,$value,$limit=null,$filter = []);

    public function associateLeadContact($data);

    public function findContactById($lead_id,$limit=null,$filter = []);

    public function countContactById($lead_id);
    
    public function checkAssociateLeadContact($lead_id,$contact_id);
    
    public function deleteContactLead($lead_id);

    public function totalSalesPipelineAmount($status_id);
    
    public function getWeeklyLeadByStatus($status_id,$date);

}