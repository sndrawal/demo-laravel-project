<?php 
namespace App\Modules\Lead\Repositories;


use App\Modules\Lead\Entities\Lead;

class HotLeadRepository implements HotLeadInterface
{
    
    public function findAll($id,$limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        if($id == ""){
             $result =Lead::where('status','=','1')       
            ->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 
        
        }else{ 
            $result =Lead::where('created_by','=',$id)->where('status','=','1')       
            ->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
           
        return $result;     
        }
       
    }
    
    public function find($id){
        return Lead::find($id);
    }
    
   public function getList(){  
       $result = Lead::pluck('title', 'id');
       return $result;
   }
    
    public function save($data){
        return Lead::create($data);
    }
    
    public function update($id,$data){
        $result = Lead::find($id);
        return $result->update($data);
    }
    
    public function delete($id){
        return Lead::destroy($id);
    }


}