<?php 
namespace App\Modules\Lead\Repositories;

use Carbon\Carbon;
use App\Modules\Lead\Entities\Lead;
use App\Modules\Lead\Entities\LeadActivityFeed;
use App\Modules\Lead\Entities\LeadContact;
use Illuminate\Support\Facades\Auth;

class DailyClientRepository implements DailyClientInterface
{
    
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    { 

        $result =Lead::when(array_keys($filter, true), function ($query) use ($filter) {

       
            if (isset($filter['status'])) {  
               
                $status_code = $filter['status'];
              
                if($status_code == '6'){
                    $status = '0';
                }else{
                    $status = $filter['status'];
                }
                    $query->where('status', '=', $status);
            }

            if (isset($filter['search_value'])) { 
                $query->where('company_name', 'like', '%' . $filter['search_value'] . '%');
            }
            if (isset($filter['next_visit_date'])) { 
                $query->where('next_visit_date', '=' ,$filter['next_visit_date']);
            }

            if (isset($filter['next_meeting_date'])) {  
                $query->where('next_meeting_date', '=' ,$filter['next_meeting_date']);
            }

            if (isset($filter['company_name'])) { 
                $query->where('company_name', 'like', '%' . $filter['company_name'] . '%');
            }

            if (isset($filter['contact_person'])) { 
                $query->where('contact_person', 'like', '%' . $filter['contact_person'] . '%');
            }

             if (isset($filter['created_by'])) {  
                $query->where('created_by', '=', $filter['created_by']);
            }
            
            
            if (isset($filter['search_from_to'])) { 

                $dateRange = explode('---', $filter['search_from_to']);
                
                $query->where('lead_created_date', '>=', $dateRange[0]);
                $query->where('lead_created_date', '<=', $dateRange[1]);
            }
            
            
        })
            ->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));

        return $result; 
    }

    public function findAllLead($subchild_user_id,$limit=null,$filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]){

            
             $where_field = 'created_by';
             $where_data = $subchild_user_id;

        $result =Lead::when(array_keys($filter, true), function ($query) use ($filter) {
             if (isset($filter['status'])) { 
                    $query->where('status', '=', $filter['status']);
            }
            if (isset($filter['search_value'])) {
                $query->where('company_name', 'like', '%' . $filter['search_value'] . '%');
            }
            if (isset($filter['next_visit_date'])) {
                $query->where('next_visit_date', '=' ,$filter['next_visit_date']);
            }

            if (isset($filter['next_meeting_date'])) {
                $query->where('next_meeting_date', '=' ,$filter['next_meeting_date']);
            }

            if (isset($filter['company_name'])) {
                $query->where('company_name', 'like', '%' . $filter['company_name'] . '%');
            }

            if (isset($filter['contact_person'])) {
                $query->where('contact_person', 'like', '%' . $filter['contact_person'] . '%');
            }

             if (isset($filter['created_by'])) {
                $query->where('created_by', '=', $filter['created_by']);
            }
            
            
            if (isset($filter['search_from_to'])) {
                 $dateRange = explode('---', $filter['search_from_to']);
               
                $query->where('lead_created_date', '>=', $dateRange[0]);
                $query->where('lead_created_date', '<=', $dateRange[1]);

            }
            
            
        })->whereIn($where_field,$where_data)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 
    }

    public function findConvertedLead($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {

        return Lead::where('status', '=', '4')->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
    }
    
    public function find($id){
        return Lead::find($id);
    }
    
   public function getList(){  
       $result = Lead::pluck('title', 'id');
       return $result;
   }
    
    public function save($data){
        return Lead::create($data);
    }
    
    public function update($id,$data){
        $result = Lead::find($id);
        return $result->update($data);
    }
    
    public function delete($id){
        return Lead::destroy($id);
    }
    
     public function upload($file){
        $imageName = $file->getClientOriginalName();
        $fileName = date('Y-m-d-h-i-s') . '-' . preg_replace('[ ]', '-', $imageName);

        $file->move(public_path() . Lead::FILE_PATH, $fileName);

        return $fileName;
    }
    
    
    public function getAllClientByStatus($id,$status_id,$limit = null){
         if($id){
            return Lead::where('created_by','=',$id)->where('status', '=', $status_id)->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        }else{ 
            return Lead::where('status', '=', $status_id)->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        } 
    } 

    public function countAllClientByStatus($id,$status_id){
        if($id){
            return Lead::where('created_by','=',$id)->where('status', '=', $status_id)->count();
        }else{
            return Lead::where('status', '=', $status_id)->count();
        } 
    }

    public function getLatestMeeting(){
        $now = Carbon::now();
        $compile_now_date = date('Y-m-d',strtotime($now)); 
        $date = Carbon::now()->addDays(15); 
        $compile_date = date('Y-m-d',strtotime($date));
        $userInfo = Auth::user();
        $user_type = $userInfo->user_type;

        if($user_type == 'super_admin' || $user_type == 'admin') {
            $result = Lead :: where( 'next_meeting_date', '>=', $compile_now_date)->where('next_meeting_date', '<=', $compile_date)->orderBy('next_meeting_date', 'ASC')->take(5)->get();
        }else{
            $result = Lead :: where( 'next_meeting_date', '>=', $compile_now_date)->where( 'next_meeting_date', '<=', $compile_date)->where('created_by',$userInfo->id)->take(5)->get();
        }
        return $result;
    }

    public function getAllLeadData($filter = [])
    {
            $result = Lead::get();
            return $result;
    }

    public function getLeadActivityFeed($field,$value,$limit=null,$filter = []){
        $result = LeadActivityFeed::where($field,'=',$value)->orderBy('created_at', 'desc')->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;
    }

    public function associateLeadContact($data)
    {
        return LeadContact::create($data);
    }

    public function findContactById($lead_id,$limit=null,$filter = [])
    {
        return LeadContact::where('lead_id','=',$lead_id)->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
    }

    public function countContactById($lead_id)
    {
        return LeadContact::where('lead_id','=',$lead_id)->count();
    }

    public function checkAssociateLeadContact($lead_id,$contact_id){
        return LeadContact::where('lead_id','=',$lead_id)->where('contact_id','=',$contact_id)->count();
    }

    public function deleteContactLead($lead_id){
        return LeadContact::where('lead_id','=',$lead_id)->delete($lead_id);   
    }

    public function totalSalesPipelineAmount($status_id){
        $result =  Lead::where('status', '=', $status_id)->get();

        return $result->sum('final_cost');

    }

    public function getWeeklyLeadByStatus($status_id,$date){
        $result =Lead::where('status', '=', $status_id)->where('lead_created_date','=',$date)->count();

        return $result; 

    }



}