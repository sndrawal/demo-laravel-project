<?php

namespace App\Modules\Lead\Repositories;


interface FolderInterface
{
    public function findAll($id);

    public function find($id);
    
    public function findById($id);
    
    public function getList();

    public function save($data);

    public function update($id,$data);

    public function delete($id);
    
    public function uploadFile($files, $key_name, $id);
    
    public function checkFolder($lead_id, $slug);

}