<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {
    
     
        /*
        |--------------------------------------------------------------------------
        | Daily Client CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('dailyClient', ['as' => 'dailyClient.index', 'uses' => 'DailyClientController@index']);

        Route::get('dailyClient-grid', ['as' => 'dailyClient.index.grid', 'uses' => 'DailyClientController@indexGrid']);


        Route::get('dailyClient/create', ['as' => 'dailyClient.create', 'uses' => 'DailyClientController@create']);
        Route::post('dailyClient/store', ['as' => 'dailyClient.store', 'uses' => 'DailyClientController@store']);
    
        Route::get('dailyClient/edit/{id}', ['as' => 'dailyClient.edit', 'uses' => 'DailyClientController@edit'])->where('id','[0-9]+');
        Route::put('dailyClient/update/{id}', ['as' => 'dailyClient.update', 'uses' => 'DailyClientController@update'])->where('id','[0-9]+');
    
        Route::get('dailyClient/delete/{id}', ['as' => 'dailyClient.delete', 'uses' => 'DailyClientController@destroy'])->where('id','[0-9]+');
    
        Route::post('dailyClient/updateStatus', ['as' => 'dailyClient.updateStatus', 'uses' => 'DailyClientController@updateStatus']);
        
        Route::post('dailyClient/updateLeadStatus', ['as' => 'dailyClient.updateLeadStatus', 'uses' => 'DailyClientController@updateLeadStatus']);
    
        Route::post('dailyClient/associateContact', ['as' => 'dailyClient.associateContact', 'uses' => 'DailyClientController@associateContact']);
    
        Route::get('dailyClient/view/{id}',['as' => 'dailyClient.view', 'uses' => 'DailyClientController@view']);

        
    
        Route::get('dailyClient/dashboard', ['as' => 'dailyClient.dashboard', 'uses' => 'DailyClientController@dashboard']);
       
        Route::get('dailyClient/profile', ['as' => 'dailyClient.profile', 'uses' => 'DailyClientController@profile']);

        Route::get('ManageFolder',['as' => 'ManageFolder.index','uses' => 'ManageFolderController@index']);
        Route::post('ManageFolder/createFolder',['as' => 'ManageFolder.createFolder','uses' => 'ManageFolderController@createFolder']);
        Route::get('ManageFolder/delete/{id}',['as' => 'ManageFolder.delete','uses' => 'ManageFolderController@destroy'])->where('id','[0-9]+');
    
        Route::get('ManageFiles',['as' => 'ManageFiles.index','uses' => 'ManageFilesController@index']);
        Route::post('ManageFiles/store/{id}',['as' => 'ManageFiles.store','uses' => 'ManageFilesController@store'])->where('id','[0-9]+');

         /*
        |--------------------------------------------------------------------------
        | Hot Lead CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('activeLead', ['as' => 'activeLead.index', 'uses' => 'HotLeadController@index']);
    
        Route::get('activeLead/create', ['as' => 'activeLead.create', 'uses' => 'HotLeadController@create']);
        Route::post('activeLead/store', ['as' => 'activeLead.store', 'uses' => 'HotLeadController@store']);
    
        Route::get('activeLead/edit/{id}', ['as' => 'activeLead.edit', 'uses' => 'HotLeadController@edit'])->where('id','[0-9]+');
        Route::put('activeLead/update/{id}', ['as' => 'activeLead.update', 'uses' => 'HotLeadController@update'])->where('id','[0-9]+');
    
        Route::get('activeLead/delete/{id}', ['as' => 'activeLead.delete', 'uses' => 'HotLeadController@destroy'])->where('id','[0-9]+');
    
        Route::post('activeLead/updateStatus', ['as' => 'activeLead.updateStatus', 'uses' => 'HotLeadController@updateStatus']);
    
        Route::get('activeLead/view/{id}',['as' => 'activeLead.view', 'uses' => 'HotLeadController@view']);


        /*
        |--------------------------------------------------------------------------
        | Hot Lead CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('hotLead', ['as' => 'hotLead.index', 'uses' => 'HotLeadController@index']);
    
        Route::get('hotLead/create', ['as' => 'hotLead.create', 'uses' => 'HotLeadController@create']);
        Route::post('hotLead/store', ['as' => 'hotLead.store', 'uses' => 'HotLeadController@store']);
    
        Route::get('hotLead/edit/{id}', ['as' => 'hotLead.edit', 'uses' => 'HotLeadController@edit'])->where('id','[0-9]+');
        Route::put('hotLead/update/{id}', ['as' => 'hotLead.update', 'uses' => 'HotLeadController@update'])->where('id','[0-9]+');
    
        Route::get('hotLead/delete/{id}', ['as' => 'hotLead.delete', 'uses' => 'HotLeadController@destroy'])->where('id','[0-9]+');
    
        Route::post('hotLead/updateStatus', ['as' => 'hotLead.updateStatus', 'uses' => 'HotLeadController@updateStatus']);
    
        Route::get('hotLead/view/{id}',['as' => 'hotLead.view', 'uses' => 'HotLeadController@view']);


         /*
        |--------------------------------------------------------------------------
        | Cold Lead CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('coldLead', ['as' => 'coldLead.index', 'uses' => 'ColdLeadController@index']);
    
        Route::get('coldLead/create', ['as' => 'coldLead.create', 'uses' => 'ColdLeadController@create']);
        Route::post('coldLead/store', ['as' => 'coldLead.store', 'uses' => 'ColdLeadController@store']);
    
        Route::get('coldLead/edit/{id}', ['as' => 'coldLead.edit', 'uses' => 'ColdLeadController@edit'])->where('id','[0-9]+');
        Route::put('coldLead/update/{id}', ['as' => 'coldLead.update', 'uses' => 'ColdLeadController@update'])->where('id','[0-9]+');
    
        Route::get('coldLead/delete/{id}', ['as' => 'coldLead.delete', 'uses' => 'ColdLeadController@destroy'])->where('id','[0-9]+');
    
        Route::post('coldLead/updateStatus', ['as' => 'coldLead.updateStatus', 'uses' => 'ColdLeadController@updateStatus']);
    
        Route::get('coldLead/view/{id}',['as' => 'coldLead.view', 'uses' => 'ColdLeadController@view']);


         /*
        |--------------------------------------------------------------------------
        | Sales Pipeline CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('salesPipeline', ['as' => 'salesPipeline.index', 'uses' => 'SalesPipelineController@index']);
    
        Route::get('salesPipeline/create', ['as' => 'salesPipeline.create', 'uses' => 'SalesPipelineController@create']);
        Route::post('salesPipeline/store', ['as' => 'salesPipeline.store', 'uses' => 'SalesPipelineController@store']);
    
        Route::get('salesPipeline/edit/{id}', ['as' => 'salesPipeline.edit', 'uses' => 'SalesPipelineController@edit'])->where('id','[0-9]+');
        Route::put('salesPipeline/update/{id}', ['as' => 'salesPipeline.update', 'uses' => 'SalesPipelineController@update'])->where('id','[0-9]+');
    
        Route::get('salesPipeline/delete/{id}', ['as' => 'salesPipeline.delete', 'uses' => 'SalesPipelineController@destroy'])->where('id','[0-9]+');
    
        Route::post('salesPipeline/updateStatus', ['as' => 'salesPipeline.updateStatus', 'uses' => 'SalesPipelineController@updateStatus']);
    
        Route::get('salesPipeline/view/{id}',['as' => 'salesPipeline.view', 'uses' => 'SalesPipelineController@view']);

    
        /*
        |--------------------------------------------------------------------------
        | Converted Lead CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('convertedLead', ['as' => 'convertedLead.index', 'uses' => 'ConvertedLeadController@index']);
    
        Route::get('convertedLead/create', ['as' => 'convertedLead.create', 'uses' => 'ConvertedLeadController@create']);
        Route::post('convertedLead/store', ['as' => 'convertedLead.store', 'uses' => 'ConvertedLeadController@store']);
    
        Route::get('convertedLead/edit/{id}', ['as' => 'convertedLead.edit', 'uses' => 'ConvertedLeadController@edit'])->where('id','[0-9]+');
        Route::put('convertedLead/update/{id}', ['as' => 'convertedLead.update', 'uses' => 'ConvertedLeadController@update'])->where('id','[0-9]+');
    
        Route::get('convertedLead/delete/{id}', ['as' => 'convertedLead.delete', 'uses' => 'ConvertedLeadController@destroy'])->where('id','[0-9]+');
    
        Route::post('convertedLead/updateStatus', ['as' => 'convertedLead.updateStatus', 'uses' => 'ConvertedLeadController@updateStatus']);
    
        Route::get('convertedLead/view/{id}',['as' => 'convertedLead.view', 'uses' => 'ConvertedLeadController@view']);

  
        /*
        |--------------------------------------------------------------------------
        | Rejected Lead CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('rejectedLead', ['as' => 'rejectedLead.index', 'uses' => 'RejectedLeadController@index']);
    
        Route::get('rejectedLead/create', ['as' => 'rejectedLead.create', 'uses' => 'RejectedLeadController@create']);
        Route::post('rejectedLead/store', ['as' => 'rejectedLead.store', 'uses' => 'RejectedLeadController@store']);
    
        Route::get('rejectedLead/edit/{id}', ['as' => 'rejectedLead.edit', 'uses' => 'RejectedLeadController@edit'])->where('id','[0-9]+');
        Route::put('rejectedLead/update/{id}', ['as' => 'rejectedLead.update', 'uses' => 'RejectedLeadController@update'])->where('id','[0-9]+');
    
        Route::get('rejectedLead/delete/{id}', ['as' => 'rejectedLead.delete', 'uses' => 'RejectedLeadController@destroy'])->where('id','[0-9]+');
    
        Route::post('rejectedLead/updateStatus', ['as' => 'rejectedLead.updateStatus', 'uses' => 'RejectedLeadController@updateStatus']);
    
        Route::get('rejectedLead/view/{id}',['as' => 'rejectedLead.view', 'uses' => 'RejectedLeadController@view']);


         /*
        |--------------------------------------------------------------------------
        | Rejected Lead CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('activity', ['as' => 'activity.index', 'uses' => 'LeadActivityController@index']);
    
        Route::get('activity/create', ['as' => 'activity.create', 'uses' => 'LeadActivityController@create']);
        Route::post('activity/store', ['as' => 'activity.store', 'uses' => 'LeadActivityController@store']);
    



        /*
        |--------------------------------------------------------------------------
        | Meeting CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::post('meeting/store', ['as' => 'meeting.store', 'uses' => 'MeetingController@store']);
        Route::put('meeting/update/{id}', ['as' => 'meeting.update', 'uses' => 'MeetingController@update'])->where('id','[0-9]+');
    
        Route::get('meeting/delete/{id}', ['as' => 'meeting.delete', 'uses' => 'MeetingController@destroy'])->where('id','[0-9]+');

        Route::post('meeting/store-meeting-comment', ['as' => 'meeting.store-meeting-comment', 'uses' => 'MeetingController@storeMeetingComment']);

        

        /*
        |--------------------------------------------------------------------------
        | CAll CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::post('call/store', ['as' => 'call.store', 'uses' => 'CallController@store']);
        Route::put('call/update/{id}', ['as' => 'call.update', 'uses' => 'CallController@update'])->where('id','[0-9]+');
    
        Route::get('call/delete/{id}', ['as' => 'call.delete', 'uses' => 'CallController@destroy'])->where('id','[0-9]+');

        Route::post('call/store-call-comment', ['as' => 'call.store-call-comment', 'uses' => 'CallController@storeCallComment']);

        /*
        |--------------------------------------------------------------------------
        | Note CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::post('note/store', ['as' => 'note.store', 'uses' => 'NoteController@store']);
       
        Route::put('note/update/{id}', ['as' => 'note.update', 'uses' => 'NoteController@update'])->where('id','[0-9]+');
    
        Route::get('note/delete/{id}', ['as' => 'note.delete', 'uses' => 'NoteController@destroy'])->where('id','[0-9]+');
         
        Route::post('note/store-note-comment', ['as' => 'note.store-note-comment', 'uses' => 'NoteController@storeNoteComment']);
            
});

    Route::get('lead/downloadSheet','DailyClientController@downloadSheet')->name('lead.downloadSheet');

    Route::get('dailyClient/summary_view/{id}',['as' => 'dailyClient.summary_view', 'uses' => 'DailyClientController@summary_view']);