<?php

namespace App\Modules\Lead\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

use App\Modules\Lead\Repositories\DailyClientInterface;
use App\Modules\Lead\Repositories\DailyClientRepository;
use App\Modules\Lead\Repositories\HotLeadInterface;
use App\Modules\Lead\Repositories\HotLeadRepository;
use App\Modules\Lead\Repositories\ColdLeadInterface;
use App\Modules\Lead\Repositories\ColdLeadRepository;
use App\Modules\Lead\Repositories\SalesPipelineInterface;
use App\Modules\Lead\Repositories\SalesPipelineRepository;
use App\Modules\Lead\Repositories\ConvertedLeadInterface;
use App\Modules\Lead\Repositories\ConvertedLeadRepository;
use App\Modules\Lead\Repositories\RejectedLeadInterface;
use App\Modules\Lead\Repositories\RejectedLeadRepository;

use App\Modules\Lead\Repositories\FileInterface;
use App\Modules\Lead\Repositories\FileRepository;
use App\Modules\Lead\Repositories\FolderInterface;
use App\Modules\Lead\Repositories\FolderRepository;

use App\Modules\Lead\Repositories\LeadLogInterface;
use App\Modules\Lead\Repositories\LeadLogRepository;
use App\Modules\Lead\Repositories\LeadActivityInterface;
use App\Modules\Lead\Repositories\LeadActivityRepository;

use App\Modules\Lead\Repositories\NoteInterface;
use App\Modules\Lead\Repositories\NoteRepository;

use App\Modules\Lead\Repositories\MeetingInterface;
use App\Modules\Lead\Repositories\MeetingRepository;

use App\Modules\Lead\Repositories\CallInterface;
use App\Modules\Lead\Repositories\CallRepository;

use App\Modules\Lead\Repositories\LeadActivityFeedInterface;
use App\Modules\Lead\Repositories\LeadActivityFeedRepository;

class LeadServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->dailyClientRegister();
        $this->hotLeadRegister();
        $this->coldLeadRegister();
        $this->salesPipelineRegister();
        $this->convertedLeadRegister();
        $this->rejectedLeadRegister();
        $this->LeadLogRegister();
        $this->LeadActivityRegister();
        $this->manageFileRegister();
        $this->manageFolderRegister();
        $this->noteRegister();
        $this->meetingrRegister();
        $this->callRegister();
        $this->LeadActivityFeedRegister();
    }

    public function dailyClientRegister(){
        $this->app->bind(
            DailyClientInterface::class,
            DailyClientRepository::class
        );
        
    }
    
    public function hotLeadRegister(){
         $this->app->bind(
            HotLeadInterface::class,
            HotLeadRepository::class
        );
    }

    public function coldLeadRegister(){
        $this->app->bind(
           ColdLeadInterface::class,
           ColdLeadRepository::class
       );
    }

    public function salesPipelineRegister(){
        $this->app->bind(
            SalesPipelineInterface::class,
            SalesPipelineRepository::class
        );
    }
    
    public function convertedLeadRegister(){
         $this->app->bind(
            ConvertedLeadInterface::class,
            ConvertedLeadRepository::class
        );
    }
    
    public function rejectedLeadRegister(){
         $this->app->bind(
            RejectedLeadInterface::class,
            RejectedLeadRepository::class
        );
    }
    
    public function LeadLogRegister(){
         $this->app->bind(
            LeadLogInterface::class,
            LeadLogRepository::class
        );
    }
    public function LeadActivityRegister(){
         $this->app->bind(
            LeadActivityInterface::class,
            LeadActivityRepository::class
        );
    }

    public function manageFileRegister(){
        $this->app->bind(
            FileInterface::class,
            FileRepository::class
        );
    }
    
    public function manageFolderRegister(){
        $this->app->bind(
            FolderInterface::class,
            FolderRepository::class
        );
    }

    public function noteRegister(){
        $this->app->bind(
            NoteInterface::class,
            NoteRepository::class
        );
    }

    public function meetingrRegister(){
        $this->app->bind(
            MeetingInterface::class,
            MeetingRepository::class
        );
    }
    
    public function callRegister(){
        $this->app->bind(
            CallInterface::class,
            CallRepository::class
        );
    }

     public function LeadActivityFeedRegister(){
         $this->app->bind(
            LeadActivityFeedInterface::class,
            LeadActivityFeedRepository::class
        );
    }
    
    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('lead.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'lead'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/lead');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/lead';
        }, \Config::get('view.paths')), [$sourcePath]), 'lead');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/lead');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'lead');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'lead');
        }
    }

    /**
     * Register an additional directory of factories.
     * 
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
