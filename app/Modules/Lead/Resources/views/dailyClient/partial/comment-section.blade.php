<li>
    <a href="javascript:void(0)" class="media">
        <div class="mr-3">
            <img src="{{ asset('admin/default.png') }}" width="36" height="36" class="rounded-circle" alt="">
        </div>
        <div class="media-body">
            <div class="media-title d-flex">
                <span class="font-weight-semibold  text-teal">{{ $commentData['comment_by'] }}</span>
                <span class="font-size-sm text-muted ml-auto">{{ $commentData['date'] }}</span>
            </div>
             <span class="font-italic">{{ $commentData['comment'] }}</span>
        </div>
    </a>
</li>