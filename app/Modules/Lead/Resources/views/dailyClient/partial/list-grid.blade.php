<div class="row">

    @if($dailyClient->total() != 0)
        @foreach($dailyClient as $key => $value)
            @php
          
             if ($value->status == '0') {
                    $status_val = 'Daily Leads';
                    $color = 'bg-slate-700';
                    $icon = 'icon-tree7';
                }
                if ($value->status == '1') {
                    $status_val = 'Hot Leads';
                    $color = 'bg-danger';
                    $icon = 'icon-user-check';
                }

                 if ($value->status == '2') {
                    $status_val = 'Cold Leads';
                    $color = 'bg-indigo-700';
                    $icon = 'icon-brain';
                }

                 if ($value->status == '3') {
                    $status_val = 'Sales Pipeline';
                    $color = 'bg-warning-800';
                    $icon = 'icon-point-up';
                }
                if ($value->status == '4') {
                    $status_val = 'Converted Leads';
                    $color = 'bg-success-700';
                    $icon = 'icon-thumbs-up3';
                }
                if ($value->status == '5') {
                    $status_val = 'Rejected Leads';
                    $color = 'bg-danger-800';
                    $icon = 'icon-thumbs-down3';
                }
            @endphp

            <div class="col-lg-4">
                <div class="card card-body border-top-warning-400">
                    <div class="text-left">
                        <h6 class="m-0 font-weight-semibold">#<span class="text-danger">{{++$key}}</span> <a href="{{route('dailyClient.profile',['id'=>$value->id])}}">{{ $value->company_name }} </a></h6>
                        <p class="text-muted m-0">Next Meeting Date : {{ $value->next_meeting_date }}</p>
                        <p class="text-muted m-0">Contact Person : {{ $value->contact_person }}</p>
                        <p class="text-muted m-0">Contact Number : {{ $value->contact_no_1 }}</p>
                        <h5 class="font-weight-semibold text-success">Approach Cost :
                            Rs.{{ $value->client_approach_final_cost }}/-</h5>
                        @if($value->updated_by == null)
                            <h5 class="text-danger">Created
                                By: {{ $value->getUser->first_name.' '. $value->getUser->last_name}}</h5>
                        @else
                            <h5 class="text-danger">Updated
                                By: {{ $value->getUpdatedUser->first_name.' '. $value->getUpdatedUser->last_name}}</h5>
                        @endif

                    </div>
                    <hr class="mt-0 mb-1">
                    <div>
    

                        <a data-toggle="modal" data-target="#modal_theme_info"
                           class="btn bg-purple-400 btn-icon rounded-round view_daily_client" lead_id="{{ $value->id }}"
                           data-popup="tooltip" data-original-title="View Detail" data-placement="bottom"><i
                                    class="icon-eye"></i></a>
                                 @php  
                                    $user = Auth::user();
                                    $user_type = $user->user_type;
                                @endphp
                                                   
                 @if($user_type == 'super_admin')        
                                     
                        @if($value->status != '5' && $value->status != '4')
                            <a style="margin-bottom: 1px;" class="btn bg-success-400 btn-icon rounded-round"
                               href="{{route('dailyClient.edit',$value->id)}}" data-popup="tooltip" data-placement="bottom"
                               data-original-title="Edit"><i class="icon-pencil6"></i></a>

                            <a style="margin-bottom: 1px;" data-toggle="modal" data-target="#modal_project_delete"
                               class="btn bg-danger-400 btn-icon rounded-round delete_lead"
                               link="{{route('dailyClient.delete',$value->id)}}" data-popup="tooltip"
                               data-placement="bottom" data-original-title="Delete"><i class="icon-bin"></i></a>
                        @endif   

                        @if($value->status == '4')
                        <a style="margin-bottom: 1px;" class="btn bg-info-800 btn-icon rounded-round"
                           href="{{route('agreement.index',['lead_id'=>$value->id])}}" data-popup="tooltip" data-placement="bottom"
                           data-original-title="Lead Agreement"><i class="icon-certificate"></i></a>
                        @endif

                        <a style="margin-bottom: 1px;" class="btn bg-orange-400 btn-icon rounded-round"
                           href="{{route('activity.index',['lead_id'=>$value->id])}}" data-popup="tooltip" data-placement="bottom"
                           data-original-title="Lead Activity"><i class="icon-reading"></i></a>

                @endif


        </div>

                    <div class="ribbon-container">
                        <div class="ribbon {{ $color }} "><i class="{{ $icon }}"></i> {{$status_val}}</div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <span class="btn btn-light">No Daily Client Found.</span>
    @endif
</div>
<span style="margin: 5px;float: right;">
    @if($dailyClient->total() != 0)
        @if(array_key_exists("search_from",$search_value))
            @php
                if(array_key_exists("created_by",$search_value)){
                $created_by=$search_value['created_by'];
                }else{
                $created_by=null;
                }
            @endphp
            {!!$dailyClient->appends(['type'=>'grid','search_from'=>$search_value['search_from'],'search_to'=>$search_value['search_to'],'created_by'=>$created_by ])->links() !!}
        @elseif(array_key_exists("status",$search_value))
            {{ $dailyClient->appends(['type'=>'grid','status'=>$search_value['status']])->links() }}
        @elseif(array_key_exists("search_value",$search_value))
            {{ $dailyClient->appends(['type'=>'grid','search_value'=>$search_value['search_value']])->links() }}
        @else
            {{ $dailyClient->appends(['type'=>'grid'])->links() }}
        @endif
    @endif
</span>

