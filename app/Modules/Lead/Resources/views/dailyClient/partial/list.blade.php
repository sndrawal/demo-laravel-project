<table class="table table-striped">
    <thead>
    <tr class="bg-slate-400">
        <th>#</th>
        <th style="width: 15%;">Company Name</th>
        <th style="width: 15%;">Updated Date</th>
        <th style="width: 10%;">Contact Person</th>
        <th style="width: 10%;">Designation</th>
        <th style="width: 10%;">Contact Number</th>
        <th style="width: 15%;">Created Date</th>
        <th style="width: 15%;">Price</th>
        <th style="width: 10%;">Created By</th>
        <th style="width: 10%;">Status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @if($dailyClient->total() != 0)
        @foreach($dailyClient as $key => $value)
            @php

                if ($value->status == '0') {
                    $status_val = 'Daily Leads';
                    $color = 'bg-slate-700';
                    $icon = 'icon-tree7';
                }
                if ($value->status == '1') {
                    $status_val = 'Hot Leads';
                    $color = 'bg-danger';
                    $icon = 'icon-user-check';
                }

                 if ($value->status == '2') {
                    $status_val = 'Cold Leads';
                    $color = 'bg-indigo-700';
                    $icon = 'icon-brain';
                }

                 if ($value->status == '3') {
                    $status_val = 'Sales Pipeline';
                    $color = 'bg-warning-800';
                    $icon = 'icon-point-up';
                }
                if ($value->status == '4') {
                    $status_val = 'Converted Leads';
                    $color = 'bg-success-700';
                    $icon = 'icon-thumbs-up3';
                }
                if ($value->status == '5') {
                    $status_val = 'Rejected Leads';
                    $color = 'bg-danger-800';
                    $icon = 'icon-thumbs-down3';
                }
            @endphp

            <tr>
                <td>{{$dailyClient->firstItem() +$key}}</td>
                <td><a class="text-teal font-weight-semibold" href="{{route('dailyClient.profile',['id'=>$value->id])}}">{{ $value->company_name }}</a></td>
                 <td>{{ date('jS M, y',strtotime($value->updated_at)) }}</td>
                <td>{{ $value->contact_person }}</td>
                <td>{{ $value->designation }}</td>
                <td>{{ $value->contact_no_1 }}</td>
                <td>{{ date('jS M, y',strtotime($value->created_at)) }}</td>
                <td>Rs.{{ number_format(($value->final_cost) ? $value->final_cost : $value->client_approach_final_cost) }}</td>
                <td>{{ $value->getUser->first_name.' '. $value->getUser->last_name}}</td>

                 <td>
                    <a class="btn {{ $color }} btn-icon rounded-round update_project" project_id="{{ $value->id }}" data-popup="tooltip" data-placement="bottom"
                       data-original-title=" {{ $status_val }}"><i class="{{ $icon }}"></i></a>
                </td>


                <td class="text-center">
                    <div class="list-icons">
                        <div class="list-icons-item dropdown">
                            <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu7"></i></a>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(16px, 16px, 0px);">

                                <a data-toggle="modal" data-target="#modal_theme_info" class="dropdown-item view_daily_client text-purple-400" lead_id="{{ $value->id }}" data-popup="tooltip" data-original-title="View Detail" data-placement="bottom"><i class="icon-eye text-purple-400"></i> View Details</a>

                                
                                @php  
                                    $user = Auth::user();
                                    $user_type = $user->user_type;
                                @endphp

                                @if($user_type == 'super_admin') 
                                     @if($value->status != '5' && $value->status != '4')  

                                        <a class="dropdown-item" href="{{route('dailyClient.edit',$value->id)}}"><i class="icon-pencil6 bg-success-400"></i>Edit Lead</a>

                                        <a data-toggle="modal" data-target="#modal_project_delete" class="dropdown-item delete_lead" link="{{route('dailyClient.delete',$value->id)}}" ><i class="icon-bin bg-danger-400"></i>Delete Lead</a>

                                    @endif

                                     @if($value->status == '4')
                                         <a class="dropdown-item" href="{{route('agreement.index',['lead_id'=>$value->id])}}"><i class="icon-certificate bg-info-800"></i>Lead Agreement</a>
                                     @endif

                                      <a class="dropdown-item" href="{{route('activity.index',['lead_id'=>$value->id])}}"><i class="icon-reading text-orange-400"></i>Lead Activity</a>

                                @endif
                            </div>
                        </div>
                    </div>
                </td>

            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="9">No Client Daily Approach Found !!!</td>
        </tr>
    @endif
    </tbody>
</table>

<span style="margin: 5px;float: right;">
    @if($dailyClient->total() != 0)
        @if(array_key_exists("search_from",$search_value))
            @php
                if(array_key_exists("created_by",$search_value)){
                $created_by=$search_value['created_by'];
                }else{
                $created_by=null;
                }
            @endphp
            {!!$dailyClient->appends(['search_from'=>$search_value['search_from'],'search_to'=>$search_value['search_to'],'created_by'=>$created_by ])->links() !!}
        @elseif(array_key_exists("status",$search_value))
            {{ $dailyClient->appends(['status'=>$search_value['status']])->links() }}
        @elseif(array_key_exists("search_value",$search_value))
            {{ $dailyClient->appends(['search_value'=>$search_value['search_value']])->links() }}
        @else
            {{ $dailyClient->links() }}
        @endif
    @endif
</span>

