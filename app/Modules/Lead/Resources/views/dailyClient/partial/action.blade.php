<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold">Daily Client Approach</legend>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Company Name:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-hat"></i>
                                </span>
                            </span>
                    {!! Form::text('company_name', $value = null, ['id'=>'company_name','placeholder'=>'Enter Company Name','class'=>'form-control','required']) !!}
                        </div>
                    </div>
            </div>
        </div>

        {{ Form::hidden('profile_lead', '0') }}

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Client Address:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-location3"></i>
                                </span>
                            </span>
                    {!! Form::text('client_address', $value = null, ['id'=>'client_address','placeholder'=>'Enter Client Address','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Contact Person:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                    {!! Form::text('contact_person', $value = null, ['id'=>'contact_person','placeholder'=>'Enter Contact Person','class'=>'form-control','required']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Contact Number 1:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-phone2"></i>
                                </span>
                            </span>
                    {!! Form::text('contact_no_1', $value = null, ['id'=>'contact_no_1','placeholder'=>'Enter Contact Number 1','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Contact Number 2:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-phone-wave"></i>
                                </span>
                            </span>
                    {!! Form::text('contact_no_2', $value = null, ['id'=>'contact_no_2','placeholder'=>'Enter Contact Number 2','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Designation:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i>
                                </span>
                            </span>
                    {!! Form::text('designation', $value = null, ['id'=>'designation','placeholder'=>'Enter Designation','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Email:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-envelop"></i>
                                </span>
                            </span>
                    {!! Form::email('email', $value = null, ['id'=>'email','placeholder'=>'Enter Email','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Visited Date:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i>
                                </span>
                            </span>
                    {!! Form::text('next_visit_date', $value = null, ['id'=>'next_visit_date','placeholder'=>'Enter Visited Date','class'=>'form-control daterange-single']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Means Of Communication:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-bubbles4"></i>
                                </span>
                            </span>
                                <select class="form-control multiselect" name="means_of_communication[]" multiple='multiple' data-fouc>
                                    @foreach($communication as $key => $value)
                                        @php
                                            $select ="";
                                            if(isset($client)){
                                            $means_of_communication = json_decode($client->means_of_communication);
                                            if(in_array($key, $means_of_communication)){
                                            $select = "selected='selected'";
                                            }
                                            }

                                        @endphp
                                        <option value="{{$key}}" {{$select}}>{{$value}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Purpose of Meeting:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user-tie"></i>
                                </span>
                            </span>
                                <select class="form-control multiselect" name="purpose_of_meeting[]" multiple='multiple' data-fouc>
                                    @foreach($purpose_of_meeting as $key => $value)
                                        @php
                                            $select ="";
                                            if(isset($client)){
                                            $purpose_of_meeting = json_decode($client->purpose_of_meeting);
                                                if($purpose_of_meeting){
                                                    if(in_array($key, $purpose_of_meeting)){
                                                    $select = "selected='selected'";
                                                    }
                                                }
                                            }

                                        @endphp
                                        <option value="{{$key}}" {{$select}}>{{$value}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Services:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                                <select class="form-control multiselect" name="services[]" multiple='multiple' data-fouc>
                                    @foreach($services as $key => $value)
                                        @php
                                            $select ="";
                                            if(isset($client)){
                                            $services = json_decode($client->services);
                                            if($services){
                                                if(in_array($key, $services)){
                                                   $select = "selected='selected'";
                                                }
                                            }
                                        }

                                        @endphp
                                        <option value="{{$key}}" {{$select}}>{{$value}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Sources of Leads:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-collaboration"></i>
                                </span>
                            </span>
                                <select class="form-control multiselect" name="source_of_leads[]" multiple='multiple' data-fouc>
                                    @foreach($source_of_lead as $key => $value)

                                        @php

                                            $select ="";
                                            if(isset($client)){
                                            $source_of_leads = json_decode($client->source_of_leads);
                                            if($source_of_leads){
                                                if(in_array($key, $source_of_leads)){
                                                $select = "selected='selected'";
                                                }
                                            }
                                        }

                                        @endphp

                                        <option value="{{$key}}" {{$select}}>{{$value}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Client Approach Cost:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-coins"></i>
                                </span>
                            </span>
                    {!! Form::text('client_approach_cost', $value = null, ['id'=>'client_approach_cost','placeholder'=>'Enter Client Approach Cost','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Discount(in %):</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-coins"></i>
                                </span>
                            </span>
                    {!! Form::text('client_approach_discount', $value = null, ['id'=>'client_approach_discount','placeholder'=>'Enter Discount in %','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Final Approach Cost:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-coins"></i>
                                </span>
                            </span>
                    {!! Form::text('client_approach_final_cost', $value = null, ['id'=>'client_approach_final_cost','placeholder'=>'Enter Final Cost','class'=>'form-control' ,'readonly']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Next Meeting Date:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar2"></i>
                                </span>
                            </span>
                    {!! Form::text('next_meeting_date', $value = null, ['id'=>'next_meeting_date','class'=>'form-control daterange-single']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Brief Description:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-notebook"></i>
                                </span>
                            </span>
                    {!! Form::textarea('brief_description', $value = null, ['id'=>'brief_description','placeholder'=>'Enter Brief Description Note','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Segment:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pie-chart"></i>
                                </span>
                            </span>
                    {!! Form::select('segment',$segment, $value = null, ['id'=>'segment','placeholder'=>'Select Segment','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

</fieldset>

@php  

    if((isset($client)) AND ($client->status != '0' AND $client->status !='5')){ 


        if ($client->status == '1') {
            $status_val = 'Hot Leads';
        }
        if ($client->status == '2') {
            $status_val = 'Cold Leads';
        }

         if ($client->status == '3') {
            $status_val = 'Sales Pipeline';
        }
        if ($client->status == '4') {
            $status_val = 'Converted Leads';
        }

@endphp
<fieldset>
    <legend class="text-uppercase font-size-sm font-weight-bold">{{ $status_val }} </legend>

     
     <div class="form-group row">
        <div class="col-lg-4">
            <div class="row">
                <label class="col-form-label col-lg-3">Status :</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-books"></i>
                                </span>
                            </span>
                    {!! Form::select('status',[ '1'=>'Hot Leads','2'=>'Cold Leads','3'=>'Sales Pipeline', '4'=> 'Converted Leads'], $value = null, ['id'=>'status','placeholder'=>'Select Status','class'=>'form-control']) !!}
                        </div>
                    </div>
                </div> 
            </div>
        </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Service Required:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-books"></i>
                                </span>
                            </span>
                    {!! Form::select('service_required',[ 'accept'=>'Accept','in-progress'=>'In-Progress','reject'=>'Reject'], $value = null, ['id'=>'service_required','class'=>'form-control','disabled']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Cost:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-coins"></i>
                                </span>
                            </span>
                    {!! Form::text('cost', $value = null, ['id'=>'cost','placeholder'=>'Enter Cost','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Discount(in %):</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-coins"></i>
                                </span>
                            </span>
                    {!! Form::text('discount', $value = null, ['id'=>'discount','placeholder'=>'Enter Discount in %','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Final Cost:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-coins"></i>
                                </span>
                            </span>
                    {!! Form::text('final_cost', $value = null, ['id'=>'final_cost','placeholder'=>'Enter Final Cost','class'=>'form-control' ,'readonly']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Active Lead Services:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                                <select class="form-control multiselect" name="active_lead_services[]" multiple='multiple' data-fouc>
                                @foreach($active_services as $key => $value)
                                    @php
                                        $select ="";
                                        if(isset($client)){
                                        $services = json_decode($client->active_lead_services);
                                        if($services){
                                            if(in_array($key, $services)){
                                                $select = "selected='selected'";
                                            }
                                        }
                                    }

                                    @endphp
                                    <option value="{{$key}}" {{$select}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Sources of Leads:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-collaboration"></i>
                                </span>
                            </span>
                                <select class="form-control multiselect" name="source_of_leads[]" multiple='multiple' data-fouc>
                                    @foreach($source_of_lead as $key => $value)

                                        @php
                                                $select ="";
                                                if(isset($client)){
                                                $source_of_leads = json_decode($client->source_of_leads);
                                                if($source_of_leads){
                                                    if(in_array($key, $source_of_leads)){
                                                    $select = "selected='selected'";
                                                    }
                                                }
                                            }

                                        @endphp

                                        <option value="{{$key}}" {{$select}}>{{$value}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Additional Note:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-notebook"></i>
                                </span>
                            </span>
                        {!! Form::textarea('additional_note', $value = null, ['id'=>'additional_note','placeholder'=>'Enter Additional Note','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Client Expectation Note:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-notebook"></i>
                                </span>
                            </span>
                        {!! Form::textarea('client_expectation_note', $value = null, ['id'=>'client_expectation_note','placeholder'=>'Enter Client Expectation Note','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

</fieldset>


@php } @endphp

@php if((isset($client)) AND ($client->is_rejected_lead == '1')){ @endphp

<fieldset>
    <legend class="text-uppercase font-size-sm font-weight-bold">Rejected Leads</legend>

    <div class="form-group row">
        <label class="col-form-label col-lg-4">Termination Reason:<span class="text-danger">*</span></label>
        <div class="col-lg-8 form-group-feedback form-group-feedback-right">
            <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-notebook"></i></span>
                </span>
                {!! Form::textarea('terminate_reason', $value = null, ['id'=>'terminate_reason','placeholder'=>'Enter Terminate Reason','class'=>'form-control']) !!}
            </div>
        </div>
    </div>

</fieldset>

@php } @endphp

<div class="text-right">
    <button type="submit"  id="contact_un_checked" class="btn bg-teal-400">{{ $btnType }} <i class="icon-database-insert"></i></button>
</div>



<script type="text/javascript">

    $(document).ready(function () {
        $(document).on('keyup', '#client_approach_discount, #client_approach_cost', function () {

            var cost = $('#client_approach_cost').val();
            var discount = $('#client_approach_discount').val();

            var final = cost - ((discount / 100) * cost);

            $('#client_approach_final_cost').val(final);
        });

        $(document).on('click', '#contact_checked', function () {
            $('#contact_check_value').val(1);
        });
        $(document).on('click', '#contact_un_checked', function () {
            $('#contact_check_value').val(0);
        });
    });
</script>
