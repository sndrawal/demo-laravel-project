<div class="form-group row">
    <label class="col-lg-3 mt-2">Visited Date</label>
    <div class="col-lg-6">
        {!! Form::text('visit_date', $value = null, ['id'=>'visit_date','placeholder'=>'Enter Visited Date','class'=>'form-control daterange-single']) !!}
    </div>
    <div class="col-lg-3">
        <button type="submit" class="btn bg-teal-400"><i class="icon-search4"></i></button>
    </div>
</div>
