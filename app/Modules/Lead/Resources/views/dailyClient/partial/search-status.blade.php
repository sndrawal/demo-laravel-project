<div class="form-group row">
    <div class="col-lg-9">
         {!! Form::select('status',[ '0'=>'Dailly Client Approach','1'=>'Hot Leads','2'=>'Cold Leads','3'=>'Sales Pipeline', '4'=> 'Converted Leads', '5'=> 'Rejected Leads'], $value = null, ['id'=>'status','placeholder'=>'Select Status','class'=>'form-control']) !!}
    </div>
    <div class="col-lg-3">
        <button type="submit" class="btn bg-teal-400"><i class="icon-search4"></i></button>
    </div>
</div>
