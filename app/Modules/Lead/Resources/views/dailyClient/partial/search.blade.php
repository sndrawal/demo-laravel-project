<div class="form-group row">
    <div class="col-lg-9">
          {!! Form::text('search_value', $value = null, ['id'=>'search_value','placeholder'=>'Search Project','class'=>'form-control']) !!}
    </div>
    <div class="col-lg-3">
        <button type="submit" class="btn bg-teal-400"><i class="icon-search4"></i></button>
    </div>
</div>
