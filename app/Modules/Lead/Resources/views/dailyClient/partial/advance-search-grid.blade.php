<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
@php
    $search_from_value = (array_key_exists('search_from',$search_value)) ? $search_value['search_from'] : null;
    $search_to_value = (array_key_exists('search_to',$search_value)) ? $search_value['search_to'] : null;
    $created_by_value = (array_key_exists('created_by',$search_value)) ? $search_value['created_by'] : null;
@endphp

        <div class="row ml-4">
            <div class="col-3">
                <div class="row">
                    <label class="col-md-3 col-form-label">From: </label>
                    <div class="col-md-9">
                        <div class="form-group form-group-feedback form-group-feedback-left">

                            {!! Form::text('search_from', $value = $search_from_value, ['id'=>'search_from','placeholder'=>'Search From','class'=>'form-control form-control-lg  daterange-single']) !!}
                            <div class="form-control-feedback form-control-feedback-lg">
                                <i class="icon-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="row">
                    <label class="col-md-3 col-form-label">To: </label>
                    <div class="col-md-9">
                        <div class="form-group form-group-feedback form-group-feedback-left">
                            {!! Form::text('search_to', $value = $search_to_value, ['id'=>'search_to','placeholder'=>'Search To','class'=>'form-control form-control-lg  daterange-single']) !!}
                            <div class="form-control-feedback form-control-feedback-lg">
                                <i class="icon-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="row">
                    <label class="col-md-4 col-form-label">User:</label>
                    <div class="col-md-8">
                        
                        <select class="form-control" name="created_by">
                            <option value=""> --Select User --</option>
                            @foreach($employee as $key => $value)
                            <option value="{{$value->id}}" @if(isset($created_by_value) && $created_by_value == $value->id) selected @endif>{{ $value->first_name .' '.$value->last_name }}</option>
                            @endforeach
                        </select>
                        
                    </div>
                </div>
            </div>
            
            <div class="col-3 text-center" style="margin-left: -33px;">
            <button type="submit" class="btn bg-teal-400 ml-2"><i class="icon-search4"></i></button>
            <a href="{{ route('dailyClient.index',['type' => 'grid']) }}" class="btn bg-danger-400 ml-2" data-popup="tooltip" data-placement="bottom" data-original-title="Reset"><i class="icon-spinner9"></i></a>
            </div>

        </div>