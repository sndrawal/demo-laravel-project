@extends('admin::layout')
@section('title')Lead @stop 
@section('breadcrum')Edit Lead @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
<!-- /theme JS files -->
<script src="{{ asset('admin/validation/dailyClient.js')}}"></script>

@stop 
@section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800 header-elements-inline">
        <h5 class="card-title">Edit Lead</h5>
        <div class="header-elements">
        </div>
    </div>

    <div class="card-body">

        {!! Form::model($client,['method'=>'PUT','route'=>['dailyClient.update',$client->id],'id'=>'dailyClient_submit','class'=>'form-horizontal','role'=>'form','files'=>true]) !!} @include('lead::dailyClient.partial.action',['btnType'=>'Update']) {!! Form::close() !!}
        
    </div>
</div>
<!-- /form inputs -->

@stop