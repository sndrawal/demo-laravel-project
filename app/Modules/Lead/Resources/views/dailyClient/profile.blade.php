@extends('admin::layout')

@section('title') Lead Profile @stop 
@section('breadcrum') Lead Profile @stop 

@section('script')
<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
<script src="{{ asset('admin/assets/js/plugins/forms/jquery-clock-timepicker.min.js') }}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<link href="{{asset('admin/assets/extra/app.css')}}" rel="stylesheet" type="text/css">
<script type="text/javascript">
    $(document).ready(function(){
            // Select with search
            $('.select-search').select2();
        });
    </script>
    <style>
        .select2-container {
            width: 93.5% !important
        }

        .form-lead {
            padding-left: 4px !important;
        }
    </style>

<script src="{{asset('admin/global/js/plugins/ui/fab.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/ui/sticky.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/ui/prism.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/extra_fab.js')}}"></script>
   
@stop

    @section('content') 


<!-- Top right menu -->
<ul class="fab-menu fab-menu-absolute fab-menu-top-left" data-fab-toggle="hover" id="fab-menu-affixed-demo-right">
    <li>
    
         <a href="{{ route('dailyClient.index') }}" class="fab-menu-btn btn bg-danger btn-float rounded-round btn-icon"><i class="icon-arrow-left15" data-popup="tooltip" data-placement="bottom" data-original-title="Back To Lead"></i></a>

    </li>
</ul>
<!-- /top right menu -->

    <div class="card bg-light"> 
        <div class="card-body">
            <div class="row tab-content">

                <div class="col-md-3">
                    <div class="card-img-actions mx-1 mt-1 text-center mb-1">
                        <figure style="height:170px; width:170px; margin: 20px auto 0; " class="text-center">
                           <img class="img-fluid rounded-round" style="width: 170px; height: 170px; object-fit: cover; border: 1px solid #eeeeec" src="{{ asset('admin/company_logo.png') }}" alt="">
                       </figure>
                   </div>

                   <div class="card-body text-center">
                    <h6 class="font-weight-semibold mb-0">{{ $lead_profile['company_name'] }}</h6>
                    <span class="d-block text-muted mb-2">{{ $lead_profile['contact_person'] }}</span>

                    <ul class="list-inline list-inline-condensed mt-1 mb-0" style="width: 294px;margin-left: -29px;">
                        <li class="list-inline-item">
                           <a data-toggle="modal" data-target="#fullHeightModalHistoryRightNote" class="btn bg-teal text-slate-400 btn-icon rounded-round ml-2" title="Note"><i class="text-light-400 icon-pencil"></i></a>
                           <br>
                           <span class="text-slate-400 ml-2">Note</span>
                       </li>
                       <li class="list-inline-item">
                        <a data-toggle="modal" data-target="#fullHeightModalHistoryRight" class="btn bg-warning text-slate-400 btn-icon rounded-round ml-2" title="View Log"><i class="text-light-400 icon-wall"></i></a>
                        <br>
                        <span class="text-slate-400 ml-2">Log</span>
                    </li>
                    <li class="list-inline-item">
                        <a data-toggle="modal" data-target="#fullHeightModalHistoryRightTask" class="btn bg-danger text-slate-400 btn-icon rounded-round ml-2" title="Task"><i class="text-light-400 icon-clipboard3"></i></a>
                        <br>
                        <span class="text-slate-400 ml-2">Task</span>
                    </li>
                    <li class="list-inline-item">
                        <a data-toggle="modal" data-target="#fullHeightModalHistoryRightCall" class="btn bg-warning text-slate-400 btn-icon rounded-round ml-2" title="Calls"><i class="text-light-400 icon-phone"></i></a>
                        <br>
                        <span class="text-slate-400 ml-2">Call</span>
                    </li>
                    <li class="list-inline-item">
                       <a data-toggle="modal" data-target="#fullHeightModalHistoryRightMeeting" class="btn bg-purple text-slate-400 btn-icon rounded-round ml-2" title="Meeting"><i class="text-light-400 icon-calendar2"></i></a>
                       <br>
                       <span class="text-slate-400 ml-2">Meeting</span>
                   </li>   

               </ul>
           </div>

       </div>

       <div class="col-md-9">
        <div class="mb-3">

            {!! Form::model($lead_profile,['method'=>'PUT','route'=>['dailyClient.update',$lead_profile->id],'id'=>'dailyClient_submit','class'=>'form-horizontal','role'=>'form','files'=>true]) !!}

            <h6 class="font-weight-semibold mt-2"><i class="text-teal icon-folder6 mr-2"></i><a href="#" class="text-teal">About This Lead<i class="ml-2 text-primary icon-question3" data-popup="tooltip" data-placement="bottom" data-original-title="Please point Icon for more Details." style="margin-top: -18px;"></i></a>

             <button id="spinner-light-4"  type="submit" class="btn bg-success border-success text-success-800 btn-icon ml-2 enabled_editLead" style="float:right;display: none;" data-popup="tooltip" data-placement="top" data-original-title="Update Lead"><i class="icon-pen-plus"></i></button>

             <button type="button" class="btn bg-warning border-warning text-warning-800 btn-icon ml-2 editContact" style="float:right;" data-popup="tooltip" data-placement="top" data-original-title="Edit Lead"><i class="icon-pencil"></i></button>

             <div class="dropdown-divider mb-2"></div></h6>

             {{ Form::hidden('profile_lead', '1') }}

             <div class="row">
                <div class="col-md-4">
                    <div class="form-group form-group-feedback" style="margin-bottom: 6px;">
    
                         <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-hat" data-popup="tooltip" data-placement="bottom" data-original-title="Company Name"></i>
                                </span>
                            </span>
                            {!! Form::text('company_name', $value = null, ['id'=>'company_name','placeholder'=>'Company Name *','class'=>'form-lead form-control edit_contact text-grey' ,'readonly','required']) !!}
                        </div>
                    </div>

                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-location4" data-popup="tooltip" data-placement="bottom" data-original-title="Client Address"></i>
                                </span>
                            </span>

                        {!! Form::text('client_address', $value = null, ['id'=>'client_address','placeholder'=>'Client Address','class'=>'form-lead form-control edit_contact text-grey' ,'readonly']) !!}

                        </div>
                    </div>

                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user" data-popup="tooltip" data-placement="bottom" data-original-title="Contact Person"></i>
                                </span>
                            </span>

                        {!! Form::text('contact_person', $value = null, ['id'=>'contact_person','placeholder'=>'Contact Person *','class'=>'form-lead form-control edit_contact text-grey' ,'readonly','required']) !!}

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-phone2" data-popup="tooltip" data-placement="bottom" data-original-title="Contact Number 1"></i>
                            </span>
                        </span>
                                
                     {!! Form::text('contact_no_1', $value = null, ['id'=>'contact_no_1','placeholder'=>'Contact Number 1','class'=>'form-lead form-control numeric edit_contact text-grey' ,'readonly']) !!}

                    </div>
                </div>

                <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-phone-wave" data-popup="tooltip" data-placement="bottom" data-original-title="Contact Number 2"></i>
                            </span>
                        </span>

                 {!! Form::text('contact_no_2', $value = null, ['id'=>'contact_no_2','placeholder'=>'Contact Number 2','class'=>'form-lead form-control numeric edit_contact text-grey' ,'readonly']) !!} 

                </div>
            </div>

            <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

                <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-pencil" data-popup="tooltip" data-placement="bottom" data-original-title="Designation"></i>
                        </span>
                    </span>

             {!! Form::text('designation', $value = null, ['id'=>'designation','placeholder'=>'Designation','class'=>'form-lead form-control edit_contact text-grey' ,'readonly']) !!}

            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

            <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-envelop" data-popup="tooltip" data-placement="bottom" data-original-title="Email"></i>
                    </span>
                </span>

            {!! Form::email('email', $value = null, ['id'=>'email','placeholder'=>' Email','class'=>'form-lead form-control edit_contact text-grey' ,'readonly']) !!}

            </div>
        </div>

        <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

            <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-calendar" data-popup="tooltip" data-placement="bottom" data-original-title="Visited Date"></i>
                        </span>
                    </span>

            {!! Form::text('next_visit_date', $value = null, ['id'=>'next_visit_date','placeholder'=>'Visited Date','class'=>'form-lead form-control daterange-single edit_contact text-grey' ,'readonly']) !!}

            </div>
        </div>

        <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

            <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-bubbles4" data-popup="tooltip" data-placement="bottom" data-original-title="Means of Communication"></i>
                    </span>
                </span>
                <select class="form-control multiselect edit_select" disabled name="means_of_communication[]" multiple='multiple' data-fouc>
                    @foreach($communication as $key => $value)
                    @php
                    $select ="";
                    if(isset($lead_profile)){
                        $means_of_communication = json_decode($lead_profile->means_of_communication);
                        if(in_array($key, $means_of_communication)){
                            $select = "selected='selected'";
                        }
                    }

                    @endphp
                    <option value="{{$key}}" {{$select}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>

        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
            <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-user-tie" data-popup="tooltip" data-placement="bottom" data-original-title="Purpose of Meeting"></i>
                    </span>
                </span>
                <select class="form-control multiselect edit_select" disabled name="purpose_of_meeting[]" multiple='multiple' data-fouc>
                    @foreach($purpose_of_meeting as $key => $value)
                    @php
                    $select ="";
                    if(isset($lead_profile)){
                        $purpose_of_meeting = json_decode($lead_profile->purpose_of_meeting);
                        if($purpose_of_meeting){
                            if(in_array($key, $purpose_of_meeting)){
                                $select = "selected='selected'";
                            }
                        }
                    }

                    @endphp
                    <option value="{{$key}}" {{$select}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

            <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-cogs" data-popup="tooltip" data-placement="bottom" data-original-title="Services"></i>
                    </span>
                </span>
                <select class="form-control multiselect edit_select" disabled name="services[]" multiple='multiple' data-fouc>
                    @foreach($services as $key => $value)
                    @php
                    $select ="";
                    if(isset($lead_profile)){
                        $services = json_decode($lead_profile->services);
                        if($services){
                            if(in_array($key, $services)){
                             $select = "selected='selected'";
                         }
                     }
                 }

                 @endphp
                 <option value="{{$key}}" {{$select}}>{{$value}}</option>
                 @endforeach
             </select>
         </div>

     </div>

     <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

         <div class="input-group">
            <span class="input-group-prepend">
                <span class="input-group-text"><i class="icon-collaboration" data-popup="tooltip" data-placement="bottom" data-original-title="Source of Leads"></i>
                </span>
            </span>
            <select class="form-control multiselect edit_select" disabled name="source_of_leads[]" multiple='multiple' data-fouc>
                @foreach($source_of_lead as $key => $value)

                @php

                $select ="";
                if(isset($lead_profile)){
                    $source_of_leads = json_decode($lead_profile->source_of_leads);
                    if($source_of_leads){
                        if(in_array($key, $source_of_leads)){
                            $select = "selected='selected'";
                        }
                    }
                }

                @endphp

                <option value="{{$key}}" {{$select}}>{{$value}}</option>

                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

         <div class="input-group">
            <span class="input-group-prepend">
                <span class="input-group-text"><i class="icon-coins" data-popup="tooltip" data-placement="bottom" data-original-title="Client Approach Cost"></i>
                </span>
            </span>

       {!! Form::text('client_approach_cost', $value = null, ['id'=>'client_approach_cost','placeholder'=>'Client Approach Cost','class'=>'form-lead form-control numeric edit_contact text-grey' ,'readonly']) !!}

    </div>
</div>

<div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

    <div class="input-group">
            <span class="input-group-prepend">
                <span class="input-group-text"><i class="icon-coins" data-popup="tooltip" data-placement="bottom" data-original-title="Discount"></i>
                </span>
            </span>

    {!! Form::text('client_approach_discount', $value = null, ['id'=>'client_approach_discount','placeholder'=>'Discount in %','class'=>'form-lead form-control numeric edit_contact text-grey' ,'readonly']) !!}

    </div>
</div>

    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

         <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-coins" data-popup="tooltip" data-placement="bottom" data-original-title="Final Cost"></i>
                    </span>
                </span>

     {!! Form::text('client_approach_final_cost', $value = null, ['id'=>'client_approach_final_cost','placeholder'=>'Final Cost','class'=>'form-lead form-control' ,'readonly']) !!}

        </div>
</div>
</div>

<div class="col-md-4">
    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

        <div class="input-group">
            <span class="input-group-prepend">
                <span class="input-group-text"><i class="icon-calendar2" data-popup="tooltip" data-placement="bottom" data-original-title="Set Next Meeting Date"></i>
                </span>
            </span>

     {!! Form::text('next_meeting_date', $value = null, ['id'=>'next_meeting_date','class'=>'form-lead form-control daterange-single edit_contact text-grey' ,'readonly']) !!}

    </div>
</div>

<div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

    <div class="input-group">
            <span class="input-group-prepend">
                <span class="input-group-text"><i class="icon-pie-chart" data-popup="tooltip" data-placement="bottom" data-original-title="Select Segment"></i>
                </span>
            </span>

    {!! Form::select('segment',$segment, $value = null, ['id'=>'segment','placeholder'=>'Select Segment','class'=>'form-lead form-control edit_select','disabled']) !!}

    </div>
</div>

    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

        <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-notebook" data-popup="tooltip" data-placement="bottom" data-original-title="Enter Brief Description Note"></i>
                    </span>
                </span>

       {!! Form::textarea('brief_description', $value = null, ['id'=>'brief_description','placeholder'=>'Enter Brief Description Note','class'=>'form-lead form-control edit_contact text-grey' ,'readonly','rows'=>'2']) !!}

        </div>
    </div>
</div>
     {{ Form::hidden('status', $lead_profile['status']) }}
</div>

{!! Form::close() !!}


</div>
</div>

</div>
</div>
</div>


<!-- SEcondary Section -->



<div class="d-flex align-items-start flex-column flex-md-row">



    <!-- Left Section -->
    <div class="tab-content w-100 order-2 order-md-1">

        <div class="card">
            <div class="card-header bg-info-600 header-elements-inline">
                <h6 class="card-title">CURRENT DEAL STATUS</h6>
            </div>

 @php   

                $color = 'bg-slate-300';
                $textcolor = 'text-slate-400';

        $leadArray = array('0'=>'New Lead','1'=>'Hot Lead','2'=>'Cold Lead','3'=>'Sales Pipeline','4'=>'Converted Lead','5'=>'Rejected Lead');

        $leadColor = array('0'=>'bg-pink-700','1'=>'bg-danger','2'=>'bg-indigo-700','3'=>'bg-warning-800','4'=>'bg-success-700','5'=>'bg-danger-800');

        $textColor = array('0'=>'text-pink-700','1'=>'text-danger','2'=>'text-indigo-700','3'=>'text-warning-800','4'=>'text-success-700','5'=>'text-danger-800');

        $icon = array('0'=>'icon-tree7','1'=>'icon-user-check','2'=>'icon-brain','3'=>'icon-point-up','4'=>'icon-thumbs-up3','5'=>'icon-thumbs-down3');

        $statusWithIcon = array('0','2','3');
        $statusWithoutIcon = array('1','4');

            $current_status = $leadArray[$lead_profile['status']];

            @endphp
            <div class="card-body">
               <ul class="list-inline list-inline-condensed mt-1 mb-0 text-center" style="margin-left: -29px;">

                    @foreach($leadArray as $key => $value)

              
                    @if($lead_profile['status'] == $key)
                        
                        @php
                            $currentColor = $leadColor[$key];
                            $textColor = $textcolor[$key];
                        @endphp
                   
                    @else
                        @php
                            $currentColor = 'bg-slate-300';
                            $textColor = 'text-slate-400';
                        @endphp

                    @endif
                   

                    <li class="list-inline-item">
                           <span class="btn  {{ $currentColor }} {{ $textColor }} btn-icon rounded-round ml-2" title="Client Lead"><i class="text-light {{ $icon[$key] }} "></i></span>
                           
                           <br>
                           <span class="text-slate-400 ml-2">{{ $value }}</span>


                    </li>

                    @if(in_array($key, $statusWithIcon))
                        <li class="list-inline-item">
                            <span class="btn btn-icon"><i class="text-danger icon-transmission"></i></span>
                        </li>
                    @endif

                    @if(in_array($key, $statusWithoutIcon))
                        <li class="list-inline-item">
                            <span class="btn btn-icon text-danger font-weight-semibold">OR</span>
                        </li>
                    @endif


                      

                    @endforeach

               </ul>
            </div>
        </div>



        <div class="tab-pane fade active show" id="activity">

            <!-- Invoices -->
            <div class="row">
                <div class="col-12">
                    <div class="card">


                        <div class="card-body">
                            <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                <li class="nav-item mb-2"><a href="#bottom-justified-tab1" class="active font-weight-semibold text-dark" data-toggle="tab">Activity </a></li>
                                <li class="nav-item mb-2"><a href="#bottom-justified-tab2" class="font-weight-semibold text-dark" data-toggle="tab">Notes<sup class="badge badge-pill bg-warning-400 md-0">{{ $note_count }}</sup></a></li>
                                <li class="nav-item mb-2"><a href="#bottom-justified-tab5" class=" font-weight-semibold text-dark" data-toggle="tab">Calls <sup class="badge badge-pill bg-warning-400 md-0">{{ $call_count }}</sup></a></li>
                                <li class="nav-item mb-2"><a href="#bottom-justified-tab3" class=" font-weight-semibold text-dark" data-toggle="tab">Tasks<sup class="badge badge-pill bg-warning-400 md-0">{{ $task_count }}</sup></a></li>
                                <li class="nav-item mb-2"><a href="#bottom-justified-tab4" class="font-weight-semibold text-dark" data-toggle="tab">Meeting<sup class="badge badge-pill bg-warning-400">{{ $meeting_count }}</sup></a></li>
                                <li class="nav-item mb-2"><a href="#bottom-justified-tab7" class=" font-weight-semibold text-dark summarytab" data-toggle="tab" link="{{ $lead_profile->id }}">Summary</a></li>

                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="bottom-justified-tab1">
                                 <div class="activity">

                                    @if($lead_activity_feed->total() != 0)
                                    @foreach($lead_activity_feed as $key => $value)

                                    @if($value->method =='Lead_Status')
                                    @php
                                    $log_id = $value->method_id;
                                    $LogInfo = App\Modules\Lead\Entities\LeadLog::find($log_id);
                                    @endphp


                                    <div class="card activity-item border-left-3 border-left-blue rounded-left-0 cursor-pointer" data-target="#collapse-text" data-toggle="collapse" aria-expanded="true">
                                        <div class="p-1">
                                            <div class="d-flex align-items-center justify-content-between">
                                                <div class="d-flex align-items-center">
                                                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                                                        <span class="btn text-blue btn-icon btn-sm alpha-blue rounded-round"><i class="icon-notebook"></i></span>
                                                        &nbsp;Lifecycle Change :
                                                    </h6>
                                                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;{!! $LogInfo->log_message !!}</h6>
                                                </div>
                                                <div class="activity-item__title text-muted pr-2 font-weight-semibold">{{ date('M d, Y H:i a',strtotime($LogInfo->created_at)) }}</div>
                                            </div>
                                        </div>
                                    </div>

                                    @endif


                                    @if($value->method =='Note')
                                    @php
                                    $note_id = $value->method_id;
                                    $NoteInfo = App\Modules\Lead\Entities\LeadActivityFeed::getNoteByLead($note_id);
                                    @endphp

                                    <div class="card activity-item border-left-3 border-left-success rounded-left-0 cursor-pointer" data-target="#collapse-text{{ $key }}" data-toggle="collapse" aria-expanded="true">
                                        <div class="p-1">
                                            <div class="d-flex align-items-center justify-content-between">
                                                <div class="d-flex align-items-center">
                                                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                                                        <span class="btn text-success btn-icon btn-sm alpha-success rounded-round"><i class="icon-notebook"></i></span>
                                                        &nbsp;Note :
                                                    </h6>
                                                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;New Note Added</h6>
                                                </div>
                                                <div class="activity-item__title text-muted pr-2 font-weight-semibold">{{ date('M d, Y H:i a',strtotime($NoteInfo->created_at)) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bg-white collapse show" id="collapse-text{{ $key }}" style="">
                                            <div class="p-3 activity-content">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="font-weight-semibold">Left Note By:
                                                        <span class="text-primary pt-1 d-block"><img src="{{ asset('admin/default.png') }}" class="rounded-circle" width="36" height="36" alt="">
                                                         {{ $NoteInfo->createUser->first_name }}
                                                     </span>
                                                 </div>
                                                 <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_note" link="{{route('note.delete',['id' => $NoteInfo->id])}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>
                                             </div>

                                             <p class="activity-content__desc mt-2 mb-0">
                                                {!! $NoteInfo->note !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @endif


                                @if($value->method  =='Task')
                                @php
                                $task_id = $value->method_id;
                                $TaskInfo = App\Modules\Lead\Entities\LeadActivityFeed::getTaskByLead($task_id);

                                $prio_color = 'bg-success';

                                if($TaskInfo->ticket_status == 'Backlog'){
                                    $prio_color = 'bg-danger';
                                }
                                if($TaskInfo->ticket_status == 'ToDo'){
                                    $prio_color = 'bg-info';
                                }
                                if($TaskInfo->ticket_status == 'InProgress'){
                                    $prio_color = 'bg-warning';
                                }
                                if($TaskInfo->ticket_status == 'Completed'){
                                    $prio_color = 'bg-success';
                                }

                                @endphp
                                <div class="card activity-item border-left-3 border-left-danger rounded-left-0 cursor-pointer" data-target="#collapse-text3" data-toggle="collapse">
                                    <div class="p-1">
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                                                    <span class="btn text-danger btn-icon btn-sm alpha-danger rounded-round"><i class="icon-task"></i></span>
                                                    &nbsp;Task :
                                                </h6>
                                                <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;{{ $TaskInfo->ticket_name }}</h6>
                                                <span class="badge {{ $prio_color }} badge-pill font-size-md ml-2">{{ $TaskInfo->ticket_status }}</span>
                                            </div>
                                            <div class="activity-item__title text-muted pr-2 font-weight-semibold">{{ date('M d, Y H:i a',strtotime($TaskInfo->created_at)) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="collapse bg-white show" id="collapse-text3">
                                        <div class="p-3 activity-content">
                                            <div class="d-flex align-items-center justify-content-between">
                                                <div class="">
                                                    <div class="font-weight-semibold">Assigned Department:
                                                        <span class="text-primary d-block">{{ ucfirst($TaskInfo->assigned_dept) }}</span>
                                                    </div>
                                                </div>
                                                <div class="ml-5">
                                                    <div class="font-weight-semibold">Assigned To:
                                                        <span class="text-primary d-block">{{ optional($TaskInfo->getEmployerUser)->first_name }}</span>
                                                    </div>
                                                </div>
                                                <div class="ml-5">
                                                    <div class="font-weight-semibold">Task Deadline:
                                                        <span class="text-danger d-block">{{ $TaskInfo->deadline }}</span>
                                                    </div>
                                                </div>
                                                <div class="ml-5">
                                                    <div class="font-weight-semibold">Change Status:
                                                        <span class="text-primary d-block"><div class="dropdown">
                                                            <a href="#" class="text-default dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false"><i class="icon-more2"></i></a>
                                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 17px, 0px);">
                                                                <a href="{{route('supportticket.changeLeadStatus',['status'=>'Backlog','ticket_id'=>$TaskInfo->id])}}" class="dropdown-item"><i class="icon-rotate-ccw text-danger"></i> Backlog</a>
                                                                <a href="{{route('supportticket.changeLeadStatus',['status'=>'ToDo','ticket_id'=>$TaskInfo->id])}}" class="dropdown-item"><i class="icon-clipboard3 text-info"></i> ToDo</a>
                                                                <a href="{{route('supportticket.changeLeadStatus',['status'=>'InProgress','ticket_id'=>$TaskInfo->id])}}" class="dropdown-item"><i class="icon-spinner6 spinner text-warning"></i> InProgress</a>
                                                                <a href="{{route('supportticket.changeLeadStatus',['status'=>'Completed','ticket_id'=>$TaskInfo->id])}}" class="dropdown-item"><i class="icon-checkmark4 text-success"></i> Completed</a>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>

                                             <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_meeting" link="{{route('supportticket.delete',['id' => $TaskInfo->id])}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>
                                           

                                        </div>


                                        <p class="activity-content__desc mt-2 mb-0">
                                            {{ $TaskInfo->message }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                @endif


                @if($value->method =='Meeting')

                @php
                $meeting_id = $value->method_id;
                $MeetingInfo = App\Modules\Lead\Entities\LeadActivityFeed::getMeetingByLead($meeting_id);

                $attendee = json_decode($MeetingInfo->attendee);
                if(isset($attendee)){
                    $total = sizeof($attendee);
                }else{
                    $total = 0;
                }
                @endphp

                <div class="card activity-item border-left-3 border-left-purple rounded-left-0 cursor-pointer" data-target="#collapse-text2-{{ $key }}" data-toggle="collapse">
        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-purple btn-icon btn-sm alpha-purple rounded-round"><i class="icon-calendar2"></i></span>
                        &nbsp;Meeting :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;{{ $MeetingInfo->meeting_about }}</h6>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold"> {{ date('M d, Y',strtotime($MeetingInfo->date)) }} at {{ date('H:i a',strtotime($MeetingInfo->time)) }}
                </div>
            </div>
        </div>
        <div class="collapse bg-white show" id="collapse-text2-{{ $key }}">
            <div class="p-3 activity-content">


                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex">
                        <div class="font-weight-semibold">Created By:
                            <span class="text-primary d-block">{{ $MeetingInfo->createUser->first_name .' '. $MeetingInfo->createUser->last_name}}</span>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Duration Time:
                                <span class="text-primary d-block">{{ ($MeetingInfo->duration) ? $MeetingInfo->duration : '-' }}</span>
                            </div>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Attendees:
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item dropdown-toggle d-flex align-items-center" data-toggle="dropdown">
                                        <span class="text-primary d-block">
                                            <img src="{{ asset('admin/default.png') }}" class="rounded-circle" alt="" width="28" height="28">
                                            <div class="btn bg-success-400 rounded-round btn-icon btn-sm circle-28">
                                                <span>{{ $total }}</span>
                                            </div>
                                        </span>
                                    </a>

                                    @if($total >=1)
                                    <div class="dropdown-menu dropdown-menu-right bd-card p-0" x-placement="bottom-end">
                                        <ul class="media-list media-list-linked">

                                            @for($i=0; $i<$total; $i++)

                                            @php
                                             $userId = $attendee[$i];
                                             $AttendeeInfo = App\Modules\Lead\Entities\Meeting::getUserByAttendee($userId);

                                             $name = $AttendeeInfo->first_name.' '. $AttendeeInfo->last_name;
                                            @endphp
                                            <li>
                                                <a href="#" class="media p-1 align-items-center">
                                                    <div class="mr-1">
                                                        <img src="{{ asset('admin/default.png') }}" class="rounded-circle" alt="" width="28" height="28">
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="msb-0 fosnt-size-sm">{{ $name }}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            @endfor

                                        </ul>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    
                     <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_meeting" link="{{route('meeting.delete',$MeetingInfo->id)}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>

                </div>


                        <p class="activity-content__desc mt-2 mb-0">
                           {{ $MeetingInfo->description }}
                        </p>
                    </div>
                </div>
            </div>

        @endif


         @if($value->method =='Call')

                @php
                $call_id = $value->method_id;
                $callInfo = App\Modules\Lead\Entities\LeadActivityFeed::getCallByLead($call_id);

                @endphp

    <div class="card activity-item border-left-3 border-left-warning rounded-left-0 cursor-pointer" data-target="#collapse-text2-{{ $key }}" data-toggle="collapse">
        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-warning btn-icon btn-sm alpha-warning rounded-round"><i class="icon-calendar2"></i></span>
                        &nbsp;Call Log :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;Call With {{ $callInfo->contact_name }}</h6>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold"> {{ date('M d, Y',strtotime($callInfo->date)) }} at {{ date('H:i a',strtotime($callInfo->time)) }}
                </div>
            </div>
        </div>
        <div class="collapse bg-white show" id="collapse-text2-{{ $key }}">
            <div class="p-3 activity-content">


                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex">
                        <div class="font-weight-semibold">Created By:
                            <span class="text-primary d-block">{{ $callInfo->createUser->first_name .' '. $callInfo->createUser->last_name}}</span>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Client Name:
                                <span class="text-primary d-block">{{ $callInfo->contact_name }}</span>
                            </div>
                        </div>

                        <div class="ml-5">
                            <div class="font-weight-semibold">Designation:
                                <span class="text-primary d-block">{{ $callInfo->designation }}</span>
                            </div>
                        </div>

                        <div class="ml-5">
                            <div class="font-weight-semibold">Date:
                                <span class="text-primary d-block">{{ date('Y-m-d',strtotime($callInfo->date)) }}</span>
                            </div>
                        </div>

                        <div class="ml-5">
                            <div class="font-weight-semibold">Duration Time:
                                <span class="text-primary d-block">{{ ($callInfo->duration) ? $callInfo->duration : '-' }}</span>
                            </div>
                        </div>
                    </div>
                    
                </div>


                        <p class="activity-content__desc mt-2 mb-0">
                           {{ $callInfo->description }}
                        </p>
                    </div>
                </div>
            </div>

        @endif




        @endforeach
        @endif
    </div>


</div>

<div class="tab-pane fade" id="bottom-justified-tab2">
    <div style="float: right;margin-top: -5px;" class="card mb-4">
       <a data-toggle="modal" data-target="#fullHeightModalHistoryRightNote" class="btn btn-slate btn-labeled btn-labeled-left btn-sm bg-slate" title="Create Note"><b><i class="text-slate-400 icon-pencil"></i></b>Create Note</a>

   </div>
   <span class="mb-2 font-weight-bold text-slate">Take notes about this record to keep track of important info</span>

   <div class="activity mt-5">

     @if($note_detail->total() != 0)
     @foreach($note_detail as $key => $value)

     @php
        $note_id = $value->id;
        $note_comment =  App\Modules\Lead\Entities\LeadComment::getNoteById($note_id);   
     @endphp


     <div class="card activity-item border-left-3 border-left-success rounded-left-0 cursor-pointer" data-target="#collapse-text{{ $key }} show" data-toggle="collapse" aria-expanded="true">
        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-success btn-icon btn-sm alpha-success rounded-round"><i class="icon-notebook"></i></span>
                        &nbsp;Note :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;New Note Added</h6>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold">{{ date('M d, Y H:i a',strtotime($value->created_at)) }}
                </div>
            </div>
        </div>
        <div class="bg-white collapse show" id="collapse-text{{ $key }}" style="">
            <div class="p-3 activity-content">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="font-weight-semibold">Left Note By:
                        <span class="text-primary pt-1 d-block"><img src="{{ asset('admin/default.png') }}" class="rounded-circle" width="36" height="36" alt="">
                         {{ $value->createUser->first_name }}
                     </span>
                 </div>
                 <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_note" link="{{route('note.delete',['id' => $value->id])}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>
             </div>
              

             <p class="activity-content__desc mt-2 mb-0">
                {!! $value->note !!}
            </p>
        </div>


        <div class="card ml-2 mr-2">
            <div class="card-header bg-light header-elements-inline">
                <span class="text-uppercase font-size-sm font-weight-semibold">Comments ({{ sizeof($note_comment) }})</span>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item rotate-180" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <ul class="media-list media-list-linked my-2 latest_note_comment">
                
                @if(sizeof($note_comment)>0)

                @foreach($note_comment as $key => $comment)
                <li>
                    <a href="#" class="media">
                        <div class="mr-3">
                            <img src="{{ asset('admin/default.png') }}" width="36" height="36" class="rounded-circle" alt="">
                        </div>
                        <div class="media-body">
                            <div class="media-title d-flex">
                                <span class="font-weight-semibold text-teal">{{ $comment->CommentBy->first_name.' '. $comment->CommentBy->last_name }}</span>
                                <span class="font-size-sm text-muted ml-auto">{{ $comment->created_at->diffForHumans() }}</span>
                            </div>
                            <span class="font-italic">{{ $comment->comment }}</span>
                        </div>
                    </a>
                </li>

                @endforeach
                @endif


            </ul>
        </div>


    </div>

    

    <div class="card-footer d-flex align-items-center" style="border-top: 0px !important;">
        <ul class="list-inline list-inline-condensed mb-0">
            <li class="list-inline-item">
                <a href="#" class="list-icons-item dropdown-toggle caret-0 text-teal font-weight-semibold" data-toggle="dropdown" aria-expanded="true"><i class="icon-bubbles4 mr-2"></i>Add Comments</a>

                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 16px, 0px);width: 530px;margin-left: 100px;">
                            
                            <div class="card-body">
                              
                                    <textarea name="comment" class="comment form-control mb-3" rows="3" cols="1" placeholder="Enter your comment..."></textarea>

                                    {{ Form::hidden('note_id',  $value->id,['class'=>'comment_note_id']) }}
                                   
                                    <div class="d-flex align-items-center">
                                        <button type="button" class="add_note_comment btn bg-success btn-labeled btn-labeled-right ml-auto"><b><i class="icon-paperplane"></i></b> Save</button>
                                    </div>
                            </div>
                </div>
            </li>
        </ul>
    </div>

</div>


@endforeach
@endif
</div>

</div>
<div class="tab-pane fade" id="bottom-justified-tab7">
   
   <span class="mb-2 font-weight-bold text-slate">Take notes about this record to keep track of important info</span>

  

    


</div>


<div class="tab-pane fade" id="bottom-justified-tab3">

   <div style="float: right;margin-top: -5px;" class="card mb-4">
       <a data-toggle="modal" data-target="#fullHeightModalHistoryRightTask" class="btn btn-slate btn-labeled btn-labeled-left btn-sm bg-slate" title="Create Task">
        <b>
            <i class="text-slate-400 icon-pencil"></i>
        </b>Create Task
    </a> 
</div>
<span class="mb-2 font-weight-bold text-slate">Create a task for yourself or a teammate. Keep track of all your to-dos for this record.</span>

<div class="activity mt-5"> 
    @if($task_detail->total() != 0)
    @foreach($task_detail as $key => $value)

    @php  
    $prio_color = 'bg-success';

    if($value->ticket_status === 'Backlog'){
        $prio_color = 'bg-danger';
    }
    if($value->ticket_status === 'ToDo'){
        $prio_color = 'bg-info';
    }
    if($value->ticket_status === 'InProgress'){
        $prio_color = 'bg-warning';
    }
    if($value->ticket_status === 'Completed'){
        $prio_color = 'bg-success';
    }
        
    @endphp

    <div class="card activity-item border-left-3 border-left-danger rounded-left-0 cursor-pointer" data-target="#collapse-text3" data-toggle="collapse">
        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-danger btn-icon btn-sm alpha-danger rounded-round"><i class="icon-task"></i></span>
                        &nbsp;Task :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;{{ $value->ticket_name }}</h6>
                    <span class="badge {{ $prio_color }} badge-pill font-size-md ml-2">{{ $value->ticket_status }}</span>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold">{{ date('M d, Y H:i a',strtotime($value->created_at)) }}
                </div>
            </div>
        </div>
        <div class="collapse bg-white show" id="collapse-text3">
            <div class="p-3 activity-content">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="">
                        <div class="font-weight-semibold">Assigned Department:
                            <span class="text-primary d-block">{{ ucfirst($value->assigned_dept) }}</span>
                        </div>
                    </div>
                    <div class="ml-5">
                        <div class="font-weight-semibold">Assigned To:
                            <span class="text-primary d-block">{{ optional($value->getEmployerUser)->first_name }}</span>
                        </div>
                    </div>
                    <div class="ml-5">
                        <div class="font-weight-semibold">Task Deadline:
                            <span class="text-danger d-block">{{ $value->deadline }}</span>
                        </div>
                    </div>
                    <div class="ml-5">
                        <div class="font-weight-semibold">Change Status:
                            <span class="text-primary d-block"><div class="dropdown">
                                <a href="#" class="text-default dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false"><i class="icon-more2"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 17px, 0px);">
                                    <a href="{{route('supportticket.changeLeadStatus',['status'=>'Backlog','ticket_id'=>$value->id])}}" class="dropdown-item"><i class="icon-rotate-ccw text-danger"></i> Backlog</a>
                                    <a href="{{route('supportticket.changeLeadStatus',['status'=>'ToDo','ticket_id'=>$value->id])}}" class="dropdown-item"><i class="icon-clipboard3 text-info"></i> ToDo</a>
                                    <a href="{{route('supportticket.changeLeadStatus',['status'=>'InProgress','ticket_id'=>$value->id])}}" class="dropdown-item"><i class="icon-spinner6 spinner text-warning"></i> InProgress</a>
                                    <a href="{{route('supportticket.changeLeadStatus',['status'=>'Completed','ticket_id'=>$value->id])}}" class="dropdown-item"><i class="icon-checkmark4 text-success"></i> Completed</a>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>

                 <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_meeting" link="{{route('supportticket.delete',['id' => $value->id])}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>
               

            </div>


            <p class="activity-content__desc mt-2 mb-0">
                {{ $value->message }}
            </p>
        </div>
    </div>
</div>
@endforeach
@endif
</div>

</div>

<div class="tab-pane fade" id="bottom-justified-tab4">
    <div style="float: right;margin-top: -5px;" class="card mb-4">
       <a data-toggle="modal" data-target="#fullHeightModalHistoryRightMeeting" class="btn btn-slate btn-labeled btn-labeled-left btn-sm bg-slate" title="Create Meeting">
        <b>
            <i class="text-slate-400 icon-pencil"></i>
        </b>Create Meeting
    </a> 
</div>

<span class="mb-2 font-weight-bold text-slate">Create a Meeting for yourself or Schedule log a meeting activity to keep track of your discussion and notes.</span>

 <div class="activity mt-5">
@if($meeting_detail->total() != 0)
@foreach($meeting_detail as $key => $value)

@php
        $meeting_id = $value->id;
        $meeting_comment =  App\Modules\Lead\Entities\LeadComment::getMeetingCommentById($meeting_id);   
     @endphp

@php 
$attendee = json_decode($value->attendee);

if(isset($attendee)){ 
    $total = sizeof($attendee);
}else{
    $total = 0;
}
@endphp
<div class="card activity-item border-left-3 border-left-purple rounded-left-0 cursor-pointer" data-target="#collapse-text2-{{ $key }}" data-toggle="collapse">
        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-purple btn-icon btn-sm alpha-purple rounded-round"><i class="icon-calendar2"></i></span>
                        &nbsp;Meeting :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;{{ $value->meeting_about }}</h6>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold"> {{ date('M d, Y',strtotime($value->date)) }} at {{ date('H:i a',strtotime($value->time)) }}
                </div>
            </div>
        </div>
        <div class="collapse bg-white show" id="collapse-text2-{{ $key }}">
            <div class="p-3 activity-content">


                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex">
                        <div class="font-weight-semibold">Created By:
                            <span class="text-primary d-block">{{ $value->createUser->first_name .' '. $value->createUser->last_name}}</span>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Duration Time:
                                <span class="text-primary d-block">{{ ($value->duration) ? $value->duration : '-' }}</span>
                            </div>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Attendees:
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item dropdown-toggle d-flex align-items-center" data-toggle="dropdown">
                                        <span class="text-primary d-block">
                                            <img src="{{ asset('admin/default.png') }}" class="rounded-circle" alt="" width="28" height="28">
                                            <div class="btn bg-success-400 rounded-round btn-icon btn-sm circle-28">
                                                <span>{{ $total }}</span>
                                            </div>
                                        </span>
                                    </a>

                                    @if($total >=1)
                                    <div class="dropdown-menu dropdown-menu-right bd-card p-0" x-placement="bottom-end">
                                        <ul class="media-list media-list-linked">

                                            @for($i=0; $i<$total; $i++)

                                            @php
                                             $userId = $attendee[$i];
                                             $AttendeeInfo = App\Modules\Lead\Entities\Meeting::getUserByAttendee($userId);

                                             $name = $AttendeeInfo->first_name.' '. $AttendeeInfo->last_name;
                                            @endphp
                                            <li>
                                                <a href="#" class="media p-1 align-items-center">
                                                    <div class="mr-1">
                                                        <img src="{{ asset('admin/default.png') }}" class="rounded-circle" alt="" width="28" height="28">
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="msb-0 fosnt-size-sm">{{ $name }}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            @endfor

                                        </ul>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    
                     <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_meeting" link="{{route('meeting.delete',$value->id)}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>

                </div>


                        <p class="activity-content__desc mt-2 mb-0">
                           {{ $value->description }}
                        </p>
                    </div>

                    <div class="card ml-2 mr-2">
            <div class="card-header bg-light header-elements-inline">
                <span class="text-uppercase font-size-sm font-weight-semibold">Comments ({{ sizeof($meeting_comment) }})</span>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item rotate-180" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <ul class="media-list media-list-linked my-2">
                
                @if(sizeof($meeting_comment)>0)

                @foreach($meeting_comment as $key => $meet_comment)
                <li>
                    <a href="javascript:void(0)" class="media">
                        <div class="mr-3">
                            <img src="{{ asset('admin/default.png') }}" width="36" height="36" class="rounded-circle" alt="">
                        </div>
                        <div class="media-body">
                            <div class="media-title d-flex">
                                <span class="font-weight-semibold text-teal">{{ $meet_comment->CommentBy->first_name.' '. $meet_comment->CommentBy->last_name }}</span>
                                <span class="font-size-sm text-muted ml-auto">{{ $meet_comment->created_at->diffForHumans() }}</span>
                            </div>
                            <span class="font-italic">{{ $meet_comment->comment }}</span>
                        </div>
                    </a>
                </li>

                @endforeach
                @endif


            </ul>
        </div>


                </div>

                <div class="card-footer d-flex align-items-center" style="border-top: 0px !important;">
                    <ul class="list-inline list-inline-condensed mb-0">
                        <li class="list-inline-item">
                            <a href="#" class="list-icons-item dropdown-toggle caret-0 text-teal font-weight-semibold" data-toggle="dropdown" aria-expanded="true"><i class="icon-bubbles4 mr-2"></i>Add Comments</a>

                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 16px, 0px);width: 530px;margin-left: 100px;">
                                        
                                        <div class="card-body">
                                          
                                                <textarea name="comment" class="meeting_comment form-control mb-3" rows="3" cols="1" placeholder="Enter your comment..."></textarea>

                                                {{ Form::hidden('meeting_id',  $value->id,['class'=>'comment_meeting_id']) }}

                                                <div class="d-flex align-items-center">
                                                    <button type="button" id ="add_meeting_comment" class="add_meeting_comment btn bg-success btn-labeled btn-labeled-right ml-auto"><b><i class="icon-paperplane"></i></b> Save</button>
                                                </div>
                                        </div>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
@endforeach
@endif
</div>
</div>

<div class="tab-pane fade" id="bottom-justified-tab6 ">
    <h1>sdfjsdfk</h1>
</div>


<div class="tab-pane fade" id="bottom-justified-tab5">
    <div style="float: right;margin-top: -5px;" class="card mb-4">
       <a data-toggle="modal" data-target="#fullHeightModalHistoryRightCall" class="btn btn-slate btn-labeled btn-labeled-left btn-sm bg-slate" title="Create Call Log">
        <b>
            <i class="text-slate-400 icon-pencil"></i>
        </b>Create Call Log
    </a> 
</div>

<span class="mb-2 font-weight-bold text-slate">Create a log a call activity to keep track of your discussion and notes</span>

 <div class="activity mt-5">

@if($call_detail->total() != 0)
@foreach($call_detail as $key => $value)

@php
    $call_id = $value->id;
    $call_comment =  App\Modules\Lead\Entities\LeadComment::getCallCommentById($call_id);   
 @endphp

<div class="card activity-item border-left-3 border-left-warning rounded-left-0 cursor-pointer" data-target="#collapse-text2-{{ $key }}" data-toggle="collapse">

        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-warning btn-icon btn-sm alpha-warning rounded-round"><i class="icon-calendar2"></i></span>
                        &nbsp;Call :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;Call With {{ $value->contact_name }}</h6>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold"> {{ date('M d, Y',strtotime($value->date)) }} at {{ date('H:i a',strtotime($value->time)) }}
                </div>
            </div>
        </div>
        <div class="collapse bg-white show" id="collapse-text2-{{ $key }}">
            <div class="p-3 activity-content">


                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex">
                        <div class="font-weight-semibold">Created By:
                            <span class="text-primary d-block">{{ $value->createUser->first_name .' '. $value->createUser->last_name}}</span>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Client Name:
                                <span class="text-primary d-block">{{ $value->contact_name }}</span>
                            </div>
                        </div>

                        <div class="ml-5">
                            <div class="font-weight-semibold">Designation:
                                <span class="text-primary d-block">{{ $value->designation }}</span>
                            </div>
                        </div>

                        <div class="ml-5">
                            <div class="font-weight-semibold">Date:
                                <span class="text-primary d-block">{{ date('Y-m-d',strtotime($value->date)) }}</span>
                            </div>
                        </div>

                        <div class="ml-5">
                            <div class="font-weight-semibold">Duration Time:
                                <span class="text-primary d-block">{{ ($value->duration) ? $value->duration : '-' }}</span>
                            </div>
                        </div>
                    </div>
                    
                </div>

                        <p class="activity-content__desc mt-2 mb-0">
                          {{ $value->description }}
                        </p>
                    </div>


                    <div class="card ml-2 mr-2">
            <div class="card-header bg-light header-elements-inline">
                <span class="text-uppercase font-size-sm font-weight-semibold">Comments ({{ sizeof($call_comment) }})</span>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item rotate-180" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <ul class="media-list media-list-linked my-2">
                
                @if(sizeof($call_comment)>0)

                @foreach($call_comment as $key => $cal_comment)
                <li>
                    <a href="javascript:void(0)" class="media">
                        <div class="mr-3">
                            <img src="{{ asset('admin/default.png') }}" width="36" height="36" class="rounded-circle" alt="">
                        </div>
                        <div class="media-body">
                            <div class="media-title d-flex">
                                <span class="font-weight-semibold text-teal">{{ $cal_comment->CommentBy->first_name.' '. $cal_comment->CommentBy->last_name }}</span>
                                <span class="font-size-sm text-muted ml-auto">{{ $cal_comment->created_at->diffForHumans() }}</span>
                            </div>
                            <span class="font-italic">{{ $cal_comment->comment }}</span>
                        </div>
                    </a>
                </li>

                @endforeach
                @endif

            </ul>
        </div>

                </div>

                <div class="card-footer d-flex align-items-center" style="border-top: 0px !important;">
                    <ul class="list-inline list-inline-condensed mb-0">
                        <li class="list-inline-item">
                            <a href="#" class="list-icons-item dropdown-toggle caret-0 text-teal font-weight-semibold" data-toggle="dropdown" aria-expanded="true"><i class="icon-bubbles4 mr-2"></i>Add Comments</a>

                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 16px, 0px);width: 530px;margin-left: 100px;">
                                        
                                        <div class="card-body">
                                          
                                                <textarea name="comment" class="call_comment form-control mb-3" rows="3" cols="1" placeholder="Enter your comment..."></textarea>

                                                {{ Form::hidden('meeting_id',  $value->id,['class'=>'comment_call_id']) }}

                                                <div class="d-flex align-items-center">
                                                    <button type="button" id ="add_call_comment" class="add_call_comment btn bg-success btn-labeled btn-labeled-right ml-auto"><b><i class="icon-paperplane"></i></b> Save</button>
                                                </div>
                                        </div>
                            </div>
                        </li>
                    </ul>
                </div>



            </div>

        @endforeach
    @endif

</div>
</div>




</div> 
</div>
</div>
</div>
</div>
<!-- /invoices -->

</div>

</div>
<!-- End of Left Section -->



<!-- Right Section -->

<div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-300 border-0 shadow-0 order-1 order-lg-2 sidebar-expand-md">

    <div class="card bg-purple-400">
        <div class="card-header bg-purple header-elements-inline">
            <h6 class="card-title">Contact ({{ $count_assoicateContact }})</h6>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        @if($assoicateContact->total() != 0)
        @foreach($assoicateContact as $key => $value)

        <div class="card mt-1 mb-1 mr-1 ml-1 text-dark">

            <div class="card-body">

                <ul class="media-list">
                    <li class="media">
                        <div class="mr-3">
                            <a href="{{route('contact.profile',['id'=>$value->contact_id])}}" class="btn bg-transparent border-pink text-pink rounded-round border-2 btn-icon">{{substr($value->contact->first_name, 0, 1).''.substr($value->contact->last_name, 0, 1)}}</a>
                        </div>

                        <div class="media-body">
                            <span class="font-weight-bold"><a class="text-purple" href="{{route('contact.profile',['id'=>$value->contact_id])}}">{{ $value->contact->first_name.' '.$value->contact->middle_name.' '.$value->contact->last_name }}</a></span> <br>
                            <span class="font-weight-semibold text-teal">{{ $value->contact->company }}</span>
                            <div class="text-muted"><i class="icon-envelop mr-1"></i>{{ $value->contact->email }}</div>
                        </div>
                    </li>

                </ul>

            </div>
        </div>
        @endforeach
        <div class="card-body text-center">
            <a href="{{route('contact.index')}}" class="text-light">View {{ $count_assoicateContact }} contacts in filtered view</a>
            <button type="button" data-toggle="modal" data-target="#fullHeightModalHistoryRightContact" class="btn btn-slate mt-1">Add Another Contact</button>
        </div>
        @else    
        <div class="card-body text-center">
            View all interactions with this company’s contacts in one place. Add an existing contact from your CRM or create a new one.

            @if($contacts)
            <button type="button" data-toggle="modal" data-target="#fullHeightModalHistoryRightContact" class="btn btn-slate mt-3">Add Another Contact</button>
            @endif
        </div>
        @endif
    </div> 


    <div class="card bg-purple-400">
        <div class="card-header bg-purple header-elements-inline">
            <h6 class="card-title">Deals</h6>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>


        <div class="card-body pb-0" style="">
            <div class="row">

                <div class="media flex-column flex-sm-row mt-0 mb-3">

                    <div class="media-body">
                        <h4 class="text-light font-weight-bold">Curent Deal :: {{ $current_status }}</h4>
                       Use deals to track all your revenue opportunities.<br> 
                       @if(($lead_profile['status'] !== 4))
                       <button type="button" data-toggle="modal" data-target="#fullHeightModalHistoryRightDeal" class="btn btn-slate mt-3" style="margin-left: 5.75em !important;">Create Deal</button>
                       @endif
                   </div>
               </div>

           </div>
       </div>
   </div>


   <div class="card bg-purple-400">
    <div class="card-header bg-purple header-elements-inline">
        <h6 class="card-title">Tasks ({{ $task_count }})</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>

    <div class="card-body pb-0" style="">
        <div class="row">

            <div class="media flex-column flex-sm-row mt-0 mb-3">

                <div class="media-body">
                   Use tasks to track all of your customer's questions and requests for help.<br>
                   <button type="button" data-toggle="modal" data-target="#fullHeightModalHistoryRightTask" class="btn btn-slate mt-3" style="margin-left: 5.75em !important;">Create Tasks</button>


               </div>
           </div>

       </div>
   </div>
</div>

</div>


<!-- End of Right Section -->

 {{ Form::hidden('lead_id',  $lead_profile->id,['id'=>'lead_id']) }}

</div>
<!-- End of SEcondary Section -->



<!--Side Popup For Log -->
<!-- Full Height Modal Right -->
<div class="modal modal_slide right" id="fullHeightModalHistoryRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true" data-direction='right'>

<!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
<div class="modal-dialog modal-full-height modal-right" role="document">

    <div class="modal-content">
        <div class="modal-header bg-success">
            <h6 class="modal-title"><i class="icon-history"></i> Lead Log History</h6>
            <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        <div class="modal-body" style="height: 371px;overflow-y: scroll;">
         <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr class="bg-slate">
                        <th>Time</th>
                        <th>Log Message</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($lead_profile->leadHistory as $key=>$leadHistory_value)

                    <tr>
                        <td>{{$leadHistory_value->created_at->diffForHumans()}}</td>
                        <td>{{$leadHistory_value->log_message}}</td>
                    </tr>

                    @endforeach  

                </tbody>
            </table>
        </div>
    </div>

    <div class="modal-footer justify-content-center mt-2">
        <button type="button" class="btn btn-success" data-dismiss="modal" id="close-btn">Close</button>
    </div>
</div>

</div>
</div>
<!-- Full Height Modal Right -->
<!--End of Side Popup For Log -->

<!--Side Popup For Note -->
<!-- Full Height Modal Right -->
<div class="modal modal_slide right" id="fullHeightModalHistoryRightNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true" data-direction='right'>

<!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
<div class="modal-dialog modal-full-height modal-right" role="document">

    <div class="modal-content">
        <div class="modal-header bg-slate">
            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Note</h6>
            <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        <div class="modal-body" style="height: 371px;overflow-y: scroll;">

            {!! Form::open(['route'=>'note.store','method'=>'POST','id'=>'note_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!} 
            
            <fieldset class="mb-3">

                <div class="form-group">
                    <label>Your Note:</label>
                    {!! Form::textarea('note', $value = null, ['id'=>'note','placeholder'=>'Start Typing to leave a note...','class'=>'form-control simple_textarea_description']) !!}
                </div>
                {{ Form::hidden('lead_id',  $lead_profile->id) }}

            </fieldset>
            <div class="text-right">

              <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

              <button id="spinner-light-4" onclick="submitForm(this);" type="submit" class="btn bg-success-400">Save Note </button>
          </div>


          {!! Form::close() !!}


      </div>

  </div>

</div>
</div>
<!-- Full Height Modal Right -->
<!--End of Side Popup For Note -->


<!--Side Popup For Task -->
<!-- Full Height Modal Right -->
<div class="modal modal_slide right" id="fullHeightModalHistoryRightTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true" data-direction='right'>

<!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
<div class="modal-dialog modal-full-height modal-right" role="document">

    <div class="modal-content">
        <div class="modal-header bg-slate">
            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Task</h6>
            <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        <div class="modal-body" style="height: 371px;overflow-y: scroll;">

            {!! Form::open(['route'=>'supportticket.store','method'=>'POST','id'=>'supportticket_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!} 
            
            <fieldset class="mb-1">

                {{ Form::hidden('task_lead', '1') }}

                {{ Form::hidden('lead_id',  $lead_profile->id) }}

                <div class="form-group">
                    <div class="input-group">
                       <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-pencil"></i></span>
                    </span>
                    {!! Form::text('ticket_name', $value = null, ['id'=>'ticket_name','placeholder'=>'Enter Your Task','class'=>'form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::select('ticket_type',$ticket_type, $value = null, ['placeholder'=>'--Select Task Type--','class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::select('assigned_dept',[ 'marketing'=>'Marketing Dept.'], $value = null, ['placeholder'=>'--Assigned Department--','id'=>'assigned_dept','class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::select('assigned_to',$assigned_user, $value = null, ['placeholder'=>'--Assigned To--','id'=>'assigned_to','class'=>'form-control','required']) !!}
            </div>

            <div class="form-group">
                {!! Form::select('ticket_status',['Backlog'=>'Backlog','ToDo'=>'ToDo','InProgress'=>'InProgress','Completed'=>'Completed'], $value = null, ['placeholder'=>'--Task Status--','id'=>'ticket_status','class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                <div class="input-group">
                   <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-calendar"></i></span>
                </span>
                {!! Form::text('deadline', $value = null, ['id'=>'deadline','placeholder'=>'Task Deadline','class'=>'form-control daterange-single']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::textarea('message', $value = null, ['id'=>'message','placeholder'=>'Enter Message','class'=>'form-control']) !!}
        </div>


    </fieldset>
    <div class="text-right">

      <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

      <button id="spinner-light-4" onclick="submitForm(this);" type="submit" class="btn bg-success-400">Save Task </button>
  </div>


  {!! Form::close() !!}


</div>

</div>

</div>
</div>
<!-- Full Height Modal Right -->
<!--End of Side Popup For Task -->



<!--Side Popup For Meeting -->
<!-- Full Height Modal Right -->
<div class="modal modal_slide right" id="fullHeightModalHistoryRightMeeting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true" data-direction='right'>

<!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
<div class="modal-dialog modal-full-height modal-right" role="document">

    <div class="modal-content">
        <div class="modal-header bg-slate">
            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Schedule</h6>
            <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        <div class="modal-body" style="height: 371px;overflow-y: scroll;">

            {!! Form::open(['route'=>'meeting.store','method'=>'POST','id'=>'meeting_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!} 
            
            <fieldset class="mb-1">

                <div class="form-group">
                    <div class="input-group">
                       <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-pencil"></i></span>
                    </span>
                    {!! Form::text('meeting_about', $value = null, ['id'=>'meeting_about','placeholder'=>'What are you meeting about ?','class'=>'form-control','required']) !!}
                </div>
            </div>


            {{ Form::hidden('lead_id',  $lead_profile->id) }}


            <div class="form-group">
               <label>Select Attendee:</label>
               {!! Form::select('attendee[]',$assigned_user, $value = null, ['id'=>'attendee','class'=>'form-control multiselect','multiple'=>'multiple','data-fouc']) !!}
           </div>

           <div class="form-group row">
            <div class="col-lg-4">
               <div class="input-group">
                   <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-calendar"></i></span>
                </span>
                {!! Form::text('date', $value = null, ['id'=>'date','placeholder'=>'Date','class'=>'form-control daterange-single']) !!}
            </div>
        </div>
        <div class="col-lg-4">
            {!! Form::text('time', $value = null, ['id'=>'start-timepicker','placeholder'=>'Start Time','class'=>'form-control']) !!}
        </div>

        <div class="col-lg-4">
            {!! Form::text('duration', $value = null, ['id'=>'duration','placeholder'=>'Duration Eg. 15 Min or 1 hr','class'=>'form-control']) !!}

        </div>
    </div>

    <div class="form-group">

        {!! Form::textarea('description', $value = null, ['id'=>'description','placeholder'=>'Describe your Meeting...','class'=>'form-control']) !!}
    </div>


</fieldset>
<div class="text-right">

  <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

  <button id="spinner-light-4" onclick="submitForm(this);" type="submit" class="btn bg-success-400">Save Meeting </button>
</div>


{!! Form::close() !!}


</div>

</div>

</div>
</div>
<!-- Full Height Modal Right -->
<!--End of Side Popup For Meeting -->


<!--Side Popup For Call Log -->
<!-- Full Height Modal Right -->
<div class="modal modal_slide right" id="fullHeightModalHistoryRightCall" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true" data-direction='right'>

<!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
<div class="modal-dialog modal-full-height modal-right" role="document">

    <div class="modal-content">
        <div class="modal-header bg-slate">
            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Call Log</h6>
            <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        <div class="modal-body" style="height: 371px;overflow-y: scroll;">

            {!! Form::open(['route'=>'call.store','method'=>'POST','id'=>'meeting_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!} 
            
            <fieldset class="mb-1">

                <div class="form-group">
                    <div class="input-group">
                       <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-users"></i></span>
                    </span>
                    {!! Form::text('contact_name', $value = null, ['id'=>'meeting_about','placeholder'=>'Contact Person','class'=>'form-control','required']) !!}
                </div>
            </div>


            {{ Form::hidden('lead_id',  $lead_profile->id) }}


             <div class="form-group">
                    <div class="input-group">
                       <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-user-tie"></i></span>
                    </span>
                    {!! Form::text('designation', $value = null, ['id'=>'designation','placeholder'=>'Designation','class'=>'form-control','required']) !!}
                </div>
            </div>

           <div class="form-group row">
            <div class="col-lg-4">
               <div class="input-group">
                   <span class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-calendar"></i></span>
                </span>
                {!! Form::text('date', $value = null, ['id'=>'date','placeholder'=>'Date','class'=>'form-control daterange-single']) !!}
            </div>
        </div>
        <div class="col-lg-4">
            {!! Form::text('time', $value = null, ['id'=>'start-timepicker-call','placeholder'=>'Start Time','class'=>'form-control']) !!}
        </div>

        <div class="col-lg-4">
            {!! Form::text('duration', $value = null, ['id'=>'duration','placeholder'=>'Duration Eg. 15 Min or 1 hr','class'=>'form-control']) !!}

        </div>
    </div>

    <div class="form-group">

        {!! Form::textarea('description', $value = null, ['id'=>'description','placeholder'=>'Describe your Meeting...','class'=>'form-control']) !!}
    </div>


</fieldset>
<div class="text-right">

  <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

  <button id="spinner-light-4" onclick="submitForm(this);" type="submit" class="btn bg-success-400">Save Call Log </button>
</div>


{!! Form::close() !!}


</div>

</div>

</div>
</div>
<!-- Full Height Modal Right -->
<!--End of Side Popup For Call Log -->



<!--Side Popup For Deal -->
<!-- Full Height Modal Right -->
<div class="modal modal_slide right" id="fullHeightModalHistoryRightDeal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true" data-direction='right'>

<!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
<div class="modal-dialog modal-full-height modal-right" role="document">

    <div class="modal-content">
        <div class="modal-header bg-slate">
            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Create Deal</h6>
            <button type="button" class="close" data-dismiss="modal">×</button>
        </div>


        <div class="modal-body" style="height: 371px;overflow-y: scroll;">
            {!! Form::open(['route'=>'dailyClient.updateStatus','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}

            <div class="form-group row">
                <label class="col-form-label col-lg-4">Deal Stage:</label>
                <div class="col-lg-8">
                    {!! Form::select('service_required',['Hot Leads'=>'Hot Leads','Cold Leads'=>'Cold Leads','Sales Pipeline'=>'Sales Pipeline' , 'Converted' => 'Converted', 'Rejected' => 'Rejected'], $value = null, ['id'=>'service_required','class'=>'form-control','placeholder'=>'Select Service Required']) !!}
                </div>
            </div>

            <div class="form-group row terminal_reason" style="display:none;">
                <label class="col-form-label col-lg-4">Terminate Reason:</label>
                <div class="col-lg-8">
                    {!! Form::textarea('terminate_reason', $value = null, ['id'=>'terminate_reason','placeholder'=>'Enter Terminate Reason','class'=>'form-control']) !!}
                </div>
            </div>

            <div class="form-group row service_accept" style="display:none;">
                <label class="col-form-label col-lg-4">Cost:</label>
                <div class="col-lg-8">
                    {!! Form::text('cost', $value = $lead_profile->client_approach_final_cost, ['id'=>'cost','placeholder'=>'Enter Cost','class'=>'form-control numeric client_cost']) !!}
                </div>
            </div>

            <div class="form-group row service_accept" style="display:none;">
                <label class="col-form-label col-lg-4">Discount(in %):</label>
                <div class="col-lg-8">
                    {!! Form::text('discount', $value = null, ['id'=>'discount','placeholder'=>'Enter Discount in %','class'=>'form-control numeric']) !!}
                </div>
            </div>

            <div class="form-group row service_accept" style="display:none;">
                <label class="col-form-label col-lg-4">Final Cost:</label>
                <div class="col-lg-8">
                    {!! Form::text('final_cost', $value = null, ['id'=>'final_cost','placeholder'=>'Enter Final Cost','class'=>'form-control' ,'readonly']) !!}
                </div>
            </div>

            <div class="form-group row service_accept" style="display:none;">
                <label class="col-form-label col-lg-4">Additional Note:</label>
                <div class="col-lg-8">
                    {!! Form::textarea('additional_note', $value = null, ['id'=>'additional_note','placeholder'=>'Enter Additional Note','class'=>'form-control']) !!}
                </div>
            </div>

            <div class="form-group row service_accept" style="display:none;">
                <label class="col-form-label col-lg-4">Client Expectation Note:</label>
                <div class="col-lg-8">
                    {!! Form::textarea('client_expectation_note', $value = null, ['id'=>'client_expectation_note','placeholder'=>'Enter Client Expectation Note','class'=>'form-control']) !!}
                </div>
            </div>

            {{ Form::hidden('client_id',  $lead_profile->id) }}
            {{ Form::hidden('deal_lead', '1') }}

            <div class="text-right">

              <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

              <button id="spinner-light-4" onclick="submitForm(this);" type="submit" class="btn bg-success-400">Update Deal </button>
          </div>


          {!! Form::close() !!}


      </div>

  </div>

</div>
</div>
<!-- Full Height Modal Right -->
<!--End of Side Popup For Deal -->


<!--Side Popup For Add Contact -->
<!-- Full Height Modal Right -->
<div class="modal modal_slide right" id="fullHeightModalHistoryRightContact" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true" data-direction='right'>

<!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
<div class="modal-dialog modal-full-height modal-right" role="document">

    <div class="modal-content">
        <div class="modal-header bg-slate">
            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Associate Lead With Following Contact</h6>
            <button type="button" class="close" data-dismiss="modal">×</button>
        </div>


        <div class="modal-body" style="height: 371px;overflow-y: scroll;">
            {!! Form::open(['route'=>'dailyClient.associateContact','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}

            <div class="form-group row">
                <label class="col-form-label col-lg-3">Select Contact:</label>
                <div class="col-lg-9">
                 <select name="contact_id" class="form-control select-search" data-fouc >
                   @foreach ($contacts as $id => $contact)
                   <option value="{{ $id }}">{{ $contact }}</option>
                   @endforeach
               </select>
           </div>
       </div>


       {{ Form::hidden('lead_id',  $lead_profile->id) }}
       {{ Form::hidden('associate_lead', 1) }}

       <div class="text-right">

          <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

          <button id="spinner-light-4" onclick="submitForm(this);" type="submit" class="btn bg-success-400">Save </button>
      </div>


      {!! Form::close() !!}


  </div>

</div>

</div>
</div>
<!-- Full Height Modal Right -->
<!--End of Side Popup For Contact -->


<div id="modal_theme_warning" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
         <div class="modal-body">
            <center>
                <i class="icon-alert text-danger icon-3x"></i>
            </center>
            <br>
            <center>
                <h2>Are You Sure Want To Delete ?</h2>
                <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </center>
        </div>
    </div>
</div>
</div>


<div id="modal_theme_warning_note" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
         <div class="modal-body">
            <center>
                <i class="icon-thumbs-up3 text-success icon-3x"></i>
            </center>
            <br>
            <center>
                <h2>Comment Added Successfully.</h2>
            </center>
        </div>
    </div>
</div>
</div>



<script type="text/javascript">

    $('document').ready(function() {

        $('#start-timepicker').clockTimePicker();
        $('#start-timepicker-call').clockTimePicker();

        $('#modal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        });

        var modal = document.getElementsByClassName('modal-backdrop');
        window.onclick = function(event) {
            if (event.target == modal[0]) {
                $('#close-btn').click();
            }
        }

        $(document).on('keyup', '#client_approach_discount, #client_approach_cost', function () {

            var cost = $('#client_approach_cost').val();
            var discount = $('#client_approach_discount').val();

            var final = cost - ((discount / 100) * cost);

            $('#client_approach_final_cost').val(final);
        });


        $('.delete_meeting').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });

        $('.delete_note').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });

        $(document).on('keyup', '#discount, #cost', function () {

            var cost = $('#cost').val();
            var discount = $('#discount').val();

            var final = cost - ((discount / 100) * cost);

            $('#final_cost').val(final);
        });

        $(document).on('change', '#service_required', function () {
            var service_required = $(this).val();

            if (service_required == 'Rejected') {
                $('.terminal_reason').show();
                $('.service_accept').hide();
            } else {
                $('.terminal_reason').hide();
                $('.service_accept').show();
            }
        });

        $(document).on('click', '.editContact', function () {

            $('.edit_contact').attr('readonly', false); 
            $('.edit_select').attr('disabled', false); 
            $('.multiselect').attr('disabled', false); 
            $('.multiselect').removeClass('disabled'); 
            $('.edit_contact').removeClass('text-grey'); 
            $('.edit_contact').addClass('text-dark'); 
            $('.editContact').hide();
            $('.enabled_editLead').show();

        });

    });

    $(document).ready(function() {

         jQuery('.add_note_comment').on('click',function(){ 

            var comment = $(this).parent().parent().find('.comment').val(); 
            var note_id = $(this).parent().parent().find('.comment_note_id').val();
            var lead_id = $('#lead_id').val(); 

             var token = $("input[name='_token']").val();
             var that = this;
                  jQuery.ajax({
                      url: "<?php echo route('note.store-note-comment') ?>",
                      method: 'POST',
                     
                      data: {comment:comment,note_id:note_id,lead_id:lead_id,_token:token},
                      success: function(data) { 

                            $('#modal_theme_warning_note').modal('show');
                            location.reload();

                        // jQuery(that).parent().parent().parent().parent().parent().parent().parent().find('.latest_note_comment').append(data);
                        //$(this).parent().parent().parent().parent().parent().parent().append(data);


                    //     $('.latest_note_comment').fadeOut(800, function() {
                    //     $('.latest_note_comment').fadeIn().delay(1000);
                    //     $(".latest_note_comment").load(location.href + " .latest_note_comment");

                    //     $('.comment').val(' ');
                    // });
                }
                  });

        });


                $('.summarytab').on('click', function () {
                    $('#bottom-justified-tab6').tab('show');
                    var lead_id = $(this).attr('link');

                    $.ajax({
                        type: 'GET',
                        // dataType: 'HTML',
                        url: '/dailyClient/summary_view/'+lead_id,
                       
                        success: function (data) {
                            console.log(data);
                            $('#bottom-justified-tab7').html(data);
                             // $('#bottom-justified-tab6').tab('show');
                        }
                    });

                });


         jQuery('.add_meeting_comment').on('click',function(){ 

            var comment = $(this).parent().parent().find('.meeting_comment').val(); 
            var meeting_id = $(this).parent().parent().find('.comment_meeting_id').val();
            var lead_id = $('#lead_id').val(); 

             var token = $("input[name='_token']").val();
             var that = this;
                  jQuery.ajax({
                      url: "<?php echo route('meeting.store-meeting-comment') ?>",
                      method: 'POST',
                     
                      data: {comment:comment,meeting_id:meeting_id,lead_id:lead_id,_token:token},
                      success: function(data) { 

                            $('#modal_theme_warning_note').modal('show');
                            location.reload();
                        }
                  });

        });


          jQuery('.add_call_comment').on('click',function(){ 

            var comment = $(this).parent().parent().find('.call_comment').val(); 
            var call_id = $(this).parent().parent().find('.comment_call_id').val();
            var lead_id = $('#lead_id').val(); 

             var token = $("input[name='_token']").val();
             var that = this;
                  jQuery.ajax({
                      url: "<?php echo route('call.store-call-comment') ?>",
                      method: 'POST',
                     
                      data: {comment:comment,call_id:call_id,lead_id:lead_id,_token:token},
                      success: function(data) { 

                            $('#modal_theme_warning_note').modal('show');
                            location.reload();
                        }
                  });

        });

    });

</script>

<style>

    .activity {
        position: relative;
        padding-left: 2rem;
        margin-left: 1rem;
    }
    .activity::before {
        content: "";
        width: 2px;
        background-color: #d6d6d6;
        height: calc(100% - 40px);
        position: absolute;
        left: 0;
        top: 20px;
    }

    .activity-item {
        background-color: #f9f9f9;
        border-top: 0;
        border-bottom: 0;
        border-right: 0;
        position: relative;
        box-shadow: 0 1px 3px 0 #ccc;
    }

    .activity-item::before {
        content: "";
        width: 18px;
        height: 18px;
        border-radius: 50%;
        background-color: #24a9ee;
        position: absolute;
        left: -43px;
        top: 12px;
    }

    .circle-28 {
        width: 28px;
        height: 28px;
    }

    .circle-28 span {
        display: block;
        line-height: 1.4;
    }


    .modal_slide{
        display: flex !important;
        width: 500px !important;
        left: unset;
        right: -500px;
        transition: 0.5s ease-in-out right;
    }
    .modal_slide .modal-full-height { 
        display: flex;
        margin: 0;
        width: 100%;
        height: 100%;
    }
    .modal_slide.show{
        right: 0;
        transition: 0.5s ease-in-out right;
    }
    .modal_slide .modal-dialog .modal-content{
        border-radius: 0;
        border: 0;
    }

</style>
@endsection