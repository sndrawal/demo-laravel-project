@extends('admin::layout')
@section('title')Daily Leads @stop
@section('breadcrum')Daily Leads @stop

@section('script')
    <script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
    <script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>

@stop

@section('content')

@php
    use Illuminate\Support\Facades\Auth;
    $user = Auth::user();
    $user_type = $user->user_type; 
@endphp

<section class="ccrm-newsticker mb-3">
    <div class="row">
        <div class="col-12">
            <div class="best-ticker bn-effect-scroll bn-direction-ltr" id="newsticker">
                <div class="bn-news bg-light">
                    <ul>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-slate">
                                        <i class="icon-tree7 rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Total Approached - <strong>{{ number_format($daily_client->total()) }}</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-danger">
                                        <i class="icon-user-check rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Hot Lead - <strong>{{ number_format($hot_client->total()) }}</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-indigo-700">
                                        <i class="icon-brain rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Cold Lead - <strong>{{ number_format($cold_client->total()) }}</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-warning-800">
                                        <i class="icon-point-up rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Sales Pipeline - <strong>{{ number_format($salespipeline_client->total()) }}</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-success-700">
                                        <i class="icon-thumbs-up3 rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Converted Lead - <strong>{{ number_format($converted_client->total()) }}</strong></h6>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex align-items-center">
                                <div class="text-dark">
                                    <div class="ccrm-icon bg-danger-800">
                                        <i class="icon-thumbs-down3 rounded-round pb-1"></i>
                                    </div>
                                </div>
                                <h6 class="pl-2 m-0 font-weight-semibold">Rejected Lead - <strong>{{ number_format($rejected_client->total()) }}</strong></h6>
                            </div>
                        </li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


    <section class="ccrm-links">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{ route('dailyClient.index') }}">
                <div class="card bd-card card-body bd-card-body bg-gradient-blue pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-tree7 icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Daily Leads</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{ route('hotLead.index') }}">
                <div class="card bd-card card-body bd-card-body bg-gradient-green pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-user-check icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Hot Leads</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{ route('coldLead.index') }}">
                <div class="card bd-card card-body bd-card-body bg-gradient-green-2 pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-brain icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Cold Leads</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{ route('salesPipeline.index')}}">
                <div class="card bd-card card-body bd-card-body bg-gradient-purple pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-point-up icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Sales Pipeline</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{ route('convertedLead.index') }}">
                <div class="card bd-card card-body bd-card-body bg-gradient-brown pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-thumbs-up3 icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Converted</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2">
            <a href="{{ route('rejectedLead.index') }}">
                <div class="card bd-card card-body bd-card-body bg-gradient-red pt-4 pb-4">
                    <div class="text-center bd-card-info">
                        <i class="icon-thumbs-down3 icon-3x rounded-round pb-1"></i>
                        <h6 class="m-0 font-weight-semibold">Rejected</h6>
                    </div>
                </div>
            </a>
        </div>

    </div>
</section>

    
{{-- Start of Advance Filter Shyam --}}
    <div class="card card border-light-600 bg-light">
        <div class="bg-purple-400 card-header header-elements-inline border-bottom-0">
            <h5 class="card-title text-uppercase font-weight-semibold">Advance Filter</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
        {!! Form::open(['route' => 'dailyClient.index', 'method' => 'get']) !!}
        <div class="row">
            <div class="col-md-3">
                <label class="d-block font-weight-semibold">From  - To Date:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                                <span class="input-group-text"><i class="text-blue icon-calendar2"></i>
                                </span>
                            </span>
                            @php
                           $search_from_to = (array_key_exists('search_from_to',$search_value)) ? $search_value['search_from_to'] : '';
                            @endphp
                   {!! Form::text('search_from_to', $value = $search_from_to  , ['id'=>'search_from','placeholder'=>'Search From','class'=>'form-control form-control-lg  daterange-buttons','readonly']) !!}
                </div>
            </div>

            

            <div class="col-md-3">
                <label class="d-block font-weight-semibold">User:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                                <span class="input-group-text"><i class="text-blue icon-users"></i>
                                </span>
                            </span>
                    <select class="form-control" name="created_by">
                            <option value=""> --Select User --</option>
                             @if($user_type == 'super_admin')
                                <option value="1" >Administrator</option> 
                            @endif
                            @foreach($employee as $key => $value)
                            <option value="{{$value->id}}" 
                                @if(array_key_exists('created_by',$search_value) && $search_value['created_by']== $value->id) 
                                selected
                                 
                                @endif>{{ $value->first_name .' '.$value->last_name }}</option>
                            @endforeach
                        </select>
                </div>
            </div>

            <div class="col-md-3">
                <label class="d-block font-weight-semibold">Status:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                                <span class="input-group-text"><i class="text-blue icon-collaboration"></i>
                                </span>
                            </span>
                             @php
                           $status = (array_key_exists('status',$search_value)) ? $search_value['status'] : '';
                            @endphp
                     {!! Form::select('status',[ '6'=>'Daily Leads','1'=>'Hot Leads','2'=>'Cold Leads','3'=>'Sales Pipeline', '4'=> 'Converted Leads', '5'=> 'Rejected Leads'], $value = $status, ['id'=>'status','placeholder'=>'Select Status','class'=>'form-control']) !!}
                </div>
            </div>

            <div class="col-md-3">
                <label class="d-block font-weight-semibold">Visited Date:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                                <span class="input-group-text"><i class="text-blue icon-calendar2"></i>
                                </span>
                            </span>
                            @php
                           $next_visit_date = (array_key_exists('next_visit_date',$search_value)) ? $search_value['next_visit_date'] : '';
                            @endphp
                    {!! Form::text('next_visit_date', $value = $next_visit_date, ['id'=>'next_visit_date','placeholder'=>'Visited Date','class'=>'form-control form-control-lg  daterange-single','readonly']) !!}
                </div>
            </div>

            <div class="col-md-3 mt-2">
                <label class="d-block font-weight-semibold">Next Meeting Date:</label>
                <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="text-blue icon-calendar2"></i>
                                </span>
                            </span>
                            @php
                           $next_meeting_date = (array_key_exists('next_meeting_date',$search_value)) ? $search_value['next_meeting_date'] : '';
                            @endphp
                    {!! Form::text('next_meeting_date', $value = $next_meeting_date, ['id'=>'next_meeting_date','placeholder'=>'Meeting Date','class'=>'form-control form-control-lg  daterange-single','readonly']) !!}
                </div>
            </div>

            <div class="col-md-3 mt-2">
                <label class="d-block font-weight-semibold">Company Name:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                                <span class="input-group-text"><i class="text-blue icon-city"></i>
                                </span>
                            </span>
                            @php
                           $company_name = (array_key_exists('company_name',$search_value)) ? $search_value['company_name'] : '';
                            @endphp
                    {!! Form::text('company_name', $value = $company_name, ['id'=>'company_name','placeholder'=>'Company Name','class'=>'form-control form-control-lg']) !!}
                </div>
            </div>

             <div class="col-md-3 mt-2">
                <label class="d-block font-weight-semibold">Contact Person:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                                <span class="input-group-text"><i class="text-blue icon-user"></i>
                                </span>
                            </span>
                            @php
                           $contact_person = (array_key_exists('contact_person',$search_value)) ? $search_value['contact_person'] : '';
                            @endphp
                    {!! Form::text('contact_person', $value = $contact_person, ['id'=>'contact_person','placeholder'=>'Contact Person','class'=>'form-control form-control-lg']) !!}
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-end mt-2">
            <button class="btn bg-success-600 btn-labeled btn-labeled-left" type="submit">
                <b><i class="icon-pen-plus"></i></b>Search Now
            </button>
            <a href="{{ route('dailyClient.index') }}" data-popup="tooltip" data-placement="top" data-original-title="Refresh Search" class="btn bg-danger ml-2">
                <i class="icon-spinner9"></i>
            </a>
        </div>
        {!! Form::close() !!}
        </div>
</div>

{{-- End of Advance Filter Shyam --}}


    <div class="card">
        <div class="card-header bg-purple-400 header-elements-inline">
            <a href="{{ route('dailyClient.create') }}" class="btn bg-success-600 btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-pen-plus"></i></b> Add Lead
               </a>
                @php    
                 if($user_type == 'super_admin'){

                @endphp

                <a href="{{ route('lead.downloadSheet') }}" class="btn bg-purple-600 btn-labeled btn-labeled-left" style="float: right"><b><i class="icon-download4"></i></b> Download Xls
                </a>
                
            @php } @endphp
        </div>
    </div>

    <div class="card">
        <div class="card-header bg-slate-600 header-elements-inline">
            <h6 class="card-title">Total Number of Leads :: [ <span>{{$dailyClient->total()}}</span> ]</h6>
            <div class="header-elements">

                @php

                $search_from_to = (array_key_exists('search_from_to',$search_value)) ? $search_value['search_from_to'] : ''; 
                $created_by = (array_key_exists('created_by',$search_value)) ? $search_value['created_by'] : '';
                $status = (array_key_exists('status',$search_value)) ? $search_value['status'] : '';
                $next_visit_date = (array_key_exists('next_visit_date',$search_value)) ? $search_value['next_visit_date'] : '';
                $next_meeting_date = (array_key_exists('next_meeting_date',$search_value)) ? $search_value['next_meeting_date'] : '';
                $company_name = (array_key_exists('company_name',$search_value)) ? $search_value['company_name'] : '';
                $contact_person = (array_key_exists('contact_person',$search_value)) ? $search_value['contact_person'] : '';


                if((sizeof($search_value)>0) AND (!array_key_exists('type',$search_value))) {

                    $search_path= 'search_from_to='.$search_from_to.'&created_by='.$created_by.'&status='.$status.'&next_visit_date='.$next_visit_date.'&next_meeting_date='.$next_meeting_date.'&company_name='.$company_name.'&contact_person='.$contact_person;

                }else{
                     $search_path = '';
                }

                @endphp

                 <a href="{{ route('dailyClient.index') }}" class="mr-3 btn btn-success btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-enlarge"></i></b> List</a>

                    <a href="{{ route('dailyClient.index',['type' => 'grid',$search_path]) }}" class="btn btn-success btn-labeled btn-labeled-left" style="float: right"><b><i class="icon-list-numbered"></i></b> Grid</a>

            </div>
        </div>

        <div class="table-responsive">
            <section class="articles">
                @include('lead::dailyClient.partial.list')
            </section>
        </div>
    </div>

        <!-- Warning modal -->
        <div id="modal_theme_active_lead" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-header bg-warning-800">
                        <h6 class="modal-title"><i class="icon-user-check mr-1"></i> Active Lead Info</h6>
                    </div>


                    <div class="modal-body">
                        {!! Form::open(['route'=>'dailyClient.updateStatus','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}





                        <div class="form-group row">
                            <label class="col-form-label col-lg-4">Product/Service Required:</label>
                            <div class="col-lg-8">
                                {!! Form::select('service_required',['Hot Leads'=>'Hot Leads','Cold Leads'=>'Cold Leads','Sales Pipeline'=>'Sales Pipeline' , 'Converted' => 'Converted', 'Rejected' => 'Rejected'], $value = null, ['id'=>'service_required','class'=>'form-control','placeholder'=>'Select Service Required']) !!}
                            </div>
                        </div>

                        <div class="form-group row terminal_reason" style="display:none;">
                            <label class="col-form-label col-lg-4">Terminate Reason:</label>
                            <div class="col-lg-8">
                                {!! Form::textarea('terminate_reason', $value = null, ['id'=>'terminate_reason','placeholder'=>'Enter Terminate Reason','class'=>'form-control']) !!}
                            </div>
                        </div>


                            <div class="form-group row service_accept" style="display:none;">
                                <div class="col-lg-6">
                                    <div class="row">
                                       <label class="col-form-label col-lg-4">Cost:</label>
                                            <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                                                <div class="input-group">
                                                    <span class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-calendar"></i>
                                                        </span>
                                                    </span>
                                                    {!! Form::text('cost', $value = null, ['id'=>'cost','placeholder'=>'Enter Cost','class'=>'form-control numeric client_cost']) !!}

                                                </div>
                                            </div>
                                    </div>
                                </div>

                                 <div class="col-lg-6">
                                    <div class="row">
                                       <label class="col-form-label col-lg-4">Discount(in %):</label>
                                            <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                                                <div class="input-group">
                                                    <span class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-calendar"></i>
                                                        </span>
                                                    </span>
                                                        {!! Form::text('discount', $value = null, ['id'=>'discount','placeholder'=>'Enter Discount in %','class'=>'form-control numeric']) !!}
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row service_accept" style="display:none;">
                                <div class="col-lg-6">
                                    <div class="row">
                                       <label class="col-form-label col-lg-4">Final Cost:</label>
                                            <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                                                <div class="input-group">
                                                    <span class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-calendar"></i>
                                                        </span>
                                                    </span>
                                                     {!! Form::text('final_cost', $value = null, ['id'=>'final_cost','placeholder'=>'Enter Final Cost','class'=>'form-control' ,'readonly']) !!}

                                                </div>
                                            </div>
                                    </div>
                                </div>

                                 <div class="col-lg-6">
                                    <div class="row">
                                       <label class="col-form-label col-lg-4">Products/Services:</label>
                                            <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                                                <div class="input-group">
                                                     <select class="form-control multiselect" name="active_lead_services[]" multiple='multiple' data-fouc>
                                                    @foreach($services as $key => $value)
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                               <div class="form-group row service_accept" style="display:none;">
                                <div class="col-lg-6">
                                    <div class="row">
                                       <label class="col-form-label col-lg-4">Additional Note:</label>
                                            <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                                                <div class="input-group">
                                                    <span class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-calendar"></i>
                                                        </span>
                                                    </span>
                                                     {!! Form::textarea('additional_note', $value = null, ['id'=>'additional_note','placeholder'=>'Enter Additional Note','class'=>'form-control']) !!}

                                                </div>
                                            </div>
                                    </div>
                                </div>

                                 <div class="col-lg-6">
                                    <div class="row">
                                       <label class="col-form-label col-lg-4">Client Expectation Note:</label>
                                            <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                                                <div class="input-group">
                                                    <span class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-calendar"></i>
                                                        </span>
                                                    </span>
                                                    {!! Form::textarea('client_expectation_note', $value = null, ['id'=>'client_expectation_note','placeholder'=>'Enter Client Expectation Note','class'=>'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        {{ Form::hidden('client_id', '', array('class' => 'client_id')) }}

                        <div class="text-center">
                                <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>Update Status</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
        <!-- /warning modal -->

        <!-- Warning modal -->
    <div id="modal_project_delete" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                 <div class="modal-body">
                    <center>
                        <i class="icon-alert text-danger icon-3x"></i>
                    </center>
                    <br>
                    <center>
                        <h2>Are You Sure Want To Delete ?</h2>
                        <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!-- /warning modal -->


        <!-- Warning modal -->
        <div id="modal_theme_info" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-warning">
                        <h6 class="modal-title">View Detail of Daily Client Approach</h6>
                    </div>

                    <div class="modal-body">
                        <div class="table-responsive result_view_detail">

                        </div><!-- table-responsive -->
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn bg-teal-400" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- warning modal -->


        <script type="text/javascript">
            $('document').ready(function () {
                $('.delete_lead').on('click', function () {
                    var link = $(this).attr('link');
                    $('.get_link').attr('href', link);
                });

                $(document).on('click', '.update_project', function () {
                    var client_id = $(this).attr('client_id');
                    var client_cost = $(this).attr('client-cost');
                    $('.client_id').val(client_id);
                    $('.client_cost').val(client_cost);
                });

                $(document).on('keyup', '#discount, #cost', function () {

                    var cost = $('#cost').val();
                    var discount = $('#discount').val();

                    var final = cost - ((discount / 100) * cost);

                    $('#final_cost').val(final);
                });

                $(document).on('keyup', '#final_discount, #payment', function () {

                    var cost = $('#payment').val();
                    var discount = $('#final_discount').val();

                    var final = cost - ((discount / 100) * cost);

                    $('#final_payment').val(final);
                });

                $(document).on('change', '#service_required', function () {
                    var service_required = $(this).val();

                    if (service_required == 'Rejected') {
                        $('.terminal_reason').show();
                        $('.service_accept').hide();
                    } else {
                        $('.terminal_reason').hide();
                        $('.service_accept').show();
                    }
                });

                $('.view_daily_client').on('click', function () {
                    var lead_id = $(this).attr('lead_id');

                    $.ajax({
                        type: 'GET',
                        // dataType: 'HTML',
                        url: 'dailyClient/view/{lead_id}',
                        data: {
                            id: lead_id
                        },

                        success: function (data) {
                            console.log(data);
                            $('.result_view_detail').html(data);
                        }
                    });

                });

                // Ajax pagination
                $(function () {
                    $('body').on('click', '.pagination a', function (e) {
                        e.preventDefault();
                        var url = $(this).attr('href');
                        getArticles(url);
                        window.history.pushState("", "", url);
                    });
                    function getArticles(url) {
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (resp) {
                                $('.articles').html(resp);
                            }
                        });
                    }
                });

            });

        </script>

        <style>

        .modal_slide{
            display: flex !important;
            width: 500px !important;
            left: unset;
            right: -500px;
            transition: 0.5s ease-in-out right;
        }
        .modal_slide .modal-full-height {
            display: flex;
            margin: 0;
            width: 100%;
            height: 100%;
        }
        .modal_slide.show{
            right: 0;
            transition: 0.5s ease-in-out right;
        }
        .modal_slide .modal-dialog .modal-content{
            border-radius: 0;
            border: 0;
        }

</style>

@endsection
