@extends('admin::layout')
@section('title')Cold Lead @stop
@section('breadcrum')Cold Lead @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>

@stop

@section('content')


<div class="card">
    <div class="card-header bg-purple-400 header-elements-inline">
        <h6 class="card-title">Cold Lead</h6>
        <span style="float:right;">
            <a class="btn bg-purple btn-icon" href="javascript:history.go(-1)" data-popup="tooltip" data-placement="bottom" data-original-title="Back To Lead"><i class="icon-rotate-ccw2"></i></a>
        </span>
    </div>

    <div class="card-body">
        
          <div class="row">

                 @if($coldLead->total() != 0)
                @foreach($coldLead as $key => $value)
                <?php 
               
                 if ($value->status == '0') {
                    $status_val = 'Daily Client Approach';
                    $color = 'bg-slate-700';
                    $icon = 'icon-tree7';
                }
                if ($value->status == '1') {
                    $status_val = 'Hot Leads';
                    $color = 'bg-danger';
                    $icon = 'icon-user-check';
                }

                 if ($value->status == '2') {
                    $status_val = 'Cold Leads';
                    $color = 'bg-indigo-700';
                    $icon = 'icon-brain';
                }

                 if ($value->status == '3') {
                    $status_val = 'Sales Pipeline';
                    $color = 'bg-warning-800';
                    $icon = 'icon-point-up';
                }
                if ($value->status == '4') {
                    $status_val = 'Converted Leads';
                    $color = 'bg-success-700';
                    $icon = 'icon-thumbs-up3';
                }
                if ($value->status == '5') {
                    $status_val = 'Rejected Leads';
                    $color = 'bg-danger-800';
                    $icon = 'icon-thumbs-down3';
                }
                ?>
                    <div class="col-lg-4">
                        <div class="card card-body border-top-warning-400">
                            <div class="text-left">
                                <h6 class="m-0 font-weight-semibold">{{ $value->company_name }} </h6>
                                <p class="text-muted m-0">Next Meeting Date : {{ $value->next_meeting_date }}</p>
                                <p class="text-muted m-0">Contact Person : {{ $value->contact_person }}</p>
                                <p class="text-muted m-0">Contact Number : {{ $value->contact_no_1 }}</p>
                                <h5 class="font-weight-semibold text-success">Approach Cost : Rs.{{ $value->client_approach_final_cost }}/-</h5>
                                <h5 class="text-danger">Created By: {{ $value->getUser->first_name.' '. $value->getUser->last_name}}</h5>
                            </div>
                            <hr class="mt-0 mb-1">
                            <div>
                        
                                <a data-toggle="modal" data-target="#modal_theme_info" class="btn bg-purple-400 btn-icon rounded-round view_active_lead" lead_id ="{{ $value->id }}" data-popup="tooltip" data-original-title="View Detail" data-placement="bottom"><i class="icon-eye"></i></a>

                                <a style="margin-bottom: 1px;" class="btn bg-orange-400 btn-icon rounded-round"href="{{route('activity.index',['lead_id'=>$value->id])}}" data-popup="tooltip" data-placement="bottom" data-original-title="Lead Activity"><i class="icon-reading"></i></a>
                                
                            </div>
                           
                            <div class="ribbon-container">
                                <div class="ribbon {{ $color }} "><i class="{{ $icon }}"></i> {{$status_val}}</div>
                            </div>
                        </div>
                    </div>
                     @endforeach
                    @else
                        <span class="btn btn-light">No Daily Client Found.</span> 
                    @endif
                </div>
                <span style="margin: 5px;float: right;">
                    @if($coldLead->total() != 0)
                    {{ $coldLead->links() }}
                    @endif
                </span>
    </div>
</div>


<!-- Warning modal -->
<div id="modal_theme_info" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h6 class="modal-title">View Detail of Active Lead</h6>
            </div>

            <div class="modal-body">
               <div class="table-responsive result_view_detail">  
            
             </div><!-- table-responsive -->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn bg-teal-400" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- warning modal -->

 <script type="text/javascript">
    $('.view_active_lead').on('click',function(){ 
        var lead_id = $(this).attr('lead_id');
    
            $.ajax({
             type: 'GET',
            // dataType: 'HTML',
             url: '{{ url("admin/coldLead/view")}}/'+lead_id,
             data: { id: lead_id },

             success: function (data) { 
                 console.log(data);
              $('.result_view_detail').html(data);
            }
          }); 
      });
  </script>


<script type="text/javascript">
    $('document').ready(function() {
    
        $(document).on('click', '.update_project', function() {
            var client_id = $(this).attr('client_id');

            $('.client_id').val(client_id);
        });

        $(document).on('keyup', '#discount, #cost', function() {

            var cost = $('#cost').val();
            var discount = $('#discount').val();

            var final = cost - ((discount / 100) * cost);

            $('#final_cost').val(final);
        });
                
    });

</script>

@endsection
