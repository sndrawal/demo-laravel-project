@extends('admin::layout')
@section('title')Project Dashboard :: Manage Files :: {{ $folder->folder_name }} @stop
@section('breadcrum')Project Dashboard :: Manage Files :: {{ $folder->folder_name }} @stop

@section('content')

<script src="{{ asset('admin/global/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/uploader_bootstrap.js')}}"></script>


<div class="card">
    <div class="card-body">

        {!! Form::open(['route'=>['ManageFiles.store',$folder_id],'name'=>'form1', 'method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}


        <fieldset class="mb-12">
            <legend class="text-uppercase font-size-sm font-weight-bold"></legend>


            <div class="card">
                <div class="card-header bg-teal d-flex justify-content-between">
                    <h6 class="card-title">Upload Files</h6>
                </div>

                <div class="card-header">
                    <div class="header-elements">

                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="file" name="folder_file[]" class="file-input" multiple="multiple" data-fouc>
                            </div>
                        </div>
                        {{ Form::hidden('lead_id', $lead_id, array('class' => 'lead_id')) }}
                    </div>
                </div>
            </div>
        </fieldset>

        {!! Form::close() !!}
    </div>
</div>




<div class="card">
    <div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">
        <span style="font-size: x-large;"><i class="icon-stack2 icon-1x text-pink-400 border-pink-400 border-3 rounded-round p-2 mb-1 mt-1"></i> List of Files :: {{ $folder->folder_name }}</span>

        <span style="float:right;">
            <a href="{{ route('ManageFolder.index',['lead_id'=>$lead_id])}}" class="btn bg-violet-400 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Back To Project Folder</a>
        </span>

    </div>
</div>


<div class="card">
    <div class="card-header header-elements-inline">
        <div class="table-responsive">
            <table class='table table-striped mb30' id='table1' cellspacing='0' width='100%'>
                <tbody>
                    <tr class="bg-slate" style="font-size:small;">
                        <?php
                        $counter=0;
                        if (count($files) > 0) { 
                            foreach ($files as $key): 
                            ++$counter;
                            
                            
                                
                            if($key->file_type == 'pdf'){
                                $file_icon = asset('admin/pdf.jpg');
                            }else{
                                $file_icon = asset('admin/image.png');
                            }
                    ?>

                        <td width="1%"><a href="{{ asset('uploads') }}/lead_projects/{{$folder->folder_slug }}/{{$key->upload_filename}}" target="_blank"><img src="{{ $file_icon }}" height="30px" width="30px"></a></td>
                        <td><a href="{{ asset('uploads') }}/lead_projects/{{ $folder->folder_slug }}/{{$key->upload_filename}}" target="_blank">{{ $key->upload_filename}}</a></td>

                        <?php  if($counter%2==0)echo '</tr><tr style="font-size:small;">'; ?>

                        <?php
                        endforeach;
                    } else {
                        ?>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <center>No Files Found !!!</center>
                        </td>
                    </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
