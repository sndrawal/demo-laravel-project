@extends('admin::layout')
@section('title') Manage Folder @stop
@section('breadcrum') Manage Folder @stop 

@section('content') 
<div class="card">
    <div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">

        <a data-toggle="modal" data-target="#modal_folder_add" class="btn bg-teal-400 btn-labeled btn-labeled-left" data-popup="tooltip" data-original-title="Add Folder" data-placement="bottom"><b><i class="icon-folder6"></i></b> Add Folder</a>

        <span style="float:right;">
            <a href="{{route('dailyClient.dashboard')}}" class="btn bg-violet-400 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Back To Running Project Lead Dashboard</a>
        </span>
    </div>
</div>


<div class="card">
    <div class="card-header bg-orange-800  header-elements-inline">
        <h5 class="card-title">List of Folders</h5>

    </div>

    <div class="row">


     @if(count($folders) >0 )
     @foreach($folders as $key => $value)

     <div class="col-lg-2">
        <div class="card mt-1 mb-1 ml-1 mr-1">
            <a style="margin-left: -40px;" data-toggle="modal" data-target="#modal_folder_delete" class="close folder-margin delete_folder" link="{{route('ManageFolder.delete',['lead_id'=>$lead_id,'id' => $value->id])}}">x</a>
            <div class="card-body text-center">
                <a class="text-dark" href="{{route('ManageFiles.index',['lead_id'=>$lead_id,'id' => $value->id])}}"><img src="{{asset('admin/folder.ico')}}" style="height:100px"></a>
                <hr>
                <a class="text-dark" href="{{route('ManageFiles.index',['lead_id'=>$lead_id,'id' => $value->id])}}"><b>{{$value->folder_name }}</b></a>
            </div>
        </div>
    </div>

    @endforeach
    @else
    <div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">
       <h3 class="font-weight-bold">No Folder Found !!!</h3>
   </div>
   @endif
</div>

</div>

<!-- Warning modal -->
<div id="modal_folder_add" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h6 class="modal-title">Creating Folder</h6>
            </div>

            <div class="modal-body">
                {!! Form::open(['route'=>'ManageFolder.createFolder','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}

                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Folder Name:</label>
                    <div class="col-lg-9">
                       {!! Form::text('folder_name', $value = null, ['id'=>'folder_name','placeholder'=>'Enter Folder Name','class'=>'form-control']) !!}
                   </div>
               </div>
               {{ Form::hidden('lead_id', request('lead_id')) }}

               <div class="text-center">
                <button type="submit" class="btn bg-teal-400">Create Folder</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>
</div>
<!-- /warning modal -->


<!-- Warning modal -->
<div id="modal_folder_delete" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                  <center>
                    <i class="icon-help text-danger icon-8x"></i>
                </center>
                <br>
                <center>
                    <h2>Are You Sure Want To Delete This Folder?</h2>
                    <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </center>
            </div>
        </div>
    </div>
</div>
<!-- warning modal -->

<script type="text/javascript">
    $('document').ready(function() {
        $('.delete_folder').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });
    });
</script>

<style type="text/css">
    .folder-margin {
        margin-bottom: -24px;
        padding-left: 200px !important;
        color: red !important;
        font-size: x-large;
    }
</style>

@endsection