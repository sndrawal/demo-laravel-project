<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>


<fieldset class="mb-3">
 <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

 <div class="form-group row">
  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Lead Executive:<span class="text-danger">*</span></label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-user-tie"></i></span>
          </span>
          {!! Form::text('lead_executive', $value = null, ['id'=>'lead_executive','placeholder'=>'Enter Lead Executive','class'=>'form-control']) !!}

        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Lead Status:<span class="text-danger">*</span></label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-lan2"></i></span>
          </span>
          {!! Form::text('lead_status', $value = null, ['id'=>'lead_status','placeholder'=>'Enter Lead Status','class'=>'form-control']) !!}
        </div>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Next Follow-Up Date:</label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-calendar"></i></span>
          </span>
           {!! Form::text('next_follow_up_date', $value = null, ['id'=>'next_follow_up_date','placeholder'=>'Enter Next Follow Up Date','class'=>'form-control daterange-single']) !!}
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Additional Message:</label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-bookmark"></i></span>
          </span>
          {!! Form::textarea('additional_message_box', $value = null, ['id'=>'additional_message_box','placeholder'=>'Enter Message','class'=>'form-control']) !!}
        </div>
      </div>
    </div>
  </div>
</div>

{{ Form::hidden('lead_id', $lead_id, array('class' => 'lead_id')) }}



</fieldset>

<div class="text-right">
 <button onclick="submitForm(this);" type="submit" class="btn bg-teal-400">{{ $btnType }} <i class="icon-database-insert"></i></button>
</div>
