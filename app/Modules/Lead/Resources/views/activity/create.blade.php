@extends('admin::layout')
@section('title')Create Lead Activity @stop 
@section('breadcrum')Create Lead Activity @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->
<script src="{{ asset('admin/validation/activity.js')}}"></script>
@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800 header-elements-inline">
        <h5 class="card-title">Create Lead Activity</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">
        {!! Form::open(['route'=>'activity.store','method'=>'POST','class'=>'form-horizontal','id'=>'activity_submit','role'=>'form','files' => true]) !!}
        
            @include('lead::activity.partial.action',['btnType'=>'Save']) 
        
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@stop