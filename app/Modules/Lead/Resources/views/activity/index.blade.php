@extends('admin::layout') 
@section('title')Lead Activity @stop
@section('breadcrum')Lead Activity @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
@stop

@section('content')

<div class="card">
    <div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">
        <a href="{{ route('activity.create',['lead_id'=>$lead_id]) }}" class="btn bg-teal-400 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Add Lead Activity</a>

        <a href="{{ route('dailyClient.index') }}" class="btn bg-violet-400 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Back To Lead</a>
    </div>
</div>


<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">List of Property</h5>

    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="bg-slate">
                    <th>#</th>
                    <th>Lead For</th>
                    <th>Lead Executive</th>
                    <th>Lead Status</th>
                    <th>Next Follow Date</th>
                    <th>Additional Message</th>
                </tr>
            </thead>
            <tbody>
                @if($activity->total() != 0) 
                @foreach($activity as $key => $value)
                <tr>
                    <td>{{$activity->firstItem() +$key}}</td>
                    <td>{{ $value->getLead->company_name }}</td>
                    <td>{{ $value->lead_executive }}</td>
                    <td>{{ $value->lead_status }}</td>
                    <td>{{ $value->next_follow_up_date }}</td>
                    <td>{{ $value->additional_message_box }}</td>
                    
                </tr>
                @endforeach @else
                <tr>
                    <td colspan="6">No Lead Activity Added !!!</td>
                </tr>
                @endif
            </tbody>
        </table>
        
        <span style="margin: 5px;float: right;">
            @if($activity->total() != 0) 
            {{ $activity->links() }}
            @endif
        </span>
        
    </div>
</div>

@endsection