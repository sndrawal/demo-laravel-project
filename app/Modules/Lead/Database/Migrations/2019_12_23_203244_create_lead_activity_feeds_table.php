<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadActivityFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_activity_feeds', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('lead_id')->nullable();
            $table->integer('contact_id')->nullable();
            $table->text('method')->nullable();
            $table->integer('method_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_activity_feeds');
    }
}
