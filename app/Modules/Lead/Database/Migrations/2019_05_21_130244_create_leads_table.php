<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');

            $table->text('company_logo')->nullable();
            $table->text('company_name')->nullable();
            $table->text('client_address')->nullable();
            $table->text('contact_person')->nullable();
            $table->text('contact_no_1')->nullable();
            $table->text('contact_no_2')->nullable();
            $table->text('designation')->nullable();
            $table->text('email')->nullable();
            $table->text('purpose_of_meeting')->nullable();
            $table->text('means_of_communication')->nullable();
            $table->text('services')->nullable();
            $table->double('client_approach_cost',15,2)->nullable();
            $table->float('client_approach_discount')->nullable();
            $table->double('client_approach_final_cost',15,2)->nullable();
            $table->text('source_of_leads')->nullable();
            $table->date('next_meeting_date')->nullable();
            $table->text('brief_description')->nullable();
           
            $table->text('service_required')->nullable();
            $table->text('terminate_reason')->nullable();
            $table->double('cost',15,2)->nullable();
            $table->float('discount')->nullable();
            $table->double('final_cost',15,2)->nullable();
            $table->text('active_lead_services')->nullable();
            $table->text('additional_note')->nullable();
            $table->text('client_expectation_note')->nullable();

            $table->integer('segment')->nullable();
            $table->integer('status')->default(0);
            $table->date('lead_created_date');
            $table->date('next_visit_date')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}

