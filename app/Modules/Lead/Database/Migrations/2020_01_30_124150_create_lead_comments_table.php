<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_comments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('lead_id')->nullable();
            $table->integer('contact_id')->nullable();
            $table->integer('note_id')->nullable();
            $table->integer('meeting_id')->nullable();
            $table->integer('call_id')->nullable();
            $table->text('comment')->nullable();
            $table->integer('comment_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_comments');
    }
}
