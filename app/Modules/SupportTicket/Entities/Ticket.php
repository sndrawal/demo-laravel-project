<?php

namespace App\Modules\SupportTicket\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Employment\Entities\Employment;
use App\Modules\User\Entities\User;
use App\Modules\Dropdown\Entities\Dropdown;
use App\Modules\Lead\Entities\Lead;
use App\Modules\Contact\Entities\Contact;

class ticket extends Model
{
    protected $fillable = [

        'lead_id',
        'contact_id',
    	'ticket_name',
    	'ticket_type',
    	'assigned_dept',
    	'assigned_to',
    	'message',
    	'ticket_status',
        'priority',
    	'deadline',
    	'approved_message',
        'feedback',
        'notes',
    	'created_by',
    	'updated_by'

    ];

    public function lead(){
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function contact(){
        return $this->belongsTo(Contact::class,'contact_id','id');
    }

    public function getTicketType(){
        return $this->belongsTo(Dropdown::class,'ticket_type','id');
    } 

    public function getEmployerUser(){
        return $this->belongsTo(User::class,'assigned_to');
    } 

    public function getUsers(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function getUpdatedUsers(){
        return $this->belongsTo(User::class,'updated_by');
    }

    public function taskHistory(){
        return $this->hasMany(TicketLog::class,'ticket_id','id');
    }

}
