<?php

namespace App\Modules\SupportTicket\Entities;

use Illuminate\Database\Eloquent\Model;

class ticketComment extends Model
{
    protected $fillable = [

    	'name',
        'ticket_id',
        'comments'
        
    ];
}
