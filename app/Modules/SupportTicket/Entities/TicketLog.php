<?php

namespace App\Modules\SupportTicket\Entities;

use Illuminate\Database\Eloquent\Model;

class ticketLog extends Model
{
    protected $fillable = [
 
 	'ticket_id',
 	'log_message'
    ];
}
