<?php 
namespace App\Modules\SupportTicket\Repositories;


use App\Modules\SupportTicket\Entities\ticketComment;

class TicketCommentRepository implements TicketCommentInterface
{
     public function save($data){
        return ticketComment::create($data);
     }
    
    public function findAll($ticketid){
        $result = ticketComment::where('ticket_id', '=', $ticketid)->orderBy('id', 'DESC')->get();
        return $result;
    } 

}