<?php

namespace App\Modules\SupportTicket\Repositories;

interface SupportTicketInterface
{
    public function findAll($user_id,$limit = null, $filter = [], $task_status = null, $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);

    public function countTickets($user_id);

    public function countByTitle($user_id,$field,$value);
    
    public function getList();
    
    public function save($data);

    public function update($id,$data);

    public function delete($id);
    
    public function getLeadTask($field,$value,$limit=null,$filter = []);

    public function countLeadTask($field,$value);
}