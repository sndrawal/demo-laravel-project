<?php
/**
 * Created by PhpStorm.
 * User: bidhee
 * Date: 7/18/19
 * Time: 11:01 AM
 */

namespace App\Modules\SupportTicket\Repositories;


interface TicketHistoryInterface
{
    public function save($data);

}