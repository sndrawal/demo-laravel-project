<?php

namespace App\Modules\SupportTicket\Repositories;

interface TicketCommentInterface
{
   public function save($data);  
    
   public function findAll($taskid);  
    
}