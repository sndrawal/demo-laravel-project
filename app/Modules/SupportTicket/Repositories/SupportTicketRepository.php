<?php 
namespace App\Modules\SupportTicket\Repositories;

use App\Modules\SupportTicket\Entities\Ticket;

class SupportTicketRepository implements SupportTicketInterface
{
    
     public function findAll($user_id,$limit = null, $filter = [], $task_status = '', $sort = ['by' => 'id', 'sort' => 'ASC'], $status = [0, 1])
    {
       
        if($user_id){
            $result = Ticket::when(array_keys($filter, true), function ($query) use ($filter) {
            if (isset($filter['priority'])) {
                $query->where('priority', '=', $filter['priority']);
            }
            if (isset($filter['ticket_status'])) {
                $query->where('ticket_status', '=', $filter['ticket_status']);
            }
            if (isset($filter['user_id'])) {
                $query->where('assigned_to', '=', $filter['user_id']);
            }
        })
            
            ->where('assigned_to', '=',$user_id)
            ->where('ticket_status','=',$task_status)
            ->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        }else{ 
            $result = Ticket::when(array_keys($filter, true), function ($query) use ($filter) {
            if (isset($filter['priority'])) {
                $query->where('priority', '=', $filter['priority']);
            }
            if (isset($filter['ticket_status'])) {
                $query->where('ticket_status', '=', $filter['ticket_status']);
            }
            if (isset($filter['user_id'])) {
                $query->where('assigned_to', '=', $filter['user_id']);
            }
        })
            ->where('ticket_status','=',$task_status)
            ->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        }
        
        return $result;
        
    }
    
    public function find($id){
        return Ticket::find($id);
    }

    public function countTickets($user_id){
        if($user_id){
            $result = Ticket::where('assigned_to', '=', $user_id)->count();
        }else{
            $result = Ticket::count();
        }
        return $result;
    }
    
     public function countByTitle($user_id,$field,$value){
       if($user_id){
            $result = Ticket::where($field, '=', $value)->where('assigned_to', '=', $user_id)->count();
        }else{
            $result = Ticket::where($field, '=', $value)->count();
        }
        return $result;
   } 

   public function getList(){  
       $vendorType = Ticket::pluck('vendor_name', 'id');
      
       return $vendorType;
   }
    
    public function save($data){
        return Ticket::create($data);
    }
    
    public function update($id,$data){
        $ticket = Ticket::find($id);
        return $ticket->update($data);
    }
    
    public function delete($id){
        return Ticket::destroy($id);
    }

    public function getLeadTask($field,$value,$limit=null,$filter = []){
        $result = Ticket::where($field,'=',$value)->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;
    }

    public function countLeadTask($field,$value){
        $result = Ticket::where($field,'=',$value)->count();
        return $result;
    }

    
    
}