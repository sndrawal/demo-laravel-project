<?php
/**
 * Created by PhpStorm.
 * User: bidhee
 * Date: 7/18/19
 * Time: 11:01 AM
 */

namespace App\Modules\SupportTicket\Repositories;


use App\Modules\SupportTicket\Entities\TicketLog;

class TicketHistoryRepository implements TicketHistoryInterface
{
    public function save($data)
    {
        return TicketLog::create($data);
    }

}