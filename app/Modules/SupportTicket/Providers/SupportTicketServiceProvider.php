<?php

namespace App\Modules\SupportTicket\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

use App\Modules\SupportTicket\Repositories\SupportTicketInterface;
use App\Modules\SupportTicket\Repositories\SupportTicketRepository;
use App\Modules\SupportTicket\Repositories\TicketHistoryInterface;
use App\Modules\SupportTicket\Repositories\TicketHistoryRepository;
use App\Modules\SupportTicket\Repositories\TicketCommentInterface;
use App\Modules\SupportTicket\Repositories\TicketCommentRepository;

class SupportTicketServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->SupportTicketRegister();
        $this->TicketHistoryRegister();
        $this->TicketCommentRegister();
    }
    
     public function SupportTicketRegister(){
        $this->app->bind(
            SupportTicketInterface::class,
            SupportTicketRepository::class
        );
    }
    
     public function TicketHistoryRegister(){
        $this->app->bind(
            TicketHistoryInterface::class,
            TicketHistoryRepository::class
        );
    }
     public function TicketCommentRegister(){
        $this->app->bind(
            TicketCommentInterface::class,
            TicketCommentRepository::class
        );
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('supportticket.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'supportticket'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/supportticket');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/supportticket';
        }, \Config::get('view.paths')), [$sourcePath]), 'supportticket');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/supportticket');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'supportticket');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'supportticket');
        }
    }

    /**
     * Register an additional directory of factories.
     * 
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
