@extends('admin::layout')
@section('title') Support Tickets @stop
@section('breadcrum') Support Tickets @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/loaders/blockui.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/extensions/jquery_ui/touch.min.js')}}"></script>

<script src="{{asset('admin/global/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>

<script src="{{asset('admin/project_pmt.js')}}"></script>
<script src="{{asset('admin/pmt_chart.js')}}"></script>
@stop

@section('content')

<script src="{{asset('admin/global/js/plugins/ui/fab.min.js')}}"></script>

<script src="{{asset('admin/global/js/plugins/ui/sticky.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/ui/prism.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/extra_fab.js')}}"></script>

 
<div class="row">
    <div class="col-lg-4">
        <div class="card card-body border-top-primary">
            <div class="text-center">
                {!! Form::model($done_tasks,['method'=>'GET','route'=>['supportticket.index'],'class'=>'form-horizontal','role'=>'form','files'=>true]) !!}
                @include('supportticket::ticket.partial.search-priority')

                {!! Form::close() !!}

            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card card-body border-top-primary">
            <div class="text-center">
                {!! Form::model($done_tasks,['method'=>'GET','route'=>['supportticket.index'],'class'=>'form-horizontal','role'=>'form','files'=>true])
                !!}
                @include('supportticket::ticket.partial.search-status')

                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card card-body border-top-primary">
            <div class="text-center">
                {!! Form::model($users,['method'=>'GET','route'=>['supportticket.index'],'class'=>'form-horizontal','role'=>'form','files'=>true]) !!}
                @include('supportticket::ticket.partial.search')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@if($search_id)
<div class="card border-left-info border-right-info rounded-0">
   
    <div class="card-body text-center">
       <h6 class="card-title">Search For User :: <span class="font-weight-bold text-slate">{{$user_name}}</span></h6>
    </div>
</div>
@endif

<div class="card task_graphic_show">
    <div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap" style="padding: 5px;">
        <a href="{{ route('supportticket.create') }}" class="btn bg-teal-400 btn-labeled btn-labeled-left"><b><i class="icon-clipboard2"></i></b>Create Support Ticket</a>

        <div class="card-body d-md-flex align-items-md-center justify-content-md-between flex-md-wrap" style="padding: 0px 0px 0px 10px;">
            <div class="d-flex align-items-center mb-3 ml-3 mb-md-0">
                {{-- <div id="tickets-status"></div> --}}

                {!! Form::hidden('total_backlog',$total_backlog,['id'=>'total_backlog']) !!}
                {!! Form::hidden('total_todo',$total_todo,['id'=>'total_todo']) !!}
                {!! Form::hidden('total_inprogress',$total_inprogress,['id'=>'total_inprogress']) !!}
                {!! Form::hidden('total_complete',$total_completed,['id'=>'total_complete']) !!}
                
                <div class="ml-3">
                    <h5 class="font-weight-semibold mb-0 text-center">{{$total_tickets}}</h5>
                    <span class="text-muted">Total Ticket/s</span>
                </div>
            </div>

            <div class="d-flex align-items-center mb-3 mb-md-0">
                <a href="#" class="btn bg-transparent border-danger text-danger rounded-round border-2 btn-icon"><i class="icon-warning22"></i></a>
                <div class="ml-3">
                    <h5 class="font-weight-semibold mb-0 text-center">{{$total_high_prio}}</h5>
                    <span class="text-muted">High Priority</span>
                </div>
            </div>

            <div class="d-flex align-items-center mb-3 mb-md-0">
                <a href="#" class="btn bg-transparent border-orange-300 text-orange-300 rounded-round border-2 btn-icon"><i class="icon-warning22"></i></a>
                <div class="ml-3">
                    <h5 class="font-weight-semibold mb-0 text-center">{{$total_medium_prio}}</h5>
                    <span class="text-muted">Medium Priority</span>
                </div>
            </div>

            <div class="d-flex align-items-center mb-3 mb-md-0">
                <a href="#" class="btn bg-transparent border-primary-400 text-primary-400 rounded-round border-2 btn-icon"><i class="icon-warning22"></i></a>
                <div class="ml-3">
                    <h5 class="font-weight-semibold mb-0 text-center">{{$total_low_prio}}</h5>
                    <span class="text-muted">Low Priority</span>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="task-board">

    <div class="status-card">
        <div class="card-header">
            <span class="card-header-text board-task-header">Backlog</span>
        </div>
        <ul class="sortable ui-sortable" id="camp-sortable-list-first" data-status-id="Backlog">
            @if($backlog_tasks->total() != 0)
            @foreach($backlog_tasks as $key => $value)
            @php

            $type_color = 'border-left-success';
            
            if($value->priority == 'High'){
            $prio_color = 'border-danger';
            }
            if($value->priority == 'Medium'){
            $prio_color = 'border-orange-300';
            }
            if($value->priority == 'Low'){
            $prio_color = 'border-primary';
            }
            else{
                 $prio_color = 'border-slate';
            }
            @endphp

            @php
            if($value->getEmployerUser->userEmployer->profile_pic !=''){
            $imagePath = asset($value->getEmployerUser->userEmployer->file_full_path).'/'.$value->getEmployerUser->userEmployer->profile_pic;
            }else{
            $imagePath = asset('admin/default.png');
            }
            @endphp

            <li class="text-row ui-sortable-handle" data-task-id="{{$value->id}}"> <a class="font-size-xs text-teal" href="{{route('supportticket.view',['id' => $value->id])}}">#{{$value->id}} | {{$value->ticket_name}} </a>
                <br>
                <span class="badge badge-light badge-striped badge-striped-left {{$type_color}} mt-1">Ticket</span>
                <span style="float:right" class="font-size-xs">

                    <a href="{{route('supportticket.edit',['id' => $value->id])}}" class="btn btn-outline bg-success border-success text-success btn-icon rounded-round font-size-xs" data-popup="tooltip" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil font-size-xs"></i></a>

                    <a data-toggle="modal" data-target="#modal_task_delete" class="btn btn-outline bg-danger border-danger text-danger btn-icon rounded-round font-size-xs delete_ticket" link="{{route('supportticket.delete',['id' => $value->id])}}" data-popup="tooltip" data-placement="bottom" data-original-title=" Delete"><i class="icon-bin font-size-xs"></i></a>

                </span>
                <br>
                <hr class="mt-2 mb-2 {{$prio_color}} border-1">
                <span class="font-size-xs">{{ date('jS M, Y',strtotime($value->created_at)) }}
                    <div style="float: right; margin-top: -10px;">
                        <a href="#" class="btn rounded-round btn-icon btn-sm">
                            <span data-popup="tooltip" data-placement="bottom" data-original-title="{{$value->getEmployerUser->first_name}}"><img class="rounded-round" style="height:30px; width:30px;" src="{{$imagePath}}" alt=""></span>
                        </a>
                    </div>
                </span>
            </li>

            @endforeach

            @endif
        </ul>
    </div>
    <div class="status-card">
        <div class="card-header">
            <span class="card-header-text board-task-header">To Do</span>
        </div>
        <ul class="sortable ui-sortable" id="camp-sortable-list-second" data-status-id="ToDo">
            @if($todo_tasks->total() != 0)
            @foreach($todo_tasks as $key => $value)
            @php

            $type_color = 'border-left-success';

            if($value->priority == 'High'){
            $prio_color = 'border-danger';
            }
            if($value->priority == 'Medium'){
            $prio_color = 'border-orange-300';
            }
            if($value->priority == 'Low'){
            $prio_color = 'border-primary';
            }else{
                 $prio_color = 'border-slate';
            }
            @endphp

            @php
            if($value->getEmployerUser->userEmployer->profile_pic !=''){
            $imagePath = asset($value->getEmployerUser->userEmployer->file_full_path).'/'.$value->getEmployerUser->userEmployer->profile_pic;
            }else{
            $imagePath = asset('admin/default.png');
            }
            @endphp

            <li class="text-row ui-sortable-handle" data-task-id="{{$value->id}}"> <a class="font-size-xs text-teal" href="{{route('supportticket.view',['id' => $value->id])}}">#{{$value->id}} | {{$value->ticket_name}} </a>
                <br>
                <span class="badge badge-light badge-striped badge-striped-left {{$type_color}} mt-1">Ticket</span>
                <span style="float:right" class="font-size-xs">

                    <a href="{{route('supportticket.edit',['id' => $value->id])}}" class="btn btn-outline bg-success border-success text-success btn-icon rounded-round font-size-xs" data-popup="tooltip" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil font-size-xs"></i></a>

                    <a data-toggle="modal" data-target="#modal_task_delete" class="btn btn-outline bg-danger border-danger text-danger btn-icon rounded-round font-size-xs delete_ticket" link="{{route('supportticket.delete',['id' => $value->id])}}" data-popup="tooltip" data-placement="bottom" data-original-title=" Delete"><i class="icon-bin font-size-xs"></i></a>

                </span>
                <br>
                <hr class="mt-2 mb-2 {{$prio_color}} border-1">
                <span class="font-size-xs">{{ date('jS M, Y',strtotime($value->created_at)) }}
                    <div style="float: right; margin-top: -10px;">
                        <a href="#" class="btn rounded-round btn-icon btn-sm">
                           
                            <span data-popup="tooltip" data-placement="bottom" data-original-title="{{$value->getEmployerUser->first_name}}"><img class="rounded-round" style="height:30px; width:30px;" src="{{$imagePath}}" alt=""></span>
                        </a>
                    </div>
                </span>
            </li>

            @endforeach
            @endif
        </ul>
    </div>

    <div class="status-card">
        <div class="card-header">
            <span class="card-header-text board-task-header">In-Progress</span>
        </div>
        <ul class="sortable ui-sortable" id="camp-sortable-list-third" data-status-id="InProgress">
            @if($inprogress_tasks->total() != 0)
            @foreach($inprogress_tasks as $key => $value)
            @php

            $type_color = 'border-left-success';

            if($value->priority == 'High'){
            $prio_color = 'border-danger';
            }
            if($value->priority == 'Medium'){
            $prio_color = 'border-orange-300';
            }
            if($value->priority == 'Low'){
            $prio_color = 'border-primary';
            }else{
                 $prio_color = 'border-slate';
            }
            @endphp

            @php
            if($value->getEmployerUser->userEmployer->profile_pic !=''){
            $imagePath = asset($value->getEmployerUser->userEmployer->file_full_path).'/'.$value->getEmployerUser->userEmployer->profile_pic;
            }else{
            $imagePath = asset('admin/default.png');
            }
            @endphp

            <li class="text-row ui-sortable-handle" data-task-id="{{$value->id}}">
                <a class="font-size-xs text-teal" href="{{route('supportticket.view',['id' => $value->id])}}">#{{$value->id}} | {{$value->ticket_name}} </a>
                <br>
                <span class="badge badge-light badge-striped badge-striped-left {{$type_color}} mt-1">Ticket</span>
                <span style="float:right" class="font-size-xs">

                    <a href="{{route('supportticket.edit',['id' => $value->id])}}" class="btn btn-outline bg-success border-success text-success btn-icon rounded-round font-size-xs" data-popup="tooltip" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil font-size-xs"></i></a>

                    <a data-toggle="modal" data-target="#modal_task_delete" class="btn btn-outline bg-danger border-danger text-danger btn-icon rounded-round font-size-xs delete_ticket" link="{{route('supportticket.delete',['id' => $value->id])}}" data-popup="tooltip" data-placement="bottom" data-original-title=" Delete"><i class="icon-bin font-size-xs"></i></a>

                </span>
                <br>
                <hr class="mt-2 mb-2 {{$prio_color}} border-1">
                <span class="font-size-xs">{{ date('jS M, Y',strtotime($value->created_at)) }}
                    <div style="float: right; margin-top: -10px;">
                        <a href="#" class="btn rounded-round btn-icon btn-sm">
                            <span data-popup="tooltip" data-placement="bottom" data-original-title="{{$value->getEmployerUser->first_name}}"><img class="rounded-round" style="height:30px; width:30px;" src="{{$imagePath}}" alt=""></span>
                        </a>
                    </div>
                </span>
            </li>

            @endforeach
            @endif
        </ul>
    </div>

    <div class="status-card">
        <div class="card-header">
            <span class="card-header-text board-task-header">Done</span>
        </div>
        <ul class="sortable ui-sortable" id="camp-sortable-list-forth" data-status-id="Completed">
            @if($done_tasks->total() != 0)
            @foreach($done_tasks as $key => $value)
            @php

            $type_color = 'border-left-success';
           
            if($value->priority == 'High'){
            $prio_color = 'border-danger';
            }
            if($value->priority == 'Medium'){
            $prio_color = 'border-orange-300';
            }
            if($value->priority == 'Low'){
            $prio_color = 'border-primary';
            }else{
                 $prio_color = 'border-slate';
            }
            @endphp

            @php
            if($value->getEmployerUser->userEmployer->profile_pic !=''){
            $imagePath = asset($value->getEmployerUser->userEmployer->file_full_path).'/'.$value->getEmployerUser->userEmployer->profile_pic;
            }else{
            $imagePath = asset('admin/default.png');
            }
            @endphp

            <li class="text-row ui-sortable-handle" data-task-id="{{$value->id}}"> <a class="font-size-xs text-teal" href="{{route('supportticket.view',['id' => $value->id])}}">#{{$value->id}} | {{$value->ticket_name}} </a>
                <br>
                <span class="badge badge-light badge-striped badge-striped-left {{$type_color}} mt-1">Ticket</span>

                <span style="float:right" class="font-size-xs">

                    <a href="{{route('supportticket.edit',['id' => $value->id])}}" class="btn btn-outline bg-success border-success text-success btn-icon rounded-round font-size-xs" data-popup="tooltip" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil font-size-xs"></i></a>

                    <a data-toggle="modal" data-target="#modal_task_delete" class="btn btn-outline bg-danger border-danger text-danger btn-icon rounded-round font-size-xs delete_ticket" link="{{route('supportticket.delete',['id' => $value->id])}}" data-popup="tooltip" data-placement="bottom" data-original-title=" Delete"><i class="icon-bin font-size-xs"></i></a>

                </span>
                <br>
                <hr class="mt-2 mb-2 {{$prio_color}} border-1">
                <span class="font-size-xs">{{ date('jS M, Y',strtotime($value->created_at)) }}
                    <div style="float: right; margin-top: -10px;">
                        <a href="#" class="btn rounded-round btn-icon btn-sm">
                           <span data-popup="tooltip" data-placement="bottom" data-original-title="{{$value->getEmployerUser->first_name}}"><img class="rounded-round" style="height:30px; width:30px;" src="{{$imagePath}}" alt=""></span>
                        </a>
                    </div>
                </span>
            </li>

            @endforeach
            @endif
        </ul>
    </div>
    
     

</div>




<!-- Warning modal -->
<div id="modal_task_delete" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h6 class="modal-title">Are you sure to Delete a Support Ticket ?</h6>
            </div>

            <div class="modal-body">
                <a class="btn btn-success get_link" href="">Yes</a> &nbsp; | &nbsp;
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- warning modal -->

<script type="text/javascript">
    $('document').ready(function() {
        $('.delete_ticket').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });
    });

</script>


<style>
    h1 {
        font-weight: normal;
    }

    .task-board {
        background: #26a69a;
        display: inline-block;
        padding: 12px;
        border-radius: 3px;
        width: 100%;
        white-space: nowrap;
        overflow-x: scroll;
        min-height: 300px;
    }

    .status-card {
        width: 24%;
        margin-right: 8px;
        background: #e2e4e6;
        border-radius: 3px;
        display: inline-block;
        vertical-align: top;
        font-size: 0.9em;
    }

    .status-card:last-child {
        margin-right: 0px;
    }

    .card-header {
        width: 100%;
        padding: 10px 10px 0px 10px;
        box-sizing: border-box;
        border-radius: 3px;
        display: block;
        font-weight: bold;
    }

    .card-header-text {
        display: block;
    }

    ul.sortable {
        padding-bottom: 10px;
    }

    ul.sortable li:last-child {
        margin-bottom: 0px;
    }

    ul {
        list-style: none;
        margin: 0;
        padding: 0px;
    }

    .text-row {
        padding: 8px 10px;
        margin: 10px;
        background: #fff;
        box-sizing: border-box;
        border-radius: 3px;
        border-bottom: 1px solid #ccc;
        cursor: pointer;
        font-size: 1.1em;
        white-space: normal;
        line-height: 20px;
    }

    .ui-sortable-placeholder {
        visibility: inherit !important;
        background: transparent;
        border: #666 2px dashed;
    }

    .board-task-header {
        text-align: center;
        font-size: 19px;
        margin-bottom: -16px;
    }

</style>

@endsection
