@extends('admin::layout')
@section('title')Update Support Ticket @stop 
@section('breadcrum')Update Support Ticket @stop
@section('script')
<script src="{{ asset('admin/validation/opulencecamp.js')}}"></script>
@stop
@section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Update Support Ticket</h5>
        <div class="header-elements">
        </div>
    </div>

    <div class="card-body">

         {!! Form::model($ticket,['method'=>'PUT','route'=>['supportticket.update',$ticket->id],'id'=>'supportticket_submit','class'=>'form-horizontal','role'=>'form','files'=>true]) !!}
            @include('supportticket::ticket.partial.action',['btnType'=>'Update'])
        {!! Form::close() !!}
        
    </div>
</div>
<!-- /form inputs -->

@stop