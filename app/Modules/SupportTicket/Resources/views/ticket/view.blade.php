@extends('admin::layout') 
@section('title')OpulenceCamp Project Management Tool @stop
@section('breadcrum')OpulenceCamp Project Management Tool @stop
@section('content')

<script src="{{ asset('admin/global/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/uploader_bootstrap.js')}}"></script>


<div class="d-flex align-items-start flex-column flex-md-row">

    <!-- Left content -->
    <div class="w-100 overflow-auto order-2 order-md-1">

        <!-- Task overview -->
        <div class="card">
            <div class="card-header header-elements-md-inline ">
                <h5 class="card-title">#{{$ticket_detail->id}}: {{$ticket_detail->ticket_name}}</h5>
               <div class="header-elements"> 
			     </div>  
                
            </div>

            <div class="card-body">
                {!! $ticket_detail->message !!}
            </div>
        </div>
        <!-- /task overview -->

        <!-- Comments -->
        <div class="card">
            <div class="card-header bg-purple header-elements-inline">
                <h5 class="card-title">Comments</h5>
                <div class="header-elements">
                    
                </div>
            </div>

            <div class="card-body">
                <div class="mb-4 comment_card">

                    @if(count($comments) > 0) @foreach($comments as $key => $value)

                    <div class="media flex-column flex-md-row">
                        <div class="mr-md-3 mb-2 mb-md-0">
                            <img id="bannerImage" src="{{ asset('admin/default.png') }}" alt="Avatar" class="rounded-circle" width="36" height="36" />
                        </div>

                        <div class="media-body">
                            <div class="media-title">
                                <span class="text-warning font-weight-semibold">{{ $value->name}}</span>
                                <span class="font-size-sm text-muted ml-sm-2 mb-2 mb-sm-0 d-block d-sm-inline-block">{{ $value->created_at->diffForHumans() }}</span>
                            </div>

                            <p>{!! $value->comments !!}</p>

                        </div>
                    </div>

                    @endforeach @else
                    <span class="text-danger">No Comments Added.</span> @endif



                </div>

                <div class="mb-3">

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Comment:</label>
                        <div class="col-lg-9">
                            {!! Form::textarea('comments', null, ['id' => 'comments','placeholder'=>'Enter Comment', 'class' =>'simple_textarea_description form-control','required']) !!}
                        </div>
                    </div>

                    {{ Form::hidden('task_id', $ticket_detail->id, array('class' => 'ticket_id')) }}


                </div>

                <div class="text-right">
                    <button  type="button" class="submit_comment btn bg-blue"><i class="icon-plus22 mr-1"></i> Add comment</button>
                </div>
            </div>
        </div>
        <!-- /comments -->

    </div>
    <!-- /left content -->


    <!-- Right sidebar component -->
    <div class="sidebar1 sidebar-light bg-light sidebar-component sidebar-component-right wmin-350 border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- Task details -->
            <div class="card">
                <div class="card-header bg-light header-elements-inline">
                    <span class="text-uppercase font-size-sm font-weight-semibold">Support Ticket details</span>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a href="{{route('supportticket.index')}}" class="btn bg-orange-700"><i class="icon-rotate-ccw2"></i></a>

                        </div>
                    </div>
                </div>

                @php 
                    
                $type_color = 'text-teal';
                
                if($ticket_detail->priority == 'High'){ 
                    $prio_color = 'btn-danger'; 
                }
                if($ticket_detail->priority == 'Medium'){
                    $prio_color = 'bg-orange-300'; 
                }
                if($ticket_detail->priority == 'Low'){
                    $prio_color = 'btn-primary'; 
                }else{
                 $prio_color = 'border-slate';
                }
                if($ticket_detail->ticket_status == 'Backlog'){
                     $status_color = 'text-teal-800 border-teal-800'; 
                }
                if($ticket_detail->ticket_status == 'ToDo'){ 
                    $status_color = 'text-primary border-primary'; 
                }
                if($ticket_detail->ticket_status == 'InProgress'){ 
                    $status_color = 'text-warning border-warning'; 
                } 
                if($ticket_detail->ticket_status == 'Completed'){ 
                    $status_color = 'text-success border-success'; 
                }else{
                    $status_color  = 'border-slate';
                }
                @endphp

                <table class="table table-borderless table-xs my-2">
                    <tbody>
                    
                        <tr>
                            <td><i class="icon-stack-check mr-2"></i> Ticket Name:</td>
                            <td class="text-right text-danger font-weight-semibold">{{ $ticket_detail->ticket_name}}</td>
                        </tr>
                        <tr>
                            <td><i class="icon-alignment-unalign mr-2"></i> Type:</td>
                            <td class="text-right {{$type_color}} font-weight-semibold">{{ $ticket_detail->getTicketType->dropvalue}}</td>
                        </tr>
                        <tr>
                            <td><i class="icon-circles2 mr-2"></i> Priority:</td>
                            <td class="text-right">
                                <button type="button" class="text-right badge btn {{$prio_color}} rounded-round">{{ $ticket_detail->priority }}</button>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="icon-file-check mr-2"></i> Status:</td>
                            <td class="text-right">
                                <button type="button" class="text-right badge btn bg-transparent {{$status_color}} ml-1">{{ $ticket_detail->ticket_status }}</button>
                            </td>

                        </tr>
                        <tr>
                            <td><i class="icon-alarm-add mr-2"></i> Updated:</td>
                            <td class="text-right text-muted">{{ date('d M, Y',strtotime($ticket_detail->updated_at)) }}</td>
                        </tr>
                        <tr>
                            <td><i class="icon-alarm-check mr-2"></i> Created:</td>
                            <td class="text-right text-muted">{{ date('d M, Y',strtotime($ticket_detail->created_at)) }}</td>
                        </tr>
                    </tbody>
                </table>

                <div class="card-footer d-flex align-items-center">
                    <ul class="list-inline list-inline-condensed mb-0">
                        <li class="list-inline-item">
                            <a data-toggle="modal" data-target="#modal_theme_status" ticket_id="{{ $ticket_detail->id }}" class="btn bg-teal-400 btn-icon rounded-round project_status" data-popup="tooltip" data-placement="bottom" data-original-title="Change Status"><i class="icon-file-check"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a data-toggle="modal" data-target="#modal_theme_priority" ticket_id="{{ $ticket_detail->id }}" class="btn bg-warning-400 btn-icon rounded-round project_priority" data-popup="tooltip" data-placement="bottom" data-original-title="Change Priority"><i class="icon-circles2"></i></a>
                        </li>

                         <li class="list-inline-item">
                            <a data-toggle="modal" data-target="#fullHeightModalHistoryRight" ticket_id="{{ $ticket_detail->id }}" class="btn bg-danger-400 btn-icon rounded-round" data-popup="tooltip" data-placement="bottom" data-original-title="View Task history"><i class="icon-wall"></i></a>
                            
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /task details -->

            <!-- Assigned users -->
            <div class="card">
                <div class="card-header bg-light header-elements-inline">
                    <span class="text-uppercase font-size-sm font-weight-semibold">Assigned users</span>
                    <div class="header-elements">
                        <a data-toggle="modal" data-target="#modal_theme_user" task_id="{{ $ticket_detail->id }}" class="btn bg-teal-400 btn-icon rounded-round project_status" data-popup="tooltip" data-placement="bottom" data-original-title="Re-Assign User"><i class="icon-user-tie"></i></a>
                    </div>
                </div>

                <div class="card-body">
                    <ul class="media-list">

                        <li class="media">

                            <div class="media-body">
                                <div class="font-weight-semibold"><i class="icon-user mr-2"></i>{{$ticket_detail->getEmployerUser->first_name.' '.$ticket_detail->getEmployerUser->last_name}}</div>
                                <span class="font-size-sm text-muted"><i class="icon-phone2 mr-2"></i>{{$ticket_detail->getEmployerUser->phone}}</span>
                            </div>

                        </li>
                    </ul>
                </div>
            </div>
            <!-- /assigned users -->


        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /right sidebar component -->

</div>



<!-- Warning modal -->
<div id="modal_theme_status" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-teal">
                <h6 class="modal-title"><i class="icon-file-check mr-2"></i>Change Status</h6>
            </div>

            <div class="modal-body">
                {!! Form::model($ticket_detail,['method'=>'POST','route'=>['supportticket.updateStatus',$ticket_detail->id],'class'=>'form-horizontal','role'=>'form','files'=>true]) !!}

                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Status:</label>
                    <div class="col-lg-9">
                        {!! Form::select('ticket_status',['Backlog'=>'Backlog','ToDo'=>'ToDo','InProgress'=>'InProgress','Completed'=>'Completed'], $value = null, ['id'=>'status','placeholder'=>'Select Status','class'=>'form-control']) !!}
                    </div>
                </div>

                {{ Form::hidden('ticket_id', '', array('class' => 'ticket_id')) }}

                <div class="text-center">
                    <button type="submit" class="btn bg-teal-400">Update Status</button>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<!-- /warning modal -->

{{-- user assigned model--}}
<!-- Warning modal -->
<div id="modal_theme_user" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-teal">
                <h6 class="modal-title"><i class="icon-file-check mr-2"></i>Assign User</h6>
            </div>

            <div class="modal-body">
                {!! Form::model($ticket_detail,['method'=>'POST','route'=>['supportticket.assigned.user',$ticket_detail->id],'class'=>'form-horizontal','role'=>'form','files'=>true]) !!}

                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Status:</label>
                    <div class="col-lg-9">
                        {!! Form::select('assigned_id',$assigned_user, $value = null, ['id'=>'assigned_id','placeholder'=>'Select Employee','class'=>'form-control']) !!}
                    </div>
                </div>
                <input name="ticket_id" type="hidden" value="{{$ticket_detail->id}}">

                <div class="text-center">
                    <button type="submit" class="btn bg-teal-400">Update User</button>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<!-- /warning modal -->



<!-- Warning modal -->
<div id="modal_theme_priority" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-teal">
                <h6 class="modal-title"><i class="icon-circles2 mr-2"></i>Change Priority</h6>
            </div>

            <div class="modal-body">

                {!! Form::model($ticket_detail,['method'=>'POST','route'=>['supportticket.updatePriority'],'class'=>'form-horizontal','role'=>'form','files'=>true]) !!}

                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Priority:</label>
                    <div class="col-lg-9">
                        {!! Form::select('priority',[ 'Low'=>'Low','Medium'=>'Medium','High'=>'High'], $value =null, ['id'=>'priority','placeholder'=>'Select Priority','class'=>'form-control']) !!}
                    </div>
                </div>

                {{ Form::hidden('ticket_id', '', array('class' => 'ticket_id')) }}

                <div class="text-center">
                    <button type="submit" class="btn bg-teal-400">Update Status</button>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<!-- /warning modal -->



            <!-- Full Height Modal Right -->
                <div class="modal modal_slide right" id="fullHeightModalHistoryRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" data-direction='right'>

                <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
                <div class="modal-dialog modal-full-height modal-right" role="document">

                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h6 class="modal-title"><i class="icon-history"></i> Task History</h6>
                        </div>
                        <div class="modal-body" style="height: 371px;overflow-y: scroll;">
                           <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr class="bg-slate">
                                    <th>Time</th>
                                    <th>Log Message</th>
                                </tr>
                            </thead>
                            <tbody>
                        
                            @foreach($ticket_detail->taskHistory as $key=>$taskHistory_value)
                                
                                <tr>
                                    <td>{{$taskHistory_value->created_at->diffForHumans()}}</td>
                                    <td>{{$taskHistory_value->log_message}}</td>
                                </tr>
                                
                            @endforeach  

                            </tbody>
                        </table>
                    </div>
                        </div>
                        
                        <div class="modal-footer justify-content-center mt-2">
                            <button type="button" class="btn btn-success" data-dismiss="modal" id="close-btn">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- Full Height Modal Right -->


<script type="text/javascript">
    
    $('document').ready(function() {

        $(document).on('click', '.project_status,.project_priority,.project_workprogress', function() {
            var ticket_id = $(this).attr('ticket_id');

            $('.ticket_id').val(ticket_id);
        });

        
        $(document).on('click', '.submit_comment', function() {
            var name = $('#name').val();
            var message = tinyMCE.get('comments').getContent();
            var ticket_id = $('.ticket_id').val();

             this.disabled = true;
            
            $.ajax({
                type: 'POST',
                url: 'storeComment',
                data: {
                    name: name,
                    message: message,
                    ticket_id: ticket_id,
                    _token: '{{csrf_token()}}'
                },

                success: function(data) {
                    // console.log(data);
                    $('.comment_card').fadeOut(800, function() {
                        $('.comment_card').fadeIn().delay(2000);
                        $(".comment_card").load(location.href + " .comment_card");
                        $('#name').val(' ');
                        tinyMCE.activeEditor.setContent('');
                    });
                    
                     $('.submit_comment').fadeOut(2000, function() {
                         $('.submit_comment').fadeIn().delay(2000);
                         $('.submit_comment').attr("disabled", false);
                     });
                    
                }
            });

        });

        
        
        $('#modal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        });

        var modal = document.getElementsByClassName('modal-backdrop');
        window.onclick = function(event) {
            if (event.target == modal[0]) {
                $('#close-btn').click();
            }
        }

    });

</script>

<style>

.modal_slide{
    display: flex !important;
    width: 500px !important;
    left: unset;
    right: -500px;
    transition: 0.5s ease-in-out right;
}
.modal_slide .modal-full-height { 
    display: flex;
    margin: 0;
    width: 100%;
    height: 100%;
}
.modal_slide.show{
    right: 0;
    transition: 0.5s ease-in-out right;
}
.modal_slide .modal-dialog .modal-content{
    border-radius: 0;
    border: 0;
}

</style>


@endsection
