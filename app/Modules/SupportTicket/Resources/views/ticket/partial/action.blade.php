<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>


<fieldset class="mb-3">
 <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

 <div class="form-group row">
  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Ticket Name:<span class="text-danger">*</span></label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-ticket"></i></span>
          </span>
          {!! Form::text('ticket_name', $value = null, ['id'=>'ticket_name','placeholder'=>'Enter Ticket Name','class'=>'form-control']) !!}

        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Ticket Type:<span class="text-danger">*</span></label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-lan2"></i></span>
          </span>
          {!! Form::select('ticket_type',$ticket_type, $value = null, ['placeholder'=>'--Select Ticket Type--','class'=>'form-control']) !!}
        </div>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Assigned Dept.:</label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-archive"></i></span>
          </span>
          {!! Form::select('assigned_dept',[ 'marketing'=>'Marketing Dept.'], $value = null, ['id'=>'assigned_dept','class'=>'form-control']) !!}
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Assigned To:<span class="text-danger">*</span></label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-bookmark"></i></span>
          </span>
          {!! Form::select('assigned_to',$assigned_user, $value = null, ['id'=>'assigned_to','class'=>'form-control']) !!}
        </div>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Ticket Status:</label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-coins"></i></span>
          </span>
          {!! Form::select('ticket_status',['Backlog'=>'Backlog','ToDo'=>'ToDo','InProgress'=>'InProgress','Completed'=>'Completed'], $value = null, ['id'=>'ticket_status','class'=>'form-control']) !!}
        </div>
      </div>
    </div>
  </div> 

  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Deadline:<span class="text-danger">*</span></label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-calendar"></i></span>
          </span>
          {!! Form::text('deadline', $value = null, ['id'=>'deadline','placeholder'=>'Enter Deadline','class'=>'form-control daterange-single']) !!}
        </div>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Message:</label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-coins"></i></span>
          </span>
          {!! Form::textarea('message', $value = null, ['id'=>'message','placeholder'=>'Enter Message','class'=>'form-control']) !!}
        </div>
      </div>
    </div>
  </div> 

  <div class="col-lg-6">
    <div class="row">
      <label class="col-form-label col-lg-3">Priority:</label>
      <div class="col-lg-9 form-group-feedback form-group-feedback-right">
        <div class="input-group">
          <span class="input-group-prepend">
            <span class="input-group-text"><i class="icon-coins"></i></span>
          </span>
          {!! Form::select('priority',[ 'Low'=>'Low','Medium'=>'Medium','High'=>'High'], $value = null, ['id'=>'priority','class'=>'form-control']) !!}
        </div>
      </div>
    </div>
  </div> 
</div>

</fieldset>

<div class="text-right">
 <button onclick="submitForm(this);" type="submit" class="btn bg-teal-400">{{ $btnType }} <i class="icon-database-insert"></i></button>
</div>
