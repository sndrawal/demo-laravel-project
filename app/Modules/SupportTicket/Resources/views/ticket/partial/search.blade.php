<div class="form-group row">
    <div class="col-lg-9">
        {!! Form::select('user_id',$users, $value = null, ['id'=>'user_id','class'=>'form-control','placeholder'=>'Select User']) !!}
    </div>


    <div class="col-lg-3">
        <button type="submit" class="btn bg-teal-400"><i class="icon-search4"></i></button>
    </div>
</div>
