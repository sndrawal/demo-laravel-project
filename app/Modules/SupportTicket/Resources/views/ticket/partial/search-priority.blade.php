<div class="form-group row">
    <div class="col-lg-9">
        {!! Form::select('priority',[ 'Low'=>'Low','Medium'=>'Medium','High'=>'High'], $value = null, ['id'=>'priority','placeholder'=>'Select Priority','class'=>'form-control']) !!}
    </div>
    <div class="col-lg-3">
        <button type="submit" class="btn bg-teal-400"><i class="icon-search4"></i></button>
    </div>
</div>
