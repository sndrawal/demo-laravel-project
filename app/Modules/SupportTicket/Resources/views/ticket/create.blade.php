@extends('admin::layout')
@section('title')Create Support Ticket @stop 
@section('breadcrum')Create Support Ticket @stop
@section('script')
<script src="{{ asset('admin/validation/opulencecamp.js')}}"></script>
@stop

@section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800 header-elements-inline">
        <h5 class="card-title">Create Support Ticket</h5>
        <div class="header-elements">
        </div>
    </div>

    <div class="card-body">

        {!! Form::open(['route'=>'supportticket.store','method'=>'POST','id'=>'supportticket_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!} 
            @include('supportticket::ticket.partial.action',['btnType'=>'Save'])
        {!! Form::close() !!}
        
    </div>
</div>
<!-- /form inputs -->

@stop