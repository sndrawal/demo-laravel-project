<?php

namespace App\Modules\SupportTicket\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Employment\Repositories\EmploymentInterface;
use App\Modules\Notification\Repositories\NotificationInterface;
use App\Modules\SupportTicket\Repositories\SupportTicketInterface;
use App\Modules\SupportTicket\Repositories\TicketHistoryInterface;
use App\Modules\SupportTicket\Repositories\TicketCommentInterface;
use App\Modules\User\Repositories\UserInterface;
use App\Modules\Dropdown\Repositories\DropdownInterface;

use App\Modules\Lead\Repositories\LeadActivityFeedInterface;
use App\Modules\Lead\Repositories\LeadLogInterface;

class SupportTicketController extends Controller
{
    protected $ticket;
    protected $employment;
    protected $notification;
    protected $user;
    protected $dropdown;
    protected $history;
    protected $comment;
    protected $feed;
    protected $log;

    /**
     * @var TaskHistoryInterface
     */
    private $taskHistory;

    /**
     * OpulenceCampController constructor.
     * @param SupportTicketInterface $task
     * @param CampTaskProgressInterface $taskProgress
     * @param EmploymentInterface $employment
     * @param CampTaskCommentInterface $taskComment
     * @param CampTaskAttachmentInterface $taskAttachment
     * @param NotificationInterface $notification
     * @param UserInterface $user
     * @param TaskTrackInterface $track
     * @param MilestoneInterface $milestone
     * @param TaskHistoryInterface $taskHistory
     */
    public function __construct(SupportTicketInterface $ticket,
                                EmploymentInterface $employment,
                                NotificationInterface $notification,
                                UserInterface $user,
                                DropdownInterface $dropdown,
                                TicketHistoryInterface $history,
                                TicketCommentInterface $comment,
                                LeadLogInterface $log,
                                 LeadActivityFeedInterface $feed)
    {
        $this->ticket = $ticket;
        $this->employment = $employment;
        $this->notification = $notification;
        $this->user = $user;
        $this->dropdown = $dropdown;
        $this->history = $history;
        $this->comment = $comment;
        $this->feed = $feed;
        $this->log = $log;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $input = $request->all(); 

        $userInfo = Auth::user();
        $user_type = $userInfo->user_type;

        if (array_key_exists("user_id", $input)) {
            $user_id = $input['user_id'];
            if ($user_id) {
                $user = $this->user->find($user_id);
                $data['user_name'] = $user->first_name . ' ' . $user->last_name;
            } else {
                $data['user_name'] = '';
            }
            $data['search_id'] = $user_id;
        } else {
            $user_id = '';
            $data['search_id'] = '';
            $data['user_name'] = '';
        }

        $employer_user = $this->user->getAllActiveUser();

        $employee_data = array();
        foreach ($employer_user as $key => $user) {

            $employee_data += array(
                $user->id => $user->first_name . ' ' . $user->last_name
            );
        }

        $data['users'] = $employee_data;


        $data['total_tickets'] = $this->ticket->countTickets($user_id);
        $data['total_low_prio'] = $this->ticket->countByTitle($user_id, 'priority', 'Low');
        $data['total_medium_prio'] = $this->ticket->countByTitle($user_id, 'priority', 'Medium');
        $data['total_high_prio'] = $this->ticket->countByTitle($user_id, 'priority', 'High');
        $data['total_backlog'] = $this->ticket->countByTitle($user_id, 'ticket_status', 'Backlog');
        $data['total_todo'] = $this->ticket->countByTitle($user_id, 'ticket_status', 'ToDo');
        $data['total_inprogress'] = $this->ticket->countByTitle($user_id, 'ticket_status', 'InProgress');
        $data['total_completed'] = $this->ticket->countByTitle($user_id, 'ticket_status', 'Completed');

        $data['backlog_tasks'] = $this->ticket->findAll($user_id, $limit = 50, $input, 'Backlog');
        $data['todo_tasks'] = $this->ticket->findAll($user_id, $limit = 50, $input, 'ToDo');
        $data['inprogress_tasks'] = $this->ticket->findAll($user_id, $limit = 50, $input, 'InProgress');
        $data['done_tasks'] = $this->ticket->findAll($user_id, $limit = 50, $input, 'Completed');
        


        return view('supportticket::ticket.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        $employer_user= $this->user->getAllActiveUser();
        
        $employee_data = array();
        foreach ($employer_user as $key => $user) {

            $employee_data += array(
                $user->id => $user->first_name . ' ' . $user->last_name
            );
        }

        $data['assigned_user'] = $employee_data;
        $data['ticket_type'] = $this->dropdown->getFieldBySlug('ticket_type');
        return view('supportticket::ticket.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if(isset($data['lead_id'])){
            $lead_id = $data['lead_id'];
            $contact_id = NULL;
        }else{
            $contact_id = $data['contact_id'];
            $lead_id = NULL;
        }

        $is_from_profile = $data['task_lead'];
 
        $user = Auth::user();
        $from = $user->id;
        $to = $data['assigned_to'];
        $data['created_by'] = $from;

        try {

            $ticket_info=$this->ticket->save($data);
            $ticket_history['ticket_id']=$ticket_info->id;
            $ticket_history['log_message']=$user->first_name." ".$user->last_name." Created ".$ticket_info->ticket_name.'.';
            $this->history->save($ticket_history);

            /* ---------------------------------------------------
                        Notification Start
            ------------------------------------------------------*/


            $message = "Your Support Ticket has been Created.";
            $link = route('supportticket.index');

            $notification_data = array(
                'creator_user_id' => $from,
                'notified_user_id' => $to,
                'message' => $message,
                'link' => $link,
                'is_read' => '0',
            );

            $this->notification->save($notification_data);
            /* ---------------------------------------------------
                        Notification End
            ------------------------------------------------------*/

            if($lead_id || $contact_id){

                 /** Lead Log Start **/

                $lead_history['lead_id'] = $lead_id ;
                $lead_history['contact_id'] = $contact_id ;
                $lead_history['log_message']=$user->first_name." ".$user->last_name." Created ".$data['ticket_name'];
                $this->log->save($lead_history);

                /** Lead Log End **/
                
                /** Activity Feed Set **/

                $feed['lead_id'] = $lead_id;
                $feed['contact_id'] = $contact_id;
                $feed['method'] = 'Task';
                $feed['method_id'] = $ticket_info['id'];

                $this->feed->save($feed);

                /** End of Activity Feed Set **/

            }
        
         alertify()->success('Support Ticket Created Successfully');


        } catch (\Throwable $e) {
            alertify($e->getMessage())->error();
        }

         if($is_from_profile == '1'){

               if(isset($lead_id)){
                    return redirect(route('dailyClient.profile',['id'=>$lead_id]));
                }else{
                     return redirect(route('contact.profile',['id'=>$contact_id]));
                }

        }else{
        return redirect(route('supportticket.index'));
        }

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('supportticket::show');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function view(Request $request)
    {
        $input = $request->all();


        $id = $input['id'];


        $data['ticket_detail'] = $this->ticket->find($id);
        $data['comments'] = $this->comment->findAll($id);

        $employer_user= $this->user->getAllActiveUser();

        $employee_data = array();
        foreach ($employer_user as $key => $user) {

            $employee_data += array(
                $user->id => $user->first_name . ' ' . $user->last_name
            );
        }

        $data['assigned_user'] = $employee_data;

        return view('supportticket::ticket.view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        $input = $request->all();

        $id = $input['id'];

        $employer_user = $this->user->getAllActiveUser();


        $employee_data = array();
        foreach ($employer_user as $key => $user) {

            $employee_data += array(
                $user->id => $user->first_name . ' ' . $user->last_name
            );
        }

        $data['assigned_user'] = $employee_data;
        $data['ticket_type'] = $this->dropdown->getFieldBySlug('ticket_type');
        $data['ticket'] = $this->ticket->find($id);
        return view('supportticket::ticket.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
       $data = $request->all();

        $user = Auth::user();
        $from = $user->id;
        $to = $data['assigned_to'];
        $data['created_by'] = $from;
        try {
            $this->ticket->update($id, $data);

            $ticket_info = $this->ticket->find($id);
            $ticket_history['ticket_id']=$ticket_info->id;
            $ticket_history['log_message']=$user->first_name." ".$user->last_name." Updated ".$ticket_info->ticket_name.'.';
            $this->history->save($ticket_history);
            /* ---------------------------------------------------
                        Notification Start
            ------------------------------------------------------*/

            $message = "Your Support Ticket has been Updated.";
            $link = route('supportticket.index');

            $notification_data = array(
                'creator_user_id' => $from,
                'notified_user_id' => $to,
                'message' => $message,
                'link' => $link,
                'is_read' => '0',
            );

            $this->notification->save($notification_data);
            /* ---------------------------------------------------
                        Notification End
            ------------------------------------------------------*/

            alertify()->success('Task/Issue Updated Successfully');
        } catch (\Throwable $e) {
            alertify($e->getMessage())->error();
        }

        return redirect(route('supportticket.index'));

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $data = $request->all();

        try {

            $task = $this->ticket->find($id);

            $lead_id = $task->lead_id;
            $contact_id = $task->contact_id;

            $userInfo = Auth::user();
            $lead_history['lead_id']= $lead_id = $task->lead_id;
            $lead_history['contact_id']= $contact_id = $task->contact_id;
            $lead_history['log_message'] = $userInfo->first_name." ".$userInfo->last_name." Deleted a Task.";
            $this->log->save($lead_history);

            $this->feed->deleteFeed('Task',$id);


            $this->ticket->delete($id);
            alertify()->success('Support Ticket Deleted Successfully');
        } catch (\Throwable $e) {
            alertify($e->getMessage())->error();
        }

        if(isset($lead_id)){
            return redirect(route('dailyClient.profile',['id'=>$lead_id]));
        }elseif(isset($contact_id)){
            return redirect(route('contact.profile',['id'=>$contact_id]));
        }
        else{
            return redirect(route('supportticket.index'));
        }
    }

    public function updateStatus(Request $request)
    {
        $input = $request->all();
        $ticket_id = $input['ticket_id'];
        $ticket_status = $input['ticket_status'];

        $ticket_info = $this->ticket->find($ticket_id);

        if (isset($ticket_info)) {
            $data = array(
                'ticket_status' => $input['ticket_status']
            );
            $user = Auth::user();

            $task_history['ticket_id']=$ticket_info->id;
            $task_history['log_message']=$user->first_name." ".$user->last_name." has Update Status of ".$ticket_info->task_title." from ".$ticket_info->ticket_status." to ".$ticket_status.'.';

            $this->ticket->update($ticket_id, $data);
            $this->history->save($task_history);

            /* ---------------------------------------------------
                        Notification Start
            ------------------------------------------------------*/

            $user = Auth::user();
            $from = $user->id;

            $to = $ticket_info->assigned_to;
            $message = "Your Support Ticket has been Updated.";
            $link = route('supportticket.view', ['id' => $ticket_id]);

            $notification_data = array(
                'creator_user_id' => $from,
                'notified_user_id' => $to,
                'message' => $message,
                'link' => $link,
                'is_read' => '0',
            );

            $this->notification->save($notification_data);
            /* ---------------------------------------------------
                        Notification End
            ------------------------------------------------------*/


        }
        alertify()->success('Status Updated Successfully');
        return redirect(route('supportticket.view', ['id' => $ticket_id]));

    }

    public function changeStatus(Request $request)
    {
        $input = $request->all();

        $ticket_id = $input['ticket_id'];
        $ticket_status = $input['ticket_status'];

        $ticket_info = $this->ticket->find($ticket_id);   

        if ($ticket_info) {

            $user = Auth::user();
            $ticket_history['ticket_id']=$ticket_info->id;
            $ticket_history['log_message']=$user->first_name." ".$user->last_name." has Change Status of".$ticket_info->ticket_name." from ".$ticket_info->ticket_status." to ".$ticket_status .'.';

            $update_data = array(
                'ticket_status' => $ticket_status
            );

            $this->ticket->update($ticket_id, $update_data);
            $this->history->save($ticket_history);

            /* ---------------------------------------------------
                        Notification Start
            ------------------------------------------------------*/

            $user = Auth::user();
            $from = $user->id;

            $to = $ticket_info->assigned_to;
            $message = "Your Support Ticket Status has been Updated.";
            $link = route('supportticket.view', ['id' => $ticket_id]);


            $notification_data = array(
                'creator_user_id' => $from,
                'notified_user_id' => $to,
                'message' => $message,
                'link' => $link,
                'is_read' => '0',
            );
            $this->notification->save($notification_data);
            /* ---------------------------------------------------
                        Notification End
            ------------------------------------------------------*/

        }

        echo 1;

    }
    
     public function changeLeadStatus(Request $request)
    {
        $input = $request->all();  

        $ticket_id = $input['ticket_id'];
        $ticket_status = $input['status'];

        $ticket_info = $this->ticket->find($ticket_id);   

        if ($ticket_info) {

            $user = Auth::user();
            $ticket_history['ticket_id']=$ticket_id;
            $ticket_history['log_message']=$user->first_name." ".$user->last_name." has Change Status of".$ticket_info->ticket_name." from ".$ticket_info->ticket_status." to ".$ticket_status .'.';

            $update_data = array(
                'ticket_status' => $ticket_status
            );

            $this->ticket->update($ticket_id, $update_data);
            $this->history->save($ticket_history);

           
           $lead_id = $ticket_info->lead_id; 
           $contact_id = $ticket_info->contact_id; 

           if($lead_id || $contact_id){
                /** Update Activity Feed Set **/


                $feed['method'] = 'Task';
                $feed['method_id'] = $ticket_id;
                $feed['created_at'] = date('Y-m-d H:i:s');

                $this->feed->updateTask('Task',$ticket_id,$feed);

                /** End of Update Activity Feed Set **/
            }

            /* ---------------------------------------------------
                        Notification Start
            ------------------------------------------------------*/

            $user = Auth::user();
            $from = $user->id;

            $to = $ticket_info->assigned_to;
            $message = "Your Support Ticket Status has been Updated.";
            $link = route('supportticket.view', ['id' => $ticket_id]);


            $notification_data = array(
                'creator_user_id' => $from,
                'notified_user_id' => $to,
                'message' => $message,
                'link' => $link,
                'is_read' => '0',
            );
            $this->notification->save($notification_data);
            /* ---------------------------------------------------
                        Notification End
            ------------------------------------------------------*/

        }

        alertify()->success('Task Status Updated Successfully');

        if(isset($lead_id)){
            return redirect(route('dailyClient.profile',['id'=>$lead_id]));
        }else{
             return redirect(route('contact.profile',['id'=>$contact_id]));
        }

    }


public function updatePriority(Request $request)
    {
        $input = $request->all(); 
        $ticket_id = $input['ticket_id'];
        $priority = $input['priority'];

        $ticket_info = $this->ticket->find($ticket_id);

        if (isset($ticket_info)) {
            $data = array(
                'priority' => $input['priority']
            );
            

            $user = Auth::user();
            $ticket_history['ticket_id']=$ticket_info->id;
            $ticket_history['log_message']=$user->first_name." ".$user->last_name." has Update Priority of ".$ticket_info->ticket_name." from ".$ticket_info->priority." to ".$priority .'.';

            $this->ticket->update($ticket_id, $data);
            $this->history->save($ticket_history);

            /* ---------------------------------------------------
                        Notification Start
            ------------------------------------------------------*/

            $user = Auth::user();
            $from = $user->id;

            $to = $ticket_info->assigned_to;
            $message = "Your Support Tickethas been Updated on respective project";
            $link = route('supportticket.view', ['id' => $ticket_id]);

            $notification_data = array(
                'creator_user_id' => $from,
                'notified_user_id' => $to,
                'message' => $message,
                'link' => $link,
                'is_read' => '0',
            );

            $this->notification->save($notification_data);
            /* ---------------------------------------------------
                        Notification End
            ------------------------------------------------------*/


        }
        alertify()->success('Support Ticket Priority Updated Successfully');
        return redirect(route('supportticket.view', ['id' => $ticket_id]));

    }


    public function assignUser(Request $request){
        $user = Auth::user();
        $data=$request->all();
        $ticket_id = $data['ticket_id'];

        $new_user=$this->user->find($data['assigned_id']);


        $ticket_info=$this->ticket->find($ticket_id);

        $task_history['milestone_id']=$ticket_info->milestone_id;
        $task_history['ticket_id']=$ticket_info->id;
        $task_history['log_message']=$user->first_name." ".$user->last_name." has Assigned/Forward Task To ".$new_user->first_name." ".$new_user->last_name." .";

        $this->history->save($task_history);
        $this->ticket->update($ticket_id,$data);

    
        /* ---------------------------------------------------
                       Notification Start
           ------------------------------------------------------*/


        $from = $user->id;

        $to = $data['assigned_id'];
        $message = "<b>".$user->first_name." ".$user->last_name."</b> Assigned task <u>".$ticket_info->ticket_name."</u> to you";
        $link = route('supportticket.view', ['id' => $ticket_id]);

        $notification_data = array(
            'creator_user_id' => $from,
            'notified_user_id' => $to,
            'message' => $message,
            'link' => $link,
            'is_read' => '0',
        );

        $this->notification->save($notification_data);
        /* ---------------------------------------------------
                    Notification End
        ------------------------------------------------------*/
        return redirect(route('supportticket.view', ['id' => $ticket_id]));
    }

    public function storeComment(Request $request)
    {
        $input = $request->all();
        $ticket_id = $input['ticket_id'];

        $ticket_info = $this->ticket->find($ticket_id);

        $user = Auth::user();

        $name = $user->first_name . ' ' . $user->last_name;

        $data = array(
            'name' => $name,
            'ticket_id' => $ticket_id,
            'comments' => $input['message']
        );

        $user = Auth::user();
            $ticket_history['ticket_id']=$ticket_id;
            $ticket_history['log_message']=$user->first_name." ".$user->last_name." has commented on ".$ticket_info->ticket_name;

        $this->history->save($ticket_history);

        $this->comment->save($data);

        echo 1;
    }
}
