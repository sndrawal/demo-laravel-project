<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {
        
    /*
    |--------------------------------------------------------------------------
    | OpulenceCamp Project Dashboard CRUD ROUTE
    |--------------------------------------------------------------------------
    */
    Route::get('supportticket', ['as' => 'supportticket.index', 'uses' => 'SupportTicketController@index']);


    Route::get('supportticket/create', ['as' => 'supportticket.create', 'uses' => 'SupportTicketController@create']);
    
    Route::post('supportticket/store', ['as' => 'supportticket.store', 'uses' => 'SupportTicketController@store']);

    Route::post('supportticket/storeComment', ['as' => 'supportticket.storeComment', 'uses' => 'SupportTicketController@storeComment']);
    
    Route::get('supportticket/edit', ['as' => 'supportticket.edit', 'uses' => 'SupportTicketController@edit']);
    
    Route::put('supportticket/update/{id}', ['as' => 'supportticket.update', 'uses' => 'SupportTicketController@update'])->where('id','[0-9]+');
    
    Route::get('supportticket/delete/{id}', ['as' => 'supportticket.delete', 'uses' => 'SupportTicketController@destroy'])->where('id','[0-9]+');
    
    Route::get('supportticket/view', ['as' => 'supportticket.view', 'uses' => 'SupportTicketController@view']);
    
    Route::post('supportticket/updateStatus', ['as' => 'supportticket.updateStatus', 'uses' => 'SupportTicketController@updateStatus']);
    
    Route::get('supportticket/changeStatus', ['as' => 'supportticket.changeStatus', 'uses' => 'SupportTicketController@changeStatus']);
    
    Route::post('supportticket/updatePriority', ['as' => 'supportticket.updatePriority', 'uses' => 'SupportTicketController@updatePriority']);

    Route::post('supportticket/assigned/user', ['as' => 'supportticket.assigned.user', 'uses' => 'SupportTicketController@assignUser']);

    Route::get('supportticket/changeLeadStatus', ['as' => 'supportticket.changeLeadStatus', 'uses' => 'SupportTicketController@changeLeadStatus']);


    
});





