<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
             $table->increments('id');

             $table->integer('lead_id')->nullable();
             $table->integer('contact_id')->nullable();
             $table->text('ticket_name')->nullable();
             $table->integer('ticket_type')->nullable();
             $table->text('assigned_dept')->nullable();
             $table->integer('assigned_to')->nullable();
             $table->text('message')->nullable();
             $table->string('ticket_status')->nullable();
             $table->string('priority')->nullable();
             $table->date('deadline')->nullable();
             $table->integer('approved_message')->nullable();
             $table->text('feedback')->nullable();
             $table->text('notes')->nullable();
             $table->integer('created_by')->nullable();
             $table->integer('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
