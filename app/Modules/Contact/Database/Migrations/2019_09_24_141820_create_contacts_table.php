<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salutation')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('contact_type')->nullable();
            $table->integer('state')->nullable();
            $table->string('municipality_vdc')->nullable();
            $table->integer('district')->nullable();
            $table->integer('country_id')->nullable();
            $table->string('city')->nullable();
            $table->string('designation')->nullable();
            $table->bigInteger('mobile_no')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('company')->nullable();
            $table->string('address')->nullable();
            $table->integer('segment')->nullable();
            $table->string('lead_source')->nullable();
            $table->string('image')->nullable();
            $table->string('additional_info')->nullable();
            $table->integer('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
