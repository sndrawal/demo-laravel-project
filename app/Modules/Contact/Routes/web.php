<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'midddleware' => ['auth', 'permission']], function (){

	/*Routes for contact*/
	Route::get('contact', 'ContactController@index')->name('contact.index');
	Route::get('contact/create', 'ContactController@create')->name('contact.create');
	Route::post('contact/store', 'ContactController@store')->name('contact.store');
    Route::get('contact/edit/{id}', 'ContactController@edit')->name('contact.edit');
    Route::patch('contact/update/{id}', 'ContactController@update')->name('contact.update');
    Route::get('contact/delete/{id}', 'ContactController@destroy')->name('contact.delete');

    Route::get('contact/profile', ['as' => 'contact.profile', 'uses' => 'ContactController@profile']);


    Route::get('contact/downloadSheet','ContactController@downloadSheet')->name('contact.downloadSheet'); 
});
