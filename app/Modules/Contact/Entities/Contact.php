<?php

namespace App\Modules\Contact\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Dropdown\Entities\Dropdown;
use App\Modules\Lead\Entities\LeadLog;
use App\Modules\User\Entities\User;

class Contact extends Model
{
	const FILE_PATH = '/uploads/contact/';

    protected $fillable = [
    	'first_name',
        'middle_name',
        'last_name',
        'state',
        'municipality_vdc',
        'district',
        'country_id',
        'city',
    	'designation',
    	'mobile_no',
    	'email',
    	'website',
    	'company',
    	'address',
    	'additional_info',
    	'status',
    	'image',
        'contact_type',
        'salutation',
        'segment',
        'lead_source',
        'created_by',
        'updated_by'
    ];

    public function getUser(){
        return $this->belongsTo(User::class,'created_by','id');
    }
    public function getUpdatedUser(){
        return $this->belongsTo(User::class,'updated_by','id');
    }

    public function getFileFullPathAttribute()
    {
        return self::FILE_PATH . $this->file_name;
    }

     public function dropdown(){
        return $this->belongsTo(Dropdown::class,'contact_type','id');
    }

     public function contactHistory(){
        return $this->hasMany(LeadLog::class,'contact_id','id')->orderBy('created_at','DESC');
    }

}
