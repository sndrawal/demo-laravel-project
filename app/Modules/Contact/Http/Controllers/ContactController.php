<?php

namespace App\Modules\Contact\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Lead\Repositories\DailyClientInterface;
use App\Modules\Dropdown\Repositories\DropdownInterface;
use App\Modules\Contact\Repositories\ContactInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Modules\Employment\Repositories\EmploymentInterface;
use App\Modules\User\Repositories\UserInterface;

use App\Modules\Lead\Repositories\LeadLogInterface;
use App\Modules\Lead\Repositories\LeadActivityInterface;

use App\Modules\Lead\Repositories\NoteInterface;
use App\Modules\Lead\Repositories\MeetingInterface;
use App\Modules\SupportTicket\Repositories\SupportTicketInterface;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;   
class ContactController extends Controller
{
    protected $dailyclient;
    private $contact;
    protected $log;
    protected $dropdown; 
    protected $user;

     protected $note;
     protected $meeting;
     protected $task;

    protected $employment;

    public function __construct(DailyClientInterface $dailyclient,EmploymentInterface $employment,ContactInterface $contact,DropdownInterface $dropdown,UserInterface $user,LeadLogInterface $log,LeadActivityInterface $activity,NoteInterface $note,MeetingInterface $meeting,SupportTicketInterface $task)
    {
        $this->dailyclient = $dailyclient;
        $this->employment = $employment;
        $this->contact = $contact;
        $this->dropdown = $dropdown;
        $this->user = $user;
        $this->log = $log;

        $this->note = $note;
        $this->meeting = $meeting;
        $this->task = $task;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {   
        // dd(request('contact_name'));
       $search = $request->all();
       $data['contacts'] = $this->contact->findAll($limit = 20,$search);
       $data['contact_type'] = $this->dropdown->getFieldBySlug('contact_type');
       $data['contact_search'] = $this->contact->getList();

       return view('contact::contact.index', $data);
   }

    public function profile(Request $request){

        $input = $request->all();

        $data['contact_profile'] = $contact_profile = $this->contact->find($input['id']);
        if(!isset($contact_profile)){
            alertify('No Contact Info Found !!!')->error();
            return redirect()->route('contact.index');
        }
        $employer_user= $this->user->getAllActiveUser();
        
        $employee_data = array();
        foreach ($employer_user as $key => $user) {

            $employee_data += array(
                $user->id => $user->first_name . ' ' . $user->last_name
            );
        }

        $data['assoicateCompany'] = $this->contact->findCompanyById($input['id']);
        $data['count_assoicateCompany'] = $this->contact->countCompanyById($input['id']);


        $data['assigned_user'] = $employee_data;
        $data['ticket_type'] = $this->dropdown->getFieldBySlug('ticket_type');
        $data['is_edit'] = false;


        $data['contact_type'] = $this->dropdown->getFieldBySlug('contact_type');
        $data['state'] = $this->dropdown->getFieldBySlug('state');
        $data['district'] = $this->dropdown->getFieldBySlug('district');
        $data['countries'] = $this->contact->getCountries();
        $data['salutation'] = $this->dropdown->getFieldBySlug('salutation');
        $data['segment'] = $this->dropdown->getFieldBySlug('segment');

        $data['active_services'] =$this->dropdown->getFieldBySlug('services'); // 3 is for Service in Field Table

        /** Contact Tab Detail  **/
         $data['contact_activity_feed'] = $this->dailyclient->getLeadActivityFeed('contact_id',$input['id'],$limit=10);
        $data['note_detail'] = $this->note->findAll('contact_id',$input['id'],$limit=10);
        $data['task_detail'] = $this->task->getLeadTask('contact_id',$input['id'],$limit=10);
        $data['meeting_detail'] = $this->meeting->findAll('contact_id',$input['id'],$limit=10);

        $data['note_count'] = $this->note->countNote('contact_id',$input['id']);
        $data['task_count'] = $this->task->countLeadTask('contact_id',$input['id']);
        $data['meeting_count'] = $this->meeting->countMeeting('contact_id',$input['id']);

        $leadList = $this->dailyclient->getAllLeadData();
        $lead_data = array(); 
        foreach ($leadList as $key => $company) {

            $lead_data += array(
                $company->id => $company->company_name 
            );
        }
        $data['companies'] = $lead_data;


        return view('contact::contact.profile',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['contact_type'] = $this->dropdown->getFieldBySlug('contact_type');
        $data['state'] = $this->dropdown->getFieldBySlug('state');
        $data['district'] = $this->dropdown->getFieldBySlug('district');
        $data['countries'] = $this->contact->getCountries();
        $data['salutation'] = $this->dropdown->getFieldBySlug('salutation');
        $data['segment'] = $this->dropdown->getFieldBySlug('segment');
        return view('contact::contact.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        try {
            $userInfo = Auth::user();
            $user_id = $userInfo->id;
            $data['created_by'] =$user_id;

            $data['status'] = '1';
            if ($request->hasFile('image')) {
                $data['image'] = $this->contact->upload($data['image']);
            }
            $contact_info = $this->contact->save($data);

            $contact_history['contact_id']=$contact_info->id;
            $contact_history['log_message']=$userInfo->first_name." ".$userInfo->last_name." Created contact of ".$contact_info->first_name.' '.$contact_info->last_name;

            $this->log->save($contact_history);

            alertify('Contact Created Successfully')->success();
        } catch (\Throwable $e) {
            alertify($e->getMessage())->error();
        }

        return redirect()->route('contact.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('contact::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
     $data['state'] = $this->dropdown->getFieldBySlug('state');
     $data['district'] = $this->dropdown->getFieldBySlug('district');
     $data['countries'] = $this->contact->getCountries();
     $data['contact'] = $this->contact->find($id);
     $data['contact_type'] = $this->dropdown->getFieldBySlug('contact_type');
     $data['salutation'] = $this->dropdown->getFieldBySlug('salutation');
     $data['segment'] = $this->dropdown->getFieldBySlug('segment');

     return view('contact::contact.edit', $data);
 }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $is_from_profile = $data['profile_lead'];

         $contact_name = $data['first_name'].' '.$data['last_name'];

        try {

            $userInfo = Auth::user();
            $user_id = $userInfo->id;
            $data['updated_by'] =$user_id;

         if ($request->hasFile('image')) {
            $data['image'] = $this->contact->upload($data['image']);
        }
        $this->contact->update($id, $data);

        $contact_history['contact_id']=$id;
        $contact_history['log_message']=$userInfo->first_name." ".$userInfo->last_name." Updated contact of ".$contact_name;
        $this->log->save($contact_history);


        alertify('Contact Updated Successfully')->success();
    } catch (\Throwable $e) {
        alertify($e->getMessage())->error();
    }

    if($is_from_profile == '1'){
            return redirect(route('contact.profile',['id'=>$id]));
        }else{
            return redirect(route('contact.index'));
        }

}

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $contactInfo =  $this->contact->find($id);

            $this->contact->delete($id);

            $userInfo = Auth::user();
            $contact_history['contact_id']=$id;
            $contact_history['log_message'] = $userInfo->first_name." ".$userInfo->last_name." Deleted Contact of ".$contactInfo->company_name.'.';
            
            $this->log->save($contact_history);

            $this->contact->deleteLeadContact($id);

            alertify('Contact Deleted Successfully.')->success();
        } catch (\Throwable $e) { 
            alertify($e->getMessage())->error();
        }

        return redirect()->route('contact.index');
    }

    public function downloadSheet(Request $request)
    {

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],

        ];

        $year = date('Y');


        $objPHPExcel = new Spreadsheet();
        $worksheet = $objPHPExcel->getActiveSheet();

        // set Header

        $objPHPExcel->getActiveSheet(0)->SetCellValue('A1', 'Contact ID');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('B1', 'First Name');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('C1', 'Middle Name');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('D1', 'Last Name');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('E1', 'State');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('F1', 'Municipality/Vdc');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('G1', 'District');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('H1', 'Country');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('I1', 'City');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('J1', 'Designation');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('K1', 'Mobile_No');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('L1', 'Email');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('M1', 'Website');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('N1', 'Company');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('O1', 'Address');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('P1', 'Additional Info');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('Q1', 'Status');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('R1', 'Contact Type');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('S1', 'Salutation');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('T1', 'Segment');
        $objPHPExcel->getActiveSheet(0)->SetCellValue('U1', 'Lead Source');
        
        $objPHPExcel->getActiveSheet()->getStyle('A1:U1')->applyFromArray($styleArray);


        $contact_data = $this->contact->getAllContactData();
        // dd($contact_data);
        $num = 2;
        foreach ($contact_data as $key => $value) {
                        $status = ($value['status'] == '0') ? 'Inactive' : 'Active';
                        $contact_type = $value->contact_type;
            


            $objPHPExcel->getActiveSheet(0)->SetCellValue('A' . $num, $value['id']);
            if (!empty($value['country_id'])) {
                $country_name = $this->employment->getCountryName($value['country_id']);
                $country = $country_name[0]['name'];
            }
            else {
                $country = '';
            }

            if (!empty($value['contact_type'])) {
                $contact_type = $this->dropdown->find($value['contact_type']);
                $contact = $contact_type->dropvalue;
                
            }
            else {
                $contact = '';
            }

            if (!empty($value['salutation'])) {
                $salutation_type = $this->dropdown->find($value['salutation']);
                $salutation = $salutation_type->dropvalue;
                
            }
            else {
                 $salutation = '';
            }

            if (!empty($value['segment'])) {
                $segment_type = $this->dropdown->find($value['segment']);
                $segment = $segment_type->dropvalue;
                
            }
            else {
                 $segment = '';
            }


            if (!empty($value['lead_source'])) {
                $lead_type = $this->dropdown->find($value['lead_source']);
                $lead = $lead_type->dropvalue;
                
            }
            else{
                 $lead = '';
            }

            

            // dd($country_name);
            // if ($value['organization_id'] == 0) {
            //     $organization_name = 'Self';
            // } else {
            //     $value->organization->dropvalue;
            // }
            // $objPHPExcel->getActiveSheet(0)->SetCellValue('B' . $num, $organization_name);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('B' . $num, $value['first_name']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('C' . $num, $value['middle_name']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('D' . $num, $value['last_name']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('E' . $num, $value['state']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('F' . $num, $value['municipality_vdc']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('G' . $num, $value['district']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('H' . $num, $country);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('I' . $num, $value['city']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('J' . $num, $value['designation']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('K' . $num, $value['mobile_no']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('L' . $num, $value['email']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('M' . $num, $value['website']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('N' . $num, $value['company']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('O' . $num, $value['address']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('P' . $num, $value['additional_info']);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('Q' . $num, $status);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('R' . $num, $contact);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('S' . $num, $salutation);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('T' . $num, $segment);
            $objPHPExcel->getActiveSheet(0)->SetCellValue('U' . $num, $lead);
          

            $num++;

        }


        $writer = new Xlsx($objPHPExcel);
        $file = 'contact_' . $year;
        $filename = $file . '.xlsx';
        header('Content-Type: application/openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache 

        $writer->save('php://output');

        exit;


    }
}
