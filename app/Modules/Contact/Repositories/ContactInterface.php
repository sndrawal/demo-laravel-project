<?php

namespace App\Modules\Contact\Repositories;

interface ContactInterface
{
	public function findAll($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

	public function find($id);

	public function getList();

	public function save($data);

	public function update($id,$data);

	public function delete($id);

	public function getCountries();

    public function upload($file);

    public function getAllContactData($filter = []);

    public function findCompanyById($contact_id,$limit=null,$filter = []);

    public function countCompanyById($contact_id);

    public function deleteLeadContact($contact_id);
}