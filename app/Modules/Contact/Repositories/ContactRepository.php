<?php

namespace App\Modules\Contact\Repositories;

use App\Modules\Contact\Entities\Contact;
use App\Modules\Employment\Entities\Country;
use App\Modules\Lead\Entities\LeadContact;


class ContactRepository implements ContactInterface
{

    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {

        $result = Contact::when(array_keys($filter, true), function ($query) use ($filter) {

             if (isset($filter['designation'])) {
                $query->where('designation', 'like', '%' . $filter['designation'] . '%');
            }
            if (isset($filter['contact_type'])) {
                $query->where('contact_type', '=' ,$filter['contact_type']);
            }

            if (isset($filter['name'])) {
                $query->where('first_name', 'like', '%' . $filter['name'] . '%');
                $query->orWhere('middle_name', 'like', '%' . $filter['name'] . '%');
                $query->orWhere('last_name', 'like', '%' . $filter['name'] . '%');
            }

            if (isset($filter['company'])) {
                $query->where('company', 'like', '%' . $filter['company'] . '%');
            }

            if (isset($filter['mobile_no'])) {
                $query->where('mobile_no', 'like', '%' . $filter['mobile_no'] . '%');
            }

            if (isset($filter['search_from_to'])) {
                 $dateRange = explode('---', $filter['search_from_to']);

                $from = date('Y-m-d H:i:s',strtotime($dateRange[0]));
                $to = date('Y-m-d H:i:s',strtotime($dateRange[1]));

                $query->where('created_at', '>=', $from);
                $query->where('created_at', '<=', $to);

            }


        })->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 
    }
    
    public function find($id)
    {
        return Contact::find($id);
    }
    
    public function getList(){  
       $result = Contact::pluck('first_name', 'id');
       return $result;
    }

   public function save($data)
   {
    return Contact::create($data);
   }

    public function update($id,$data){
        $result = Contact::find($id);
        return $result->update($data);
    }

    public function delete($id){
        return Contact::destroy($id);
    }

    public function upload($file)
    {
        $imageName = $file->getClientOriginalName();
        $fileName = date('Y-m-d-h-i-s') . '-' . preg_replace('[ ]', '-', $imageName);
        $file->move(public_path() . Contact::FILE_PATH, $fileName);
        return $fileName;
    }

    public function getCountries()
    {
        return Country::pluck('name', 'id');
    }

    public function getAllContactData($filter = [])
    {
        $result = Contact::get();
        return $result;
    }

    public function findCompanyById($contact_id,$limit=null,$filter = []){
        return LeadContact::where('contact_id','=',$contact_id)->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
    }

    public function countCompanyById($contact_id){
        return LeadContact::where('contact_id','=',$contact_id)->count();
    }

    public function deleteLeadContact($contact_id){
        return LeadContact::where('contact_id','=',$contact_id)->delete($contact_id);   
    }

 }