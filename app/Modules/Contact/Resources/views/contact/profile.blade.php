@extends('admin::layout')

@section('title') Contact Profile @stop 
@section('breadcrum') Contact Profile @stop 

@section('script')
<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
<script src="{{ asset('admin/assets/js/plugins/forms/jquery-clock-timepicker.min.js') }}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript">
        $(document).ready(function(){
            // Select with search
            $('.select-search').select2();
        });
</script>
@stop

@section('content') 

<div class="card bg-light">
    <div class="card-body"> 
        <div class="row tab-content">

            <div class="col-md-3">
                <div class="card-img-actions mx-1 mt-5 text-center mb-1">

                    <figure style="height:150px; width:150px; margin: 20px auto 0; " class="text-center">
                             <img class="rounded-round" style="width: 100%; height: 100%; object-fit: cover;border: 1px solid #eeeeec" src="{{ asset('admin/default1.png') }}" alt="">
                    </figure>
                </div>

                <div class="card-body text-center">
                    <h6 class="font-weight-semibold mb-0">{{ $contact_profile['company_name'] }}</h6>
                    <span class="d-block text-muted mb-2">{{ $contact_profile['contact_person'] }}</span>

                    <ul class="list-inline list-inline-condensed mt-1 mb-0">
                        <li class="list-inline-item">
                             <a data-toggle="modal" data-target="#fullHeightModalHistoryRightNote" class="btn bg-teal text-slate-400 btn-icon rounded-round ml-2" title="Note"><i class="text-light-400 icon-pencil"></i></a>
                                <br>
                                <span class="text-slate-400 ml-2">Note</span>
                        </li>
                        <li class="list-inline-item">
                            <a data-toggle="modal" data-target="#fullHeightModalHistoryRight" class="btn bg-warning text-slate-400 btn-icon rounded-round ml-2" title="View Log"><i class="text-light-400 icon-wall"></i></a>
                            <br>
                            <span class="text-slate-400 ml-2">Log</span>
                            </li>
                            <li class="list-inline-item">
                                <a data-toggle="modal" data-target="#fullHeightModalHistoryRightTask" class="btn bg-danger text-slate-400 btn-icon rounded-round ml-2" title="Task"><i class="text-light-400 icon-clipboard3"></i></a>
                                    <br>
                                    <span class="text-slate-400 ml-2">Task</span>
                                </li>
                            <li class="list-inline-item">
                                 <a data-toggle="modal" data-target="#fullHeightModalHistoryRightMeeting" class="btn bg-purple text-slate-400 btn-icon rounded-round ml-2" title="Meeting"><i class="text-light-400 icon-calendar2"></i></a>
                                    <br>
                                    <span class="text-slate-400 ml-2">Meeting</span>
                                </li>    
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-9">
                        <div class="mb-3">

                            {!! Form::model($contact_profile, ['route'=>['contact.update', $contact_profile->id], 'method'=>'PATCH','class'=>'form-horizontal','role'=>'form', 'id' => 'contact_submit','files' => true]) !!}
        

                            <h6 class="font-weight-semibold mt-2"><i class="text-teal icon-folder6 mr-2"></i><a href="#" class="text-teal">About This Contact<i class="ml-2 text-primary icon-question3" data-popup="tooltip" data-placement="bottom" data-original-title="Please point Icon for more Details." style="margin-top: -18px;"></i></a>

                                <button id="spinner-light-4" type="submit" class="btn bg-success border-success text-success-800 btn-icon ml-2" style="float:right;" data-popup="tooltip" data-placement="top" data-original-title="Update Contact"><i class="icon-pen-plus"></i></button>
                               <button type="button" class="btn bg-warning border-warning text-warning-800 btn-icon ml-2 editContact" style="float:right;" data-popup="tooltip" data-placement="top" data-original-title="Edit Contact"><i class="icon-pencil"></i></button>

                               <div class="dropdown-divider mb-2"></div></h6>

                                {{ Form::hidden('profile_lead', '1') }}

                               <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                        {!! Form::select('salutation',$salutation, $value = null, ['id'=>'salutation_title','placeholder'=>'Select Salutation Title','class'=>'form-control edit_select','disabled']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-hat" data-popup="tooltip" data-placement="bottom" data-original-title="Select Salutation"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                       
                                       {!! Form::text('last_name', null, ['placeholder'=>'Enter Last Name','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}
                                        

                                        <div class="form-control-feedback">
                                            <i class="icon-pencil" data-popup="tooltip" data-placement="bottom" data-original-title="Last Name"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

                                        {!! Form::email('email', null, ['placeholder'=>'Enter Email','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-envelop" data-popup="tooltip" data-placement="bottom" data-original-title="Email"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

                                       {!! Form::text('first_name', null, ['placeholder'=>'Enter First Name','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-pencil" data-popup="tooltip" data-placement="bottom" data-original-title="First Name"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">

                                       {!! Form::select('contact_type',$contact_type, $value = null, [ 'class'=>'form-control edit_contact' ,'disabled']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-notebook" data-popup="tooltip" data-placement="bottom" data-original-title="Select Contact Type"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                       
                                       {!! Form::text('mobile_no', null, ['placeholder'=>'Enter Mobile No.','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-phone2" data-popup="tooltip" data-placement="bottom" data-original-title="Mobile Num"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                       
                                       {!! Form::text('middle_name', null, ['placeholder'=>'Enter Middle Name','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-pencil" data-popup="tooltip" data-placement="bottom" data-original-title="Middle Name"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                        {!! Form::text('designation', null, ['placeholder'=>'Enter Designation','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-vcard" data-popup="tooltip" data-placement="bottom" data-original-title="Designation"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                        {!! Form::text('company', null, ['placeholder'=>'Enter Company','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-office" data-popup="tooltip" data-placement="bottom" data-original-title="Company"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                        {!! Form::text('website', null, ['placeholder'=>'Enter Website','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-browser" data-popup="tooltip" data-placement="bottom" data-original-title="Website"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                        {!! Form::text('address', null, ['placeholder'=>'Enter Address','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-location4" data-popup="tooltip" data-placement="bottom" data-original-title="Address"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                        {!! Form::text('municipality_vdc', $value = null, ['id'=>'municipality_vdc','placeholder'=>'Enter Municipality/Vdc','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-browser" data-popup="tooltip" data-placement="bottom" data-original-title="Municipality/Vdc"></i>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                         {!! Form::text('city', $value = null, ['id'=>'city','placeholder'=>'Enter City','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-location3" data-popup="tooltip" data-placement="bottom" data-original-title="City"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                        {!! Form::select('district',$district, $value = null, ['id'=>'district','placeholder'=>'Select District','class'=>'form-control edit_select' ,'disabled']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-location4" data-popup="tooltip" data-placement="bottom" data-original-title="Select District"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                       
                                       {!! Form::select('state',$state, $value = null, ['id'=>'state','placeholder'=>'Select State/Province','class'=>'form-control edit_select' ,'disabled']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-direction" data-popup="tooltip" data-placement="bottom" data-original-title="Select State"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                       
                                       {!! Form::select('country_id',$countries, $value = 156, ['id'=>'country_id','placeholder'=>'Select Country','class'=>'form-control edit_select' ,'disabled']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-compass4" data-popup="tooltip" data-placement="bottom" data-original-title="Select County"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                        {!! Form::select('segment',$segment, $value = null, ['id'=>'segment','placeholder'=>'Select Segment','class'=>'form-control edit_select' ,'disabled']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-user" data-popup="tooltip" data-placement="bottom" data-original-title="Select Segment"></i>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                         {!! Form::text('lead_source', null, ['placeholder'=>'Enter Lead Source','class'=>'form-control edit_contact text-grey' ,'readonly']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-pen" data-popup="tooltip" data-placement="bottom" data-original-title="Lead Source"></i>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-group-feedback form-group-feedback-left" style="margin-bottom: 6px;">
                                        
                                        {!! Form::textarea('additional_info', null, ['id' => 'additional_info','placeholder'=>'Enter Additional Info', 'class' =>'form-control edit_contact text-grey' ,'readonly','rows'=>'2']) !!}

                                        <div class="form-control-feedback">
                                            <i class="icon-notebook" data-popup="tooltip" data-placement="bottom" data-original-title="Additional Note"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                {!! Form::close() !!}


                        </div>
                    </div>

                </div>
            </div>
        </div>


        <!-- SEcondary Section -->

        <div class="d-flex align-items-start flex-column flex-md-row">


            <!-- Left Section -->
            <div class="tab-content w-100 order-2 order-md-1">
                <div class="tab-pane fade active show" id="activity">


                     <!-- Invoices -->
            <div class="row">
                <div class="col-12">
                    <div class="card">


                        <div class="card-body">
                            <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                <li class="nav-item"><a href="#bottom-justified-tab1" class="nav-link active font-weight-black" data-toggle="tab">Activity </a></li>
                                <li class="nav-item"><a href="#bottom-justified-tab2" class="nav-link font-weight-black text-teal" data-toggle="tab">Notes<sup class="badge badge-pill bg-warning-400 ml-1 md-0">{{ $note_count }}</sup></a></li>
                                <li class="nav-item"><a href="#bottom-justified-tab3" class="nav-link font-weight-black text-danger" data-toggle="tab">Tasks<sup class="badge badge-pill bg-warning-400 ml-1 md-0">{{ $task_count }}</sup></a></li>
                                <li class="nav-item"><a href="#bottom-justified-tab4" class="nav-link font-weight-black text-purple" data-toggle="tab">Meeting<sup class="badge badge-pill bg-warning-400 ml-1 md-0">{{ $meeting_count }}</sup></a></li>

                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="bottom-justified-tab1">
                                 <div class="activity">

                                    @if($contact_activity_feed->total() != 0)
                                    @foreach($contact_activity_feed as $key => $value)

                                    @if($value->method =='Note')
                                    @php
                                    $note_id = $value->method_id;
                                    $NoteInfo = App\Modules\Lead\Entities\LeadActivityFeed::getNoteByLead($note_id);
                                    @endphp

                                    <div class="card activity-item border-left-3 border-left-success rounded-left-0 cursor-pointer" data-target="#collapse-text{{ $key }}" data-toggle="collapse" aria-expanded="true">
                                        <div class="p-1">
                                            <div class="d-flex align-items-center justify-content-between">
                                                <div class="d-flex align-items-center">
                                                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                                                        <span class="btn text-success btn-icon btn-sm alpha-success rounded-round"><i class="icon-notebook"></i></span>
                                                        &nbsp;Note :
                                                    </h6>
                                                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;New Note Added</h6>
                                                </div>
                                                <div class="activity-item__title text-muted pr-2 font-weight-semibold">{{ date('M d, Y H:i a',strtotime($NoteInfo->created_at)) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bg-white collapse" id="collapse-text{{ $key }}" style="">
                                            <div class="p-3 activity-content">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="font-weight-semibold">Left Note By:
                                                        <span class="text-primary pt-1 d-block"><img src="{{ asset('admin/default.png') }}" class="rounded-circle" width="36" height="36" alt="">
                                                         {{ $NoteInfo->createUser->first_name }}
                                                     </span>
                                                 </div>
                                                 <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_note" link="{{route('note.delete',['id' => $NoteInfo->id])}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>
                                             </div>

                                             <p class="activity-content__desc mt-2 mb-0">
                                                {!! $NoteInfo->note !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @endif


                                @if($value->method  =='Task')
                                @php
                                $task_id = $value->method_id;
                                $TaskInfo = App\Modules\Lead\Entities\LeadActivityFeed::getTaskByLead($task_id);

                                $prio_color = 'bg-success';

                                if($TaskInfo->ticket_status == 'Backlog'){
                                    $prio_color = 'bg-danger';
                                }
                                if($TaskInfo->ticket_status == 'ToDo'){
                                    $prio_color = 'bg-info';
                                }
                                if($TaskInfo->ticket_status == 'InProgress'){
                                    $prio_color = 'bg-warning';
                                }
                                if($TaskInfo->ticket_status == 'Completed'){
                                    $prio_color = 'bg-success';
                                }

                                @endphp
                                <div class="card activity-item border-left-3 border-left-danger rounded-left-0 cursor-pointer" data-target="#collapse-text3" data-toggle="collapse">
                                    <div class="p-1">
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                                                    <span class="btn text-danger btn-icon btn-sm alpha-danger rounded-round"><i class="icon-task"></i></span>
                                                    &nbsp;Task :
                                                </h6>
                                                <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;{{ $TaskInfo->ticket_name }}</h6>
                                                <span class="badge {{ $prio_color }} badge-pill font-size-md ml-2">{{ $TaskInfo->ticket_status }}</span>
                                            </div>
                                            <div class="activity-item__title text-muted pr-2 font-weight-semibold">{{ date('M d, Y H:i a',strtotime($TaskInfo->created_at)) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="collapse bg-white" id="collapse-text3">
                                        <div class="p-3 activity-content">
                                            <div class="d-flex align-items-center justify-content-between">
                                                <div class="">
                                                    <div class="font-weight-semibold">Assigned Department:
                                                        <span class="text-primary d-block">{{ ucfirst($TaskInfo->assigned_dept) }}</span>
                                                    </div>
                                                </div>
                                                <div class="ml-5">
                                                    <div class="font-weight-semibold">Assigned To:
                                                        <span class="text-primary d-block">{{ $TaskInfo->getEmployerUser->first_name }}</span>
                                                    </div>
                                                </div>
                                                <div class="ml-5">
                                                    <div class="font-weight-semibold">Task Deadline:
                                                        <span class="text-danger d-block">{{ $TaskInfo->deadline }}</span>
                                                    </div>
                                                </div>
                                                <div class="ml-5">
                                                    <div class="font-weight-semibold">Change Status:
                                                        <span class="text-primary d-block"><div class="dropdown">
                                                            <a href="#" class="text-default dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false"><i class="icon-more2"></i></a>
                                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 17px, 0px);">
                                                                <a href="{{route('supportticket.changeLeadStatus',['status'=>'Backlog','ticket_id'=>$TaskInfo->id])}}" class="dropdown-item"><i class="icon-rotate-ccw text-danger"></i> Backlog</a>
                                                                <a href="{{route('supportticket.changeLeadStatus',['status'=>'ToDo','ticket_id'=>$TaskInfo->id])}}" class="dropdown-item"><i class="icon-clipboard3 text-info"></i> ToDo</a>
                                                                <a href="{{route('supportticket.changeLeadStatus',['status'=>'InProgress','ticket_id'=>$TaskInfo->id])}}" class="dropdown-item"><i class="icon-spinner6 spinner text-warning"></i> InProgress</a>
                                                                <a href="{{route('supportticket.changeLeadStatus',['status'=>'Completed','ticket_id'=>$TaskInfo->id])}}" class="dropdown-item"><i class="icon-checkmark4 text-success"></i> Completed</a>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>

                                             <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_meeting" link="{{route('supportticket.delete',['id' => $TaskInfo->id])}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>
                                           

                                        </div>


                                        <p class="activity-content__desc mt-2 mb-0">
                                            {{ $TaskInfo->message }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                @endif


                @if($value->method =='Meeting')

                @php
                $meeting_id = $value->method_id;
                $MeetingInfo = App\Modules\Lead\Entities\LeadActivityFeed::getMeetingByLead($meeting_id);

                $attendee = json_decode($MeetingInfo->attendee);
                if(isset($attendee)){
                    $total = sizeof($attendee);
                }else{
                    $total = 0;
                }
                @endphp

                <div class="card activity-item border-left-3 border-left-purple rounded-left-0 cursor-pointer" data-target="#collapse-text2-{{ $key }}" data-toggle="collapse">
        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-purple btn-icon btn-sm alpha-purple rounded-round"><i class="icon-calendar2"></i></span>
                        &nbsp;Meeting :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;{{ $MeetingInfo->meeting_about }}</h6>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold"> {{ date('M d, Y',strtotime($MeetingInfo->date)) }} at {{ date('H:i a',strtotime($MeetingInfo->time)) }}
                </div>
            </div>
        </div>
        <div class="collapse bg-white" id="collapse-text2-{{ $key }}">
            <div class="p-3 activity-content">


                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex">
                        <div class="font-weight-semibold">Created By:
                            <span class="text-primary d-block">{{ $MeetingInfo->createUser->first_name .' '. $MeetingInfo->createUser->last_name}}</span>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Duration Time:
                                <span class="text-primary d-block">{{ ($MeetingInfo->duration) ? $MeetingInfo->duration : '-' }}</span>
                            </div>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Attendees:
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item dropdown-toggle d-flex align-items-center" data-toggle="dropdown">
                                        <span class="text-primary d-block">
                                            <img src="{{ asset('admin/default.png') }}" class="rounded-circle" alt="" width="28" height="28">
                                            <div class="btn bg-success-400 rounded-round btn-icon btn-sm circle-28">
                                                <span>{{ $total }}</span>
                                            </div>
                                        </span>
                                    </a>

                                    @if($total >=1)
                                    <div class="dropdown-menu dropdown-menu-right bd-card p-0" x-placement="bottom-end">
                                        <ul class="media-list media-list-linked">

                                            @for($i=0; $i<$total; $i++)

                                            @php
                                             $userId = $attendee[$i];
                                             $AttendeeInfo = App\Modules\Lead\Entities\Meeting::getUserByAttendee($userId);

                                             $name = $AttendeeInfo->first_name.' '. $AttendeeInfo->last_name;
                                            @endphp
                                            <li>
                                                <a href="#" class="media p-1 align-items-center">
                                                    <div class="mr-1">
                                                        <img src="{{ asset('admin/default.png') }}" class="rounded-circle" alt="" width="28" height="28">
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="msb-0 fosnt-size-sm">{{ $name }}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            @endfor

                                        </ul>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    
                     <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_meeting" link="{{route('meeting.delete',$MeetingInfo->id)}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>

                </div>


                        <p class="activity-content__desc mt-2 mb-0">
                           {{ $MeetingInfo->description }}
                        </p>
                    </div>
                </div>
            </div>

        @endif

        @endforeach
        @endif
    </div>


</div>

<div class="tab-pane fade" id="bottom-justified-tab2">
    <div style="float: right;margin-top: -5px;" class="card mb-4">
       <a data-toggle="modal" data-target="#fullHeightModalHistoryRightNote" class="btn btn-slate btn-labeled btn-labeled-left btn-sm bg-slate" title="Create Note"><b><i class="text-slate-400 icon-pencil"></i></b>Create Note</a>

   </div>
   <span class="mb-2 font-weight-bold text-slate">Take notes about this record to keep track of important info</span>

   <div class="activity mt-5">

     @if($note_detail->total() != 0)
     @foreach($note_detail as $key => $value)


     <div class="card activity-item border-left-3 border-left-success rounded-left-0 cursor-pointer" data-target="#collapse-text{{ $key }}" data-toggle="collapse" aria-expanded="true">
        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-success btn-icon btn-sm alpha-success rounded-round"><i class="icon-notebook"></i></span>
                        &nbsp;Note :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;New Note Added</h6>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold">{{ date('M d, Y H:i a',strtotime($value->created_at)) }}
                </div>
            </div>
        </div>
        <div class="bg-white collapse show" id="collapse-text{{ $key }}" style="">
            <div class="p-3 activity-content">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="font-weight-semibold">Left Note By:
                        <span class="text-primary pt-1 d-block"><img src="{{ asset('admin/default.png') }}" class="rounded-circle" width="36" height="36" alt="">
                         {{ $value->createUser->first_name }}
                     </span>
                 </div>
                 <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_note" link="{{route('note.delete',['id' => $value->id])}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>
             </div>
              

             <p class="activity-content__desc mt-2 mb-0">
                {!! $value->note !!}
            </p>
        </div>
    </div>
</div>


@endforeach
@endif
</div>

</div>


<div class="tab-pane fade" id="bottom-justified-tab3">

   <div style="float: right;margin-top: -5px;" class="card mb-4">
       <a data-toggle="modal" data-target="#fullHeightModalHistoryRightTask" class="btn btn-slate btn-labeled btn-labeled-left btn-sm bg-slate" title="Create Task">
        <b>
            <i class="text-slate-400 icon-pencil"></i>
        </b>Create Task
    </a> 
</div>
<span class="mb-2 font-weight-bold text-slate">Create a task for yourself or a teammate. Keep track of all your to-dos for this record.</span>

<div class="activity mt-5"> 
    @if($task_detail->total() != 0)
    @foreach($task_detail as $key => $value)

    @php  
    $prio_color = 'bg-success';

    if($value->ticket_status === 'Backlog'){
        $prio_color = 'bg-danger';
    }
    if($value->ticket_status === 'ToDo'){
        $prio_color = 'bg-info';
    }
    if($value->ticket_status === 'InProgress'){
        $prio_color = 'bg-warning';
    }
    if($value->ticket_status === 'Completed'){
        $prio_color = 'bg-success';
    }
        
    @endphp

    <div class="card activity-item border-left-3 border-left-danger rounded-left-0 cursor-pointer" data-target="#collapse-text3" data-toggle="collapse">
        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-danger btn-icon btn-sm alpha-danger rounded-round"><i class="icon-task"></i></span>
                        &nbsp;Task :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;{{ $value->ticket_name }}</h6>
                    <span class="badge {{ $prio_color }} badge-pill font-size-md ml-2">{{ $value->ticket_status }}</span>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold">{{ date('M d, Y H:i a',strtotime($value->created_at)) }}
                </div>
            </div>
        </div>
        <div class="collapse bg-white" id="collapse-text3">
            <div class="p-3 activity-content">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="">
                        <div class="font-weight-semibold">Assigned Department:
                            <span class="text-primary d-block">{{ ucfirst($value->assigned_dept) }}</span>
                        </div>
                    </div>
                    <div class="ml-5">
                        <div class="font-weight-semibold">Assigned To:
                            <span class="text-primary d-block">{{ $value->getEmployerUser->first_name }}</span>
                        </div>
                    </div>
                    <div class="ml-5">
                        <div class="font-weight-semibold">Task Deadline:
                            <span class="text-danger d-block">{{ $value->deadline }}</span>
                        </div>
                    </div>
                    <div class="ml-5">
                        <div class="font-weight-semibold">Change Status:
                            <span class="text-primary d-block"><div class="dropdown">
                                <a href="#" class="text-default dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false"><i class="icon-more2"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 17px, 0px);">
                                    <a href="{{route('supportticket.changeLeadStatus',['status'=>'Backlog','ticket_id'=>$value->id])}}" class="dropdown-item"><i class="icon-rotate-ccw text-danger"></i> Backlog</a>
                                    <a href="{{route('supportticket.changeLeadStatus',['status'=>'ToDo','ticket_id'=>$value->id])}}" class="dropdown-item"><i class="icon-clipboard3 text-info"></i> ToDo</a>
                                    <a href="{{route('supportticket.changeLeadStatus',['status'=>'InProgress','ticket_id'=>$value->id])}}" class="dropdown-item"><i class="icon-spinner6 spinner text-warning"></i> InProgress</a>
                                    <a href="{{route('supportticket.changeLeadStatus',['status'=>'Completed','ticket_id'=>$value->id])}}" class="dropdown-item"><i class="icon-checkmark4 text-success"></i> Completed</a>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>

                 <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_meeting" link="{{route('supportticket.delete',['id' => $value->id])}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>
               

            </div>


            <p class="activity-content__desc mt-2 mb-0">
                {{ $value->message }}
            </p>
        </div>
    </div>
</div>
@endforeach
@endif
</div>

</div>

<div class="tab-pane fade" id="bottom-justified-tab4">
    <div style="float: right;margin-top: -5px;" class="card mb-4">
       <a data-toggle="modal" data-target="#fullHeightModalHistoryRightMeeting" class="btn btn-slate btn-labeled btn-labeled-left btn-sm bg-slate" title="Create Meeting">
        <b>
            <i class="text-slate-400 icon-pencil"></i>
        </b>Create Meeting
    </a> 
</div>

<span class="mb-2 font-weight-bold text-slate">Create a Meeting for yourself or Schedule log a meeting activity to keep track of your discussion and notes.</span>

 <div class="activity mt-5">
@if($meeting_detail->total() != 0)
@foreach($meeting_detail as $key => $value)

@php 
$attendee = json_decode($value->attendee);

if(isset($attendee)){ 
    $total = sizeof($attendee);
}else{
    $total = 0;
}
@endphp
<div class="card activity-item border-left-3 border-left-purple rounded-left-0 cursor-pointer" data-target="#collapse-text2-{{ $key }}" data-toggle="collapse">
        <div class="p-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <h6 class="activity-item__title text-muted mb-0 font-weight-semibold">
                        <span class="btn text-purple btn-icon btn-sm alpha-purple rounded-round"><i class="icon-calendar2"></i></span>
                        &nbsp;Meeting :
                    </h6>
                    <h6 class="activity-item__title font-weight-semibold mb-0"> &nbsp;&nbsp;{{ $value->meeting_about }}</h6>
                </div>
                <div class="activity-item__title text-muted pr-2 font-weight-semibold"> {{ date('M d, Y',strtotime($value->date)) }} at {{ date('H:i a',strtotime($value->time)) }}
                </div>
            </div>
        </div>
        <div class="collapse bg-white" id="collapse-text2-{{ $key }}">
            <div class="p-3 activity-content">


                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex">
                        <div class="font-weight-semibold">Created By:
                            <span class="text-primary d-block">{{ $value->createUser->first_name .' '. $value->createUser->last_name}}</span>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Duration Time:
                                <span class="text-primary d-block">{{ ($value->duration) ? $value->duration : '-' }}</span>
                            </div>
                        </div>
                        <div class="ml-5">
                            <div class="font-weight-semibold">Attendees:
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item dropdown-toggle d-flex align-items-center" data-toggle="dropdown">
                                        <span class="text-primary d-block">
                                            <img src="{{ asset('admin/default.png') }}" class="rounded-circle" alt="" width="28" height="28">
                                            <div class="btn bg-success-400 rounded-round btn-icon btn-sm circle-28">
                                                <span>{{ $total }}</span>
                                            </div>
                                        </span>
                                    </a>

                                    @if($total >=1)
                                    <div class="dropdown-menu dropdown-menu-right bd-card p-0" x-placement="bottom-end">
                                        <ul class="media-list media-list-linked">

                                            @for($i=0; $i<$total; $i++)

                                            @php
                                             $userId = $attendee[$i];
                                             $AttendeeInfo = App\Modules\Lead\Entities\Meeting::getUserByAttendee($userId);

                                             $name = $AttendeeInfo->first_name.' '. $AttendeeInfo->last_name;
                                            @endphp
                                            <li>
                                                <a href="#" class="media p-1 align-items-center">
                                                    <div class="mr-1">
                                                        <img src="{{ asset('admin/default.png') }}" class="rounded-circle" alt="" width="28" height="28">
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="msb-0 fosnt-size-sm">{{ $name }}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            @endfor

                                        </ul>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    
                     <a data-toggle="modal" data-target="#modal_theme_warning" class="btn alpha-danger text-danger btn-icon btn-sm rounded-round ml-2 delete_meeting" link="{{route('meeting.delete',$value->id)}}" data-popup="tooltip"data-placement="bottom" data-original-title="Delete"><i class="icon-bin2"></i></a>

                </div>


                        <p class="activity-content__desc mt-2 mb-0">
                           {{ $value->description }}
                        </p>
                    </div>
                </div>
            </div>
@endforeach
@endif
</div>

</div>
</div> 
</div>
</div>
</div>
</div>
<!-- /invoices -->

                </div>

            </div>
            <!-- End of Left Section -->



            <!-- Right Section -->
 
            <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-300 border-0 shadow-0 order-1 order-lg-2 sidebar-expand-md">

                <div class="card bg-light">
                <div class="card-header bg-purple header-elements-inline">
                    <h6 class="card-title">Company ({{ $count_assoicateCompany }})</h6>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                 @if($assoicateCompany->total() != 0)
                    @foreach($assoicateCompany as $key => $value)

                        <div class="card mt-1 mb-1 mr-1 ml-1">
                            
                            <div class="card-body">

                                <ul class="media-list">
                                    <li class="media">
                                        <div class="mr-2">
                                            <a href="{{route('dailyClient.profile',['id'=>$value->lead_id])}}" class="btn bg-transparent text-pink rounded-round btn-icon"><img class="rounded-round" style="height:40px; width:40px;" src="{{asset('admin/company_logo.png')}}" alt=""></a>
                                        </div>
                                        
                                        <div class="media-body">
                                            <span class="font-weight-bold"><a class="text-dark" href="{{route('dailyClient.profile',['id'=>$value->lead_id])}}">{{ $value->lead->company_name }}</a></span> <br>
                                             <span class="font-weight-semibold text-teal">{{ $value->lead->client_address }}</span>
                                            <div class="text-muted"><i class="icon-envelop mr-1"></i>{{ $value->lead->email }}</div>
                                        </div>
                                    </li>

                                </ul>

                            </div>
                        </div>
                    @endforeach
                    <div class="card-body text-center">
                        <a href="{{route('dailyClient.index')}}" class="text-purple">View Company in filtered view</a>
                    </div>
                 @else    
                        <div class="card-body text-center">
                            View all interactions with this contact’s company in one place. Add an existing company from your CRM or create a new one.
                            <br>

                            @if($companies)
                                <button type="button" data-toggle="modal" data-target="#fullHeightModalHistoryRightCompany" class="btn btn-slate mt-1">Add Company</button>
                            @endif

                        </div>
                @endif
            </div> 


            <div class="card bg-purple-400">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">Deals</h6>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body pb-0" style="">
                    <div class="row">

                        <div class="media flex-column flex-sm-row mt-0 mb-3">

                            <div class="media-body">
                             Use deals to track all your revenue opportunities.<br>
                             @if($assoicateCompany->total() != 0)
                                 @if(($assoicateCompany['status'] !== 4) AND ($assoicateCompany['status'] !== 5))
                                  <button type="button" data-toggle="modal" data-target="#fullHeightModalHistoryRightDeal" class="btn btn-slate mt-1">Create Deal</button>
                                  @endif
                              @endif 
                         </div>
                     </div>

                 </div>
             </div>
         </div>


         <div class="card bg-purple-400">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Tasks ({{ $task_count }})</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body pb-0" style="">
                <div class="row">

                    <div class="media flex-column flex-sm-row mt-0 mb-3">

                        <div class="media-body">
                         Use tasks to track all of your customer's questions and requests for help.<br>
                         <button type="button" data-toggle="modal" data-target="#fullHeightModalHistoryRightTask" class="btn btn-slate mt-1">Create Tasks</button>
                        

                     </div>
                 </div>

             </div>
         </div>
     </div>

 </div>

 <!-- End of Right Section -->

</div>
<!-- End of SEcondary Section -->



<!--Side Popup For Log -->
 <!-- Full Height Modal Right -->
                <div class="modal modal_slide right" id="fullHeightModalHistoryRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" data-direction='right'>

                <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
                <div class="modal-dialog modal-full-height modal-right" role="document">

                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h6 class="modal-title"><i class="icon-history"></i> Contact Log History</h6>
                        </div>
                        <div class="modal-body" style="height: 371px;overflow-y: scroll;">
                           <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr class="bg-slate">
                                    <th>Time</th>
                                    <th>Log Message</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($contact_profile->contactHistory as $key=>$contactHistory_value)
                                
                                <tr>
                                    <td>{{$contactHistory_value->created_at->diffForHumans()}}</td>
                                    <td>{{$contactHistory_value->log_message}}</td>
                                </tr>
                                
                            @endforeach  

                            </tbody>
                        </table>
                    </div>
                        </div>
                        
                        <div class="modal-footer justify-content-center mt-2">
                            <button type="button" class="btn btn-success" data-dismiss="modal" id="close-btn">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- Full Height Modal Right -->
<!--End of Side Popup For Log -->

<!--Side Popup For Note -->
 <!-- Full Height Modal Right -->
                <div class="modal modal_slide right" id="fullHeightModalHistoryRightNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" data-direction='right'>

                <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
                <div class="modal-dialog modal-full-height modal-right" role="document">

                    <div class="modal-content">
                        <div class="modal-header bg-slate">
                            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Note</h6>
                        </div>
                        <div class="modal-body" style="height: 371px;overflow-y: scroll;">
                           
                            {!! Form::open(['route'=>'note.store','method'=>'POST','id'=>'note_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!} 
            
                            <fieldset class="mb-3">
                                
                                <div class="form-group">
                                        <label>Your Note:</label>
                                        {!! Form::textarea('note', $value = null, ['id'=>'note','placeholder'=>'Start Typing to leave a note...','class'=>'form-control simple_textarea_description']) !!}
                                </div>
                                {{ Form::hidden('contact_id',  $contact_profile->id) }}

                            </fieldset>
                            <div class="text-right">
                             
                              <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

                             <button onclick="submitForm(this);" type="submit" class="btn bg-success-400">Save Note </button>
                            </div>


                            {!! Form::close() !!}


                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <!-- Full Height Modal Right -->
<!--End of Side Popup For Note -->


<!--Side Popup For Task -->
 <!-- Full Height Modal Right -->
                <div class="modal modal_slide right" id="fullHeightModalHistoryRightTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" data-direction='right'>

                <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
                <div class="modal-dialog modal-full-height modal-right" role="document">

                    <div class="modal-content">
                        <div class="modal-header bg-slate">
                            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Task</h6>
                        </div>
                        <div class="modal-body" style="height: 371px;overflow-y: scroll;">
                           
                            {!! Form::open(['route'=>'supportticket.store','method'=>'POST','id'=>'supportticket_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!} 
            
                            <fieldset class="mb-1">
                                
                                {{ Form::hidden('task_lead', '1') }}

                                 {{ Form::hidden('contact_id',  $contact_profile->id) }}

                                <div class="form-group">
                                    <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-pencil"></i></span>
                                      </span>
                                        {!! Form::text('ticket_name', $value = null, ['id'=>'ticket_name','placeholder'=>'Enter Your Task','class'=>'form-control']) !!}
                                    </div>
                                </div>
  
                                <div class="form-group">
                                        {!! Form::select('ticket_type',$ticket_type, $value = null, ['placeholder'=>'--Select Task Type--','class'=>'form-control']) !!}
                                </div>

                                  <div class="form-group">
                                        {!! Form::select('assigned_dept',[ 'marketing'=>'Marketing Dept.'], $value = null, ['placeholder'=>'--Assigned Department--','id'=>'assigned_dept','class'=>'form-control']) !!}
                                </div>

                                  <div class="form-group">
                                        {!! Form::select('assigned_to',$assigned_user, $value = null, ['placeholder'=>'--Assigned To--','id'=>'assigned_to','class'=>'form-control','required']) !!}
                                </div>

                                <div class="form-group">
                                        {!! Form::select('ticket_status',['Backlog'=>'Backlog','ToDo'=>'ToDo','InProgress'=>'InProgress','Completed'=>'Completed'], $value = null, ['placeholder'=>'--Task Status--','id'=>'ticket_status','class'=>'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-calendar"></i></span>
                                      </span>
                                        {!! Form::text('deadline', $value = null, ['id'=>'deadline','placeholder'=>'Task Deadline','class'=>'form-control daterange-single']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::textarea('message', $value = null, ['id'=>'message','placeholder'=>'Enter Message','class'=>'form-control']) !!}
                                </div>


                            </fieldset>
                            <div class="text-right">
                             
                              <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

                             <button onclick="submitForm(this);" type="submit" class="btn bg-success-400">Save Task </button>
                            </div>


                            {!! Form::close() !!}


                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <!-- Full Height Modal Right -->
<!--End of Side Popup For Task -->



<!--Side Popup For Meeting -->
 <!-- Full Height Modal Right -->
                <div class="modal modal_slide right" id="fullHeightModalHistoryRightMeeting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" data-direction='right'>

                <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
                <div class="modal-dialog modal-full-height modal-right" role="document">

                    <div class="modal-content">
                        <div class="modal-header bg-slate">
                            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Schedule</h6>
                        </div>
                        <div class="modal-body" style="height: 371px;overflow-y: scroll;">
                           
                            {!! Form::open(['route'=>'meeting.store','method'=>'POST','id'=>'meeting_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!} 
            
                            <fieldset class="mb-1">
                                
                                <div class="form-group">
                                    <div class="input-group">
                                     <span class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-pencil"></i></span>
                                      </span>
                                        {!! Form::text('meeting_about', $value = null, ['id'=>'meeting_about','placeholder'=>'What are you meeting about ?','class'=>'form-control','required']) !!}
                                    </div>
                                </div>

                               
                                {{ Form::hidden('contact_id',  $contact_profile->id) }}

 
                                <div class="form-group">
                                     <label>Select Attendee:</label>
                                        {!! Form::select('attendee[]',$assigned_user, $value = null, ['id'=>'attendee','class'=>'form-control multiselect','multiple'=>'multiple','data-fouc']) !!}
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-4">
                                         <div class="input-group">
                                             <span class="input-group-prepend">
                                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                                              </span>
                                            {!! Form::text('date', $value = null, ['id'=>'date','placeholder'=>'Date','class'=>'form-control daterange-single']) !!}
                                        </div>
                                    </div>
                                     <div class="col-lg-4">
                                            {!! Form::text('time', $value = null, ['id'=>'start-timepicker','placeholder'=>'Start Time','class'=>'form-control']) !!}
                                         </div>

                                     <div class="col-lg-4">
                                            {!! Form::text('duration', $value = null, ['id'=>'duration','placeholder'=>'Duration Eg. 15 Min or 1 hr','class'=>'form-control']) !!}

                                    </div>
                                </div>

                                <div class="form-group">

                                        {!! Form::textarea('description', $value = null, ['id'=>'description','placeholder'=>'Describe your Meeting...','class'=>'form-control']) !!}
                                </div>


                            </fieldset>
                            <div class="text-right">
                             
                              <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

                             <button onclick="submitForm(this);" type="submit" class="btn bg-success-400">Save Task </button>
                            </div>

 
                            {!! Form::close() !!}


                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <!-- Full Height Modal Right -->
<!--End of Side Popup For Meeting -->




<!--Side Popup For Deal -->
 <!-- Full Height Modal Right -->
                <div class="modal modal_slide right" id="fullHeightModalHistoryRightDeal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" data-direction='right'>

                <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
                <div class="modal-dialog modal-full-height modal-right" role="document">

                    <div class="modal-content">
                        <div class="modal-header bg-slate">
                            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Create Deal</h6>
                        </div>
                           

                            <div class="modal-body" style="height: 371px;overflow-y: scroll;">
                    {!! Form::open(['route'=>'dailyClient.updateStatus','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}

                    <div class="form-group row">
                        <label class="col-form-label col-lg-4">Deal Stage:</label>
                        <div class="col-lg-8">
                            {!! Form::select('service_required',['Hot Leads'=>'Hot Leads','Cold Leads'=>'Cold Leads','Sales Pipeline'=>'Sales Pipeline' , 'Converted' => 'Converted', 'Rejected' => 'Rejected'], $value = null, ['id'=>'service_required','class'=>'form-control','placeholder'=>'Select Service Required']) !!}
                        </div>
                    </div>

                    <div class="form-group row terminal_reason" style="display:none;">
                        <label class="col-form-label col-lg-4">Terminate Reason:</label>
                        <div class="col-lg-8">
                            {!! Form::textarea('terminate_reason', $value = null, ['id'=>'terminate_reason','placeholder'=>'Enter Terminate Reason','class'=>'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group row service_accept" style="display:none;">
                        <label class="col-form-label col-lg-4">Cost:</label>
                        <div class="col-lg-8">
                            {!! Form::text('cost', $value = $contact_profile->client_approach_final_cost, ['id'=>'cost','placeholder'=>'Enter Cost','class'=>'form-control numeric client_cost']) !!}
                        </div>
                    </div>

                    <div class="form-group row service_accept" style="display:none;">
                        <label class="col-form-label col-lg-4">Discount(in %):</label>
                        <div class="col-lg-8">
                            {!! Form::text('discount', $value = null, ['id'=>'discount','placeholder'=>'Enter Discount in %','class'=>'form-control numeric']) !!}
                        </div>
                    </div>

                    <div class="form-group row service_accept" style="display:none;">
                        <label class="col-form-label col-lg-4">Final Cost:</label>
                        <div class="col-lg-8">
                            {!! Form::text('final_cost', $value = null, ['id'=>'final_cost','placeholder'=>'Enter Final Cost','class'=>'form-control' ,'readonly']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group row service_accept" style="display:none;">
                        <label class="col-form-label col-lg-4">Services:</label>
                        <div class="col-lg-8">
                            <select class="form-control multiselect" name="active_lead_services[]" multiple='multiple' data-fouc>
                                @foreach($active_services as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="form-group row service_accept" style="display:none;">
                        <label class="col-form-label col-lg-4">Additional Note:</label>
                        <div class="col-lg-8">
                            {!! Form::textarea('additional_note', $value = null, ['id'=>'additional_note','placeholder'=>'Enter Additional Note','class'=>'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group row service_accept" style="display:none;">
                        <label class="col-form-label col-lg-4">Client Expectation Note:</label>
                        <div class="col-lg-8">
                            {!! Form::textarea('client_expectation_note', $value = null, ['id'=>'client_expectation_note','placeholder'=>'Enter Client Expectation Note','class'=>'form-control']) !!}
                        </div>
                    </div>

                    {{ Form::hidden('client_id',  $contact_profile->id) }}
                     {{ Form::hidden('deal_lead', '1') }}
                 
                            <div class="text-right">
                             
                              <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

                             <button onclick="submitForm(this);" type="submit" class="btn bg-success-400">Update Deal </button>
                            </div>

 
                            {!! Form::close() !!}


                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <!-- Full Height Modal Right -->
<!--End of Side Popup For Deal -->



<!--Side Popup For Add Company -->
 <!-- Full Height Modal Right -->
                <div class="modal modal_slide right" id="fullHeightModalHistoryRightCompany" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" data-direction='right'>

                <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
                <div class="modal-dialog modal-full-height modal-right" role="document">

                    <div class="modal-content">
                        <div class="modal-header bg-slate">
                            <h6 class="modal-title"><i class="icon-pencil mr-2"></i>Associate Contact With Following Company</h6>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                           

                <div class="modal-body" style="height: 371px;overflow-y: scroll;">
                    {!! Form::open(['route'=>'dailyClient.associateContact','method'=>'POST','class'=>'form-horizontal','role'=>'form','files' => true]) !!}

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Select Company:</label>
                        <div class="col-lg-9">
                           <select name="lead_id" class="form-control select-search" data-fouc="" >
                                     <option>--Select Company--</option>
                                     @foreach ($companies as $id => $company)
                                        <option value="{{ $id }}">{{ $company }}</option>
                                     @endforeach
                                 </select>
                        </div>
                    </div>


                    {{ Form::hidden('contact_id',  $contact_profile->id) }}
                     {{ Form::hidden('associate_lead', 0) }}
                 
                            <div class="text-right">
                             
                              <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-btn">Close</button>

                             <button onclick="submitForm(this);" type="submit" class="btn bg-success-400">Save </button>
                            </div>

 
                            {!! Form::close() !!}


                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <!-- Full Height Modal Right -->
<!--End of Side Popup For Company -->


<div id="modal_theme_warning" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-body">
                <center>
                    <i class="icon-alert text-danger icon-3x"></i>
                </center>
                <br>
                <center>
                    <h2>Are You Sure Want To Delete ?</h2>
                     <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </center>
            </div>
        </div>
    </div>
</div>






<script type="text/javascript">
    
    $('document').ready(function() {
        
        $('#start-timepicker').clockTimePicker();

        $('#modal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        });

        var modal = document.getElementsByClassName('modal-backdrop');
        window.onclick = function(event) {
            if (event.target == modal[0]) {
                $('#close-btn').click();
            }
        }

         $(document).on('keyup', '#client_approach_discount, #client_approach_cost', function () {

            var cost = $('#client_approach_cost').val();
            var discount = $('#client_approach_discount').val();

            var final = cost - ((discount / 100) * cost);

            $('#client_approach_final_cost').val(final);
        });


         $('.delete_meeting').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });

         $('.delete_note').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });

         $(document).on('keyup', '#discount, #cost', function () {

                var cost = $('#cost').val();
                var discount = $('#discount').val();

                var final = cost - ((discount / 100) * cost);

                $('#final_cost').val(final);
            });

         $(document).on('change', '#service_required', function () {
                var service_required = $(this).val();

                if (service_required == 'Rejected') {
                    $('.terminal_reason').show();
                    $('.service_accept').hide();
                } else {
                    $('.terminal_reason').hide();
                    $('.service_accept').show();
                }
            });

            $(document).on('click', '.editContact', function () {
               
                $('.edit_contact').attr('readonly', false); 
                $('.edit_select').attr('disabled', false); 
                $('.edit_contact').removeClass('text-grey'); 
                $('.edit_contact').addClass('text-dark'); 
              
             });
   
    });

</script>

<style>

    .activity {
        position: relative;
        padding-left: 2rem;
        margin-left: 1rem;
    }
    .activity::before {
        content: "";
        width: 2px;
        background-color: #d6d6d6;
        height: calc(100% - 40px);
        position: absolute;
        left: 0;
        top: 20px;
    }

    .activity-item {
        background-color: #f9f9f9;
        border-top: 0;
        border-bottom: 0;
        border-right: 0;
        position: relative;
        box-shadow: 0 1px 3px 0 #ccc;
    }

    .activity-item::before {
        content: "";
        width: 18px;
        height: 18px;
        border-radius: 50%;
        background-color: #24a9ee;
        position: absolute;
        left: -43px;
        top: 12px;
    }

    .circle-28 {
        width: 28px;
        height: 28px;
    }

    .circle-28 span {
        display: block;
        line-height: 1.4;
    }

.modal_slide{
    display: flex !important;
    width: 500px !important;
    left: unset;
    right: -500px;
    transition: 0.5s ease-in-out right;
}
.modal_slide .modal-full-height { 
    display: flex;
    margin: 0;
    width: 100%;
    height: 100%;
}
.modal_slide.show{
    right: 0;
    transition: 0.5s ease-in-out right;
}
.modal_slide .modal-dialog .modal-content{
    border-radius: 0;
    border: 0;
}

</style>
@endsection