@extends('admin::layout')
@section('title')Contact @stop
@section('breadcrum')Contact @stop

@section('script')
<script src="{{ asset('admin/validation/contact.js') }}"></script>
@stop

@section('content')
<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-teal-400 header-elements-inline">
        <h5 class="card-title">Create Contact</h5>
        <div class="header-elements">
        </div>
    </div>


    <div class="card-body">
        {!! Form::open(['route'=>'contact.store', 'method'=>'POST','class'=>'form-horizontal','id' => 'contact_submit','files' => true]) !!}
        
        @include('contact::contact.partial.action',['btnType'=>'Save']) 
        
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@endsection
