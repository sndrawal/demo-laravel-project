<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Salutation:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-hat"></i>
                                </span>
                            </span>
                    {!! Form::select('salutation',$salutation, $value = null, ['id'=>'salutation_title','placeholder'=>'Select Salutation Title','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        {{ Form::hidden('profile_lead', '0') }}

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">First Name:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i>
                                </span>
                            </span>
                    {!! Form::text('first_name', null, ['placeholder'=>'Enter First Name','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Middle Name:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i>
                                </span>
                            </span>
                     {!! Form::text('middle_name', null, ['placeholder'=>'Enter Middle Name','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Last Name:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pencil"></i>
                                </span>
                            </span>
                    {!! Form::text('last_name', null, ['placeholder'=>'Enter Last Name','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

     <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Contact Type:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-notebook"></i>
                                </span>
                            </span>
                     {!! Form::select('contact_type',$contact_type, $value = null, [ 'class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Mobile No.:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-phone2"></i>
                                </span>
                            </span>
                    {!! Form::text('mobile_no', null, ['placeholder'=>'Enter Mobile No.','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

     <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Email:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-envelop3"></i>
                                </span>
                            </span>
                     {!! Form::email('email', null, ['placeholder'=>'Enter Email','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Designation:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-vcard"></i>
                                </span>
                            </span>
                    {!! Form::text('designation', null, ['placeholder'=>'Enter Designation','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Company:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-office"></i>
                                </span>
                            </span>
                     {!! Form::text('company', null, ['placeholder'=>'Enter Company','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Website:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-browser"></i>
                                </span>
                            </span>
                    {!! Form::text('website', null, ['placeholder'=>'Enter Website','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Address:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-location4"></i>
                                </span>
                            </span>
                      {!! Form::text('address', null, ['placeholder'=>'Enter Address','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Municipality/Vdc:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-browser"></i>
                                </span>
                            </span>
                    {!! Form::text('municipality_vdc', $value = null, ['id'=>'municipality_vdc','placeholder'=>'Enter Municipality/Vdc','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">City:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-location3"></i>
                                </span>
                            </span>
                      {!! Form::text('city', $value = null, ['id'=>'city','placeholder'=>'Enter City','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">District:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-location4"></i>
                                </span>
                            </span>
                     {!! Form::select('district',$district, $value = null, ['id'=>'district','placeholder'=>'Select District','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">State/Province:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-direction"></i>
                                </span>
                            </span>
                      {!! Form::select('state',$state, $value = null, ['id'=>'state','placeholder'=>'Select State/Province','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Country:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-compass4"></i>
                                </span>
                            </span>
                     {!! Form::select('country_id',$countries, $value = 156, ['id'=>'country_id','placeholder'=>'Select Country','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Segment:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                      {!! Form::select('segment',$segment, $value = null, ['id'=>'segment','placeholder'=>'Select Segment','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Lead Source:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-pen"></i>
                                </span>
                            </span>
                     {!! Form::text('lead_source', null, ['placeholder'=>'Enter Lead Source','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

     <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Image:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-images3"></i>
                                </span>
                            </span>
                      {!! Form::file('image', ['id'=>'image','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Additional Info:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-notebook"></i>
                                </span>
                            </span>
                     {!! Form::textarea('additional_info', null, ['id' => 'additional_info','placeholder'=>'Enter Additional Info', 'class' =>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

</fieldset>


<div class="text-right">
    <button type="submit" id="type_submit" class="btn bg-teal-400">{{ $btnType }} <i class="icon-database-insert"></i></button>
</div>