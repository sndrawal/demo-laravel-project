<div class="form-group row">
    <div class="col-lg-9">
         {!! Form::select('contact_type_id',$contact_type, $value = null, ['id'=>'contact_type_id','placeholder'=>'Select Contact Type','class'=>'form-control']) !!}
    </div>
    <div class="col-lg-3">
        <button type="submit" class="btn bg-teal-400"><i class="icon-search4"></i></button>
    </div>
</div>