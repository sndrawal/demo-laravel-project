@extends('admin::layout')
@section('title')Contacts @stop
@section('breadcrum')Contacts @stop

@section('script')
    <script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
@stop

@section('content')


{{-- Start of Advance Filter Shyam --}}
    <div class="card">
        <div class="bg-warning card-header header-elements-inline border-bottom-0">
            <h5 class="card-title text-uppercase font-weight-semibold">Advance Filter</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
        {!! Form::open(['route' => 'contact.index', 'method' => 'get']) !!}
        <div class="row">
           <div class="col-md-3">
                <label class="d-block font-weight-semibold">From  - To Date:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar2"></i>
                                </span>
                            </span>
                   {!! Form::text('search_from_to', $value = null, ['id'=>'search_from','placeholder'=>'Search From','class'=>'form-control form-control-lg  daterange-buttons']) !!}
                </div>
            </div>

            <div class="col-md-3">
                <label class="d-block font-weight-semibold">Designation:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-users"></i>
                                </span>
                            </span>
                    {!! Form::text('designation', $value = null, ['id'=>'designation','placeholder'=>'Search Via Designation','class'=>'form-control form-control-lg']) !!}
                </div>
            </div>

            <div class="col-md-3">
                <label class="d-block font-weight-semibold">Contact Type:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-collaboration"></i>
                                </span>
                            </span>
                     {!! Form::select('contact_type',$contact_type, $value = null, ['placeholder'=>'Select Contact Type', 'class'=>'form-control']) !!}
                </div>
            </div>

            <div class="col-md-3 mt-2">
                <label class="d-block font-weight-semibold">Mobile Number:</label>
                <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-mobile"></i>
                                </span>
                            </span>
                    {!! Form::text('mobile_no', $value = null, ['id'=>'mobile_no','placeholder'=>'Search Via Mobile No.','class'=>'form-control form-control-lg']) !!}
                </div>
            </div>

            <div class="col-md-3 mt-2">
                <label class="d-block font-weight-semibold">Company Name:</label>
                <div class="input-group">
                     <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-city"></i>
                                </span>
                            </span>
                    {!! Form::text('company', $value = null, ['id'=>'company','placeholder'=>'Search Via Company','class'=>'form-control form-control-lg']) !!}
                </div>
            </div>

             <div class="col-md-3 mt-2">
                <label class="d-block font-weight-semibold">Contact Person:</label>
                <div class="input-group">
                     <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                    {!! Form::text('name', $value = null, ['id'=>'name','placeholder'=>'Search Via Person','class'=>'form-control form-control-lg']) !!}
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-end mt-2">
            <button class="btn bg-primary" type="submit">
                Search Now
            </button>
            <a href="{{ route('contact.index') }}" data-popup="tooltip" data-placement="top" data-original-title="Refresh Search" class="btn bg-danger ml-2">
                <i class="icon-spinner9"></i>
            </a>
        </div>
        {!! Form::close() !!}
        </div>
</div>

{{-- End of Advance Filter Shyam --}}


    <div class="card" style="border: 1px solid cadetblue;">
        <div class="card-header bg-grey-300 header-elements-inline">
            <a href="{{ route('contact.create') }}" class="btn bg-teal-400 btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-pen-plus"></i></b> Add Contact
               </a>
            <a href="{{ route('contact.downloadSheet') }}" class="btn bg-purple-400 btn-labeled btn-labeled-left" style="float: right"><b><i class="icon-download4"></i></b> Download Xls
            </a>
        </div>
    </div>


<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">List of Contacts</h5>
    </div>
    <div class="card-body">  
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="bg-slate">
                    <th>#</th>
                    <th>Image</th>
                    <th>Contact Name</th>
                    <th>Contact Type</th>
                    <th>Designation</th>
                    <th>Mobile No.</th>
                    <th>Company</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($contacts->total() != 0)
                @foreach ($contacts as $contact)
                <tr>
                    <td>{{ ++$loop->index }}</td>
                    <td>

                      
                          @if(!is_null($contact->image) && file_exists(public_path($contact->file_full_path.'/'.$contact->image)))
                                 <a target="_blank" href="{{ asset($contact->file_full_path).'/'.$contact->image }}"><img src="{{ asset($contact->file_full_path).'/'.$contact->image }}" style="width: 30px;"></a>

                                 @else 
                                  <a target="_blank" href="{{ asset('admin/default.png')}}"><img src="{{ asset('admin/default.png') }}" style="width: 50px;"></a>
                        @endif

                       

                    </td>
                    <td><a href="{{route('contact.profile',['id'=>$contact->id])}}">{{ $contact->first_name.' '.$contact->middle_name.' '.$contact->last_name }}</a></td>
                    <td>{{ $contact->dropdown['dropvalue'] }}</td>
                    <td>{{ strip_tags($contact->designation) }}</td>
                    <td>{{ strip_tags($contact->mobile_no) }}</td>
                    <td>{{ strip_tags($contact->company) }}</td>
 
                    <td>
                        <a class="btn bg-{{ config('admin.color-class.edit') }} btn-icon rounded-round" href="{{ route('contact.edit', $contact->id) }}" data-popup="tooltip" data-placement="bottom" data-original-title="Edit">
                            <i class="icon-pencil6"></i>
                        </a>

                        <a data-toggle="modal" data-target="#modal_theme_warning" class="btn bg-{{ config('admin.color-class.delete') }} btn-icon rounded-round delete_contact" link="{{ route('contact.delete', $contact->id) }}" data-placement="bottom" data-popup="tooltip" data-original-title="Delete">
                            <i class="icon-bin"></i>
                        </a>

                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="8">No Contact Found !!!</td>
                </tr>
                @endif
            </tbody>
        </table>
        <span style="margin: 5px;float: right;">
            @if($contacts->total() != 0)
            {{ $contacts->links() }}
            @endif
        </span>
    </div>
    </div>
</div>

<!-- Warning modal -->
<div id="modal_theme_warning" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h6 class="modal-title">Are you sure to Delete Contact ?</h6>
            </div>

            <div class="modal-body">
                <a class="btn btn-success get_link" href="">Yes</a> &nbsp; | &nbsp;
                <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- /warning modal -->
<script type="text/javascript">
    $('document').ready(function() {
        $('.delete_contact').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });
    });
</script>

@endsection
