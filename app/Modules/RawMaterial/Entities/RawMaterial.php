<?php

namespace App\Modules\RawMaterial\Entities;

use Illuminate\Database\Eloquent\Model;

class RawMaterial extends Model
{
    protected $fillable = [

    	'name',
    	'unit'

    ];

    protected $hidden=[
        'created_at',
        'updated_at',


    ];
}
