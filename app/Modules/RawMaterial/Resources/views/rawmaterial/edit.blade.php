@extends('admin::layout')
@section('title')Raw Material @stop 
@section('breadcrum')Edit Raw Material @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<!-- /theme JS files -->

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Edit Raw Material</h5>
        <div class="header-elements">

        </div>
    </div>

    <div class="card-body">



                {!! Form::model($raw_material,['method'=>'PUT','route'=>['rawmaterial.update',$raw_material->id],'class'=>'form-horizontal','id'=>'defaultnorms_submit','role'=>'form','files'=>true]) !!} @include('rawmaterial::rawmaterial.partial.action',['btnType'=>'Update']) {!! Form::close() !!}

        
    </div>
</div>
<!-- /form inputs -->

@stop