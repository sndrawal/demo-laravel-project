<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/validation/raw_material.js') }}"></script>
<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

    <div class="row">

        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-form-label col-lg-4">Raw Material:</label>
                    <div class="col-lg-8">
                       <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-box"></i></span>
                        </span>
                        {!! Form::text('name', $value = null, ['placeholder'=>'Enter Raw Material','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

    <div class="col-md-6">

        <div class="form-group row">
            <label class="col-form-label col-lg-4">Unit:</label>
                <div class="col-lg-8">
                   <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-balance"></i></span>
                    </span>
                    {!! Form::text('unit', $value = null, ['placeholder'=>'Enter Unit','class'=>'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>

</fieldset>


<div class="text-right"> 
    <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b> {{ $btnType }}</button>
</div>