<?php 
namespace App\Modules\RawMaterial\Repositories;

use App\Modules\RawMaterial\Entities\RawMaterial;

class RawMaterialRepository implements RawMaterialInterface
{
    
    
    public function findAll($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'ASC'], $status = [0, 1])
    {
        
        $result =RawMaterial::when(array_keys($filter, true), function ($query) use ($filter) {

        })
            
            ->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result; 
    }
    
    public function find($id){
        return RawMaterial::find($id);
    }
    
    public function getList(){  
        $RawMaterial = RawMaterial::pluck('name', 'id');
        return $RawMaterial;
    }

    public function save($data){
        return RawMaterial::create($data);
    }
    
    public function update($id,$data){
        $RawMaterial = RawMaterial::find($id);
        return $RawMaterial->update($data);
    }
    
    public function delete($id){
        return RawMaterial::destroy($id);
    }

    public function countTotal()
    {
        return RawMaterial::count();
    }


}