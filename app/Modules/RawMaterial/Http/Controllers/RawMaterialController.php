<?php

namespace App\Modules\RawMaterial\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

// For excel
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Respositories
use App\Modules\RawMaterial\Repositories\RawMaterialInterface;

class RawMaterialController extends Controller
{
    protected $raw_material;
    
    public function __construct(RawMaterialInterface $raw_material)
    {
        $this->raw_material = $raw_material;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $search = $request->all();
        $data['raw_material'] = $this->raw_material->findAll($limit= 50, $search); 
        $data['search_value']=$search;
        return view('rawmaterial::rawmaterial.index',$data);
    }


     public function upload(Request $request) 
    {
        $data = $request->all();
        if ($request->hasFile('upload_raw_material')) {
            $file_info = $request->upload_raw_material->getClientOriginalName();
            $extension = \File::extension($file_info);
            if ($extension == 'xlsx' || $extension == 'xlx') {
                $file_directory = public_path().'/uploads/uploads_xls/';
                $new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $extension;
                if(move_uploaded_file($_FILES['upload_raw_material']['tmp_name'], $file_directory . $new_file_name))
                {    
                    $inputFileName = $file_directory . $new_file_name;
                    $file_type = IOFactory::identify($inputFileName);
                    $objReader = IOFactory::createReader($file_type);

                    $objPHPExcel = $objReader->load($file_directory . $new_file_name);
                    $raw_material_data  = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

                    array_shift($raw_material_data);
                    
                    foreach ($raw_material_data as $key => $material) {
                        if ($material['A']) {
                         
                                $data = [
                                    'name' => $material['A'],
                                    'unit' => $material['B']
                                ];

                                try {
                                    $this->raw_material->save($data);
                                } catch (\Throwable $e) {
                                    toastr($e->getMessage())->error();
                                }
                            }
                    }
                }
                toastr()->success('Raw Material Excel Data Added Successfully');
                return redirect()->back();
            }
        }
        toastr()->error('Please Choose a Correct file');
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {  
        return view('rawmaterial::rawmaterial.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
         $data = $request->all();

         try{ 

            $this->raw_material->save($data); 
           
            toastr()->success('Raw Material Created Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('rawmaterial.index'));
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['raw_material'] = $this->raw_material->find($id);    
        return view('rawmaterial::rawmaterial.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

         try{ 
            
            $this->raw_material->update($id,$data);
            
            toastr()->success('Raw Material Updated Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('rawmaterial.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
       try{
            $this->raw_material->delete($id);
            toastr()->success('Raw Material Deleted Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        return redirect(route('rawmaterial.index'));  
    }


}
