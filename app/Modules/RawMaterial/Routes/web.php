<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {

        /*
        |--------------------------------------------------------------------------
        | Raw Material  CRUD ROUTE
        |--------------------------------------------------------------------------
        */
        Route::get('rawmaterial', ['as' => 'rawmaterial.index', 'uses' => 'RawMaterialController@index']);

        Route::post('rawmaterial/upload', ['as' => 'rawmaterial.upload', 'uses' => 'RawMaterialController@upload']);

        // Raw Material Create
        Route::get('rawmaterial/create', ['as' => 'rawmaterial.create', 'uses' => 'RawMaterialController@create']);
        Route::post('rawmaterial/store', ['as' => 'rawmaterial.store', 'uses' => 'RawMaterialController@store']);
        // Raw Material Edit
        Route::get('rawmaterial/edit/{id}', ['as' => 'rawmaterial.edit', 'uses' => 'RawMaterialController@edit'])->where('id','[0-9]+');
        Route::put('rawmaterial/update/{id}', ['as' => 'rawmaterial.update', 'uses' => 'RawMaterialController@update'])->where('id','[0-9]+');
        // Raw Material Delete
        Route::get('rawmaterial/delete/{id}', ['as' => 'rawmaterial.delete', 'uses' => 'RawMaterialController@destroy'])->where('id','[0-9]+');

});
