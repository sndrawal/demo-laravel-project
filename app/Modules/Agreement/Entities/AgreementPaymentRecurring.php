<?php

namespace App\Modules\Agreement\Entities;

use Illuminate\Database\Eloquent\Model;

class agreementPaymentRecurring extends Model
{
    protected $fillable = [

    	'agreement_id',
    	'recurring_payment',
    	'remind_before'

    ];
}
