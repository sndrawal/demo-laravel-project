<?php

namespace App\Modules\Agreement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Lead\Entities\Lead;

class Billing extends Model
{
    const FILE_PATH = '/uploads/billing/';

    protected $fillable = [

    	'agreement_id',
    	'lead_id',
    	'pan_vat',
    	'total_contract_amount',
    	'billing_amount',
    	'attached_invoice',
        'created_by',
        'updated_by',

    ];

    public function getFileFullPathAttribute()
    {
        return self::FILE_PATH . $this->file_name;
    }

    public function getLead(){
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

}
