<?php

namespace App\Modules\Agreement\Entities;

use Illuminate\Database\Eloquent\Model;

class agreementPaymentOneTime extends Model
{
    protected $fillable = [

    	'agreement_id',
    	'installment',
    	'percent',
    	'amount',
    	'payment_date'

    ];
}
