<?php

namespace App\Modules\Agreement\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Lead\Entities\Lead;
use App\Modules\User\Entities\User;

class agreement extends Model
{
    const FILE_PATH = '/uploads/agreement/';

    protected $fillable = [

        'lead_id',
    	'client_name',

        'agreement_from',
        'contact_no',
        'address',
        'email',
        'scan_copy',
        'notes',

    	'service',
    	'cost_of_project',
    	'payment_type',
    	'agreement_by',
    	'agreement_date',
    	'created_by',
    	'updated_by'

    ];

    public function getFileFullPathAttribute(){
        return self::FILE_PATH . $this->file_name;
    }

    public function getUser(){
        return $this->belongsTo(User::class,'created_by','id');
    }
    public function getUpdatedUser(){
        return $this->belongsTo(User::class,'updated_by','id');
    }

    public function getLead(){
        return $this->belongsTo(Lead::class,'lead_id','id');
    }
}
