<?php

namespace App\Modules\Agreement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use App\Modules\Lead\Repositories\DailyClientInterface;
use App\Modules\Agreement\Repositories\AgreementInterface;
use App\Modules\Lead\Repositories\ConvertedLeadInterface;

class AgreementController extends Controller
{
     protected $dailyclient;
     protected $agreement;
     protected $lead;

    
    public function __construct(DailyClientInterface $dailyclient,AgreementInterface $agreement, ConvertedLeadInterface $lead)
    {
        $this->agreement = $agreement;
        $this->lead = $lead;
        $this->dailyclient = $dailyclient;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $input = $request->all();

        if (array_key_exists('lead_id', $input)) {
            $lead_id = $input['lead_id'];
            $data['is_agree'] = TRUE;
            $data['lead_id'] = $lead_id;
            $data['agreement'] = $this->agreement->findAll($lead_id,$limit =50);
        }else{
            $data['agreement'] = $this->agreement->findAllAgreement($limit= 50,$input);
            $data['is_agree'] = FALSE;
        }

        return view('agreement::agreement.index',$data);
    }
     /**
     * Display a listing of the resource.
     * @return Response
     */
    public function dashboard()
    {
        $data['running_lead'] = $running_lead = $this->dailyclient->findConvertedLead($limit = 50);

        if($running_lead->total() != 0){
            return view('agreement::agreement.dashboard',$data);
        }else{
            alertify()->error('No Running Lead Found.');
            return redirect(route('dailyClient.index'));   
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        $data['is_edit']= '';
        $input = $request->all();
        
        if (array_key_exists('lead_id', $input)) {
            $data['is_agree'] = TRUE;
            $data['lead_id'] = $input['lead_id'];

            $lead_info = $this->dailyclient->find($input['lead_id']);
            $data['client_name'] = $lead_info->company_name;

        }else{
            $data['is_agree'] = FALSE;
        }

        return view('agreement::agreement.create',$data);
    }

    public function appendPayment(Request $request)
    {
        if($request->ajax()){
            $data = view('agreement::agreement.partial.one-time-payment-ajax')->render();
            return response()->json(['options'=>$data]);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
         $data = $request->all();

         $payment_type = $data['payment_type'];

          $userInfo = Auth::user();
          $user_id = $userInfo->id;

        try{ 

            $agreement_data = array(
                'client_name' => $data['client_name'],
                'service' => $data['service'],
                'cost_of_project' => $data['cost_of_project'],
                'payment_type' => $data['payment_type'],
                'agreement_by' => $user_id,
                'agreement_date' => date('Y-m-d'),
                'created_by' => $user_id,
            );

            if (array_key_exists('lead_id', $data)) {
                 $agreement_data['agreement_from'] = 'Lead';
                 $agreement_data['lead_id'] = $data['lead_id'];
            }else{
                $agreement_data['lead_id'] = 0;
                 $agreement_data['agreement_from'] = 'Direct';
                 $agreement_data['contact_no'] = $data['contact_no'];
                 $agreement_data['address'] = $data['address'];
                 $agreement_data['email'] = $data['email'];
                 $agreement_data['notes'] = $data['notes'];

                if ($request->hasFile('scan_copy')) {
                    $agreement_data['scan_copy'] = $this->agreement->upload($data['scan_copy']);
                }

            }

            $agreementInfo = $this->agreement->save($agreement_data);  
            $agreement_id = $agreementInfo['id'];

            if($payment_type == 'One-Time'){
                
                for ($i = 0; $i < sizeof($data['installment']); $i++) {
                    $oneTimePaymentData = array(
                    'agreement_id' => $agreement_id,
                    'installment' => $data['installment'][$i],
                    'percent' => $data['percent'][$i],
                    'amount' => $data['amount'][$i],
                    'payment_date' => $data['payment_date'][$i]
                    );     

                    $this->agreement->saveOneTimePayment($oneTimePaymentData);             
                }

            }else{

                    $recurringPaymentData = array(
                    'agreement_id' => $agreement_id,
                    'recurring_payment' => $data['recurring_payment'],
                    'remind_before' => $data['remind_before']
                    );     

                    $this->agreement->saveRecurringPayment($recurringPaymentData);             

            }


            alertify()->success('Lead Agreement Created Successfully');
        } catch(\Throwable $e) {
            alertify($e->getMessage())->error();
        }

        if (array_key_exists('lead_id', $data)) {
            $lead_id = $data['lead_id'];

            return redirect(route('agreement.index',['lead_id'=>$lead_id]));
        }else{

            return redirect(route('agreement.index'));
        }
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function view(Request $request)
    {
        $data = $request->all();
        
        $id = $data['id'];
        $agreement_info = $this->agreement->find($id);
        $payment_type = $agreement_info->payment_type;
        $agreement_from = $agreement_info->agreement_from;
        
        $html ='';
            $html .= "<table class='table table-striped mb30' id='table1' cellspacing='0' width='100%' frame='box'>";
            $html .= "<tbody>";
            $html .= "<tr>";
            $html .= "<td colspan='8'><h3>Running Lead Agreement</h3></td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td class='font-weight-bold'>Client Name : </td>";
            $html .= "<td>".$agreement_info->client_name."</td>";
            $html .= "<td class='font-weight-bold'>Service</td>";
            $html .= "<td>".$agreement_info->service ."</td>";

            $html .= "<td class='font-weight-bold'>Cost Of Project: </td>";
            $html .= "<td>Rs.".number_format($agreement_info->cost_of_project,2)."/-</td>";
            $html .= "<td class='font-weight-bold'>Payment Type : </td>";
            $html .= "<td>".$agreement_info->payment_type."</td>";
            $html .= "</tr>";

             if($agreement_from == 'Direct'){

            $image = ($agreement_info->scan_copy) ? asset($agreement_info->file_full_path).'/'.$agreement_info->scan_copy : '';

            $html .= "<tr>";
            $html .= "<td class='font-weight-bold'>Contact No. : </td>";
            $html .= "<td>".$agreement_info->contact_no."</td>";
            $html .= "<td class='font-weight-bold'>Address</td>";
            $html .= "<td>".$agreement_info->address ."</td>";

            $html .= "<td class='font-weight-bold'>Email: </td>";
            $html .= "<td>".$agreement_info->email."</td>";
            $html .= "<td class='font-weight-bold'>Notes : </td>";
            $html .= "<td>".$agreement_info->notes."</td>";
            $html .= "</tr>";

            $html .= "<tr>";
            $html .= "<td class='font-weight-bold'>Scan Image. : </td>";
            $html .= "<td><a target='_blank' href=".$image.">".$agreement_info->scan_copy."</</td>";
            $html .= "</tr>";
         

            }


            if($payment_type == 'One-Time'){

            $onetimePayment = $this->agreement->getAllOneTimePayment($id);  

                $html .= "<tr>";
                $html .= "<td colspan='8'><h3>One Time Payment</h3></td>";
                $html .= "</tr>";

                foreach ($onetimePayment as $key => $value) {
                  
                $html .= "<tr>";
                $html .= "<td class='font-weight-bold'>Installment : </td>";
                $html .= "<td>".$value->installment."</td>";
                $html .= "<td class='font-weight-bold'>Percent: </td>";
                $html .= "<td>".$value->percent." %</td>";  
                $html .= "<td class='font-weight-bold'>Amount : </td>";
                $html .= "<td>".number_format($value->amount,2)."/-</td>";
                $html .= "<td class='font-weight-bold'>Payment Date: </td>";
                $html .= "<td>".$value->payment_date."</td>";  
                $html .= "</tr>";

                }


            }else{

                $recurringPayment = $this->agreement->getAllRecurringPayment($id);  

                $html .= "<tr>";
                $html .= "<td colspan='8'><h3>Recurring Payment</h3></td>";
                $html .= "</tr>";
        
                $html .= "<tr>";
                $html .= "<td class='font-weight-bold'>Recurring Payment : </td>";
                $html .= "<td>".$recurringPayment->recurring_payment."</td>";
                $html .= "<td class='font-weight-bold'>Remind Before: </td>";
                $html .= "<td>".$recurringPayment->remind_before." day/s</td>";  
                $html .= "<td colspan='4'></td>";
                $html .= "</tr>";

            }
           
        
            $html .= "</tbody>";
            $html .= "</table>";

        return  $html;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request,$id)
    {
        $data['is_edit']= 'true';
        $input = $request->all();
        $data['lead_id'] =$input['lead_id'];

        $lead_info = $this->dailyclient->find($input['lead_id']);
        $data['client_name'] = $lead_info->company_name;

        $data['agreement_detail'] = $this->agreement->find($id);

        return view('agreement::agreement.edit',$data);

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $this->agreement->delete($id);
            $this->agreement->deleteOneTime($id);
            $this->agreement->deleteRecurring($id);
            alertify()->success('Agreement Deleted Successfully');
        }catch(\Throwable $e){
            alertify($e->getMessage())->error();
        }
        return redirect(route('agreement.index'));  
    }
}
  