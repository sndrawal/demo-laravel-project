<?php

namespace App\Modules\Agreement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use File;

use App\Modules\Lead\Repositories\DailyClientInterface;
use App\Modules\Agreement\Repositories\AgreementInterface;
use App\Modules\Agreement\Repositories\BillingInterface;

class BillingController extends Controller
{
     protected $agreement;
     protected $lead;
     protected $billing;
    
    public function __construct(DailyClientInterface $lead,AgreementInterface $agreement, BillingInterface $billing)
    {
        $this->agreement = $agreement;
        $this->lead = $lead;
        $this->billing = $billing;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $data['agreement_id'] = $agreement_id =$input['agreement_id'];
        $data['lead_id']  =$input['lead_id'];

        $data['billing_detail'] = $this->billing->findAll($agreement_id,$limit =50);

        return view('agreement::billing.index',$data);
    }
 
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        $data['is_edit']= '';
        $input = $request->all();

        $data['agreement_id'] =$input['agreement_id'];
        $data['lead_id'] =$input['lead_id'];

        $lead_info = $this->lead->find($input['lead_id']);
        $data['client_name'] = $lead_info->company_name;

        $agreement_info = $this->agreement->find($input['agreement_id']);
        $data['total_contract_amount'] = $agreement_info->cost_of_project;

        return view('agreement::billing.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
         $data = $request->all(); 

         unset($data['client_name']);
         unset($data['contract_amount']);

         $lead_id = $data['lead_id'];
         $agreement_id = $data['agreement_id'];

          $userInfo = Auth::user();
          $user_id = $userInfo->id;

        try{

            if ($request->hasFile('attached_invoice')) {
                $attached_invoice = $this->billing->uploadInvoice($data['attached_invoice']);
            }else{
                $attached_invoice = '';
            }

            $billing_data = array(
                'agreement_id' => $agreement_id,
                'lead_id' => $lead_id,
                'pan_vat' => $data['pan_vat'],
                'total_contract_amount' => $data['total_contract_amount'],
                'billing_amount' => $data['billing_amount'],
                'attached_invoice' => $attached_invoice,
                'created_by' => $user_id,
            );


            $this->billing->save($billing_data);  

            alertify()->success('Agreement Billing Created Successfully');
        } catch(\Throwable $e) {
            alertify($e->getMessage())->error();
        }

        return redirect(route('billing.index',['agreement_id'=>$agreement_id,'lead_id'=>$lead_id]));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function view(Request $request)
    {
        $data = $request->all();
        
        $id = $data['id'];
        $billing_info = $this->billing->find($id);
        
        $html ='';
            $html .= "<table class='table table-striped mb30' id='table1' cellspacing='0' width='100%' frame='box'>";
            $html .= "<tbody>";
            
            $html .= "<tr>";
            $html .= "<td class='font-weight-bold'>Client Name : </td>";
            $html .= "<td>".$billing_info->getLead->company_name."</td>";
            $html .= "<td class='font-weight-bold'>Client Address</td>";
            $html .= "<td>".$billing_info->getLead->client_address ."</td>";
            $html .= "<td class='font-weight-bold'>PAN/VAT No. : </td>";
            $html .= "<td>".$billing_info->pan_vat."</td>";
            $html .= "</tr>";
            
            $html .= "<td class='font-weight-bold'>Total Contract Amount : </td>";
            $html .= "<td>Rs.".number_format($billing_info->total_contract_amount,2) ."/-</td>";
            $html .= "<td class='font-weight-bold'>Billed Amount: </td>";
            $html .= "<td>Rs.".number_format($billing_info->billing_amount,2)."/-</td>";
            $html .= "<td class='font-weight-bold'>Attached Invoice : </td>";
            $html .= "<td><a target='_blank' href='".asset($billing_info->file_full_path).'/'.$billing_info->attached_invoice."'><img src='".asset($billing_info->file_full_path).'/'.$billing_info->attached_invoice."'></td>";
            $html .= "</tr>";
        
            $html .= "</tbody>";
            $html .= "</table>";

        return  $html;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request,$id)
    {
        $data['is_edit']= 'true';
        $input = $request->all();
        $data['lead_id'] =$input['lead_id'];

        $lead_info = $this->dailyclient->find($input['lead_id']);
        $data['client_name'] = $lead_info->company_name;

        $data['agreement_detail'] = $this->agreement->find($id);

        return view('agreement::agreement.edit',$data);

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
