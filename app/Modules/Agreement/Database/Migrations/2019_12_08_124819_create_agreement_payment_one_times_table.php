<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementPaymentOneTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_payment_one_times', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('agreement_id')->nullable();
            $table->text('installment')->nullable();
            $table->double('percent',8,2)->nullable();
            $table->double('amount',14,2)->nullable();
            $table->date('payment_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_payment_one_times');
    }
}