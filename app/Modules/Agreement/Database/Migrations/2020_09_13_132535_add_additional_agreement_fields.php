<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalAgreementFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agreements', function (Blueprint $table) {
            $table->string('agreement_from')->after('client_name')->nullable();
            $table->string('contact_no')->after('client_name')->nullable();
            $table->string('address')->after('client_name')->nullable();
            $table->string('email')->after('client_name')->nullable();
            $table->string('scan_copy')->after('client_name')->nullable();
            $table->text('notes')->after('client_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agreements', function (Blueprint $table) {
            $table->dropColumn('contact_no');
            $table->dropColumn('address');
            $table->dropColumn('email');
            $table->dropColumn('scan_copy');
            $table->dropColumn('notes');
        });
    }
}
