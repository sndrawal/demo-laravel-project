<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementPaymentRecurringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_payment_recurrings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('agreement_id')->nullable();
            $table->text('recurring_payment')->nullable();
            $table->integer('remind_before')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_payment_recurrings');
    }
}