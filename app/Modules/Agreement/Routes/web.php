<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'admin', 'middleware' => ['auth','web','permission']], function () {

        Route::get('agreement', ['as' => 'agreement.index', 'uses' => 'AgreementController@index']);
        Route::get('agreement/dashboard', ['as' => 'agreement.dashboard', 'uses' => 'AgreementController@dashboard']);
    
        Route::get('agreement/create', ['as' => 'agreement.create', 'uses' => 'AgreementController@create']);
        Route::post('agreement/store', ['as' => 'agreement.store', 'uses' => 'AgreementController@store']);
    
        Route::get('agreement/edit/{id}', ['as' => 'agreement.edit', 'uses' => 'AgreementController@edit'])->where('id','[0-9]+');
        Route::put('agreement/update/{id}', ['as' => 'agreement.update', 'uses' => 'AgreementController@update'])->where('id','[0-9]+');
    
        Route::get('agreement/delete/{id}', ['as' => 'agreement.delete', 'uses' => 'AgreementController@destroy'])->where('id','[0-9]+');

        Route::get('agreement/appendPayment', ['as' => 'agreement.appendPayment', 'uses' => 'AgreementController@appendPayment']);

        Route::get('agreement/view',['as' => 'agreement.view', 'uses' => 'AgreementController@view']);


		
		/* Billing modules */
        Route::get('billing', ['as' => 'billing.index', 'uses' => 'BillingController@index']);
        Route::get('billing/dashboard', ['as' => 'billing.dashboard', 'uses' => 'BillingController@dashboard']);
    
        Route::get('billing/create', ['as' => 'billing.create', 'uses' => 'BillingController@create']);
        Route::post('billing/store', ['as' => 'billing.store', 'uses' => 'BillingController@store']);
    
        Route::get('billing/edit/{id}', ['as' => 'billing.edit', 'uses' => 'BillingController@edit'])->where('id','[0-9]+');
        Route::put('billing/update/{id}', ['as' => 'billing.update', 'uses' => 'BillingController@update'])->where('id','[0-9]+');
    
        Route::get('billing/delete/{id}', ['as' => 'billing.delete', 'uses' => 'BillingController@destroy'])->where('id','[0-9]+');

        Route::get('billing/view',['as' => 'billing.view', 'uses' => 'BillingController@view']);

});
