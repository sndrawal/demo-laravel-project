<?php 
namespace App\Modules\Agreement\Repositories;


use App\Modules\Agreement\Entities\Agreement;
use App\Modules\Agreement\Entities\AgreementPaymentOneTime;
use App\Modules\Agreement\Entities\AgreementPaymentRecurring;
use App\Modules\Lead\Entities\Lead;

class AgreementRepository implements AgreementInterface
{
    
    public function findAll($lead_id,$limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result = Agreement::where('lead_id','=',$lead_id)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;     
    }

    public function findAllAgreement($limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result = Agreement::orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;     
    }
    
    public function find($id){
        return Agreement::find($id);
    }
    
     public function getAll($filter = []){
        $result = Agreement::get();
        return $result;
   }
    
   public function getList(){
       $Lead = Lead::where('status','=','4')->pluck('client_name', 'id');
       return $Lead;
   }
    
    public function save($data){
        return Agreement::create($data);
    }
    
    public function update($id,$data){
        $Agreement = Agreement::find($id);
        return $Agreement->update($data);
    }
    
    public function delete($id){
        return Agreement::destroy($id);
    }

    public function deleteOneTime($id){
         return AgreementPaymentOneTime::where('agreement_id','=',$id)->delete();
    }

    public function deleteRecurring($id){
        return AgreementPaymentRecurring::where('agreement_id','=',$id)->delete();
    }

    public function countTotalAgreement($id)
    {
        if($id){
            return Agreement::where('created_by','=',$id)->count();
        }else{
            return Agreement::count();
        } 
    }

    public function saveOneTimePayment($data){
        return AgreementPaymentOneTime::create($data);
    }
    
    public function saveRecurringPayment($data){
         return AgreementPaymentRecurring::create($data);   
    }

    public function getAllOneTimePayment($id,$filter = []){
        return AgreementPaymentOneTime::where('agreement_id','=',$id)->get();
   }

   public function getAllRecurringPayment($id,$filter = []){
        return AgreementPaymentRecurring::where('agreement_id','=',$id)->get()->first();
   }

    public function upload($file){
        $imageName = $file->getClientOriginalName();
        $fileName = date('Y-m-d-h-i-s') . '-' . preg_replace('[ ]', '-', $imageName);

        $file->move(public_path() . Agreement::FILE_PATH, $fileName);

        return $fileName;
    }
}