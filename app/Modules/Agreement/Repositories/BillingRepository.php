<?php 
namespace App\Modules\Agreement\Repositories;


use App\Modules\Agreement\Entities\Billing;
use App\Modules\Lead\Entities\Lead;

class BillingRepository implements BillingInterface
{
    
    public function findAll($agreement_id,$limit = null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1])
    {
        $result = Billing::where('agreement_id','=',$agreement_id)->orderBy($sort['by'], $sort['sort'])->paginate($limit ? $limit : env('DEF_PAGE_LIMIT', 9999));
        return $result;     
    }
    
    public function find($id){
        return Billing::find($id);
    }
    
     public function getAll($filter = []){
        $result = Billing::get();
        return $result;
   }
    
   public function getList(){
       $Lead = Lead::where('status','=','4')->pluck('client_name', 'id');
       return $Lead;
   }
    
    public function save($data){
        return Billing::create($data);
    }
    
    public function update($id,$data){
        $Billing = Billing::find($id);
        return $Billing->update($data);
    }
    
    public function delete($id){
        return Billing::destroy($id);
    }

    public function uploadInvoice($file){

        $imageName = $file->getClientOriginalName();
        $fileName = date('Y-m-d-h-i-s') . '-' . preg_replace('[ ]', '-', $imageName);

        $file->move(public_path() . Billing::FILE_PATH, $fileName);

        return $fileName;
    }

    public function totalBilledAmount()
    {
        $result =  Billing::get();

        $total_billed_amount = $result->sum('billing_amount');
        $total_contract_amount = $result->sum('total_contract_amount');

        return array('total_billed_amount'=>$total_billed_amount,'total_contract_amount'=>$total_contract_amount); 
    }

}