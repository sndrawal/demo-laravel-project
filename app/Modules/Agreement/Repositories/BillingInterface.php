<?php

namespace App\Modules\Agreement\Repositories;


interface BillingInterface
{
     public function findAll($agreement_id,$limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function getAll();
    
    public function getList();
    
    public function save($data);

    public function update($id,$data);

    public function delete($id);

    public function uploadInvoice($filename);

    public function totalBilledAmount();

}