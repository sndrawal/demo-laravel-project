<?php

namespace App\Modules\Agreement\Repositories;


interface AgreementInterface
{
     public function findAll($lead_id,$limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

     public function findAllAgreement($limit=null, $filter = [], $sort = ['by' => 'id', 'sort' => 'DESC'], $status = [0, 1]);

    public function find($id);
    
    public function getAll();
    
    public function getList();
    
    public function save($data);

    public function update($id,$data);

    public function delete($id);
    public function deleteOneTime($id);
    public function deleteRecurring($id);
  

    public function countTotalAgreement($id);
    
    public function saveOneTimePayment($data);
    
    public function saveRecurringPayment($data);

    public function getAllOneTimePayment($id,$filter = []);

    public function getAllRecurringPayment($id,$filter = []);	

    public function upload($file_name);

}