 @extends('admin::layout')
@section('title')Agreement Billing @stop 
@section('breadcrum')Create Agreement Billing @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
<!-- /theme JS files -->
<script src="{{ asset('admin/validation/billing.js')}}"></script>

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800 header-elements-inline">
        <h5 class="card-title">Create Agreement Billing of {{ $client_name }}</h5>
        <div class="header-elements">

        </div>
    </div>
    
    <div class="card-body">

        {!! Form::open(['route'=>'billing.store','method'=>'POST','id'=>'billing_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!}
        
            @include('agreement::billing.partial.action',['btnType'=>'Save']) 
        
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@stop