@extends('admin::layout') 
@section('title')Billing @stop
@section('breadcrum')Billing @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
@stop

@section('content')

<div class="card">
    <div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">
        <a href="{{ route('billing.create',['agreement_id'=>$agreement_id,'lead_id'=>$lead_id]) }}" class="btn bg-teal-400 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Add Billing</a>

        <a href="{{ route('agreement.index',['lead_id'=>$lead_id]) }}" class="btn bg-violet-400 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Back To Agreement</a>
    </div>
</div>

<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">List of Billing</h5>

    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="bg-slate">
                    <th>#</th>
                    <th>Client Name</th>
                    <th>Client Address</th>
                    <th>Pan/Vat</th>
                    <th>Contract Amount</th>
                    <th>Billed Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($billing_detail->total() != 0) 
                @foreach($billing_detail as $key => $value)
                <tr>
                    <td>{{$billing_detail->firstItem() +$key}}</td>
                    <td>{{ $value->getLead->company_name }}</td>
                    <td>{{ $value->getLead->client_address }}</td>
                    <td>{{ $value->pan_vat }}</td>
                    <td>{{ $value->total_contract_amount }}</td>
                    <td>{{ $value->billing_amount }}</td>
                    <td>

                        <a data-toggle="modal" data-target="#modal_billing_info" class="btn bg-purple-400 btn-icon rounded-round view_billing" billing_id="{{ $value->id }}" data-popup="tooltip" data-original-title="View Lead Agreement" data-placement="bottom"><i class="icon-eye"></i></a>

                    </td>
                </tr>
                @endforeach @else
                <tr>
                    <td colspan="7">No Billing Found !!!</td>
                </tr>
                @endif
            </tbody>
        </table>
        
        <span style="margin: 5px;float: right;">
            @if($billing_detail->total() != 0) 
            {{ $billing_detail->links() }}
            @endif
        </span>
        
    </div>
</div>


<!-- Warning modal -->
    <div id="modal_billing_info" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h6 class="modal-title">View Agreement Billing</h6>
                </div>

                <div class="modal-body">
                    <div class="table-responsive result_view_detail">

                    </div><!-- table-responsive -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn bg-teal-400" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- warning modal -->
<!-- warning modal -->


<script type="text/javascript">
    $('.view_billing').on('click',function(){ 
        var billing_id = $(this).attr('billing_id');

        $.ajax({
            type: 'GET',
            // dataType: 'HTML',
            url: 'billing/view',
            data: { id: billing_id },

            success: function (data) { 
                $('.result_view_detail').html(data);
            }
            }); 
    });
</script>

@endsection