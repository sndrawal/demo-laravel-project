<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Client Name:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                    {!! Form::text('client_name', $value = $client_name, ['id'=>'client_name','placeholder'=>'Enter Client Name','class'=>'form-control','readonly']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
           <div class="row">
                <label class="col-form-label col-lg-3">PAN/VAT:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                    {!! Form::text('pan_vat', $value = null, ['id'=>'pan_vat','placeholder'=>'Enter PAN/VAT','class'=>'form-control','required']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Total Contracted Amount:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cash4"></i>
                                </span>
                            </span>
                    {!! Form::text('contract_amount', $value = 'Rs.'.number_format($total_contract_amount), ['id'=>'contract_amount','placeholder'=>'Rs.00','class'=>'form-control numeric','readonly']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Billed Amount:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-tree7"></i>
                                </span>
                            </span>
                            {!! Form::text('billing_amount', $value = null, ['id'=>'billing_amount','placeholder'=>'Enter Billing Amount','class'=>'form-control numeric','required']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-6">
            <div class="form-group row">
                <label class="col-form-label col-lg-3">Attached Invoice:</label>
                <div class="col-lg-9">
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-image2"></i></span>
                        </span>
                        {!! Form::file('attached_invoice', ['id'=>'attached_invoice','class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-lg-3">
                </div>
                <div class="col-lg-9">
                    @if($is_edit && $project->attached_invoice)
                    <img id="bannerImage" src="{{asset($project->file_full_path)}}uploads/billing/{{ $project->attached_invoice}}" alt="your image" class="preview-image mt-2 " style="height: 100px;width: auto;"/>
                    @else
                    <img id="bannerImage" src="{{ asset('admin/default_invoice.png') }}" alt="your Invoice"
                    class="preview-image mt-2 " style="height: 100px; width: auto;"/>
                    @endif
                </div>
            </div>
        </div>
    </div>


    {{ Form::hidden('lead_id', $lead_id, array('class' => 'lead_id')) }}
    {{ Form::hidden('agreement_id', $agreement_id, array('class' => 'agreement_id')) }}
    {{ Form::hidden('total_contract_amount', $total_contract_amount, array('class' => 'total_contract_amount')) }}

</fieldset>


<div class="text-right">
    <button type="submit"  id="contact_un_checked" class="btn bg-teal-400">{{ $btnType }} <i class="icon-database-insert"></i></button>
</div>


<script type="text/javascript">
    $(document).ready(function(){

        $('#payment_type').on('change',function(){
            var payment_type = $(this).val();
            if(payment_type == 'One-Time'){
                $('.one_time').show();
                $('.recurring').hide();
            }

            if(payment_type == 'Recurring'){
                $('.one_time').hide();
                $('.recurring').show();
            }         
        });

     $('.add_more_payment').on('click',function(){

        $.ajax({
                type: 'GET',
                url: '/admin/agreement/appendPayment',
                success: function (data) {
                    $('.appendOneTimePayment').last().append(data.options);
                    $('.daterange-single').daterangepicker({
                        singleDatePicker: true,
                        locale: {
                            format: 'YYYY-MM-DD'
                        }

                    });

                }
            });

     });   

    });
</script>


