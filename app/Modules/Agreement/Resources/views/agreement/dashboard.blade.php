@extends('admin::layout')

@section('title') Lead Agreement Dashboard @stop 
@section('breadcrum') Lead Agreement Dashboard @stop 

@section('content') 

<section class="project-dtl">
    <div class="row">
        <div class="col-md-12">
            <div class="card bd-card">
                <div class="bg-slate-600 card-header header-elements-inline border-bottom-0">
                    <h5 class="card-title text-uppercase font-weight-semibold">Agreement Lead</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        @if($running_lead)
                        @php

                        $gradient = array('0'=>'bg-gradient-blue','1'=>'bg-gradient-green-3','2'=>'bg-gradient-dark','3'=>'bg-gradient-red-2','4'=>'bg-gradient-green-2','5'=>'bg-gradient-brown','6'=>'bg-gradient-red')

                        @endphp
                             @foreach($running_lead as $key => $value)

                             @php
                             $length = strlen($value->company_name);
                             if($length > 21){
                                $more = '...';
                             }else{
                                $more = '';
                             }
                             @endphp
                                    <div class="col-md-6 col-lg-3">
                                        <a href="{{ route('agreement.index',['lead_id'=>$value->id]) }}">
                                            <div class="card bd-card p-4 {{ $gradient[0] }}">
                                                <div class="text-center bd-card-info">
                                                    <h4 class="m-0 font-weight-semibold" data-popup="tooltip" data-placement="bottom" data-original-title="{{$value->company_name}}">{{ substr($value->company_name,0,21)}}{{ $more }}</h4>
                                                    <p>View All Agreement</p>
                                                    <i class="icon-folder icon-3x rounded-round pb-1"></i>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                         @endforeach
                        @endif   
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
