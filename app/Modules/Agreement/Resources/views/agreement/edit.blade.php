@extends('admin::layout')
@section('title')Lead Agreement @stop 
@section('breadcrum')Edit Lead Agreement @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
<!-- /theme JS files -->
<script src="{{ asset('admin/validation/agreement.js')}}"></script>

@stop 
@section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800 header-elements-inline">
        <h5 class="card-title">Edit Lead Agreement</h5>
        <div class="header-elements">
        </div>
    </div>

    <div class="card-body">

        {!! Form::model($agreement_detail,['method'=>'PUT','route'=>['agreement.update',$agreement_detail->id],'id'=>'agreement_submit','class'=>'form-horizontal','role'=>'form','files'=>true]) !!} @include('agreement::agreement.partial.action',['btnType'=>'Update']) {!! Form::close() !!}
        
    </div>
</div>
<!-- /form inputs -->

@stop