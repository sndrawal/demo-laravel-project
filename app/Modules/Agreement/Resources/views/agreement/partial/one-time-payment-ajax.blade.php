 <div class="form-group row">
        <div class="col-lg-3">
            <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                    {!! Form::text('installment[]', $value = null, ['id'=>'installment','placeholder'=>'Enter Installment','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            
                    {!! Form::text('percent[]', $value = null, ['id'=>'percent','placeholder'=>'Enter Percent','class'=>'form-control numeric']) !!}
                            <span class="input-group-prepend">
                                <span class="input-group-text">%
                                </span>
                            </span>

                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-3">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                    {!! Form::text('amount[]', $value = null, ['id'=>'amount','placeholder'=>'Enter Amount','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                    {!! Form::text('payment_date[]', $value = null, ['id'=>'payment_date','placeholder'=>'Enter Payment Date','class'=>'form-control daterange-single']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <button type="button" class="ml-2 remove_payment btn bg-danger-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Remove</button>
            </div>
        </div>

    </div>



    <script type="text/javascript">
        $(document).ready(function(){
            $('.remove_payment').on('click',function(){ 
            $(this).parent().parent().parent().remove();
         });
        });
    </script>