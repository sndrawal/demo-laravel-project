<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>

<fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>


    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Client Name:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                    {!! Form::text('client_name', $value = null, ['id'=>'client_name','placeholder'=>'Enter Client Name','class'=>'form-control','required']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
           <div class="row">
                <label class="col-form-label col-lg-3">Product/Services:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                    {!! Form::text('service', $value = null, ['id'=>'service','placeholder'=>'Enter Product/Service','class'=>'form-control','required']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    @if(!$is_agree)

      <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Contact No.: </label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                    {!! Form::text('contact_no', $value = null, ['id'=>'contact_no','placeholder'=>'Enter Contact No','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
           <div class="row">
                <label class="col-form-label col-lg-3">Address:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                    {!! Form::text('address', $value = null, ['id'=>'address','placeholder'=>'Enter Address','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div> 

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Email: </label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                    {!! Form::text('email', $value = null, ['id'=>'email','placeholder'=>'Enter Email','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
           <div class="row">
                <label class="col-form-label col-lg-3">Notes:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                            {!! Form::text('notes', $value = null, ['id'=>'notes','placeholder'=>'Enter Notes','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

      <div class="form-group row">

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Scan Copy Image:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-book3"></i></span>
                        </span>
                        {!! Form::file('scan_copy', ['id'=>'scan_copy','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
  
        <div class="col-lg-6">
            <div class="row">
                 <label class="col-form-label col-lg-3"></label>
                <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                    @if($is_edit)
                        @php
                             $image = ($spare->scan_copy) ? asset($spare->file_full_path).'/'.$spare->scan_copy : asset('admin/image.png');
                        @endphp

                        <img id="scan_copy" src="{{$image}}" alt="your image" class="preview-image" style="height: 100px;width: auto;" />
                        @else
                        <img id="scan_copy" src="{{ asset('admin/image.png') }}" alt="your image" class="preview-image" style="height: 100px; width: auto;" />
                        @endif
                </div>
            </div>
        </div>
    </div>


    @endif

    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Cost of Project:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cash4"></i>
                                </span>
                            </span>
                    {!! Form::text('cost_of_project', $value = null, ['id'=>'cost_of_project','placeholder'=>'Enter Cost of Project','class'=>'form-control numeric','required']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Payment Type:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-tree7"></i>
                                </span>
                            </span>
                            {!! Form::select('payment_type',[ 'One-Time'=>'One-Time Payment','Recurring'=>'Recurring Payment'], $value = null, ['id'=>'payment_type','placeholder'=>'Select Payment Type','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    @if($is_agree)
    
    {{ Form::hidden('lead_id', $lead_id, array('class' => 'lead_id')) }}
    
    @endif

</fieldset>

<div class="one_time" style="display: none;">
    <fieldset>
        <legend class="text-uppercase font-size-sm font-weight-bold">One-Time Payment</legend>
<div class="appendOneTimePayment">
        <div class="form-group row">
            <div class="col-lg-3">
            <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-user"></i>
                                </span>
                            </span>
                    {!! Form::text('installment[]', $value = null, ['id'=>'installment','placeholder'=>'Enter Installment','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            
                    {!! Form::text('percent[]', $value = null, ['id'=>'percent','placeholder'=>'Enter Percent','class'=>'form-control numeric']) !!}
                            <span class="input-group-prepend">
                                <span class="input-group-text">%
                                </span>
                            </span>

                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-3">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                    {!! Form::text('amount[]', $value = null, ['id'=>'amount','placeholder'=>'Enter Amount','class'=>'form-control numeric']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                    <div class="col-lg-12 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cogs"></i>
                                </span>
                            </span>
                    {!! Form::text('payment_date[]', $value = null, ['id'=>'payment_date','placeholder'=>'Enter Payment Date','class'=>'form-control daterange-single']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-lg-2">
           <div class="row">
                   <button type="button" class="add_more_payment btn bg-success-800 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Add More</button>
            </div>
        </div>

    </div>
</div>


     </fieldset>   
</div>

<div class="recurring" style="display: none;">
</fieldset >
    <legend class="text-uppercase font-size-sm font-weight-bold">Recurring Payment</legend>
 
    <div class="form-group row">
        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Recurring Payment:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-cash4"></i>
                                </span>
                            </span>
                            {!! Form::select('recurring_payment',[ 'Monthly'=>'Monthly','Quaterly'=>'Quaterly','Semi-Annually'=>'Semi-Annually','Annually'=>'Annually'], $value = null, ['id'=>'recurring_payment','placeholder'=>'Select Payment Type','class'=>'form-control']) !!}
                    
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Remind Me Before:</label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            
                            {!! Form::text('remind_before', $value = null, ['id'=>'remind_before','placeholder'=>'Remind Me Before','class'=>'form-control numeric']) !!}
                            <span class="input-group-prepend">
                                <span class="input-group-text">Day/s
                                </span>
                            </span>
                        </div>
                    </div>
            </div>
        </div>
    </div>

 </fieldset>  
</div>
    




<div class="text-right">
    <button type="submit" id="add_spare_image" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>{{$btnType}}</button>
</div>


<script type="text/javascript">
    $(document).ready(function(){

        $('#payment_type').on('change',function(){
            var payment_type = $(this).val();
            if(payment_type == 'One-Time'){
                $('.one_time').show();
                $('.recurring').hide();
            }

            if(payment_type == 'Recurring'){
                $('.one_time').hide();
                $('.recurring').show();
            }         
        });

     $('.add_more_payment').on('click',function(){

        $.ajax({
                type: 'GET',
                url: '/admin/agreement/appendPayment',
                success: function (data) {
                    $('.appendOneTimePayment').last().append(data.options);
                    $('.daterange-single').daterangepicker({
                        singleDatePicker: true,
                        locale: {
                            format: 'YYYY-MM-DD'
                        }

                    });

                }
            });

     });   

    });
</script>


