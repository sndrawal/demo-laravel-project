@extends('admin::layout') 
@section('title')Agreement @stop
@section('breadcrum')Agreement @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
@stop

@section('content')

<div class="card">
    <div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">

          @if($is_agree)
            <a href="{{ route('agreement.create',['lead_id' => $lead_id])}}" class="btn bg-blue">
                <i class="icon-plus2"></i> Add Agreement
            </a>
            @else
             <a href="{{ route('agreement.create')}}" class="btn bg-blue">
                <i class="icon-plus2"></i> Add Agreement
            </a>   
            @endif

        <a href="{{ route('dailyClient.index') }}" class="btn bg-violet-400 btn-labeled btn-labeled-left"><b><i class="icon-pen-plus"></i></b> Go To Lead</a>
    </div>
</div>


<div class="card">
        <div class="card-body">
            <section class="articles">
                
                <div class="row">

             @if($agreement->total() != 0)
                @foreach($agreement as $key => $value)
            @php
          
             if ($value->agreement_from == 'Lead') {
                    $status_val = 'Lead';
                    $color = 'bg-warning-700';
                    $icon = 'icon-tree7';
                }else{
                    $status_val = 'Direct';
                    $color = 'bg-success';
                    $icon = 'icon-user-check';
                }
            @endphp

            <div class="col-lg-4">
                <div class="card card-body border-top-warning-400">
                    <div class="text-left">
                        <h6 class="m-0 font-weight-semibold">#<span class="text-danger">{{++$key}}</span> {{ $value->client_name }}</h6>
                        @if($value->agreement_from == 'Direct') 
                            <p class="text-muted m-0">Address : {{ $value->address }}</p>
                            <p class="text-muted m-0 mb-2">Email : {{ $value->email }}</p>
                            <h5 class="font-weight-semibold text-success">Approach Cost :
                            Rs.{{ $value->cost_of_project }}/-</h5>
                        @endif

                        @if($value->agreement_from == 'Lead') 
                            <p class="text-muted m-0">Service : {{ $value->service }}</p>
                            <p class="text-muted m-0 mb-2">Payment Type : {{ $value->payment_type }}</p>
                            <h5 class="font-weight-semibold text-success">Approach Cost :
                            Rs.{{ $value->cost_of_project }}/-</h5>
                        @endif
                        
                        @if($value->updated_by == null)
                            <h5 class="text-danger">Created
                                By: {{ $value->getUser->first_name.' '. $value->getUser->last_name}}</h5>
                        @else
                            <h5 class="text-danger">Updated
                                By: {{ $value->getUpdatedUser->first_name.' '. $value->getUpdatedUser->last_name}}</h5>
                        @endif

                    </div>
                    <hr class="mt-0 mb-1">
                    <div>
    
                        <a data-toggle="modal" data-target="#modal_agreement_info" class="btn bg-purple-400 btn-icon rounded-round view_agreement" agreement_id="{{ $value->id }}" data-popup="tooltip" data-original-title="View Detail" data-placement="bottom"><i class="icon-eye"></i></a>

                          <a data-toggle="modal" data-target="#modal_theme_warning" class="btn bg-danger-400 btn-icon rounded-round delete_agreement" link="{{route('agreement.delete',$value->id)}}" data-popup="tooltip" data-original-title="Delete" data-placement="bottom"><i class="icon-bin"></i></a>

                     </div>

                    <div class="ribbon-container">
                        <div class="ribbon {{ $color }} "><i class="{{ $icon }}"></i> {{$status_val}}</div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <span class="btn btn-light">No Agreement Found.</span>
    @endif
</div>

         <span style="margin: 5px;float: right;">
            @if($agreement->total() != 0) 
            {{ $agreement->links() }}
            @endif
        </span>



            </section>
        </div>
    </div>



<div id="modal_theme_warning" class="modal fade" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-body">
            <center>
                <i class="icon-help text-danger icon-4x"></i>
                <br>
                <h2>Are You Sure Want To Delete?</h2>
                <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </center>
        </div>
        </div>
    </div>
</div>

<!-- Warning modal -->
    <div id="modal_agreement_info" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h6 class="modal-title">View Lead Agreement</h6>
                </div>

                <div class="modal-body">
                    <div class="table-responsive result_view_detail">

                    </div><!-- table-responsive -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn bg-teal-400" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- warning modal -->
<!-- warning modal -->

<script type="text/javascript">
    $('document').ready(function() {
        $('.delete_agreement').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });
    });
</script>

<script type="text/javascript">
    $('.view_agreement').on('click',function(){ 
        var agreement_id = $(this).attr('agreement_id');

        $.ajax({
            type: 'GET',
            // dataType: 'HTML',
            url: 'agreement/view',
            data: { id: agreement_id },

            success: function (data) { 
                $('.result_view_detail').html(data);
            }
            }); 
    });
</script>

@endsection