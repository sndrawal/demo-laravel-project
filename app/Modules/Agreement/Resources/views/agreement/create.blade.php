@extends('admin::layout')
@section('title')Lead Agreement @stop 
@section('breadcrum')Create Lead Agreement @stop

@section('script')
<!-- Theme JS files -->
<script src="{{asset('admin/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_inputs.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('admin/global/js/demo_pages/form_multiselect.js')}}"></script>
<!-- /theme JS files -->
<script src="{{ asset('admin/validation/agreement.js')}}"></script>

@stop @section('content')

<!-- Form inputs -->
<div class="card">
    <div class="card-header bg-orange-800 header-elements-inline">
        <h5 class="card-title">Create Lead Agreement </h5>
        <div class="header-elements">

        </div>
    </div>
    
    <div class="card-body">

        {!! Form::open(['route'=>'agreement.store','method'=>'POST','id'=>'agreement_submit','class'=>'form-horizontal','role'=>'form','files' => true]) !!}
        
            @include('agreement::agreement.partial.action',['btnType'=>'Save']) 
        
        {!! Form::close() !!}
    </div>
</div>
<!-- /form inputs -->

@stop