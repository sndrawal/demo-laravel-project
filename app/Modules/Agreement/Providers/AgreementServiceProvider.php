<?php

namespace App\Modules\Agreement\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

// Repositories
use App\Modules\Agreement\Repositories\AgreementInterface;
use App\Modules\Agreement\Repositories\AgreementRepository;
use App\Modules\Agreement\Repositories\BillingInterface;
use App\Modules\Agreement\Repositories\BillingRepository;

class AgreementServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->agreementRegister();
        $this->billingRegister();
    }

    public function agreementRegister()
    {
        return $this->app->bind(
            AgreementInterface::class,
            AgreementRepository::class
        );
    }

    public function billingRegister()
    {
        return $this->app->bind(
            BillingInterface::class,
            BillingRepository::class
        );
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('agreement.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'agreement'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/agreement');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/agreement';
        }, \Config::get('view.paths')), [$sourcePath]), 'agreement');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/agreement');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'agreement');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'agreement');
        }
    }

    /**
     * Register an additional directory of factories.
     * 
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
